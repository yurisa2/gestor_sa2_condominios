<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

require_once("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

require_once("include/fin_despesas_variables.php");

$mode = postvalue("mode");

if(!isLogged())
{ 
	return;
}
if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search"))
{
	return;
}

require_once("classes/searchclause.php");

$cipherer = new RunnerCipherer($strTableName);

require_once('include/xtempl.php');
$xt = new Xtempl();





$layout = new TLayout("detailspreview_bootstrap", "AvenueWhite_label", "MobileWhite_label");
$layout->version = 3;
	$layout->bootstrapTheme = "yeti";
$layout->blocks["bare"] = array();
$layout->containers["dcount"] = array();
$layout->container_properties["dcount"] = array(  );
$layout->containers["dcount"][] = array("name"=>"bsdetailspreviewcount",
	"block"=>"", "substyle"=>1  );

$layout->skins["dcount"] = "";

$layout->blocks["bare"][] = "dcount";
$layout->containers["detailspreviewgrid"] = array();
$layout->container_properties["detailspreviewgrid"] = array(  );
$layout->containers["detailspreviewgrid"][] = array("name"=>"detailspreviewfields",
	"block"=>"details_data", "substyle"=>1  );

$layout->skins["detailspreviewgrid"] = "";

$layout->blocks["bare"][] = "detailspreviewgrid";
$page_layouts["fin_despesas_detailspreview"] = $layout;




$recordsCounter = 0;

//	process masterkey value
$mastertable = postvalue("mastertable");
$masterKeys = my_json_decode(postvalue("masterKeys"));
$sessionPrefix = "_detailsPreview";
if($mastertable != "")
{
	$_SESSION[$sessionPrefix."_mastertable"]=$mastertable;
//	copy keys to session
	$i = 1;
	if(is_array($masterKeys) && count($masterKeys) > 0)
	{
		while(array_key_exists ("masterkey".$i, $masterKeys))
		{
			$_SESSION[$sessionPrefix."_masterkey".$i] = $masterKeys["masterkey".$i];
			$i++;
		}
	}
	if(isset($_SESSION[$sessionPrefix."_masterkey".$i]))
		unset($_SESSION[$sessionPrefix."_masterkey".$i]);
}
else
	$mastertable = $_SESSION[$sessionPrefix."_mastertable"];

$params = array();
$params['id'] = 1;
$params['xt'] = &$xt;
$params['tName'] = $strTableName;
$params['pageType'] = "detailspreview";
$pageObject = new DetailsPreview($params);

if($mastertable == "ger_grupo")
{
	$where = "";
		$formattedValue = make_db_value("link_ger_grupo",$_SESSION[$sessionPrefix."_masterkey1"]);
	if( $formattedValue == "null" )
		$where .= $pageObject->getFieldSQLDecrypt("link_ger_grupo") . " is null";
	else
		$where .= $pageObject->getFieldSQLDecrypt("link_ger_grupo") . "=" . $formattedValue;
}

$str = SecuritySQL("Search", $strTableName);
if(strlen($str))
	$where.=" and ".$str;
$strSQL = $gQuery->gSQLWhere($where);

$strSQL.=" ".$gstrOrderBy;

$rowcount = $gQuery->gSQLRowCount($where, $pageObject->connection);
$xt->assign("row_count",$rowcount);
if($rowcount) 
{
	$xt->assign("details_data",true);

	$display_count = 10;
	if($mode == "inline")
		$display_count*=2;
		
	if($rowcount>$display_count+2)
	{
		$xt->assign("display_first",true);
		$xt->assign("display_count",$display_count);
	}
	else
		$display_count = $rowcount;

	$rowinfo = array();
	
	require_once getabspath('classes/controls/ViewControlsContainer.php');
	$pSet = new ProjectSettings($strTableName, PAGE_LIST);
	$viewContainer = new ViewControlsContainer($pSet, PAGE_LIST);
	$viewContainer->isDetailsPreview = true;

	$b = true;
	$qResult = $pageObject->connection->query( $strSQL );
	$data = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	while($data && $recordsCounter<$display_count) {
		$recordsCounter++;
		$row = array();
		$keylink = "";
		$keylink.="&key1=".runner_htmlspecialchars(rawurlencode(@$data["idDespesa"]));
	
	
	//	idDespesa - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("idDespesa", $data, $keylink);
			$row["idDespesa_value"] = $value;
			$format = $pSet->getViewFormat("idDespesa");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("idDespesa")))
				$class = ' rnr-field-number';
			$row["idDespesa_class"] = $class;
	//	vencimento - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("vencimento", $data, $keylink);
			$row["vencimento_value"] = $value;
			$format = $pSet->getViewFormat("vencimento");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("vencimento")))
				$class = ' rnr-field-number';
			$row["vencimento_class"] = $class;
	//	link_fin_meiospgto - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("link_fin_meiospgto", $data, $keylink);
			$row["link_fin_meiospgto_value"] = $value;
			$format = $pSet->getViewFormat("link_fin_meiospgto");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("link_fin_meiospgto")))
				$class = ' rnr-field-number';
			$row["link_fin_meiospgto_class"] = $class;
	//	anotado - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("anotado", $data, $keylink);
			$row["anotado_value"] = $value;
			$format = $pSet->getViewFormat("anotado");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("anotado")))
				$class = ' rnr-field-number';
			$row["anotado_class"] = $class;
	//	obs - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("obs", $data, $keylink);
			$row["obs_value"] = $value;
			$format = $pSet->getViewFormat("obs");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("obs")))
				$class = ' rnr-field-number';
			$row["obs_class"] = $class;
	//	link_fin_tipogasto - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("link_fin_tipogasto", $data, $keylink);
			$row["link_fin_tipogasto_value"] = $value;
			$format = $pSet->getViewFormat("link_fin_tipogasto");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("link_fin_tipogasto")))
				$class = ' rnr-field-number';
			$row["link_fin_tipogasto_class"] = $class;
	//	link_fin_tipodoc - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("link_fin_tipodoc", $data, $keylink);
			$row["link_fin_tipodoc_value"] = $value;
			$format = $pSet->getViewFormat("link_fin_tipodoc");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("link_fin_tipodoc")))
				$class = ' rnr-field-number';
			$row["link_fin_tipodoc_class"] = $class;
	//	ndoc - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ndoc", $data, $keylink);
			$row["ndoc_value"] = $value;
			$format = $pSet->getViewFormat("ndoc");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ndoc")))
				$class = ' rnr-field-number';
			$row["ndoc_class"] = $class;
	//	link_fin_fornecedor - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("link_fin_fornecedor", $data, $keylink);
			$row["link_fin_fornecedor_value"] = $value;
			$format = $pSet->getViewFormat("link_fin_fornecedor");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("link_fin_fornecedor")))
				$class = ' rnr-field-number';
			$row["link_fin_fornecedor_class"] = $class;
	//	valor - Currency
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("valor", $data, $keylink);
			$row["valor_value"] = $value;
			$format = $pSet->getViewFormat("valor");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("valor")))
				$class = ' rnr-field-number';
			$row["valor_class"] = $class;
	//	link_fin_conta - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("link_fin_conta", $data, $keylink);
			$row["link_fin_conta_value"] = $value;
			$format = $pSet->getViewFormat("link_fin_conta");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("link_fin_conta")))
				$class = ' rnr-field-number';
			$row["link_fin_conta_class"] = $class;
	//	pagamento - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("pagamento", $data, $keylink);
			$row["pagamento_value"] = $value;
			$format = $pSet->getViewFormat("pagamento");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("pagamento")))
				$class = ' rnr-field-number';
			$row["pagamento_class"] = $class;
	//	ultimaalteracao - Datetime
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ultimaalteracao", $data, $keylink);
			$row["ultimaalteracao_value"] = $value;
			$format = $pSet->getViewFormat("ultimaalteracao");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ultimaalteracao")))
				$class = ' rnr-field-number';
			$row["ultimaalteracao_class"] = $class;
	//	ultimousuario - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ultimousuario", $data, $keylink);
			$row["ultimousuario_value"] = $value;
			$format = $pSet->getViewFormat("ultimousuario");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ultimousuario")))
				$class = ' rnr-field-number';
			$row["ultimousuario_class"] = $class;
	//	link_ger_grupo - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("link_ger_grupo", $data, $keylink);
			$row["link_ger_grupo_value"] = $value;
			$format = $pSet->getViewFormat("link_ger_grupo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("link_ger_grupo")))
				$class = ' rnr-field-number';
			$row["link_ger_grupo_class"] = $class;
	//	documento - Document Download
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("documento", $data, $keylink);
			$row["documento_value"] = $value;
			$format = $pSet->getViewFormat("documento");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("documento")))
				$class = ' rnr-field-number';
			$row["documento_class"] = $class;
		$rowinfo[] = $row;
		if ($b) {
			$rowinfo2[] = $row;
			$b = false;
		}
		$data = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	}
	$xt->assign_loopsection("details_row",$rowinfo);
	$xt->assign_loopsection("details_row_header",$rowinfo2); // assign class for header
}
$returnJSON = array("success" => true);
$xt->load_template(GetTemplateName("fin_despesas", "detailspreview"));
$returnJSON["body"] = $xt->fetch_loaded();

if($mode!="inline")
{
	$returnJSON["counter"] = postvalue("counter");
	$layout = GetPageLayout(GoodFieldName($strTableName), 'detailspreview');
	if($layout)
	{
		foreach($layout->getCSSFiles(isRTL(), isMobile()) as $css)
		{
			$returnJSON['CSSFiles'][] = $css;
		}
	}	
}	

echo printJSON($returnJSON);
exit();
?>