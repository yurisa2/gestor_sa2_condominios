<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

require_once("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

require_once("include/fin_cobranca_variables.php");

$mode = postvalue("mode");

if(!isLogged())
{ 
	return;
}
if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search"))
{
	return;
}

require_once("classes/searchclause.php");

$cipherer = new RunnerCipherer($strTableName);

require_once('include/xtempl.php');
$xt = new Xtempl();





$layout = new TLayout("detailspreview_bootstrap", "AvenueWhite_label", "MobileWhite_label");
$layout->version = 3;
	$layout->bootstrapTheme = "yeti";
$layout->blocks["bare"] = array();
$layout->containers["dcount"] = array();
$layout->container_properties["dcount"] = array(  );
$layout->containers["dcount"][] = array("name"=>"bsdetailspreviewcount",
	"block"=>"", "substyle"=>1  );

$layout->skins["dcount"] = "";

$layout->blocks["bare"][] = "dcount";
$layout->containers["detailspreviewgrid"] = array();
$layout->container_properties["detailspreviewgrid"] = array(  );
$layout->containers["detailspreviewgrid"][] = array("name"=>"detailspreviewfields",
	"block"=>"details_data", "substyle"=>1  );

$layout->skins["detailspreviewgrid"] = "";

$layout->blocks["bare"][] = "detailspreviewgrid";
$page_layouts["fin_cobranca_detailspreview"] = $layout;




$recordsCounter = 0;

//	process masterkey value
$mastertable = postvalue("mastertable");
$masterKeys = my_json_decode(postvalue("masterKeys"));
$sessionPrefix = "_detailsPreview";
if($mastertable != "")
{
	$_SESSION[$sessionPrefix."_mastertable"]=$mastertable;
//	copy keys to session
	$i = 1;
	if(is_array($masterKeys) && count($masterKeys) > 0)
	{
		while(array_key_exists ("masterkey".$i, $masterKeys))
		{
			$_SESSION[$sessionPrefix."_masterkey".$i] = $masterKeys["masterkey".$i];
			$i++;
		}
	}
	if(isset($_SESSION[$sessionPrefix."_masterkey".$i]))
		unset($_SESSION[$sessionPrefix."_masterkey".$i]);
}
else
	$mastertable = $_SESSION[$sessionPrefix."_mastertable"];

$params = array();
$params['id'] = 1;
$params['xt'] = &$xt;
$params['tName'] = $strTableName;
$params['pageType'] = "detailspreview";
$pageObject = new DetailsPreview($params);

if($mastertable == "ger_unidades")
{
	$where = "";
		$formattedValue = make_db_value("link_ger_unidade",$_SESSION[$sessionPrefix."_masterkey1"]);
	if( $formattedValue == "null" )
		$where .= $pageObject->getFieldSQLDecrypt("link_ger_unidade") . " is null";
	else
		$where .= $pageObject->getFieldSQLDecrypt("link_ger_unidade") . "=" . $formattedValue;
}

$str = SecuritySQL("Search", $strTableName);
if(strlen($str))
	$where.=" and ".$str;
$strSQL = $gQuery->gSQLWhere($where);

$strSQL.=" ".$gstrOrderBy;

$rowcount = $gQuery->gSQLRowCount($where, $pageObject->connection);
$xt->assign("row_count",$rowcount);
if($rowcount) 
{
	$xt->assign("details_data",true);

	$display_count = 10;
	if($mode == "inline")
		$display_count*=2;
		
	if($rowcount>$display_count+2)
	{
		$xt->assign("display_first",true);
		$xt->assign("display_count",$display_count);
	}
	else
		$display_count = $rowcount;

	$rowinfo = array();
	
	require_once getabspath('classes/controls/ViewControlsContainer.php');
	$pSet = new ProjectSettings($strTableName, PAGE_LIST);
	$viewContainer = new ViewControlsContainer($pSet, PAGE_LIST);
	$viewContainer->isDetailsPreview = true;

	$b = true;
	$qResult = $pageObject->connection->query( $strSQL );
	$data = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	while($data && $recordsCounter<$display_count) {
		$recordsCounter++;
		$row = array();
		$keylink = "";
		$keylink.="&key1=".runner_htmlspecialchars(rawurlencode(@$data["idCobranca"]));
	
	
	//	vencimento - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("vencimento", $data, $keylink);
			$row["vencimento_value"] = $value;
			$format = $pSet->getViewFormat("vencimento");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("vencimento")))
				$class = ' rnr-field-number';
			$row["vencimento_class"] = $class;
	//	tipo - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("tipo", $data, $keylink);
			$row["tipo_value"] = $value;
			$format = $pSet->getViewFormat("tipo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("tipo")))
				$class = ' rnr-field-number';
			$row["tipo_class"] = $class;
	//	numdoc - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("numdoc", $data, $keylink);
			$row["numdoc_value"] = $value;
			$format = $pSet->getViewFormat("numdoc");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("numdoc")))
				$class = ' rnr-field-number';
			$row["numdoc_class"] = $class;
	//	datadoc - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("datadoc", $data, $keylink);
			$row["datadoc_value"] = $value;
			$format = $pSet->getViewFormat("datadoc");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("datadoc")))
				$class = ' rnr-field-number';
			$row["datadoc_class"] = $class;
	//	vlrdoc - Currency
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("vlrdoc", $data, $keylink);
			$row["vlrdoc_value"] = $value;
			$format = $pSet->getViewFormat("vlrdoc");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("vlrdoc")))
				$class = ' rnr-field-number';
			$row["vlrdoc_class"] = $class;
	//	desc - Currency
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("desc", $data, $keylink);
			$row["desc_value"] = $value;
			$format = $pSet->getViewFormat("desc");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("desc")))
				$class = ' rnr-field-number';
			$row["desc_class"] = $class;
	//	outded - Currency
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("outded", $data, $keylink);
			$row["outded_value"] = $value;
			$format = $pSet->getViewFormat("outded");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("outded")))
				$class = ' rnr-field-number';
			$row["outded_class"] = $class;
	//	moramulta - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("moramulta", $data, $keylink);
			$row["moramulta_value"] = $value;
			$format = $pSet->getViewFormat("moramulta");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("moramulta")))
				$class = ' rnr-field-number';
			$row["moramulta_class"] = $class;
	//	outacr - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("outacr", $data, $keylink);
			$row["outacr_value"] = $value;
			$format = $pSet->getViewFormat("outacr");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("outacr")))
				$class = ' rnr-field-number';
			$row["outacr_class"] = $class;
	//	obs - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("obs", $data, $keylink);
			$row["obs_value"] = $value;
			$format = $pSet->getViewFormat("obs");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("obs")))
				$class = ' rnr-field-number';
			$row["obs_class"] = $class;
	//	conta - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("conta", $data, $keylink);
			$row["conta_value"] = $value;
			$format = $pSet->getViewFormat("conta");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("conta")))
				$class = ' rnr-field-number';
			$row["conta_class"] = $class;
	//	liq - Checkbox
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("liq", $data, $keylink);
			$row["liq_value"] = $value;
			$format = $pSet->getViewFormat("liq");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("liq")))
				$class = ' rnr-field-number';
			$row["liq_class"] = $class;
	//	vlrcob - Currency
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("vlrcob", $data, $keylink);
			$row["vlrcob_value"] = $value;
			$format = $pSet->getViewFormat("vlrcob");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("vlrcob")))
				$class = ' rnr-field-number';
			$row["vlrcob_class"] = $class;
	//	dtpagamento - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("dtpagamento", $data, $keylink);
			$row["dtpagamento_value"] = $value;
			$format = $pSet->getViewFormat("dtpagamento");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("dtpagamento")))
				$class = ' rnr-field-number';
			$row["dtpagamento_class"] = $class;
	//	baixado - Checkbox
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("baixado", $data, $keylink);
			$row["baixado_value"] = $value;
			$format = $pSet->getViewFormat("baixado");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("baixado")))
				$class = ' rnr-field-number';
			$row["baixado_class"] = $class;
	//	motivobaixa - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("motivobaixa", $data, $keylink);
			$row["motivobaixa_value"] = $value;
			$format = $pSet->getViewFormat("motivobaixa");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("motivobaixa")))
				$class = ' rnr-field-number';
			$row["motivobaixa_class"] = $class;
	//	acordo - Checkbox
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("acordo", $data, $keylink);
			$row["acordo_value"] = $value;
			$format = $pSet->getViewFormat("acordo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("acordo")))
				$class = ' rnr-field-number';
			$row["acordo_class"] = $class;
	//	dtacordo - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("dtacordo", $data, $keylink);
			$row["dtacordo_value"] = $value;
			$format = $pSet->getViewFormat("dtacordo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("dtacordo")))
				$class = ' rnr-field-number';
			$row["dtacordo_class"] = $class;
	//	detaacordo - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("detaacordo", $data, $keylink);
			$row["detaacordo_value"] = $value;
			$format = $pSet->getViewFormat("detaacordo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("detaacordo")))
				$class = ' rnr-field-number';
			$row["detaacordo_class"] = $class;
	//	ultimousuario - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ultimousuario", $data, $keylink);
			$row["ultimousuario_value"] = $value;
			$format = $pSet->getViewFormat("ultimousuario");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ultimousuario")))
				$class = ' rnr-field-number';
			$row["ultimousuario_class"] = $class;
	//	ultimaalteracao - Datetime
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ultimaalteracao", $data, $keylink);
			$row["ultimaalteracao_value"] = $value;
			$format = $pSet->getViewFormat("ultimaalteracao");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ultimaalteracao")))
				$class = ' rnr-field-number';
			$row["ultimaalteracao_class"] = $class;
	//	link_ger_unidade - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("link_ger_unidade", $data, $keylink);
			$row["link_ger_unidade_value"] = $value;
			$format = $pSet->getViewFormat("link_ger_unidade");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("link_ger_unidade")))
				$class = ' rnr-field-number';
			$row["link_ger_unidade_class"] = $class;
	//	numidacordo - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("numidacordo", $data, $keylink);
			$row["numidacordo_value"] = $value;
			$format = $pSet->getViewFormat("numidacordo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("numidacordo")))
				$class = ' rnr-field-number';
			$row["numidacordo_class"] = $class;
	//	unidadeir - Custom
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("unidadeir", $data, $keylink);
			$row["unidadeir_value"] = $value;
			$format = $pSet->getViewFormat("unidadeir");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("unidadeir")))
				$class = ' rnr-field-number';
			$row["unidadeir_class"] = $class;
	//	bolSantander - Hyperlink
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("bolSantander", $data, $keylink);
			$row["bolSantander_value"] = $value;
			$format = $pSet->getViewFormat("bolSantander");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("bolSantander")))
				$class = ' rnr-field-number';
			$row["bolSantander_class"] = $class;
	//	cadastro - Hyperlink
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("cadastro", $data, $keylink);
			$row["cadastro_value"] = $value;
			$format = $pSet->getViewFormat("cadastro");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("cadastro")))
				$class = ' rnr-field-number';
			$row["cadastro_class"] = $class;
	//	auth - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("auth", $data, $keylink);
			$row["auth_value"] = $value;
			$format = $pSet->getViewFormat("auth");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("auth")))
				$class = ' rnr-field-number';
			$row["auth_class"] = $class;
	//	jurosam - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("jurosam", $data, $keylink);
			$row["jurosam_value"] = $value;
			$format = $pSet->getViewFormat("jurosam");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("jurosam")))
				$class = ' rnr-field-number';
			$row["jurosam_class"] = $class;
	//	multa - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("multa", $data, $keylink);
			$row["multa_value"] = $value;
			$format = $pSet->getViewFormat("multa");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("multa")))
				$class = ' rnr-field-number';
			$row["multa_class"] = $class;
		$rowinfo[] = $row;
		if ($b) {
			$rowinfo2[] = $row;
			$b = false;
		}
		$data = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	}
	$xt->assign_loopsection("details_row",$rowinfo);
	$xt->assign_loopsection("details_row_header",$rowinfo2); // assign class for header
}
$returnJSON = array("success" => true);
$xt->load_template(GetTemplateName("fin_cobranca", "detailspreview"));
$returnJSON["body"] = $xt->fetch_loaded();

if($mode!="inline")
{
	$returnJSON["counter"] = postvalue("counter");
	$layout = GetPageLayout(GoodFieldName($strTableName), 'detailspreview');
	if($layout)
	{
		foreach($layout->getCSSFiles(isRTL(), isMobile()) as $css)
		{
			$returnJSON['CSSFiles'][] = $css;
		}
	}	
}	

echo printJSON($returnJSON);
exit();
?>