<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");


require_once("include/dbcommon.php");


if(!isLogged())
{
	HeaderRedirect("login");
	return;
}

Security::processLogoutRequest();

if (($_SESSION["MyURL"] == "") || (!isLoggedAsGuest())) {
	Security::saveRedirectURL();
}




$layout = new TLayout("menu_bootstrap", "AvenueWhite_label", "MobileWhite_label");
$layout->version = 3;
	$layout->bootstrapTheme = "yeti";
$layout->blocks["top"] = array();
$layout->containers["menu"] = array();
$layout->container_properties["menu"] = array(  );
$layout->containers["menu"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"hdr" );
$layout->containers["hdr"] = array();
$layout->container_properties["hdr"] = array(  );
$layout->containers["hdr"][] = array("name"=>"logo",
	"block"=>"logo_block", "substyle"=>1  );

$layout->containers["hdr"][] = array("name"=>"bsnavbarcollapse",
	"block"=>"collapse_block", "substyle"=>1  );

$layout->skins["hdr"] = "";


$layout->containers["menu"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"menu_1" );
$layout->containers["menu_1"] = array();
$layout->container_properties["menu_1"] = array(  );
$layout->containers["menu_1"][] = array("name"=>"hmenu",
	"block"=>"menu_block", "substyle"=>1  );

$layout->containers["menu_1"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"login" );
$layout->containers["login"] = array();
$layout->container_properties["login"] = array(  );
$layout->containers["login"][] = array("name"=>"loggedas",
	"block"=>"security_block", "substyle"=>1  );

$layout->skins["login"] = "";


$layout->skins["menu_1"] = "";


$layout->skins["menu"] = "";

$layout->blocks["top"][] = "menu";
$layout->containers["center"] = array();
$layout->container_properties["center"] = array(  );
$layout->containers["center"][] = array("name"=>"welcome",
	"block"=>"", "substyle"=>1  );

$layout->skins["center"] = "";

$layout->blocks["top"][] = "center";
$page_layouts["menu"] = $layout;




require_once('include/xtempl.php');
require_once(getabspath("classes/cipherer.php"));

include_once(getabspath("include/fin_cheques_events.php"));
$tableEvents["fin_cheques"] = new eventclass_fin_cheques;
include_once(getabspath("include/fin_cobranca_events.php"));
$tableEvents["fin_cobranca"] = new eventclass_fin_cobranca;
include_once(getabspath("include/fin_correcao_monetaria_events.php"));
$tableEvents["fin_correcao_monetaria"] = new eventclass_fin_correcao_monetaria;
include_once(getabspath("include/fin_despesas_events.php"));
$tableEvents["fin_despesas"] = new eventclass_fin_despesas;
include_once(getabspath("include/fin_fornecedores_events.php"));
$tableEvents["fin_fornecedores"] = new eventclass_fin_fornecedores;
include_once(getabspath("include/fin_meiospgto_events.php"));
$tableEvents["fin_meiospgto"] = new eventclass_fin_meiospgto;
include_once(getabspath("include/fin_tipodocs_events.php"));
$tableEvents["fin_tipodocs"] = new eventclass_fin_tipodocs;
include_once(getabspath("include/fin_tipogastos_events.php"));
$tableEvents["fin_tipogastos"] = new eventclass_fin_tipogastos;
include_once(getabspath("include/ger_cadastro_events.php"));
$tableEvents["ger_cadastro"] = new eventclass_ger_cadastro;
include_once(getabspath("include/ger_lista_uf_events.php"));
$tableEvents["ger_lista_uf"] = new eventclass_ger_lista_uf;
include_once(getabspath("include/ger_moradores_events.php"));
$tableEvents["ger_moradores"] = new eventclass_ger_moradores;
include_once(getabspath("include/ger_unidades_events.php"));
$tableEvents["ger_unidades"] = new eventclass_ger_unidades;
include_once(getabspath("include/ger_lista_relacao_com_unidade_events.php"));
$tableEvents["ger_lista_relacao_com_unidade"] = new eventclass_ger_lista_relacao_com_unidade;
include_once(getabspath("include/fin_cobranca_atualizacao_aberto_events.php"));
$tableEvents["fin_cobranca_atualizacao_aberto"] = new eventclass_fin_cobranca_atualizacao_aberto;
include_once(getabspath("include/jur_processos_events.php"));
$tableEvents["jur_processos"] = new eventclass_jur_processos;
include_once(getabspath("include/fin_contas_events.php"));
$tableEvents["fin_contas"] = new eventclass_fin_contas;
include_once(getabspath("include/ger_ocorrencias_events.php"));
$tableEvents["ger_ocorrencias"] = new eventclass_ger_ocorrencias;
include_once(getabspath("include/ger_op_inserirtdboleto_events.php"));
$tableEvents["ger_op_inserirtdboleto"] = new eventclass_ger_op_inserirtdboleto;
include_once(getabspath("include/fin_debitos_contador_events.php"));
$tableEvents["fin_debitos_contador"] = new eventclass_fin_debitos_contador;
include_once(getabspath("include/fin_debitos_resumo_events.php"));
$tableEvents["fin_debitos_resumo"] = new eventclass_fin_debitos_resumo;
include_once(getabspath("include/ger_cadastro_moradores_plista_events.php"));
$tableEvents["ger_cadastro_moradores_plista"] = new eventclass_ger_cadastro_moradores_plista;

$xt = new Xtempl();

$id = postvalue("id")!=="" ? postvalue("id") : 1;

//array of params for classes
$params = array();
$params["id"] = $id; 
$params["xt"] = &$xt;
$params["tName"] = NOT_TABLE_BASED_TNAME;
$params["pageType"] = PAGE_MENU;
$params["templatefile"] = "menu.htm";
$params["isGroupSecurity"] = $isGroupSecurity;
$params["needSearchClauseObj"] = false;
$pageObject = new RunnerPage($params);
$pageObject->init();
$pageObject->commonAssign();
// button handlers file names

//	Before Process event
if($globalEvents->exists("BeforeProcessMenu"))
	$globalEvents->BeforeProcessMenu( $pageObject );

$pageObject->body["begin"] .= GetBaseScriptsForPage(false);

$pageObject->addCommonJs();

//fill jsSettings and ControlsHTMLMap
$pageObject->fillSetCntrlMaps();
$pageObject->body['end'] .= '<script>';
$pageObject->body['end'] .= "window.controlsMap = ".my_json_encode($pageObject->controlsHTMLMap).";";
$pageObject->body['end'] .= "window.viewControlsMap = ".my_json_encode($pageObject->viewControlsHTMLMap).";";
$pageObject->body['end'] .= "window.settings = ".my_json_encode($pageObject->jsSettings).";</script>";
$pageObject->body["end"] .= "<script type=\"text/javascript\" src=\"".GetRootPathForResources("include/runnerJS/RunnerAll.js")."\"></script>";
$pageObject->body["end"] .= '<script>'.$pageObject->PrepareJS()."</script>";
$xt->assignbyref("body",$pageObject->body);

// The user might rewrite $_SESSION["UserName"] value with HTML code in an event, so no encoding will be performed while printing this value.
$xt->assign("username", $_SESSION["UserName"]);
$xt->assign("id", $id);
$xt->assign("changepwd_link",$_SESSION["AccessLevel"] != ACCESS_LEVEL_GUEST && $_SESSION["fromFacebook"] == false);
$xt->assign("changepwdlink_attrs","onclick=\"window.location.href='".GetTableLink("changepwd")."';return false;\"");

$xt->assign("logoutlink_attrs", 'id="logoutButton'.$id.'"');
$xt->assign("guestloginlink_attrs", 'id="loginButton'.$id.'"');

$xt->assign("loggedas_block", !isLoggedAsGuest());
$xt->assign("loggedas_message", !isLoggedAsGuest());

$xt->assign("logout_link", true);
$xt->assign("guestloginbutton", isLoggedAsGuest());
$xt->assign("logoutbutton", isSingleSign() && !isLoggedAsGuest());

if( IsAdmin() )
	$xt->assign("adminarea_link", true);

// get redirect location for menu page
$redirect = $pageObject->getRedirectForMenuPage();
if($redirect)
{
	header("Location: ".$redirect); 
	exit();
}

$xt->assign("menu_block",true);
if($globalEvents->exists("BeforeShowMenu"))
	$globalEvents->BeforeShowMenu($xt, $pageObject->templatefile, $pageObject);

$pageObject->display($pageObject->templatefile);
?>