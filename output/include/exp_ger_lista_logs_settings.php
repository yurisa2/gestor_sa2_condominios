<?php
require_once(getabspath("classes/cipherer.php"));




$tdataexp_ger_lista_logs = array();
	$tdataexp_ger_lista_logs[".truncateText"] = true;
	$tdataexp_ger_lista_logs[".NumberOfChars"] = 80;
	$tdataexp_ger_lista_logs[".ShortName"] = "exp_ger_lista_logs";
	$tdataexp_ger_lista_logs[".OwnerID"] = "";
	$tdataexp_ger_lista_logs[".OriginalTable"] = "exp_ger_lista_logs";

//	field labels
$fieldLabelsexp_ger_lista_logs = array();
$fieldToolTipsexp_ger_lista_logs = array();
$pageTitlesexp_ger_lista_logs = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsexp_ger_lista_logs["Portuguese(Brazil)"] = array();
	$fieldToolTipsexp_ger_lista_logs["Portuguese(Brazil)"] = array();
	$pageTitlesexp_ger_lista_logs["Portuguese(Brazil)"] = array();
	$fieldLabelsexp_ger_lista_logs["Portuguese(Brazil)"]["idLog"] = "idLog";
	$fieldToolTipsexp_ger_lista_logs["Portuguese(Brazil)"]["idLog"] = "";
	$fieldLabelsexp_ger_lista_logs["Portuguese(Brazil)"]["log"] = "log";
	$fieldToolTipsexp_ger_lista_logs["Portuguese(Brazil)"]["log"] = "";
	if (count($fieldToolTipsexp_ger_lista_logs["Portuguese(Brazil)"]))
		$tdataexp_ger_lista_logs[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsexp_ger_lista_logs[""] = array();
	$fieldToolTipsexp_ger_lista_logs[""] = array();
	$pageTitlesexp_ger_lista_logs[""] = array();
	if (count($fieldToolTipsexp_ger_lista_logs[""]))
		$tdataexp_ger_lista_logs[".isUseToolTips"] = true;
}


	$tdataexp_ger_lista_logs[".NCSearch"] = true;



$tdataexp_ger_lista_logs[".shortTableName"] = "exp_ger_lista_logs";
$tdataexp_ger_lista_logs[".nSecOptions"] = 0;
$tdataexp_ger_lista_logs[".recsPerRowList"] = 1;
$tdataexp_ger_lista_logs[".recsPerRowPrint"] = 1;
$tdataexp_ger_lista_logs[".mainTableOwnerID"] = "";
$tdataexp_ger_lista_logs[".moveNext"] = 1;
$tdataexp_ger_lista_logs[".entityType"] = 0;

$tdataexp_ger_lista_logs[".strOriginalTableName"] = "exp_ger_lista_logs";





$tdataexp_ger_lista_logs[".showAddInPopup"] = false;

$tdataexp_ger_lista_logs[".showEditInPopup"] = false;

$tdataexp_ger_lista_logs[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataexp_ger_lista_logs[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataexp_ger_lista_logs[".fieldsForRegister"] = array();

$tdataexp_ger_lista_logs[".listAjax"] = false;

	$tdataexp_ger_lista_logs[".audit"] = true;

	$tdataexp_ger_lista_logs[".locking"] = true;


$tdataexp_ger_lista_logs[".add"] = true;
$tdataexp_ger_lista_logs[".afterAddAction"] = 1;
$tdataexp_ger_lista_logs[".closePopupAfterAdd"] = 1;
$tdataexp_ger_lista_logs[".afterAddActionDetTable"] = "";

$tdataexp_ger_lista_logs[".list"] = true;


$tdataexp_ger_lista_logs[".import"] = true;

$tdataexp_ger_lista_logs[".exportTo"] = true;

$tdataexp_ger_lista_logs[".printFriendly"] = true;


$tdataexp_ger_lista_logs[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataexp_ger_lista_logs[".searchSaving"] = false;
//

$tdataexp_ger_lista_logs[".showSearchPanel"] = true;
		$tdataexp_ger_lista_logs[".flexibleSearch"] = true;

if (isMobile())
	$tdataexp_ger_lista_logs[".isUseAjaxSuggest"] = false;
else
	$tdataexp_ger_lista_logs[".isUseAjaxSuggest"] = true;

$tdataexp_ger_lista_logs[".rowHighlite"] = true;



$tdataexp_ger_lista_logs[".addPageEvents"] = false;

// use timepicker for search panel
$tdataexp_ger_lista_logs[".isUseTimeForSearch"] = false;





$tdataexp_ger_lista_logs[".allSearchFields"] = array();
$tdataexp_ger_lista_logs[".filterFields"] = array();
$tdataexp_ger_lista_logs[".requiredSearchFields"] = array();

$tdataexp_ger_lista_logs[".allSearchFields"][] = "idLog";
	$tdataexp_ger_lista_logs[".allSearchFields"][] = "log";
	

$tdataexp_ger_lista_logs[".googleLikeFields"] = array();
$tdataexp_ger_lista_logs[".googleLikeFields"][] = "idLog";
$tdataexp_ger_lista_logs[".googleLikeFields"][] = "log";


$tdataexp_ger_lista_logs[".advSearchFields"] = array();
$tdataexp_ger_lista_logs[".advSearchFields"][] = "idLog";
$tdataexp_ger_lista_logs[".advSearchFields"][] = "log";

$tdataexp_ger_lista_logs[".tableType"] = "list";

$tdataexp_ger_lista_logs[".printerPageOrientation"] = 0;
$tdataexp_ger_lista_logs[".nPrinterPageScale"] = 100;

$tdataexp_ger_lista_logs[".nPrinterSplitRecords"] = 40;

$tdataexp_ger_lista_logs[".nPrinterPDFSplitRecords"] = 40;



$tdataexp_ger_lista_logs[".geocodingEnabled"] = false;





$tdataexp_ger_lista_logs[".listGridLayout"] = 3;

$tdataexp_ger_lista_logs[".isDisplayLoading"] = true;


$tdataexp_ger_lista_logs[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdataexp_ger_lista_logs[".pageSize"] = 20;

$tdataexp_ger_lista_logs[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataexp_ger_lista_logs[".strOrderBy"] = $tstrOrderBy;

$tdataexp_ger_lista_logs[".orderindexes"] = array();

$tdataexp_ger_lista_logs[".sqlHead"] = "SELECT idLog,  	log";
$tdataexp_ger_lista_logs[".sqlFrom"] = "FROM exp_ger_lista_logs";
$tdataexp_ger_lista_logs[".sqlWhereExpr"] = "";
$tdataexp_ger_lista_logs[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataexp_ger_lista_logs[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataexp_ger_lista_logs[".arrGroupsPerPage"] = $arrGPP;

$tdataexp_ger_lista_logs[".highlightSearchResults"] = true;

$tableKeysexp_ger_lista_logs = array();
$tdataexp_ger_lista_logs[".Keys"] = $tableKeysexp_ger_lista_logs;

$tdataexp_ger_lista_logs[".listFields"] = array();
$tdataexp_ger_lista_logs[".listFields"][] = "idLog";
$tdataexp_ger_lista_logs[".listFields"][] = "log";

$tdataexp_ger_lista_logs[".hideMobileList"] = array();


$tdataexp_ger_lista_logs[".viewFields"] = array();

$tdataexp_ger_lista_logs[".addFields"] = array();
$tdataexp_ger_lista_logs[".addFields"][] = "idLog";
$tdataexp_ger_lista_logs[".addFields"][] = "log";

$tdataexp_ger_lista_logs[".masterListFields"] = array();

$tdataexp_ger_lista_logs[".inlineAddFields"] = array();

$tdataexp_ger_lista_logs[".editFields"] = array();

$tdataexp_ger_lista_logs[".inlineEditFields"] = array();

$tdataexp_ger_lista_logs[".exportFields"] = array();
$tdataexp_ger_lista_logs[".exportFields"][] = "idLog";
$tdataexp_ger_lista_logs[".exportFields"][] = "log";

$tdataexp_ger_lista_logs[".importFields"] = array();
$tdataexp_ger_lista_logs[".importFields"][] = "idLog";
$tdataexp_ger_lista_logs[".importFields"][] = "log";

$tdataexp_ger_lista_logs[".printFields"] = array();
$tdataexp_ger_lista_logs[".printFields"][] = "idLog";
$tdataexp_ger_lista_logs[".printFields"][] = "log";

//	idLog
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idLog";
	$fdata["GoodName"] = "idLog";
	$fdata["ownerTable"] = "exp_ger_lista_logs";
	$fdata["Label"] = GetFieldLabel("exp_ger_lista_logs","idLog");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idLog";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idLog";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataexp_ger_lista_logs["idLog"] = $fdata;
//	log
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "log";
	$fdata["GoodName"] = "log";
	$fdata["ownerTable"] = "exp_ger_lista_logs";
	$fdata["Label"] = GetFieldLabel("exp_ger_lista_logs","log");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "log";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "log";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=15";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataexp_ger_lista_logs["log"] = $fdata;


$tables_data["exp_ger_lista_logs"]=&$tdataexp_ger_lista_logs;
$field_labels["exp_ger_lista_logs"] = &$fieldLabelsexp_ger_lista_logs;
$fieldToolTips["exp_ger_lista_logs"] = &$fieldToolTipsexp_ger_lista_logs;
$page_titles["exp_ger_lista_logs"] = &$pageTitlesexp_ger_lista_logs;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["exp_ger_lista_logs"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["exp_ger_lista_logs"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_exp_ger_lista_logs()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idLog,  	log";
$proto0["m_strFrom"] = "FROM exp_ger_lista_logs";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idLog",
	"m_strTable" => "exp_ger_lista_logs",
	"m_srcTableName" => "exp_ger_lista_logs"
));

$proto6["m_sql"] = "idLog";
$proto6["m_srcTableName"] = "exp_ger_lista_logs";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "log",
	"m_strTable" => "exp_ger_lista_logs",
	"m_srcTableName" => "exp_ger_lista_logs"
));

$proto8["m_sql"] = "log";
$proto8["m_srcTableName"] = "exp_ger_lista_logs";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "exp_ger_lista_logs";
$proto11["m_srcTableName"] = "exp_ger_lista_logs";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "idLog";
$proto11["m_columns"][] = "log";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "exp_ger_lista_logs";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "exp_ger_lista_logs";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="exp_ger_lista_logs";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_exp_ger_lista_logs = createSqlQuery_exp_ger_lista_logs();


	
		;

		

$tdataexp_ger_lista_logs[".sqlquery"] = $queryData_exp_ger_lista_logs;

$tableEvents["exp_ger_lista_logs"] = new eventsBase;
$tdataexp_ger_lista_logs[".hasEvents"] = false;

?>