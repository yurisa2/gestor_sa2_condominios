<?php

/**
* getLookupMainTableSettings - tests whether the lookup link exists between the tables
*
*  returns array with ProjectSettings class for main table if the link exists in project settings.
*  returns NULL otherwise
*/
function getLookupMainTableSettings($lookupTable, $mainTableShortName, $mainField, $desiredPage = "")
{
	global $lookupTableLinks;
	if(!isset($lookupTableLinks[$lookupTable]))
		return null;
	if(!isset($lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField]))
		return null;
	$arr = &$lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField];
	$effectivePage = $desiredPage;
	if(!isset($arr[$effectivePage]))
	{
		$effectivePage = PAGE_EDIT;
		if(!isset($arr[$effectivePage]))
		{
			if($desiredPage == "" && 0 < count($arr))
			{
				$effectivePage = $arr[0];
			}
			else
				return null;
		}
	}
	return new ProjectSettings($arr[$effectivePage]["table"], $effectivePage);
}

/** 
* $lookupTableLinks array stores all lookup links between tables in the project
*/
function InitLookupLinks()
{
	global $lookupTableLinks;

	$lookupTableLinks = array();

	$lookupTableLinks["fin_despesas"]["fin_cheques.link_fin_despesa"]["edit"] = array("table" => "fin_cheques", "field" => "link_fin_despesa", "page" => "edit");
	$lookupTableLinks["fin_contas"]["fin_cheques.link_fin_conta"]["edit"] = array("table" => "fin_cheques", "field" => "link_fin_conta", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["fin_cheques.link_ger_grupo"]["edit"] = array("table" => "fin_cheques", "field" => "link_ger_grupo", "page" => "edit");
	$lookupTableLinks["ger_selecao_guni_ger_unidades"]["fin_cobranca.Unidade"]["edit"] = array("table" => "fin_cobranca", "field" => "Unidade", "page" => "edit");
	$lookupTableLinks["fin_contas"]["fin_cobranca.conta"]["edit"] = array("table" => "fin_cobranca", "field" => "conta", "page" => "edit");
	$lookupTableLinks["fin_tipodocs"]["fin_cobranca.tipodoc"]["edit"] = array("table" => "fin_cobranca", "field" => "tipodoc", "page" => "edit");
	$lookupTableLinks["ger_selecao_ger_unidades"]["fin_cobranca.link_ger_unidade"]["edit"] = array("table" => "fin_cobranca", "field" => "link_ger_unidade", "page" => "edit");
	$lookupTableLinks["fin_meiospgto"]["fin_despesas.link_fin_meiospgto"]["edit"] = array("table" => "fin_despesas", "field" => "link_fin_meiospgto", "page" => "edit");
	$lookupTableLinks["fin_tipogastos"]["fin_despesas.link_fin_tipogasto"]["edit"] = array("table" => "fin_despesas", "field" => "link_fin_tipogasto", "page" => "edit");
	$lookupTableLinks["fin_tipodocs"]["fin_despesas.link_fin_tipodoc"]["edit"] = array("table" => "fin_despesas", "field" => "link_fin_tipodoc", "page" => "edit");
	$lookupTableLinks["fin_fornecedores"]["fin_despesas.link_fin_fornecedor"]["edit"] = array("table" => "fin_despesas", "field" => "link_fin_fornecedor", "page" => "edit");
	$lookupTableLinks["fin_contas"]["fin_despesas.link_fin_conta"]["edit"] = array("table" => "fin_despesas", "field" => "link_fin_conta", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["fin_despesas.link_ger_grupo"]["edit"] = array("table" => "fin_despesas", "field" => "link_ger_grupo", "page" => "edit");
	$lookupTableLinks["exp_ger_lista_logs"]["fin_fornecedores.log"]["edit"] = array("table" => "fin_fornecedores", "field" => "log", "page" => "edit");
	$lookupTableLinks["ger_lista_uf"]["fin_fornecedores.uf"]["edit"] = array("table" => "fin_fornecedores", "field" => "uf", "page" => "edit");
	$lookupTableLinks["exp_ger_lista_logs"]["ger_cadastro.log"]["edit"] = array("table" => "ger_cadastro", "field" => "log", "page" => "edit");
	$lookupTableLinks["ger_lista_uf"]["ger_cadastro.uf"]["edit"] = array("table" => "ger_cadastro", "field" => "uf", "page" => "edit");
	$lookupTableLinks["ger_selecao_ger_unidades"]["ger_moradores.link_ger_unidades"]["edit"] = array("table" => "ger_moradores", "field" => "link_ger_unidades", "page" => "edit");
	$lookupTableLinks["ger_lista_relacao_com_unidade"]["ger_moradores.relacaocomunid"]["edit"] = array("table" => "ger_moradores", "field" => "relacaocomunid", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["ger_unidades.grupo"]["edit"] = array("table" => "ger_unidades", "field" => "grupo", "page" => "edit");
	$lookupTableLinks["ger_selecao_guni_ger_unidades"]["ger_unidades.grupounidade"]["edit"] = array("table" => "ger_unidades", "field" => "grupounidade", "page" => "edit");
	$lookupTableLinks["ger_cadastro"]["ger_unidades.link_ger_cadastro"]["edit"] = array("table" => "ger_unidades", "field" => "link_ger_cadastro", "page" => "edit");
	$lookupTableLinks["ger_selecao_GUNI_ger_unidades"]["fin_cobranca_atualizacao_aberto.grupo"]["edit"] = array("table" => "fin_cobranca_atualizacao_aberto", "field" => "grupo", "page" => "edit");
	$lookupTableLinks["ger_selecao_guni_ger_unidades"]["fin_cobranca_atualizacao_aberto.unidade"]["edit"] = array("table" => "fin_cobranca_atualizacao_aberto", "field" => "unidade", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["ger_usuarios.grupo"]["edit"] = array("table" => "ger_usuarios", "field" => "grupo", "page" => "edit");
	$lookupTableLinks["exp_lista_foruns"]["jur_processos.Forum"]["edit"] = array("table" => "jur_processos", "field" => "Forum", "page" => "edit");
	$lookupTableLinks["ger_selecao_ger_unidades"]["jur_processos.link_ger_unidade"]["edit"] = array("table" => "jur_processos", "field" => "link_ger_unidade", "page" => "edit");
	$lookupTableLinks["ger_selecao_ger_unidades"]["jur_processos.Unidade"]["edit"] = array("table" => "jur_processos", "field" => "Unidade", "page" => "edit");
	$lookupTableLinks["ger_cadastro"]["ger_ocorrencias.link_ger_cadastro"]["edit"] = array("table" => "ger_ocorrencias", "field" => "link_ger_cadastro", "page" => "edit");
	$lookupTableLinks["fin_contas"]["ger_op_inserirtdboleto.link_fin_conta"]["edit"] = array("table" => "ger_op_inserirtdboleto", "field" => "link_fin_conta", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["ger_op_inserirtdboleto.grupo"]["edit"] = array("table" => "ger_op_inserirtdboleto", "field" => "grupo", "page" => "edit");
	$lookupTableLinks["ger_selecao_GUNI_ger_unidades"]["fin_debitos_contador.link_ger_unidade"]["edit"] = array("table" => "fin_debitos_contador", "field" => "link_ger_unidade", "page" => "edit");
	$lookupTableLinks["ger_selecao_guni_ger_unidades"]["fin_debitos_resumo.link_ger_unidade"]["edit"] = array("table" => "fin_debitos_resumo", "field" => "link_ger_unidade", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["ger_cadastro_moradores_plista.grupo"]["edit"] = array("table" => "ger_cadastro_moradores_plista", "field" => "grupo", "page" => "edit");
	$lookupTableLinks["ger_selecao_guni_ger_unidades"]["ger_arquivos_unidades.link_ger_unidade"]["edit"] = array("table" => "ger_arquivos_unidades", "field" => "link_ger_unidade", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["fin_bco_ext1.grupo"]["edit"] = array("table" => "fin_bco_ext1", "field" => "grupo", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["SaldosBlocos.grupo"]["edit"] = array("table" => "SaldosBlocos", "field" => "grupo", "page" => "edit");
	$lookupTableLinks["fin_tipodocs"]["ger_arquivos_geral.link_ger_tipos_arquivo"]["edit"] = array("table" => "ger_arquivos_geral", "field" => "link_ger_tipos_arquivo", "page" => "edit");
	$lookupTableLinks["ger_grupo"]["ger_arquivos_grupos.link_ger_grupos"]["edit"] = array("table" => "ger_arquivos_grupos", "field" => "link_ger_grupos", "page" => "edit");
	$lookupTableLinks["fin_tipodocs"]["ger_arquivos_grupos.link_ger_tipos_arquivo"]["edit"] = array("table" => "ger_arquivos_grupos", "field" => "link_ger_tipos_arquivo", "page" => "edit");
	$lookupTableLinks["fin_tipodocs"]["ger_arquivos_adm.link_ger_tipos_arquivo"]["edit"] = array("table" => "ger_arquivos_adm", "field" => "link_ger_tipos_arquivo", "page" => "edit");
}

?>