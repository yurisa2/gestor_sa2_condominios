<?php
include_once(getabspath("classes/printpage.php"));

function DisplayMasterTableInfoForPrint_fin_despesas($params)
{
	global $cman;
	
	$detailtable = $params["detailtable"];
	$keys = $params["keys"];
	
	$xt = new Xtempl();
	
	$tName = "fin_despesas";
	$xt->eventsObject = getEventObject($tName);

	$pageType = PAGE_PRINT;

	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = PRINT_MASTER;
	$mParams["pageType"] = $pageType;
	$mParams["tName"] = $tName;
	$masterPage = new PrintPage($mParams);
	
	$cipherer = new RunnerCipherer( $tName );
	$settings = new ProjectSettings($tName, $pageType);
	$connection = $cman->byTable( $tName );
	
	$masterQuery = $settings->getSQLQuery();
	$viewControls = new ViewControlsContainer($settings, $pageType, $masterPage);
	
	$where = "";
	$keysAssoc = array();
	$showKeys = "";

	if( $detailtable == "fin_cheques" )
	{
		$keysAssoc["idDespesa"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("idDespesa", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("idDespesa", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("idDespesa", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("fin_despesas","idDespesa").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}
	
	if( !$where )
		return;
	
	$str = SecuritySQL("Export", $tName );
	if( strlen($str) )
		$where.= " and ".$str;
	
	$strWhere = whereAdd( $masterQuery->m_where->toSql($masterQuery), $where );
	if( strlen($strWhere) )
		$strWhere= " where ".$strWhere." ";
		
	$strSQL = $masterQuery->HeadToSql().' '.$masterQuery->FromToSql().$strWhere.$masterQuery->TailToSql();
	LogInfo($strSQL);
	
	$data = $cipherer->DecryptFetchedArray( $connection->query( $strSQL )->fetchAssoc() );
	if( !$data )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));	
	
	$keylink = "";
	$keylink.= "&key1=".runner_htmlspecialchars(rawurlencode(@$data["idDespesa"]));
	
	$xt->assign("vencimento_mastervalue", $viewControls->showDBValue("vencimento", $data, $keylink));
	$format = $settings->getViewFormat("vencimento");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("vencimento")))
		$class = ' rnr-field-number';
		
	$xt->assign("vencimento_class", $class); // add class for field header as field value
	$xt->assign("link_fin_meiospgto_mastervalue", $viewControls->showDBValue("link_fin_meiospgto", $data, $keylink));
	$format = $settings->getViewFormat("link_fin_meiospgto");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("link_fin_meiospgto")))
		$class = ' rnr-field-number';
		
	$xt->assign("link_fin_meiospgto_class", $class); // add class for field header as field value
	$xt->assign("obs_mastervalue", $viewControls->showDBValue("obs", $data, $keylink));
	$format = $settings->getViewFormat("obs");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("obs")))
		$class = ' rnr-field-number';
		
	$xt->assign("obs_class", $class); // add class for field header as field value
	$xt->assign("link_fin_tipogasto_mastervalue", $viewControls->showDBValue("link_fin_tipogasto", $data, $keylink));
	$format = $settings->getViewFormat("link_fin_tipogasto");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("link_fin_tipogasto")))
		$class = ' rnr-field-number';
		
	$xt->assign("link_fin_tipogasto_class", $class); // add class for field header as field value
	$xt->assign("link_fin_tipodoc_mastervalue", $viewControls->showDBValue("link_fin_tipodoc", $data, $keylink));
	$format = $settings->getViewFormat("link_fin_tipodoc");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("link_fin_tipodoc")))
		$class = ' rnr-field-number';
		
	$xt->assign("link_fin_tipodoc_class", $class); // add class for field header as field value
	$xt->assign("ndoc_mastervalue", $viewControls->showDBValue("ndoc", $data, $keylink));
	$format = $settings->getViewFormat("ndoc");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("ndoc")))
		$class = ' rnr-field-number';
		
	$xt->assign("ndoc_class", $class); // add class for field header as field value
	$xt->assign("link_fin_fornecedor_mastervalue", $viewControls->showDBValue("link_fin_fornecedor", $data, $keylink));
	$format = $settings->getViewFormat("link_fin_fornecedor");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("link_fin_fornecedor")))
		$class = ' rnr-field-number';
		
	$xt->assign("link_fin_fornecedor_class", $class); // add class for field header as field value
	$xt->assign("valor_mastervalue", $viewControls->showDBValue("valor", $data, $keylink));
	$format = $settings->getViewFormat("valor");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("valor")))
		$class = ' rnr-field-number';
		
	$xt->assign("valor_class", $class); // add class for field header as field value
	$xt->assign("link_fin_conta_mastervalue", $viewControls->showDBValue("link_fin_conta", $data, $keylink));
	$format = $settings->getViewFormat("link_fin_conta");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("link_fin_conta")))
		$class = ' rnr-field-number';
		
	$xt->assign("link_fin_conta_class", $class); // add class for field header as field value
	$xt->assign("pagamento_mastervalue", $viewControls->showDBValue("pagamento", $data, $keylink));
	$format = $settings->getViewFormat("pagamento");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("pagamento")))
		$class = ' rnr-field-number';
		
	$xt->assign("pagamento_class", $class); // add class for field header as field value
	$xt->assign("link_ger_grupo_mastervalue", $viewControls->showDBValue("link_ger_grupo", $data, $keylink));
	$format = $settings->getViewFormat("link_ger_grupo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("link_ger_grupo")))
		$class = ' rnr-field-number';
		
	$xt->assign("link_ger_grupo_class", $class); // add class for field header as field value
	$xt->assign("documento_mastervalue", $viewControls->showDBValue("documento", $data, $keylink));
	$format = $settings->getViewFormat("documento");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("documento")))
		$class = ' rnr-field-number';
		
	$xt->assign("documento_class", $class); // add class for field header as field value

	$layout = GetPageLayout("fin_despesas", 'masterprint');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');

	$xt->displayPartial(GetTemplateName("fin_despesas", "masterprint"));
}

?>