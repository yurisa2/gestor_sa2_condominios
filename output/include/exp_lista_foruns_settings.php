<?php
require_once(getabspath("classes/cipherer.php"));




$tdataexp_lista_foruns = array();
	$tdataexp_lista_foruns[".truncateText"] = true;
	$tdataexp_lista_foruns[".NumberOfChars"] = 80;
	$tdataexp_lista_foruns[".ShortName"] = "exp_lista_foruns";
	$tdataexp_lista_foruns[".OwnerID"] = "";
	$tdataexp_lista_foruns[".OriginalTable"] = "exp_lista_foruns";

//	field labels
$fieldLabelsexp_lista_foruns = array();
$fieldToolTipsexp_lista_foruns = array();
$pageTitlesexp_lista_foruns = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsexp_lista_foruns["Portuguese(Brazil)"] = array();
	$fieldToolTipsexp_lista_foruns["Portuguese(Brazil)"] = array();
	$pageTitlesexp_lista_foruns["Portuguese(Brazil)"] = array();
	$fieldLabelsexp_lista_foruns["Portuguese(Brazil)"]["idForum"] = "idForum";
	$fieldToolTipsexp_lista_foruns["Portuguese(Brazil)"]["idForum"] = "";
	$fieldLabelsexp_lista_foruns["Portuguese(Brazil)"]["Forum"] = "Forum";
	$fieldToolTipsexp_lista_foruns["Portuguese(Brazil)"]["Forum"] = "";
	$fieldLabelsexp_lista_foruns["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsexp_lista_foruns["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsexp_lista_foruns["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsexp_lista_foruns["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsexp_lista_foruns["Portuguese(Brazil)"]))
		$tdataexp_lista_foruns[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsexp_lista_foruns[""] = array();
	$fieldToolTipsexp_lista_foruns[""] = array();
	$pageTitlesexp_lista_foruns[""] = array();
	if (count($fieldToolTipsexp_lista_foruns[""]))
		$tdataexp_lista_foruns[".isUseToolTips"] = true;
}


	$tdataexp_lista_foruns[".NCSearch"] = true;



$tdataexp_lista_foruns[".shortTableName"] = "exp_lista_foruns";
$tdataexp_lista_foruns[".nSecOptions"] = 0;
$tdataexp_lista_foruns[".recsPerRowList"] = 1;
$tdataexp_lista_foruns[".recsPerRowPrint"] = 1;
$tdataexp_lista_foruns[".mainTableOwnerID"] = "";
$tdataexp_lista_foruns[".moveNext"] = 1;
$tdataexp_lista_foruns[".entityType"] = 0;

$tdataexp_lista_foruns[".strOriginalTableName"] = "exp_lista_foruns";





$tdataexp_lista_foruns[".showAddInPopup"] = false;

$tdataexp_lista_foruns[".showEditInPopup"] = false;

$tdataexp_lista_foruns[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataexp_lista_foruns[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataexp_lista_foruns[".fieldsForRegister"] = array();

$tdataexp_lista_foruns[".listAjax"] = false;

	$tdataexp_lista_foruns[".audit"] = true;

	$tdataexp_lista_foruns[".locking"] = true;



$tdataexp_lista_foruns[".list"] = true;


$tdataexp_lista_foruns[".import"] = true;

$tdataexp_lista_foruns[".exportTo"] = true;



$tdataexp_lista_foruns[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataexp_lista_foruns[".searchSaving"] = false;
//

$tdataexp_lista_foruns[".showSearchPanel"] = true;
		$tdataexp_lista_foruns[".flexibleSearch"] = true;

if (isMobile())
	$tdataexp_lista_foruns[".isUseAjaxSuggest"] = false;
else
	$tdataexp_lista_foruns[".isUseAjaxSuggest"] = true;

$tdataexp_lista_foruns[".rowHighlite"] = true;



$tdataexp_lista_foruns[".addPageEvents"] = false;

// use timepicker for search panel
$tdataexp_lista_foruns[".isUseTimeForSearch"] = false;





$tdataexp_lista_foruns[".allSearchFields"] = array();
$tdataexp_lista_foruns[".filterFields"] = array();
$tdataexp_lista_foruns[".requiredSearchFields"] = array();

$tdataexp_lista_foruns[".allSearchFields"][] = "idForum";
	$tdataexp_lista_foruns[".allSearchFields"][] = "Forum";
	$tdataexp_lista_foruns[".allSearchFields"][] = "ultimousuario";
	$tdataexp_lista_foruns[".allSearchFields"][] = "ultimaalteracao";
	

$tdataexp_lista_foruns[".googleLikeFields"] = array();
$tdataexp_lista_foruns[".googleLikeFields"][] = "idForum";
$tdataexp_lista_foruns[".googleLikeFields"][] = "Forum";
$tdataexp_lista_foruns[".googleLikeFields"][] = "ultimousuario";
$tdataexp_lista_foruns[".googleLikeFields"][] = "ultimaalteracao";


$tdataexp_lista_foruns[".advSearchFields"] = array();
$tdataexp_lista_foruns[".advSearchFields"][] = "idForum";
$tdataexp_lista_foruns[".advSearchFields"][] = "Forum";
$tdataexp_lista_foruns[".advSearchFields"][] = "ultimousuario";
$tdataexp_lista_foruns[".advSearchFields"][] = "ultimaalteracao";

$tdataexp_lista_foruns[".tableType"] = "list";

$tdataexp_lista_foruns[".printerPageOrientation"] = 0;
$tdataexp_lista_foruns[".nPrinterPageScale"] = 100;

$tdataexp_lista_foruns[".nPrinterSplitRecords"] = 40;

$tdataexp_lista_foruns[".nPrinterPDFSplitRecords"] = 40;



$tdataexp_lista_foruns[".geocodingEnabled"] = false;





$tdataexp_lista_foruns[".listGridLayout"] = 3;

$tdataexp_lista_foruns[".isDisplayLoading"] = true;


$tdataexp_lista_foruns[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdataexp_lista_foruns[".pageSize"] = 20;

$tdataexp_lista_foruns[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataexp_lista_foruns[".strOrderBy"] = $tstrOrderBy;

$tdataexp_lista_foruns[".orderindexes"] = array();

$tdataexp_lista_foruns[".sqlHead"] = "SELECT idForum,  	Forum,  	ultimousuario,  	ultimaalteracao";
$tdataexp_lista_foruns[".sqlFrom"] = "FROM exp_lista_foruns";
$tdataexp_lista_foruns[".sqlWhereExpr"] = "";
$tdataexp_lista_foruns[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataexp_lista_foruns[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataexp_lista_foruns[".arrGroupsPerPage"] = $arrGPP;

$tdataexp_lista_foruns[".highlightSearchResults"] = true;

$tableKeysexp_lista_foruns = array();
$tableKeysexp_lista_foruns[] = "idForum";
$tdataexp_lista_foruns[".Keys"] = $tableKeysexp_lista_foruns;

$tdataexp_lista_foruns[".listFields"] = array();
$tdataexp_lista_foruns[".listFields"][] = "idForum";
$tdataexp_lista_foruns[".listFields"][] = "Forum";

$tdataexp_lista_foruns[".hideMobileList"] = array();


$tdataexp_lista_foruns[".viewFields"] = array();

$tdataexp_lista_foruns[".addFields"] = array();

$tdataexp_lista_foruns[".masterListFields"] = array();

$tdataexp_lista_foruns[".inlineAddFields"] = array();

$tdataexp_lista_foruns[".editFields"] = array();

$tdataexp_lista_foruns[".inlineEditFields"] = array();

$tdataexp_lista_foruns[".exportFields"] = array();
$tdataexp_lista_foruns[".exportFields"][] = "idForum";
$tdataexp_lista_foruns[".exportFields"][] = "Forum";
$tdataexp_lista_foruns[".exportFields"][] = "ultimousuario";
$tdataexp_lista_foruns[".exportFields"][] = "ultimaalteracao";

$tdataexp_lista_foruns[".importFields"] = array();
$tdataexp_lista_foruns[".importFields"][] = "idForum";
$tdataexp_lista_foruns[".importFields"][] = "Forum";
$tdataexp_lista_foruns[".importFields"][] = "ultimousuario";
$tdataexp_lista_foruns[".importFields"][] = "ultimaalteracao";

$tdataexp_lista_foruns[".printFields"] = array();

//	idForum
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idForum";
	$fdata["GoodName"] = "idForum";
	$fdata["ownerTable"] = "exp_lista_foruns";
	$fdata["Label"] = GetFieldLabel("exp_lista_foruns","idForum");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idForum";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idForum";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataexp_lista_foruns["idForum"] = $fdata;
//	Forum
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Forum";
	$fdata["GoodName"] = "Forum";
	$fdata["ownerTable"] = "exp_lista_foruns";
	$fdata["Label"] = GetFieldLabel("exp_lista_foruns","Forum");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Forum";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Forum";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataexp_lista_foruns["Forum"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "exp_lista_foruns";
	$fdata["Label"] = GetFieldLabel("exp_lista_foruns","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataexp_lista_foruns["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "exp_lista_foruns";
	$fdata["Label"] = GetFieldLabel("exp_lista_foruns","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataexp_lista_foruns["ultimaalteracao"] = $fdata;


$tables_data["exp_lista_foruns"]=&$tdataexp_lista_foruns;
$field_labels["exp_lista_foruns"] = &$fieldLabelsexp_lista_foruns;
$fieldToolTips["exp_lista_foruns"] = &$fieldToolTipsexp_lista_foruns;
$page_titles["exp_lista_foruns"] = &$pageTitlesexp_lista_foruns;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["exp_lista_foruns"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["exp_lista_foruns"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_exp_lista_foruns()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idForum,  	Forum,  	ultimousuario,  	ultimaalteracao";
$proto0["m_strFrom"] = "FROM exp_lista_foruns";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idForum",
	"m_strTable" => "exp_lista_foruns",
	"m_srcTableName" => "exp_lista_foruns"
));

$proto6["m_sql"] = "idForum";
$proto6["m_srcTableName"] = "exp_lista_foruns";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Forum",
	"m_strTable" => "exp_lista_foruns",
	"m_srcTableName" => "exp_lista_foruns"
));

$proto8["m_sql"] = "Forum";
$proto8["m_srcTableName"] = "exp_lista_foruns";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "exp_lista_foruns",
	"m_srcTableName" => "exp_lista_foruns"
));

$proto10["m_sql"] = "ultimousuario";
$proto10["m_srcTableName"] = "exp_lista_foruns";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "exp_lista_foruns",
	"m_srcTableName" => "exp_lista_foruns"
));

$proto12["m_sql"] = "ultimaalteracao";
$proto12["m_srcTableName"] = "exp_lista_foruns";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "exp_lista_foruns";
$proto15["m_srcTableName"] = "exp_lista_foruns";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "idForum";
$proto15["m_columns"][] = "Forum";
$proto15["m_columns"][] = "ultimousuario";
$proto15["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "exp_lista_foruns";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "exp_lista_foruns";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="exp_lista_foruns";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_exp_lista_foruns = createSqlQuery_exp_lista_foruns();


	
		;

				

$tdataexp_lista_foruns[".sqlquery"] = $queryData_exp_lista_foruns;

$tableEvents["exp_lista_foruns"] = new eventsBase;
$tdataexp_lista_foruns[".hasEvents"] = false;

?>