<?php
require_once(getabspath("classes/cipherer.php"));




$tdatajur_processos = array();
	$tdatajur_processos[".truncateText"] = true;
	$tdatajur_processos[".NumberOfChars"] = 80;
	$tdatajur_processos[".ShortName"] = "jur_processos";
	$tdatajur_processos[".OwnerID"] = "";
	$tdatajur_processos[".OriginalTable"] = "jur_processos";

//	field labels
$fieldLabelsjur_processos = array();
$fieldToolTipsjur_processos = array();
$pageTitlesjur_processos = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsjur_processos["Portuguese(Brazil)"] = array();
	$fieldToolTipsjur_processos["Portuguese(Brazil)"] = array();
	$pageTitlesjur_processos["Portuguese(Brazil)"] = array();
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["idProc"] = "Código";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["idProc"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["Forum"] = "Forum";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["Forum"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["partes"] = "Partes";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["partes"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["vara"] = "Vara";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["vara"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["numgde"] = "N. Completo";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["numgde"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["controle"] = "Controle";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["controle"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["tipo"] = "Tipo";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["tipo"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["ano"] = "Ano";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["ano"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["ReuAutor"] = "Cliente no processo";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["ReuAutor"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["Titulo"] = "Titulo da ação";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["Titulo"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["recurso"] = "Recurso";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["recurso"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["distrib"] = "Distribuição";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["distrib"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["link_ger_unidade"] = "Unidade";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["link_ger_unidade"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["situacao"] = "Situação";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["situacao"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["ativo"] = "Ativo";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["ativo"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["linkdebitos"] = "Débitos";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["linkdebitos"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["unidadeir"] = "Unidade";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["unidadeir"] = "";
	$fieldLabelsjur_processos["Portuguese(Brazil)"]["Unidade"] = "Unidade";
	$fieldToolTipsjur_processos["Portuguese(Brazil)"]["Unidade"] = "";
	if (count($fieldToolTipsjur_processos["Portuguese(Brazil)"]))
		$tdatajur_processos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsjur_processos[""] = array();
	$fieldToolTipsjur_processos[""] = array();
	$pageTitlesjur_processos[""] = array();
	if (count($fieldToolTipsjur_processos[""]))
		$tdatajur_processos[".isUseToolTips"] = true;
}


	$tdatajur_processos[".NCSearch"] = true;



$tdatajur_processos[".shortTableName"] = "jur_processos";
$tdatajur_processos[".nSecOptions"] = 0;
$tdatajur_processos[".recsPerRowList"] = 1;
$tdatajur_processos[".recsPerRowPrint"] = 1;
$tdatajur_processos[".mainTableOwnerID"] = "";
$tdatajur_processos[".moveNext"] = 1;
$tdatajur_processos[".entityType"] = 0;

$tdatajur_processos[".strOriginalTableName"] = "jur_processos";





$tdatajur_processos[".showAddInPopup"] = false;

$tdatajur_processos[".showEditInPopup"] = false;

$tdatajur_processos[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatajur_processos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatajur_processos[".fieldsForRegister"] = array();

$tdatajur_processos[".listAjax"] = false;

	$tdatajur_processos[".audit"] = true;

	$tdatajur_processos[".locking"] = true;

$tdatajur_processos[".edit"] = true;
$tdatajur_processos[".afterEditAction"] = 1;
$tdatajur_processos[".closePopupAfterEdit"] = 1;
$tdatajur_processos[".afterEditActionDetTable"] = "";

$tdatajur_processos[".add"] = true;
$tdatajur_processos[".afterAddAction"] = 1;
$tdatajur_processos[".closePopupAfterAdd"] = 1;
$tdatajur_processos[".afterAddActionDetTable"] = "";

$tdatajur_processos[".list"] = true;

$tdatajur_processos[".view"] = true;

$tdatajur_processos[".import"] = true;

$tdatajur_processos[".exportTo"] = true;

$tdatajur_processos[".printFriendly"] = true;

$tdatajur_processos[".delete"] = true;

$tdatajur_processos[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatajur_processos[".searchSaving"] = false;
//

$tdatajur_processos[".showSearchPanel"] = true;
		$tdatajur_processos[".flexibleSearch"] = true;

if (isMobile())
	$tdatajur_processos[".isUseAjaxSuggest"] = false;
else
	$tdatajur_processos[".isUseAjaxSuggest"] = true;

$tdatajur_processos[".rowHighlite"] = true;



$tdatajur_processos[".addPageEvents"] = false;

// use timepicker for search panel
$tdatajur_processos[".isUseTimeForSearch"] = false;



$tdatajur_processos[".badgeColor"] = "CD5C5C";


$tdatajur_processos[".allSearchFields"] = array();
$tdatajur_processos[".filterFields"] = array();
$tdatajur_processos[".requiredSearchFields"] = array();

$tdatajur_processos[".allSearchFields"][] = "ativo";
	$tdatajur_processos[".allSearchFields"][] = "distrib";
	$tdatajur_processos[".allSearchFields"][] = "link_ger_unidade";
	$tdatajur_processos[".allSearchFields"][] = "Forum";
	$tdatajur_processos[".allSearchFields"][] = "partes";
	$tdatajur_processos[".allSearchFields"][] = "vara";
	$tdatajur_processos[".allSearchFields"][] = "numgde";
	$tdatajur_processos[".allSearchFields"][] = "controle";
	$tdatajur_processos[".allSearchFields"][] = "ano";
	$tdatajur_processos[".allSearchFields"][] = "tipo";
	$tdatajur_processos[".allSearchFields"][] = "Titulo";
	$tdatajur_processos[".allSearchFields"][] = "ReuAutor";
	$tdatajur_processos[".allSearchFields"][] = "situacao";
	$tdatajur_processos[".allSearchFields"][] = "obs";
	$tdatajur_processos[".allSearchFields"][] = "recurso";
	$tdatajur_processos[".allSearchFields"][] = "ultimousuario";
	$tdatajur_processos[".allSearchFields"][] = "ultimaalteracao";
	

$tdatajur_processos[".googleLikeFields"] = array();
$tdatajur_processos[".googleLikeFields"][] = "Forum";
$tdatajur_processos[".googleLikeFields"][] = "partes";
$tdatajur_processos[".googleLikeFields"][] = "vara";
$tdatajur_processos[".googleLikeFields"][] = "numgde";
$tdatajur_processos[".googleLikeFields"][] = "controle";
$tdatajur_processos[".googleLikeFields"][] = "tipo";
$tdatajur_processos[".googleLikeFields"][] = "obs";
$tdatajur_processos[".googleLikeFields"][] = "ano";
$tdatajur_processos[".googleLikeFields"][] = "ReuAutor";
$tdatajur_processos[".googleLikeFields"][] = "Titulo";
$tdatajur_processos[".googleLikeFields"][] = "recurso";
$tdatajur_processos[".googleLikeFields"][] = "distrib";
$tdatajur_processos[".googleLikeFields"][] = "ultimousuario";
$tdatajur_processos[".googleLikeFields"][] = "ultimaalteracao";
$tdatajur_processos[".googleLikeFields"][] = "link_ger_unidade";
$tdatajur_processos[".googleLikeFields"][] = "situacao";
$tdatajur_processos[".googleLikeFields"][] = "ativo";


$tdatajur_processos[".advSearchFields"] = array();
$tdatajur_processos[".advSearchFields"][] = "ativo";
$tdatajur_processos[".advSearchFields"][] = "distrib";
$tdatajur_processos[".advSearchFields"][] = "link_ger_unidade";
$tdatajur_processos[".advSearchFields"][] = "Forum";
$tdatajur_processos[".advSearchFields"][] = "partes";
$tdatajur_processos[".advSearchFields"][] = "vara";
$tdatajur_processos[".advSearchFields"][] = "numgde";
$tdatajur_processos[".advSearchFields"][] = "controle";
$tdatajur_processos[".advSearchFields"][] = "ano";
$tdatajur_processos[".advSearchFields"][] = "tipo";
$tdatajur_processos[".advSearchFields"][] = "Titulo";
$tdatajur_processos[".advSearchFields"][] = "ReuAutor";
$tdatajur_processos[".advSearchFields"][] = "situacao";
$tdatajur_processos[".advSearchFields"][] = "obs";
$tdatajur_processos[".advSearchFields"][] = "recurso";
$tdatajur_processos[".advSearchFields"][] = "ultimousuario";
$tdatajur_processos[".advSearchFields"][] = "ultimaalteracao";

$tdatajur_processos[".tableType"] = "list";

$tdatajur_processos[".printerPageOrientation"] = 0;
$tdatajur_processos[".nPrinterPageScale"] = 100;

$tdatajur_processos[".nPrinterSplitRecords"] = 40;

$tdatajur_processos[".nPrinterPDFSplitRecords"] = 40;



$tdatajur_processos[".geocodingEnabled"] = false;





$tdatajur_processos[".listGridLayout"] = 3;

$tdatajur_processos[".isDisplayLoading"] = true;


$tdatajur_processos[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatajur_processos[".pageSize"] = 20;

$tdatajur_processos[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatajur_processos[".strOrderBy"] = $tstrOrderBy;

$tdatajur_processos[".orderindexes"] = array();

$tdatajur_processos[".sqlHead"] = "SELECT idProc,  Forum,  partes,  vara,  numgde,  controle,  tipo,  obs,  ano,  ReuAutor,  Titulo,  recurso,  distrib,  ultimousuario,  ultimaalteracao,  link_ger_unidade,  situacao,  link_ger_unidade AS Unidade,  ativo,  link_ger_unidade AS linkdebitos,  link_ger_unidade AS unidadeir";
$tdatajur_processos[".sqlFrom"] = "FROM jur_processos";
$tdatajur_processos[".sqlWhereExpr"] = "";
$tdatajur_processos[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatajur_processos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatajur_processos[".arrGroupsPerPage"] = $arrGPP;

$tdatajur_processos[".highlightSearchResults"] = true;

$tableKeysjur_processos = array();
$tableKeysjur_processos[] = "idProc";
$tdatajur_processos[".Keys"] = $tableKeysjur_processos;

$tdatajur_processos[".listFields"] = array();
$tdatajur_processos[".listFields"][] = "ativo";
$tdatajur_processos[".listFields"][] = "linkdebitos";
$tdatajur_processos[".listFields"][] = "unidadeir";
$tdatajur_processos[".listFields"][] = "link_ger_unidade";
$tdatajur_processos[".listFields"][] = "Unidade";
$tdatajur_processos[".listFields"][] = "distrib";
$tdatajur_processos[".listFields"][] = "Forum";
$tdatajur_processos[".listFields"][] = "vara";
$tdatajur_processos[".listFields"][] = "numgde";
$tdatajur_processos[".listFields"][] = "controle";
$tdatajur_processos[".listFields"][] = "ano";
$tdatajur_processos[".listFields"][] = "tipo";
$tdatajur_processos[".listFields"][] = "Titulo";
$tdatajur_processos[".listFields"][] = "ReuAutor";
$tdatajur_processos[".listFields"][] = "situacao";
$tdatajur_processos[".listFields"][] = "obs";
$tdatajur_processos[".listFields"][] = "recurso";

$tdatajur_processos[".hideMobileList"] = array();


$tdatajur_processos[".viewFields"] = array();
$tdatajur_processos[".viewFields"][] = "linkdebitos";
$tdatajur_processos[".viewFields"][] = "ativo";
$tdatajur_processos[".viewFields"][] = "Unidade";
$tdatajur_processos[".viewFields"][] = "unidadeir";
$tdatajur_processos[".viewFields"][] = "idProc";
$tdatajur_processos[".viewFields"][] = "distrib";
$tdatajur_processos[".viewFields"][] = "link_ger_unidade";
$tdatajur_processos[".viewFields"][] = "Forum";
$tdatajur_processos[".viewFields"][] = "partes";
$tdatajur_processos[".viewFields"][] = "vara";
$tdatajur_processos[".viewFields"][] = "numgde";
$tdatajur_processos[".viewFields"][] = "controle";
$tdatajur_processos[".viewFields"][] = "ano";
$tdatajur_processos[".viewFields"][] = "tipo";
$tdatajur_processos[".viewFields"][] = "Titulo";
$tdatajur_processos[".viewFields"][] = "ReuAutor";
$tdatajur_processos[".viewFields"][] = "situacao";
$tdatajur_processos[".viewFields"][] = "obs";
$tdatajur_processos[".viewFields"][] = "recurso";
$tdatajur_processos[".viewFields"][] = "ultimousuario";
$tdatajur_processos[".viewFields"][] = "ultimaalteracao";

$tdatajur_processos[".addFields"] = array();
$tdatajur_processos[".addFields"][] = "ativo";
$tdatajur_processos[".addFields"][] = "distrib";
$tdatajur_processos[".addFields"][] = "link_ger_unidade";
$tdatajur_processos[".addFields"][] = "Forum";
$tdatajur_processos[".addFields"][] = "partes";
$tdatajur_processos[".addFields"][] = "vara";
$tdatajur_processos[".addFields"][] = "numgde";
$tdatajur_processos[".addFields"][] = "controle";
$tdatajur_processos[".addFields"][] = "ano";
$tdatajur_processos[".addFields"][] = "tipo";
$tdatajur_processos[".addFields"][] = "Titulo";
$tdatajur_processos[".addFields"][] = "ReuAutor";
$tdatajur_processos[".addFields"][] = "situacao";
$tdatajur_processos[".addFields"][] = "obs";
$tdatajur_processos[".addFields"][] = "recurso";

$tdatajur_processos[".masterListFields"] = array();

$tdatajur_processos[".inlineAddFields"] = array();

$tdatajur_processos[".editFields"] = array();
$tdatajur_processos[".editFields"][] = "ativo";
$tdatajur_processos[".editFields"][] = "idProc";
$tdatajur_processos[".editFields"][] = "distrib";
$tdatajur_processos[".editFields"][] = "link_ger_unidade";
$tdatajur_processos[".editFields"][] = "Forum";
$tdatajur_processos[".editFields"][] = "partes";
$tdatajur_processos[".editFields"][] = "vara";
$tdatajur_processos[".editFields"][] = "numgde";
$tdatajur_processos[".editFields"][] = "controle";
$tdatajur_processos[".editFields"][] = "ano";
$tdatajur_processos[".editFields"][] = "tipo";
$tdatajur_processos[".editFields"][] = "Titulo";
$tdatajur_processos[".editFields"][] = "ReuAutor";
$tdatajur_processos[".editFields"][] = "situacao";
$tdatajur_processos[".editFields"][] = "obs";
$tdatajur_processos[".editFields"][] = "recurso";

$tdatajur_processos[".inlineEditFields"] = array();

$tdatajur_processos[".exportFields"] = array();
$tdatajur_processos[".exportFields"][] = "linkdebitos";
$tdatajur_processos[".exportFields"][] = "ativo";
$tdatajur_processos[".exportFields"][] = "unidadeir";
$tdatajur_processos[".exportFields"][] = "distrib";
$tdatajur_processos[".exportFields"][] = "link_ger_unidade";
$tdatajur_processos[".exportFields"][] = "Forum";
$tdatajur_processos[".exportFields"][] = "partes";
$tdatajur_processos[".exportFields"][] = "vara";
$tdatajur_processos[".exportFields"][] = "numgde";
$tdatajur_processos[".exportFields"][] = "controle";
$tdatajur_processos[".exportFields"][] = "ano";
$tdatajur_processos[".exportFields"][] = "tipo";
$tdatajur_processos[".exportFields"][] = "Titulo";
$tdatajur_processos[".exportFields"][] = "ReuAutor";
$tdatajur_processos[".exportFields"][] = "situacao";
$tdatajur_processos[".exportFields"][] = "obs";
$tdatajur_processos[".exportFields"][] = "recurso";
$tdatajur_processos[".exportFields"][] = "ultimousuario";
$tdatajur_processos[".exportFields"][] = "ultimaalteracao";

$tdatajur_processos[".importFields"] = array();
$tdatajur_processos[".importFields"][] = "idProc";
$tdatajur_processos[".importFields"][] = "Forum";
$tdatajur_processos[".importFields"][] = "partes";
$tdatajur_processos[".importFields"][] = "vara";
$tdatajur_processos[".importFields"][] = "numgde";
$tdatajur_processos[".importFields"][] = "controle";
$tdatajur_processos[".importFields"][] = "tipo";
$tdatajur_processos[".importFields"][] = "obs";
$tdatajur_processos[".importFields"][] = "ano";
$tdatajur_processos[".importFields"][] = "ReuAutor";
$tdatajur_processos[".importFields"][] = "Titulo";
$tdatajur_processos[".importFields"][] = "recurso";
$tdatajur_processos[".importFields"][] = "distrib";
$tdatajur_processos[".importFields"][] = "ultimousuario";
$tdatajur_processos[".importFields"][] = "ultimaalteracao";
$tdatajur_processos[".importFields"][] = "link_ger_unidade";
$tdatajur_processos[".importFields"][] = "situacao";
$tdatajur_processos[".importFields"][] = "Unidade";
$tdatajur_processos[".importFields"][] = "ativo";
$tdatajur_processos[".importFields"][] = "linkdebitos";
$tdatajur_processos[".importFields"][] = "unidadeir";

$tdatajur_processos[".printFields"] = array();
$tdatajur_processos[".printFields"][] = "linkdebitos";
$tdatajur_processos[".printFields"][] = "ativo";
$tdatajur_processos[".printFields"][] = "unidadeir";
$tdatajur_processos[".printFields"][] = "distrib";
$tdatajur_processos[".printFields"][] = "link_ger_unidade";
$tdatajur_processos[".printFields"][] = "Forum";
$tdatajur_processos[".printFields"][] = "partes";
$tdatajur_processos[".printFields"][] = "vara";
$tdatajur_processos[".printFields"][] = "numgde";
$tdatajur_processos[".printFields"][] = "controle";
$tdatajur_processos[".printFields"][] = "ano";
$tdatajur_processos[".printFields"][] = "tipo";
$tdatajur_processos[".printFields"][] = "Titulo";
$tdatajur_processos[".printFields"][] = "ReuAutor";
$tdatajur_processos[".printFields"][] = "situacao";
$tdatajur_processos[".printFields"][] = "obs";
$tdatajur_processos[".printFields"][] = "recurso";

//	idProc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idProc";
	$fdata["GoodName"] = "idProc";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","idProc");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idProc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idProc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatajur_processos["idProc"] = $fdata;
//	Forum
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Forum";
	$fdata["GoodName"] = "Forum";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","Forum");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Forum";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Forum";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "exp_lista_foruns";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idForum";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Forum";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["Forum"] = $fdata;
//	partes
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "partes";
	$fdata["GoodName"] = "partes";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","partes");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "partes";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "partes";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["partes"] = $fdata;
//	vara
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "vara";
	$fdata["GoodName"] = "vara";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","vara");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vara";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vara";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["vara"] = $fdata;
//	numgde
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "numgde";
	$fdata["GoodName"] = "numgde";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","numgde");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numgde";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "numgde";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["numgde"] = $fdata;
//	controle
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "controle";
	$fdata["GoodName"] = "controle";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","controle");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "controle";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "controle";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["controle"] = $fdata;
//	tipo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "tipo";
	$fdata["GoodName"] = "tipo";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","tipo");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Civil";
	$edata["LookupValues"][] = "Trabalhista";
	$edata["LookupValues"][] = "Penal";
	$edata["LookupValues"][] = "Outros";

	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["tipo"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 250;
			$edata["nCols"] = 500;

	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["obs"] = $fdata;
//	ano
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "ano";
	$fdata["GoodName"] = "ano";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","ano");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ano";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ano";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["ano"] = $fdata;
//	ReuAutor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "ReuAutor";
	$fdata["GoodName"] = "ReuAutor";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","ReuAutor");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ReuAutor";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ReuAutor";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Réu";
	$edata["LookupValues"][] = "Autor";

	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["ReuAutor"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["Titulo"] = $fdata;
//	recurso
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "recurso";
	$fdata["GoodName"] = "recurso";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","recurso");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "recurso";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "recurso";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["recurso"] = $fdata;
//	distrib
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "distrib";
	$fdata["GoodName"] = "distrib";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","distrib");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "distrib";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "distrib";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["distrib"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["ultimaalteracao"] = $fdata;
//	link_ger_unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "link_ger_unidade";
	$fdata["GoodName"] = "link_ger_unidade";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","link_ger_unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "grupounidade";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["link_ger_unidade"] = $fdata;
//	situacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "situacao";
	$fdata["GoodName"] = "situacao";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","situacao");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "situacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "situacao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["situacao"] = $fdata;
//	Unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "Unidade";
	$fdata["GoodName"] = "Unidade";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","Unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatajur_processos["Unidade"] = $fdata;
//	ativo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "ativo";
	$fdata["GoodName"] = "ativo";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","ativo");
	$fdata["FieldType"] = 16;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ativo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ativo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatajur_processos["ativo"] = $fdata;
//	linkdebitos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "linkdebitos";
	$fdata["GoodName"] = "linkdebitos";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","linkdebitos");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatajur_processos["linkdebitos"] = $fdata;
//	unidadeir
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "unidadeir";
	$fdata["GoodName"] = "unidadeir";
	$fdata["ownerTable"] = "jur_processos";
	$fdata["Label"] = GetFieldLabel("jur_processos","unidadeir");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatajur_processos["unidadeir"] = $fdata;


$tables_data["jur_processos"]=&$tdatajur_processos;
$field_labels["jur_processos"] = &$fieldLabelsjur_processos;
$fieldToolTips["jur_processos"] = &$fieldToolTipsjur_processos;
$page_titles["jur_processos"] = &$pageTitlesjur_processos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["jur_processos"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["jur_processos"] = array();


	
				$strOriginalDetailsTable="ger_unidades";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_unidades";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_unidades";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "1";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["jur_processos"][0] = $masterParams;
				$masterTablesData["jur_processos"][0]["masterKeys"] = array();
	$masterTablesData["jur_processos"][0]["masterKeys"][]="idUnidade";
				$masterTablesData["jur_processos"][0]["detailKeys"] = array();
	$masterTablesData["jur_processos"][0]["detailKeys"][]="link_ger_unidade";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_jur_processos()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idProc,  Forum,  partes,  vara,  numgde,  controle,  tipo,  obs,  ano,  ReuAutor,  Titulo,  recurso,  distrib,  ultimousuario,  ultimaalteracao,  link_ger_unidade,  situacao,  link_ger_unidade AS Unidade,  ativo,  link_ger_unidade AS linkdebitos,  link_ger_unidade AS unidadeir";
$proto0["m_strFrom"] = "FROM jur_processos";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idProc",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto6["m_sql"] = "idProc";
$proto6["m_srcTableName"] = "jur_processos";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Forum",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto8["m_sql"] = "Forum";
$proto8["m_srcTableName"] = "jur_processos";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "partes",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto10["m_sql"] = "partes";
$proto10["m_srcTableName"] = "jur_processos";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "vara",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto12["m_sql"] = "vara";
$proto12["m_srcTableName"] = "jur_processos";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "numgde",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto14["m_sql"] = "numgde";
$proto14["m_srcTableName"] = "jur_processos";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "controle",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto16["m_sql"] = "controle";
$proto16["m_srcTableName"] = "jur_processos";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "tipo",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto18["m_sql"] = "tipo";
$proto18["m_srcTableName"] = "jur_processos";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto20["m_sql"] = "obs";
$proto20["m_srcTableName"] = "jur_processos";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "ano",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto22["m_sql"] = "ano";
$proto22["m_srcTableName"] = "jur_processos";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "ReuAutor",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto24["m_sql"] = "ReuAutor";
$proto24["m_srcTableName"] = "jur_processos";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto26["m_sql"] = "Titulo";
$proto26["m_srcTableName"] = "jur_processos";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "recurso",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto28["m_sql"] = "recurso";
$proto28["m_srcTableName"] = "jur_processos";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "distrib",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto30["m_sql"] = "distrib";
$proto30["m_srcTableName"] = "jur_processos";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto32["m_sql"] = "ultimousuario";
$proto32["m_srcTableName"] = "jur_processos";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto34["m_sql"] = "ultimaalteracao";
$proto34["m_srcTableName"] = "jur_processos";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto36["m_sql"] = "link_ger_unidade";
$proto36["m_srcTableName"] = "jur_processos";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "situacao",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto38["m_sql"] = "situacao";
$proto38["m_srcTableName"] = "jur_processos";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto40["m_sql"] = "link_ger_unidade";
$proto40["m_srcTableName"] = "jur_processos";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "Unidade";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "ativo",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto42["m_sql"] = "ativo";
$proto42["m_srcTableName"] = "jur_processos";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto44["m_sql"] = "link_ger_unidade";
$proto44["m_srcTableName"] = "jur_processos";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "linkdebitos";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "jur_processos",
	"m_srcTableName" => "jur_processos"
));

$proto46["m_sql"] = "link_ger_unidade";
$proto46["m_srcTableName"] = "jur_processos";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "unidadeir";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto48=array();
$proto48["m_link"] = "SQLL_MAIN";
			$proto49=array();
$proto49["m_strName"] = "jur_processos";
$proto49["m_srcTableName"] = "jur_processos";
$proto49["m_columns"] = array();
$proto49["m_columns"][] = "idProc";
$proto49["m_columns"][] = "link_ger_unidade";
$proto49["m_columns"][] = "Forum";
$proto49["m_columns"][] = "partes";
$proto49["m_columns"][] = "vara";
$proto49["m_columns"][] = "numgde";
$proto49["m_columns"][] = "controle";
$proto49["m_columns"][] = "tipo";
$proto49["m_columns"][] = "obs";
$proto49["m_columns"][] = "ano";
$proto49["m_columns"][] = "ReuAutor";
$proto49["m_columns"][] = "Titulo";
$proto49["m_columns"][] = "recurso";
$proto49["m_columns"][] = "distrib";
$proto49["m_columns"][] = "ultimousuario";
$proto49["m_columns"][] = "ultimaalteracao";
$proto49["m_columns"][] = "situacao";
$proto49["m_columns"][] = "ativo";
$obj = new SQLTable($proto49);

$proto48["m_table"] = $obj;
$proto48["m_sql"] = "jur_processos";
$proto48["m_alias"] = "";
$proto48["m_srcTableName"] = "jur_processos";
$proto50=array();
$proto50["m_sql"] = "";
$proto50["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto50["m_column"]=$obj;
$proto50["m_contained"] = array();
$proto50["m_strCase"] = "";
$proto50["m_havingmode"] = false;
$proto50["m_inBrackets"] = false;
$proto50["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto50);

$proto48["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto48);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="jur_processos";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_jur_processos = createSqlQuery_jur_processos();


	
		;

																					

$tdatajur_processos[".sqlquery"] = $queryData_jur_processos;

include_once(getabspath("include/jur_processos_events.php"));
$tableEvents["jur_processos"] = new eventclass_jur_processos;
$tdatajur_processos[".hasEvents"] = true;

?>