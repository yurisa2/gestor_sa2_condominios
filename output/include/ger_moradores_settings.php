<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_moradores = array();
	$tdatager_moradores[".truncateText"] = true;
	$tdatager_moradores[".NumberOfChars"] = 80;
	$tdatager_moradores[".ShortName"] = "ger_moradores";
	$tdatager_moradores[".OwnerID"] = "";
	$tdatager_moradores[".OriginalTable"] = "ger_moradores";

//	field labels
$fieldLabelsger_moradores = array();
$fieldToolTipsger_moradores = array();
$pageTitlesger_moradores = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_moradores["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_moradores["Portuguese(Brazil)"] = array();
	$pageTitlesger_moradores["Portuguese(Brazil)"] = array();
	$fieldLabelsger_moradores["Portuguese(Brazil)"]["idMorador"] = "Código";
	$fieldToolTipsger_moradores["Portuguese(Brazil)"]["idMorador"] = "";
	$fieldLabelsger_moradores["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsger_moradores["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsger_moradores["Portuguese(Brazil)"]["link_ger_unidades"] = "Unidade";
	$fieldToolTipsger_moradores["Portuguese(Brazil)"]["link_ger_unidades"] = "";
	$fieldLabelsger_moradores["Portuguese(Brazil)"]["relacaocomunid"] = "Relação com a unidade";
	$fieldToolTipsger_moradores["Portuguese(Brazil)"]["relacaocomunid"] = "";
	$fieldLabelsger_moradores["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsger_moradores["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsger_moradores["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsger_moradores["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_moradores["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsger_moradores["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_moradores["Portuguese(Brazil)"]))
		$tdatager_moradores[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_moradores[""] = array();
	$fieldToolTipsger_moradores[""] = array();
	$pageTitlesger_moradores[""] = array();
	if (count($fieldToolTipsger_moradores[""]))
		$tdatager_moradores[".isUseToolTips"] = true;
}


	$tdatager_moradores[".NCSearch"] = true;



$tdatager_moradores[".shortTableName"] = "ger_moradores";
$tdatager_moradores[".nSecOptions"] = 0;
$tdatager_moradores[".recsPerRowList"] = 1;
$tdatager_moradores[".recsPerRowPrint"] = 1;
$tdatager_moradores[".mainTableOwnerID"] = "";
$tdatager_moradores[".moveNext"] = 1;
$tdatager_moradores[".entityType"] = 0;

$tdatager_moradores[".strOriginalTableName"] = "ger_moradores";





$tdatager_moradores[".showAddInPopup"] = false;

$tdatager_moradores[".showEditInPopup"] = false;

$tdatager_moradores[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_moradores[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_moradores[".fieldsForRegister"] = array();

$tdatager_moradores[".listAjax"] = false;

	$tdatager_moradores[".audit"] = true;

	$tdatager_moradores[".locking"] = true;

$tdatager_moradores[".edit"] = true;
$tdatager_moradores[".afterEditAction"] = 1;
$tdatager_moradores[".closePopupAfterEdit"] = 1;
$tdatager_moradores[".afterEditActionDetTable"] = "";

$tdatager_moradores[".add"] = true;
$tdatager_moradores[".afterAddAction"] = 1;
$tdatager_moradores[".closePopupAfterAdd"] = 1;
$tdatager_moradores[".afterAddActionDetTable"] = "";

$tdatager_moradores[".list"] = true;

$tdatager_moradores[".copy"] = true;
$tdatager_moradores[".view"] = true;


$tdatager_moradores[".exportTo"] = true;

$tdatager_moradores[".printFriendly"] = true;

$tdatager_moradores[".delete"] = true;

$tdatager_moradores[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_moradores[".searchSaving"] = false;
//

$tdatager_moradores[".showSearchPanel"] = true;
		$tdatager_moradores[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_moradores[".isUseAjaxSuggest"] = false;
else
	$tdatager_moradores[".isUseAjaxSuggest"] = true;

$tdatager_moradores[".rowHighlite"] = true;



$tdatager_moradores[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_moradores[".isUseTimeForSearch"] = false;



$tdatager_moradores[".badgeColor"] = "008B8B";


$tdatager_moradores[".allSearchFields"] = array();
$tdatager_moradores[".filterFields"] = array();
$tdatager_moradores[".requiredSearchFields"] = array();

$tdatager_moradores[".allSearchFields"][] = "nome";
	$tdatager_moradores[".allSearchFields"][] = "link_ger_unidades";
	$tdatager_moradores[".allSearchFields"][] = "relacaocomunid";
	$tdatager_moradores[".allSearchFields"][] = "obs";
	$tdatager_moradores[".allSearchFields"][] = "ultimousuario";
	$tdatager_moradores[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_moradores[".googleLikeFields"] = array();
$tdatager_moradores[".googleLikeFields"][] = "nome";
$tdatager_moradores[".googleLikeFields"][] = "link_ger_unidades";
$tdatager_moradores[".googleLikeFields"][] = "relacaocomunid";
$tdatager_moradores[".googleLikeFields"][] = "obs";
$tdatager_moradores[".googleLikeFields"][] = "ultimousuario";
$tdatager_moradores[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_moradores[".advSearchFields"] = array();
$tdatager_moradores[".advSearchFields"][] = "nome";
$tdatager_moradores[".advSearchFields"][] = "link_ger_unidades";
$tdatager_moradores[".advSearchFields"][] = "relacaocomunid";
$tdatager_moradores[".advSearchFields"][] = "obs";
$tdatager_moradores[".advSearchFields"][] = "ultimousuario";
$tdatager_moradores[".advSearchFields"][] = "ultimaalteracao";

$tdatager_moradores[".tableType"] = "list";

$tdatager_moradores[".printerPageOrientation"] = 0;
$tdatager_moradores[".nPrinterPageScale"] = 100;

$tdatager_moradores[".nPrinterSplitRecords"] = 40;

$tdatager_moradores[".nPrinterPDFSplitRecords"] = 40;



$tdatager_moradores[".geocodingEnabled"] = false;





$tdatager_moradores[".listGridLayout"] = 3;

$tdatager_moradores[".isDisplayLoading"] = true;


$tdatager_moradores[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_moradores[".pageSize"] = 20;

$tdatager_moradores[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_moradores[".strOrderBy"] = $tstrOrderBy;

$tdatager_moradores[".orderindexes"] = array();

$tdatager_moradores[".sqlHead"] = "select idMorador,  nome,  link_ger_unidades,  relacaocomunid,  obs,  ultimousuario,  ultimaalteracao";
$tdatager_moradores[".sqlFrom"] = "FROM ger_moradores";
$tdatager_moradores[".sqlWhereExpr"] = "";
$tdatager_moradores[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_moradores[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_moradores[".arrGroupsPerPage"] = $arrGPP;

$tdatager_moradores[".highlightSearchResults"] = true;

$tableKeysger_moradores = array();
$tableKeysger_moradores[] = "idMorador";
$tdatager_moradores[".Keys"] = $tableKeysger_moradores;

$tdatager_moradores[".listFields"] = array();
$tdatager_moradores[".listFields"][] = "nome";
$tdatager_moradores[".listFields"][] = "link_ger_unidades";
$tdatager_moradores[".listFields"][] = "relacaocomunid";
$tdatager_moradores[".listFields"][] = "obs";

$tdatager_moradores[".hideMobileList"] = array();


$tdatager_moradores[".viewFields"] = array();
$tdatager_moradores[".viewFields"][] = "idMorador";
$tdatager_moradores[".viewFields"][] = "nome";
$tdatager_moradores[".viewFields"][] = "link_ger_unidades";
$tdatager_moradores[".viewFields"][] = "relacaocomunid";
$tdatager_moradores[".viewFields"][] = "obs";
$tdatager_moradores[".viewFields"][] = "ultimousuario";
$tdatager_moradores[".viewFields"][] = "ultimaalteracao";

$tdatager_moradores[".addFields"] = array();
$tdatager_moradores[".addFields"][] = "nome";
$tdatager_moradores[".addFields"][] = "link_ger_unidades";
$tdatager_moradores[".addFields"][] = "relacaocomunid";
$tdatager_moradores[".addFields"][] = "obs";

$tdatager_moradores[".masterListFields"] = array();

$tdatager_moradores[".inlineAddFields"] = array();

$tdatager_moradores[".editFields"] = array();
$tdatager_moradores[".editFields"][] = "idMorador";
$tdatager_moradores[".editFields"][] = "nome";
$tdatager_moradores[".editFields"][] = "link_ger_unidades";
$tdatager_moradores[".editFields"][] = "relacaocomunid";
$tdatager_moradores[".editFields"][] = "obs";

$tdatager_moradores[".inlineEditFields"] = array();

$tdatager_moradores[".exportFields"] = array();
$tdatager_moradores[".exportFields"][] = "nome";
$tdatager_moradores[".exportFields"][] = "link_ger_unidades";
$tdatager_moradores[".exportFields"][] = "relacaocomunid";
$tdatager_moradores[".exportFields"][] = "obs";
$tdatager_moradores[".exportFields"][] = "ultimousuario";
$tdatager_moradores[".exportFields"][] = "ultimaalteracao";

$tdatager_moradores[".importFields"] = array();
$tdatager_moradores[".importFields"][] = "idMorador";
$tdatager_moradores[".importFields"][] = "nome";
$tdatager_moradores[".importFields"][] = "link_ger_unidades";
$tdatager_moradores[".importFields"][] = "relacaocomunid";
$tdatager_moradores[".importFields"][] = "obs";
$tdatager_moradores[".importFields"][] = "ultimousuario";
$tdatager_moradores[".importFields"][] = "ultimaalteracao";

$tdatager_moradores[".printFields"] = array();
$tdatager_moradores[".printFields"][] = "nome";
$tdatager_moradores[".printFields"][] = "link_ger_unidades";
$tdatager_moradores[".printFields"][] = "relacaocomunid";
$tdatager_moradores[".printFields"][] = "obs";

//	idMorador
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idMorador";
	$fdata["GoodName"] = "idMorador";
	$fdata["ownerTable"] = "ger_moradores";
	$fdata["Label"] = GetFieldLabel("ger_moradores","idMorador");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idMorador";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idMorador";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_moradores["idMorador"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "ger_moradores";
	$fdata["Label"] = GetFieldLabel("ger_moradores","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_moradores["nome"] = $fdata;
//	link_ger_unidades
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "link_ger_unidades";
	$fdata["GoodName"] = "link_ger_unidades";
	$fdata["ownerTable"] = "ger_moradores";
	$fdata["Label"] = GetFieldLabel("ger_moradores","link_ger_unidades");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidades";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_unidades";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "grupounidade";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_moradores["link_ger_unidades"] = $fdata;
//	relacaocomunid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "relacaocomunid";
	$fdata["GoodName"] = "relacaocomunid";
	$fdata["ownerTable"] = "ger_moradores";
	$fdata["Label"] = GetFieldLabel("ger_moradores","relacaocomunid");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "relacaocomunid";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "relacaocomunid";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_lista_relacao_com_unidade";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
			$edata["LookupUnique"] = true;

	$edata["LinkField"] = "idRelacao";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "relacao";

	
	$edata["LookupOrderBy"] = "relacao";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_moradores["relacaocomunid"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_moradores";
	$fdata["Label"] = GetFieldLabel("ger_moradores","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_moradores["obs"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_moradores";
	$fdata["Label"] = GetFieldLabel("ger_moradores","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_moradores["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_moradores";
	$fdata["Label"] = GetFieldLabel("ger_moradores","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_moradores["ultimaalteracao"] = $fdata;


$tables_data["ger_moradores"]=&$tdatager_moradores;
$field_labels["ger_moradores"] = &$fieldLabelsger_moradores;
$fieldToolTips["ger_moradores"] = &$fieldToolTipsger_moradores;
$page_titles["ger_moradores"] = &$pageTitlesger_moradores;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_moradores"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_moradores"] = array();


	
				$strOriginalDetailsTable="ger_unidades";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_unidades";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_unidades";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["ger_moradores"][0] = $masterParams;
				$masterTablesData["ger_moradores"][0]["masterKeys"] = array();
	$masterTablesData["ger_moradores"][0]["masterKeys"][]="idUnidade";
				$masterTablesData["ger_moradores"][0]["detailKeys"] = array();
	$masterTablesData["ger_moradores"][0]["detailKeys"][]="link_ger_unidades";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_moradores()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idMorador,  nome,  link_ger_unidades,  relacaocomunid,  obs,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_moradores";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idMorador",
	"m_strTable" => "ger_moradores",
	"m_srcTableName" => "ger_moradores"
));

$proto6["m_sql"] = "idMorador";
$proto6["m_srcTableName"] = "ger_moradores";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_moradores",
	"m_srcTableName" => "ger_moradores"
));

$proto8["m_sql"] = "nome";
$proto8["m_srcTableName"] = "ger_moradores";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidades",
	"m_strTable" => "ger_moradores",
	"m_srcTableName" => "ger_moradores"
));

$proto10["m_sql"] = "link_ger_unidades";
$proto10["m_srcTableName"] = "ger_moradores";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "relacaocomunid",
	"m_strTable" => "ger_moradores",
	"m_srcTableName" => "ger_moradores"
));

$proto12["m_sql"] = "relacaocomunid";
$proto12["m_srcTableName"] = "ger_moradores";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_moradores",
	"m_srcTableName" => "ger_moradores"
));

$proto14["m_sql"] = "obs";
$proto14["m_srcTableName"] = "ger_moradores";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_moradores",
	"m_srcTableName" => "ger_moradores"
));

$proto16["m_sql"] = "ultimousuario";
$proto16["m_srcTableName"] = "ger_moradores";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_moradores",
	"m_srcTableName" => "ger_moradores"
));

$proto18["m_sql"] = "ultimaalteracao";
$proto18["m_srcTableName"] = "ger_moradores";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "ger_moradores";
$proto21["m_srcTableName"] = "ger_moradores";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "idMorador";
$proto21["m_columns"][] = "nome";
$proto21["m_columns"][] = "link_ger_unidades";
$proto21["m_columns"][] = "relacaocomunid";
$proto21["m_columns"][] = "obs";
$proto21["m_columns"][] = "ultimousuario";
$proto21["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "ger_moradores";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "ger_moradores";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_moradores";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_moradores = createSqlQuery_ger_moradores();


	
		;

							

$tdatager_moradores[".sqlquery"] = $queryData_ger_moradores;

include_once(getabspath("include/ger_moradores_events.php"));
$tableEvents["ger_moradores"] = new eventclass_ger_moradores;
$tdatager_moradores[".hasEvents"] = true;

?>