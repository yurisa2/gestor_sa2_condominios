<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_cobranca_atualizacao_aberto = array();
	$tdatafin_cobranca_atualizacao_aberto[".truncateText"] = true;
	$tdatafin_cobranca_atualizacao_aberto[".NumberOfChars"] = 80;
	$tdatafin_cobranca_atualizacao_aberto[".ShortName"] = "fin_cobranca_atualizacao_aberto";
	$tdatafin_cobranca_atualizacao_aberto[".OwnerID"] = "auth";
	$tdatafin_cobranca_atualizacao_aberto[".OriginalTable"] = "fin_cobranca_atualizacao_aberto";

//	field labels
$fieldLabelsfin_cobranca_atualizacao_aberto = array();
$fieldToolTipsfin_cobranca_atualizacao_aberto = array();
$pageTitlesfin_cobranca_atualizacao_aberto = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"] = array();
	$pageTitlesfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["idCobranca"] = "Código";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["idCobranca"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["vencimento"] = "Vcto";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["vencimento"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["numdoc"] = "N. Doc";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["numdoc"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["datadoc"] = "Data do Doc";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["datadoc"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["vlrcob"] = "Valor do documento";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["vlrcob"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["link_ger_unidade"] = "Cod Unidade";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["link_ger_unidade"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["correcao"] = "Correção total";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["correcao"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["grupo"] = "Grupo";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["grupo"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["unidade"] = "Unidade";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["unidade"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["obs"] = "obs";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["unidadeir"] = "Unidade";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["unidadeir"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["idUnidade"] = "Unidade";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["idUnidade"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["mesesemaberto"] = "Meses";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["mesesemaberto"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["juroscalculado"] = "Juros totais";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["juroscalculado"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["multacalculada"] = "Multa calculada";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["multacalculada"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["vlrtotal"] = "Valor total";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["vlrtotal"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["bolItau"] = "Itau";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["bolItau"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["bolReal"] = "Real";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["bolReal"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["bolBB"] = "BB";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["bolBB"] = "";
	$fieldLabelsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["bolSantander"] = "Boleto";
	$fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]["bolSantander"] = "";
	if (count($fieldToolTipsfin_cobranca_atualizacao_aberto["Portuguese(Brazil)"]))
		$tdatafin_cobranca_atualizacao_aberto[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_cobranca_atualizacao_aberto[""] = array();
	$fieldToolTipsfin_cobranca_atualizacao_aberto[""] = array();
	$pageTitlesfin_cobranca_atualizacao_aberto[""] = array();
	if (count($fieldToolTipsfin_cobranca_atualizacao_aberto[""]))
		$tdatafin_cobranca_atualizacao_aberto[".isUseToolTips"] = true;
}


	$tdatafin_cobranca_atualizacao_aberto[".NCSearch"] = true;



$tdatafin_cobranca_atualizacao_aberto[".shortTableName"] = "fin_cobranca_atualizacao_aberto";
$tdatafin_cobranca_atualizacao_aberto[".nSecOptions"] = 1;
$tdatafin_cobranca_atualizacao_aberto[".recsPerRowList"] = 1;
$tdatafin_cobranca_atualizacao_aberto[".recsPerRowPrint"] = 1;
$tdatafin_cobranca_atualizacao_aberto[".mainTableOwnerID"] = "auth";
$tdatafin_cobranca_atualizacao_aberto[".moveNext"] = 1;
$tdatafin_cobranca_atualizacao_aberto[".entityType"] = 0;

$tdatafin_cobranca_atualizacao_aberto[".strOriginalTableName"] = "fin_cobranca_atualizacao_aberto";





$tdatafin_cobranca_atualizacao_aberto[".showAddInPopup"] = false;

$tdatafin_cobranca_atualizacao_aberto[".showEditInPopup"] = false;

$tdatafin_cobranca_atualizacao_aberto[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_cobranca_atualizacao_aberto[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_cobranca_atualizacao_aberto[".fieldsForRegister"] = array();

$tdatafin_cobranca_atualizacao_aberto[".listAjax"] = false;

	$tdatafin_cobranca_atualizacao_aberto[".audit"] = true;

	$tdatafin_cobranca_atualizacao_aberto[".locking"] = true;



$tdatafin_cobranca_atualizacao_aberto[".list"] = true;


$tdatafin_cobranca_atualizacao_aberto[".import"] = true;

$tdatafin_cobranca_atualizacao_aberto[".exportTo"] = true;

$tdatafin_cobranca_atualizacao_aberto[".printFriendly"] = true;


$tdatafin_cobranca_atualizacao_aberto[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_cobranca_atualizacao_aberto[".searchSaving"] = false;
//

$tdatafin_cobranca_atualizacao_aberto[".showSearchPanel"] = true;
		$tdatafin_cobranca_atualizacao_aberto[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_cobranca_atualizacao_aberto[".isUseAjaxSuggest"] = false;
else
	$tdatafin_cobranca_atualizacao_aberto[".isUseAjaxSuggest"] = true;

$tdatafin_cobranca_atualizacao_aberto[".rowHighlite"] = true;



$tdatafin_cobranca_atualizacao_aberto[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_cobranca_atualizacao_aberto[".isUseTimeForSearch"] = false;



$tdatafin_cobranca_atualizacao_aberto[".badgeColor"] = "DB7093";


$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".filterFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".requiredSearchFields"] = array();

$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "numdoc";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "link_ger_unidade";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "obs";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "vencimento";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "vlrcob";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "multacalculada";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "mesesemaberto";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "juroscalculado";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "correcao";
	$tdatafin_cobranca_atualizacao_aberto[".allSearchFields"][] = "vlrtotal";
	

$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "vencimento";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "numdoc";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "vlrcob";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "link_ger_unidade";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "mesesemaberto";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "juroscalculado";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "correcao";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "multacalculada";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "vlrtotal";
$tdatafin_cobranca_atualizacao_aberto[".googleLikeFields"][] = "obs";


$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "numdoc";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "link_ger_unidade";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "obs";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "vencimento";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "vlrcob";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "multacalculada";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "mesesemaberto";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "juroscalculado";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "correcao";
$tdatafin_cobranca_atualizacao_aberto[".advSearchFields"][] = "vlrtotal";

$tdatafin_cobranca_atualizacao_aberto[".tableType"] = "list";

$tdatafin_cobranca_atualizacao_aberto[".printerPageOrientation"] = 0;
$tdatafin_cobranca_atualizacao_aberto[".nPrinterPageScale"] = 100;

$tdatafin_cobranca_atualizacao_aberto[".nPrinterSplitRecords"] = 40;

$tdatafin_cobranca_atualizacao_aberto[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_cobranca_atualizacao_aberto[".geocodingEnabled"] = false;





$tdatafin_cobranca_atualizacao_aberto[".listGridLayout"] = 3;

$tdatafin_cobranca_atualizacao_aberto[".isDisplayLoading"] = true;


$tdatafin_cobranca_atualizacao_aberto[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf

$tdatafin_cobranca_atualizacao_aberto[".totalsFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".totalsFields"][] = array(
	"fName" => "unidade",
	"numRows" => 0,
	"totalsType" => "COUNT",
	"viewFormat" => '');
$tdatafin_cobranca_atualizacao_aberto[".totalsFields"][] = array(
	"fName" => "vlrcob",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdatafin_cobranca_atualizacao_aberto[".totalsFields"][] = array(
	"fName" => "multacalculada",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdatafin_cobranca_atualizacao_aberto[".totalsFields"][] = array(
	"fName" => "juroscalculado",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdatafin_cobranca_atualizacao_aberto[".totalsFields"][] = array(
	"fName" => "correcao",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdatafin_cobranca_atualizacao_aberto[".totalsFields"][] = array(
	"fName" => "vlrtotal",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');

$tdatafin_cobranca_atualizacao_aberto[".pageSize"] = 20;

$tdatafin_cobranca_atualizacao_aberto[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY fin_cobranca_atualizacao_aberto.link_ger_unidade DESC, fin_cobranca_atualizacao_aberto.mesesemaberto DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_cobranca_atualizacao_aberto[".strOrderBy"] = $tstrOrderBy;

$tdatafin_cobranca_atualizacao_aberto[".orderindexes"] = array();
$tdatafin_cobranca_atualizacao_aberto[".orderindexes"][] = array(6, (0 ? "ASC" : "DESC"), "fin_cobranca_atualizacao_aberto.link_ger_unidade");
$tdatafin_cobranca_atualizacao_aberto[".orderindexes"][] = array(7, (0 ? "ASC" : "DESC"), "fin_cobranca_atualizacao_aberto.mesesemaberto");

$tdatafin_cobranca_atualizacao_aberto[".sqlHead"] = "SELECT fin_cobranca_atualizacao_aberto.idCobranca,  fin_cobranca_atualizacao_aberto.vencimento,  fin_cobranca_atualizacao_aberto.numdoc,  fin_cobranca_atualizacao_aberto.datadoc,  fin_cobranca_atualizacao_aberto.vlrcob,  fin_cobranca_atualizacao_aberto.link_ger_unidade,  fin_cobranca_atualizacao_aberto.mesesemaberto,  fin_cobranca_atualizacao_aberto.juroscalculado,  fin_cobranca_atualizacao_aberto.correcao,  fin_cobranca_atualizacao_aberto.multacalculada,  fin_cobranca_atualizacao_aberto.vlrtotal,  fin_cobranca_atualizacao_aberto.link_ger_unidade AS grupo,  fin_cobranca_atualizacao_aberto.link_ger_unidade AS unidade,  fin_cobranca_atualizacao_aberto.obs,  fin_cobranca_atualizacao_aberto.link_ger_unidade AS unidadeir,  ger_cadastro.nome,  ger_unidades.idUnidade,  fin_cobranca_atualizacao_aberto.idCobranca AS bolItau,  fin_cobranca_atualizacao_aberto.idCobranca AS bolReal,  fin_cobranca_atualizacao_aberto.idCobranca AS bolBB,  fin_cobranca_atualizacao_aberto.idCobranca AS bolSantander";
$tdatafin_cobranca_atualizacao_aberto[".sqlFrom"] = "FROM fin_cobranca_atualizacao_aberto  INNER JOIN ger_unidades ON fin_cobranca_atualizacao_aberto.link_ger_unidade = ger_unidades.idUnidade  INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$tdatafin_cobranca_atualizacao_aberto[".sqlWhereExpr"] = "";
$tdatafin_cobranca_atualizacao_aberto[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_cobranca_atualizacao_aberto[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_cobranca_atualizacao_aberto[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_cobranca_atualizacao_aberto[".highlightSearchResults"] = true;

$tableKeysfin_cobranca_atualizacao_aberto = array();
$tableKeysfin_cobranca_atualizacao_aberto[] = "idCobranca";
$tdatafin_cobranca_atualizacao_aberto[".Keys"] = $tableKeysfin_cobranca_atualizacao_aberto;

$tdatafin_cobranca_atualizacao_aberto[".listFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "bolSantander";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "numdoc";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "nome";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "unidadeir";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "unidade";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "vencimento";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "obs";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "vlrcob";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "multacalculada";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "mesesemaberto";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "juroscalculado";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "correcao";
$tdatafin_cobranca_atualizacao_aberto[".listFields"][] = "vlrtotal";

$tdatafin_cobranca_atualizacao_aberto[".hideMobileList"] = array();


$tdatafin_cobranca_atualizacao_aberto[".viewFields"] = array();

$tdatafin_cobranca_atualizacao_aberto[".addFields"] = array();

$tdatafin_cobranca_atualizacao_aberto[".masterListFields"] = array();

$tdatafin_cobranca_atualizacao_aberto[".inlineAddFields"] = array();

$tdatafin_cobranca_atualizacao_aberto[".editFields"] = array();

$tdatafin_cobranca_atualizacao_aberto[".inlineEditFields"] = array();

$tdatafin_cobranca_atualizacao_aberto[".exportFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "numdoc";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "unidade";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "link_ger_unidade";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "obs";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "vencimento";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "vlrcob";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "multacalculada";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "mesesemaberto";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "juroscalculado";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "correcao";
$tdatafin_cobranca_atualizacao_aberto[".exportFields"][] = "vlrtotal";

$tdatafin_cobranca_atualizacao_aberto[".importFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "idCobranca";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "vencimento";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "numdoc";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "datadoc";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "vlrcob";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "link_ger_unidade";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "mesesemaberto";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "juroscalculado";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "correcao";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "multacalculada";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "vlrtotal";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "grupo";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "unidade";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "obs";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "unidadeir";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "nome";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "idUnidade";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "bolItau";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "bolReal";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "bolBB";
$tdatafin_cobranca_atualizacao_aberto[".importFields"][] = "bolSantander";

$tdatafin_cobranca_atualizacao_aberto[".printFields"] = array();
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "numdoc";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "unidade";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "obs";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "vencimento";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "vlrcob";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "multacalculada";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "mesesemaberto";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "juroscalculado";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "correcao";
$tdatafin_cobranca_atualizacao_aberto[".printFields"][] = "vlrtotal";

//	idCobranca
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idCobranca";
	$fdata["GoodName"] = "idCobranca";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","idCobranca");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["idCobranca"] = $fdata;
//	vencimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "vencimento";
	$fdata["GoodName"] = "vencimento";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","vencimento");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vencimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.vencimento";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["vencimento"] = $fdata;
//	numdoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "numdoc";
	$fdata["GoodName"] = "numdoc";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","numdoc");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numdoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.numdoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["numdoc"] = $fdata;
//	datadoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "datadoc";
	$fdata["GoodName"] = "datadoc";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","datadoc");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "datadoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.datadoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["datadoc"] = $fdata;
//	vlrcob
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "vlrcob";
	$fdata["GoodName"] = "vlrcob";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","vlrcob");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vlrcob";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.vlrcob";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["vlrcob"] = $fdata;
//	link_ger_unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "link_ger_unidade";
	$fdata["GoodName"] = "link_ger_unidade";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","link_ger_unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["link_ger_unidade"] = $fdata;
//	mesesemaberto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "mesesemaberto";
	$fdata["GoodName"] = "mesesemaberto";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","mesesemaberto");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "mesesemaberto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.mesesemaberto";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["mesesemaberto"] = $fdata;
//	juroscalculado
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "juroscalculado";
	$fdata["GoodName"] = "juroscalculado";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","juroscalculado");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "juroscalculado";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.juroscalculado";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["juroscalculado"] = $fdata;
//	correcao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "correcao";
	$fdata["GoodName"] = "correcao";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","correcao");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "correcao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.correcao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["correcao"] = $fdata;
//	multacalculada
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "multacalculada";
	$fdata["GoodName"] = "multacalculada";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","multacalculada");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "multacalculada";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.multacalculada";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["multacalculada"] = $fdata;
//	vlrtotal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "vlrtotal";
	$fdata["GoodName"] = "vlrtotal";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","vlrtotal");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vlrtotal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.vlrtotal";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["vlrtotal"] = $fdata;
//	grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "grupo";
	$fdata["GoodName"] = "grupo";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","grupo");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_guni_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "grupounidade";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["grupo"] = $fdata;
//	unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "unidade";
	$fdata["GoodName"] = "unidade";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_guni_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["unidade"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca_atualizacao_aberto["obs"] = $fdata;
//	unidadeir
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "unidadeir";
	$fdata["GoodName"] = "unidadeir";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","unidadeir");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
	
	
		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["unidadeir"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
	
	
		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ger_cadastro.nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "ger_cadastro_list.php?a=advsearch&type=and&asearchfield[]=nome&asearchopt_nome=Contains&value_nome=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="ger_cadastro_list.php?a=advsearch&type=and&asearchfield[]=nome&asearchopt_nome=Contains&value_nome=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 2;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Link";
	$vdata["hlTitleField"] = "nome";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["nome"] = $fdata;
//	idUnidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "idUnidade";
	$fdata["GoodName"] = "idUnidade";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","idUnidade");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idUnidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ger_unidades.idUnidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["idUnidade"] = $fdata;
//	bolItau
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "bolItau";
	$fdata["GoodName"] = "bolItau";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","bolItau");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "boletophp/deb_boleto_itau.php?nnum1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="boletophp/deb_boleto_itau.php?nnum1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Itau";
	$vdata["hlTitleField"] = "Itau";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["bolItau"] = $fdata;
//	bolReal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "bolReal";
	$fdata["GoodName"] = "bolReal";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","bolReal");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "boletophp/deb_boleto_real.php?nnum1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="boletophp/deb_boleto_real.php?nnum1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Real";
	$vdata["hlTitleField"] = "Real";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["bolReal"] = $fdata;
//	bolBB
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "bolBB";
	$fdata["GoodName"] = "bolBB";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","bolBB");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "boletophp/deb_boleto_bb.php?nnum1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="boletophp/deb_boleto_bb.php?nnum1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "BB";
	$vdata["hlTitleField"] = "BB";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["bolBB"] = $fdata;
//	bolSantander
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "bolSantander";
	$fdata["GoodName"] = "bolSantander";
	$fdata["ownerTable"] = "fin_cobranca_atualizacao_aberto";
	$fdata["Label"] = GetFieldLabel("fin_cobranca_atualizacao_aberto","bolSantander");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca_atualizacao_aberto.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "boletophp/deb_boleto.php?nnum1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="boletophp/deb_boleto.php?nnum1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Boleto";
	$vdata["hlTitleField"] = "Boleto";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca_atualizacao_aberto["bolSantander"] = $fdata;


$tables_data["fin_cobranca_atualizacao_aberto"]=&$tdatafin_cobranca_atualizacao_aberto;
$field_labels["fin_cobranca_atualizacao_aberto"] = &$fieldLabelsfin_cobranca_atualizacao_aberto;
$fieldToolTips["fin_cobranca_atualizacao_aberto"] = &$fieldToolTipsfin_cobranca_atualizacao_aberto;
$page_titles["fin_cobranca_atualizacao_aberto"] = &$pageTitlesfin_cobranca_atualizacao_aberto;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_cobranca_atualizacao_aberto"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_cobranca_atualizacao_aberto"] = array();


	
				$strOriginalDetailsTable="ger_unidades";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_unidades";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_unidades";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "1";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["fin_cobranca_atualizacao_aberto"][0] = $masterParams;
				$masterTablesData["fin_cobranca_atualizacao_aberto"][0]["masterKeys"] = array();
	$masterTablesData["fin_cobranca_atualizacao_aberto"][0]["masterKeys"][]="idUnidade";
				$masterTablesData["fin_cobranca_atualizacao_aberto"][0]["detailKeys"] = array();
	$masterTablesData["fin_cobranca_atualizacao_aberto"][0]["detailKeys"][]="link_ger_unidade";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_cobranca_atualizacao_aberto()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "fin_cobranca_atualizacao_aberto.idCobranca,  fin_cobranca_atualizacao_aberto.vencimento,  fin_cobranca_atualizacao_aberto.numdoc,  fin_cobranca_atualizacao_aberto.datadoc,  fin_cobranca_atualizacao_aberto.vlrcob,  fin_cobranca_atualizacao_aberto.link_ger_unidade,  fin_cobranca_atualizacao_aberto.mesesemaberto,  fin_cobranca_atualizacao_aberto.juroscalculado,  fin_cobranca_atualizacao_aberto.correcao,  fin_cobranca_atualizacao_aberto.multacalculada,  fin_cobranca_atualizacao_aberto.vlrtotal,  fin_cobranca_atualizacao_aberto.link_ger_unidade AS grupo,  fin_cobranca_atualizacao_aberto.link_ger_unidade AS unidade,  fin_cobranca_atualizacao_aberto.obs,  fin_cobranca_atualizacao_aberto.link_ger_unidade AS unidadeir,  ger_cadastro.nome,  ger_unidades.idUnidade,  fin_cobranca_atualizacao_aberto.idCobranca AS bolItau,  fin_cobranca_atualizacao_aberto.idCobranca AS bolReal,  fin_cobranca_atualizacao_aberto.idCobranca AS bolBB,  fin_cobranca_atualizacao_aberto.idCobranca AS bolSantander";
$proto0["m_strFrom"] = "FROM fin_cobranca_atualizacao_aberto  INNER JOIN ger_unidades ON fin_cobranca_atualizacao_aberto.link_ger_unidade = ger_unidades.idUnidade  INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY fin_cobranca_atualizacao_aberto.link_ger_unidade DESC, fin_cobranca_atualizacao_aberto.mesesemaberto DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto6["m_sql"] = "fin_cobranca_atualizacao_aberto.idCobranca";
$proto6["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "vencimento",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto8["m_sql"] = "fin_cobranca_atualizacao_aberto.vencimento";
$proto8["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "numdoc",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto10["m_sql"] = "fin_cobranca_atualizacao_aberto.numdoc";
$proto10["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "datadoc",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto12["m_sql"] = "fin_cobranca_atualizacao_aberto.datadoc";
$proto12["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "vlrcob",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto14["m_sql"] = "fin_cobranca_atualizacao_aberto.vlrcob";
$proto14["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto16["m_sql"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade";
$proto16["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "mesesemaberto",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto18["m_sql"] = "fin_cobranca_atualizacao_aberto.mesesemaberto";
$proto18["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "juroscalculado",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto20["m_sql"] = "fin_cobranca_atualizacao_aberto.juroscalculado";
$proto20["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "correcao",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto22["m_sql"] = "fin_cobranca_atualizacao_aberto.correcao";
$proto22["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "multacalculada",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto24["m_sql"] = "fin_cobranca_atualizacao_aberto.multacalculada";
$proto24["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "vlrtotal",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto26["m_sql"] = "fin_cobranca_atualizacao_aberto.vlrtotal";
$proto26["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto28["m_sql"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade";
$proto28["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "grupo";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto30["m_sql"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade";
$proto30["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "unidade";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto32["m_sql"] = "fin_cobranca_atualizacao_aberto.obs";
$proto32["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto34["m_sql"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade";
$proto34["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "unidadeir";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto36["m_sql"] = "ger_cadastro.nome";
$proto36["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "idUnidade",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto38["m_sql"] = "ger_unidades.idUnidade";
$proto38["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto40["m_sql"] = "fin_cobranca_atualizacao_aberto.idCobranca";
$proto40["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "bolItau";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto42["m_sql"] = "fin_cobranca_atualizacao_aberto.idCobranca";
$proto42["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "bolReal";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto44["m_sql"] = "fin_cobranca_atualizacao_aberto.idCobranca";
$proto44["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "bolBB";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto46["m_sql"] = "fin_cobranca_atualizacao_aberto.idCobranca";
$proto46["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "bolSantander";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto48=array();
$proto48["m_link"] = "SQLL_MAIN";
			$proto49=array();
$proto49["m_strName"] = "fin_cobranca_atualizacao_aberto";
$proto49["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto49["m_columns"] = array();
$proto49["m_columns"][] = "idCobranca";
$proto49["m_columns"][] = "auth";
$proto49["m_columns"][] = "vencimento";
$proto49["m_columns"][] = "numdoc";
$proto49["m_columns"][] = "datadoc";
$proto49["m_columns"][] = "vlrcob";
$proto49["m_columns"][] = "link_ger_unidade";
$proto49["m_columns"][] = "multa";
$proto49["m_columns"][] = "jurosam";
$proto49["m_columns"][] = "obs";
$proto49["m_columns"][] = "conta";
$proto49["m_columns"][] = "multacalculada";
$proto49["m_columns"][] = "mesesemaberto";
$proto49["m_columns"][] = "juroscalculado";
$proto49["m_columns"][] = "correcao";
$proto49["m_columns"][] = "vlrtotal";
$obj = new SQLTable($proto49);

$proto48["m_table"] = $obj;
$proto48["m_sql"] = "fin_cobranca_atualizacao_aberto";
$proto48["m_alias"] = "";
$proto48["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto50=array();
$proto50["m_sql"] = "";
$proto50["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto50["m_column"]=$obj;
$proto50["m_contained"] = array();
$proto50["m_strCase"] = "";
$proto50["m_havingmode"] = false;
$proto50["m_inBrackets"] = false;
$proto50["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto50);

$proto48["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto48);

$proto0["m_fromlist"][]=$obj;
												$proto52=array();
$proto52["m_link"] = "SQLL_INNERJOIN";
			$proto53=array();
$proto53["m_strName"] = "ger_unidades";
$proto53["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto53["m_columns"] = array();
$proto53["m_columns"][] = "idUnidade";
$proto53["m_columns"][] = "grupo";
$proto53["m_columns"][] = "unidade";
$proto53["m_columns"][] = "link_ger_cadastro";
$proto53["m_columns"][] = "dia_pref_venc";
$proto53["m_columns"][] = "ativo";
$proto53["m_columns"][] = "obs";
$proto53["m_columns"][] = "frideal";
$proto53["m_columns"][] = "ultimousuario";
$proto53["m_columns"][] = "ultimaalteracao";
$proto53["m_columns"][] = "auth";
$obj = new SQLTable($proto53);

$proto52["m_table"] = $obj;
$proto52["m_sql"] = "INNER JOIN ger_unidades ON fin_cobranca_atualizacao_aberto.link_ger_unidade = ger_unidades.idUnidade";
$proto52["m_alias"] = "";
$proto52["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto54=array();
$proto54["m_sql"] = "fin_cobranca_atualizacao_aberto.link_ger_unidade = ger_unidades.idUnidade";
$proto54["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto54["m_column"]=$obj;
$proto54["m_contained"] = array();
$proto54["m_strCase"] = "= ger_unidades.idUnidade";
$proto54["m_havingmode"] = false;
$proto54["m_inBrackets"] = false;
$proto54["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto54);

$proto52["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto52);

$proto0["m_fromlist"][]=$obj;
												$proto56=array();
$proto56["m_link"] = "SQLL_INNERJOIN";
			$proto57=array();
$proto57["m_strName"] = "ger_cadastro";
$proto57["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto57["m_columns"] = array();
$proto57["m_columns"][] = "idCadastro";
$proto57["m_columns"][] = "nome";
$proto57["m_columns"][] = "rg";
$proto57["m_columns"][] = "cpf";
$proto57["m_columns"][] = "TelPrim";
$proto57["m_columns"][] = "CompPrim";
$proto57["m_columns"][] = "TelSec";
$proto57["m_columns"][] = "CompSec";
$proto57["m_columns"][] = "TelTerc";
$proto57["m_columns"][] = "CompTerc";
$proto57["m_columns"][] = "log";
$proto57["m_columns"][] = "end";
$proto57["m_columns"][] = "num";
$proto57["m_columns"][] = "comp";
$proto57["m_columns"][] = "bairro";
$proto57["m_columns"][] = "cep";
$proto57["m_columns"][] = "cidade";
$proto57["m_columns"][] = "uf";
$proto57["m_columns"][] = "obs";
$proto57["m_columns"][] = "situacao";
$proto57["m_columns"][] = "morador";
$proto57["m_columns"][] = "cepnet";
$proto57["m_columns"][] = "ativo";
$proto57["m_columns"][] = "processado";
$proto57["m_columns"][] = "email";
$proto57["m_columns"][] = "webuser";
$proto57["m_columns"][] = "ultimousuario";
$proto57["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto57);

$proto56["m_table"] = $obj;
$proto56["m_sql"] = "INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto56["m_alias"] = "";
$proto56["m_srcTableName"] = "fin_cobranca_atualizacao_aberto";
$proto58=array();
$proto58["m_sql"] = "ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto58["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "link_ger_cadastro",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto58["m_column"]=$obj;
$proto58["m_contained"] = array();
$proto58["m_strCase"] = "= ger_cadastro.idCadastro";
$proto58["m_havingmode"] = false;
$proto58["m_inBrackets"] = false;
$proto58["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto58);

$proto56["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto56);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto60=array();
						$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto60["m_column"]=$obj;
$proto60["m_bAsc"] = 0;
$proto60["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto60);

$proto0["m_orderby"][]=$obj;					
												$proto62=array();
						$obj = new SQLField(array(
	"m_strName" => "mesesemaberto",
	"m_strTable" => "fin_cobranca_atualizacao_aberto",
	"m_srcTableName" => "fin_cobranca_atualizacao_aberto"
));

$proto62["m_column"]=$obj;
$proto62["m_bAsc"] = 0;
$proto62["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto62);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="fin_cobranca_atualizacao_aberto";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_cobranca_atualizacao_aberto = createSqlQuery_fin_cobranca_atualizacao_aberto();


	
		;

																					

$tdatafin_cobranca_atualizacao_aberto[".sqlquery"] = $queryData_fin_cobranca_atualizacao_aberto;

include_once(getabspath("include/fin_cobranca_atualizacao_aberto_events.php"));
$tableEvents["fin_cobranca_atualizacao_aberto"] = new eventclass_fin_cobranca_atualizacao_aberto;
$tdatafin_cobranca_atualizacao_aberto[".hasEvents"] = true;

?>