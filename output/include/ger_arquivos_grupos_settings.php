<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_arquivos_grupos = array();
	$tdatager_arquivos_grupos[".truncateText"] = true;
	$tdatager_arquivos_grupos[".NumberOfChars"] = 80;
	$tdatager_arquivos_grupos[".ShortName"] = "ger_arquivos_grupos";
	$tdatager_arquivos_grupos[".OwnerID"] = "link_ger_grupos";
	$tdatager_arquivos_grupos[".OriginalTable"] = "ger_arquivos_grupos";

//	field labels
$fieldLabelsger_arquivos_grupos = array();
$fieldToolTipsger_arquivos_grupos = array();
$pageTitlesger_arquivos_grupos = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"] = array();
	$pageTitlesger_arquivos_grupos["Portuguese(Brazil)"] = array();
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"]["idArquivosGrupos"] = "Id Arquivos Grupos";
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]["idArquivosGrupos"] = "";
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"]["nomedoc"] = "Nome do documento";
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]["nomedoc"] = "";
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"]["arquivo"] = "Arquivo";
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]["arquivo"] = "";
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"]["link_ger_grupos"] = "Grupo";
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]["link_ger_grupos"] = "";
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"]["link_ger_tipos_arquivo"] = "Tipo de arquivo";
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]["link_ger_tipos_arquivo"] = "";
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"]["ultimousuario"] = "Ultimo usuário";
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_arquivos_grupos["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima alteração";
	$fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_arquivos_grupos["Portuguese(Brazil)"]))
		$tdatager_arquivos_grupos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_arquivos_grupos[""] = array();
	$fieldToolTipsger_arquivos_grupos[""] = array();
	$pageTitlesger_arquivos_grupos[""] = array();
	if (count($fieldToolTipsger_arquivos_grupos[""]))
		$tdatager_arquivos_grupos[".isUseToolTips"] = true;
}


	$tdatager_arquivos_grupos[".NCSearch"] = true;



$tdatager_arquivos_grupos[".shortTableName"] = "ger_arquivos_grupos";
$tdatager_arquivos_grupos[".nSecOptions"] = 1;
$tdatager_arquivos_grupos[".recsPerRowList"] = 1;
$tdatager_arquivos_grupos[".recsPerRowPrint"] = 1;
$tdatager_arquivos_grupos[".mainTableOwnerID"] = "link_ger_grupos";
$tdatager_arquivos_grupos[".moveNext"] = 1;
$tdatager_arquivos_grupos[".entityType"] = 0;

$tdatager_arquivos_grupos[".strOriginalTableName"] = "ger_arquivos_grupos";





$tdatager_arquivos_grupos[".showAddInPopup"] = false;

$tdatager_arquivos_grupos[".showEditInPopup"] = false;

$tdatager_arquivos_grupos[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_arquivos_grupos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_arquivos_grupos[".fieldsForRegister"] = array();

$tdatager_arquivos_grupos[".listAjax"] = false;

	$tdatager_arquivos_grupos[".audit"] = true;

	$tdatager_arquivos_grupos[".locking"] = true;

$tdatager_arquivos_grupos[".edit"] = true;
$tdatager_arquivos_grupos[".afterEditAction"] = 1;
$tdatager_arquivos_grupos[".closePopupAfterEdit"] = 1;
$tdatager_arquivos_grupos[".afterEditActionDetTable"] = "";

$tdatager_arquivos_grupos[".add"] = true;
$tdatager_arquivos_grupos[".afterAddAction"] = 1;
$tdatager_arquivos_grupos[".closePopupAfterAdd"] = 1;
$tdatager_arquivos_grupos[".afterAddActionDetTable"] = "";

$tdatager_arquivos_grupos[".list"] = true;

$tdatager_arquivos_grupos[".view"] = true;

$tdatager_arquivos_grupos[".import"] = true;

$tdatager_arquivos_grupos[".exportTo"] = true;

$tdatager_arquivos_grupos[".printFriendly"] = true;

$tdatager_arquivos_grupos[".delete"] = true;

$tdatager_arquivos_grupos[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_arquivos_grupos[".searchSaving"] = false;
//

$tdatager_arquivos_grupos[".showSearchPanel"] = true;
		$tdatager_arquivos_grupos[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_arquivos_grupos[".isUseAjaxSuggest"] = false;
else
	$tdatager_arquivos_grupos[".isUseAjaxSuggest"] = true;

$tdatager_arquivos_grupos[".rowHighlite"] = true;



$tdatager_arquivos_grupos[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_arquivos_grupos[".isUseTimeForSearch"] = false;



$tdatager_arquivos_grupos[".badgeColor"] = "E07878";


$tdatager_arquivos_grupos[".allSearchFields"] = array();
$tdatager_arquivos_grupos[".filterFields"] = array();
$tdatager_arquivos_grupos[".requiredSearchFields"] = array();

$tdatager_arquivos_grupos[".allSearchFields"][] = "nomedoc";
	$tdatager_arquivos_grupos[".allSearchFields"][] = "obs";
	$tdatager_arquivos_grupos[".allSearchFields"][] = "arquivo";
	$tdatager_arquivos_grupos[".allSearchFields"][] = "link_ger_grupos";
	$tdatager_arquivos_grupos[".allSearchFields"][] = "link_ger_tipos_arquivo";
	$tdatager_arquivos_grupos[".allSearchFields"][] = "ultimousuario";
	$tdatager_arquivos_grupos[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_arquivos_grupos[".googleLikeFields"] = array();
$tdatager_arquivos_grupos[".googleLikeFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".googleLikeFields"][] = "obs";
$tdatager_arquivos_grupos[".googleLikeFields"][] = "arquivo";
$tdatager_arquivos_grupos[".googleLikeFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".googleLikeFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_grupos[".googleLikeFields"][] = "ultimousuario";
$tdatager_arquivos_grupos[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_arquivos_grupos[".advSearchFields"] = array();
$tdatager_arquivos_grupos[".advSearchFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".advSearchFields"][] = "obs";
$tdatager_arquivos_grupos[".advSearchFields"][] = "arquivo";
$tdatager_arquivos_grupos[".advSearchFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".advSearchFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_grupos[".advSearchFields"][] = "ultimousuario";
$tdatager_arquivos_grupos[".advSearchFields"][] = "ultimaalteracao";

$tdatager_arquivos_grupos[".tableType"] = "list";

$tdatager_arquivos_grupos[".printerPageOrientation"] = 0;
$tdatager_arquivos_grupos[".nPrinterPageScale"] = 100;

$tdatager_arquivos_grupos[".nPrinterSplitRecords"] = 40;

$tdatager_arquivos_grupos[".nPrinterPDFSplitRecords"] = 40;



$tdatager_arquivos_grupos[".geocodingEnabled"] = false;





$tdatager_arquivos_grupos[".listGridLayout"] = 3;

$tdatager_arquivos_grupos[".isDisplayLoading"] = true;


$tdatager_arquivos_grupos[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_arquivos_grupos[".pageSize"] = 20;

$tdatager_arquivos_grupos[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_arquivos_grupos[".strOrderBy"] = $tstrOrderBy;

$tdatager_arquivos_grupos[".orderindexes"] = array();

$tdatager_arquivos_grupos[".sqlHead"] = "SELECT idArquivosGrupos,  	nomedoc,  	obs,  	arquivo,  	link_ger_grupos,  	link_ger_tipos_arquivo,  	ultimousuario,  	ultimaalteracao";
$tdatager_arquivos_grupos[".sqlFrom"] = "FROM ger_arquivos_grupos";
$tdatager_arquivos_grupos[".sqlWhereExpr"] = "";
$tdatager_arquivos_grupos[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_arquivos_grupos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_arquivos_grupos[".arrGroupsPerPage"] = $arrGPP;

$tdatager_arquivos_grupos[".highlightSearchResults"] = true;

$tableKeysger_arquivos_grupos = array();
$tableKeysger_arquivos_grupos[] = "idArquivosGrupos";
$tdatager_arquivos_grupos[".Keys"] = $tableKeysger_arquivos_grupos;

$tdatager_arquivos_grupos[".listFields"] = array();
$tdatager_arquivos_grupos[".listFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".listFields"][] = "obs";
$tdatager_arquivos_grupos[".listFields"][] = "arquivo";
$tdatager_arquivos_grupos[".listFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".listFields"][] = "link_ger_tipos_arquivo";

$tdatager_arquivos_grupos[".hideMobileList"] = array();


$tdatager_arquivos_grupos[".viewFields"] = array();
$tdatager_arquivos_grupos[".viewFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".viewFields"][] = "obs";
$tdatager_arquivos_grupos[".viewFields"][] = "arquivo";
$tdatager_arquivos_grupos[".viewFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".viewFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_grupos[".viewFields"][] = "ultimousuario";
$tdatager_arquivos_grupos[".viewFields"][] = "ultimaalteracao";

$tdatager_arquivos_grupos[".addFields"] = array();
$tdatager_arquivos_grupos[".addFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".addFields"][] = "obs";
$tdatager_arquivos_grupos[".addFields"][] = "arquivo";
$tdatager_arquivos_grupos[".addFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".addFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_grupos[".addFields"][] = "ultimousuario";
$tdatager_arquivos_grupos[".addFields"][] = "ultimaalteracao";

$tdatager_arquivos_grupos[".masterListFields"] = array();

$tdatager_arquivos_grupos[".inlineAddFields"] = array();

$tdatager_arquivos_grupos[".editFields"] = array();
$tdatager_arquivos_grupos[".editFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".editFields"][] = "obs";
$tdatager_arquivos_grupos[".editFields"][] = "arquivo";
$tdatager_arquivos_grupos[".editFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".editFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_grupos[".editFields"][] = "ultimousuario";
$tdatager_arquivos_grupos[".editFields"][] = "ultimaalteracao";

$tdatager_arquivos_grupos[".inlineEditFields"] = array();

$tdatager_arquivos_grupos[".exportFields"] = array();
$tdatager_arquivos_grupos[".exportFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".exportFields"][] = "obs";
$tdatager_arquivos_grupos[".exportFields"][] = "arquivo";
$tdatager_arquivos_grupos[".exportFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".exportFields"][] = "link_ger_tipos_arquivo";

$tdatager_arquivos_grupos[".importFields"] = array();
$tdatager_arquivos_grupos[".importFields"][] = "idArquivosGrupos";
$tdatager_arquivos_grupos[".importFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".importFields"][] = "obs";
$tdatager_arquivos_grupos[".importFields"][] = "arquivo";
$tdatager_arquivos_grupos[".importFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".importFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_grupos[".importFields"][] = "ultimousuario";
$tdatager_arquivos_grupos[".importFields"][] = "ultimaalteracao";

$tdatager_arquivos_grupos[".printFields"] = array();
$tdatager_arquivos_grupos[".printFields"][] = "nomedoc";
$tdatager_arquivos_grupos[".printFields"][] = "obs";
$tdatager_arquivos_grupos[".printFields"][] = "arquivo";
$tdatager_arquivos_grupos[".printFields"][] = "link_ger_grupos";
$tdatager_arquivos_grupos[".printFields"][] = "link_ger_tipos_arquivo";

//	idArquivosGrupos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idArquivosGrupos";
	$fdata["GoodName"] = "idArquivosGrupos";
	$fdata["ownerTable"] = "ger_arquivos_grupos";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_grupos","idArquivosGrupos");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idArquivosGrupos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idArquivosGrupos";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_arquivos_grupos["idArquivosGrupos"] = $fdata;
//	nomedoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nomedoc";
	$fdata["GoodName"] = "nomedoc";
	$fdata["ownerTable"] = "ger_arquivos_grupos";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_grupos","nomedoc");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nomedoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nomedoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=45";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_grupos["nomedoc"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_arquivos_grupos";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_grupos","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_grupos["obs"] = $fdata;
//	arquivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "arquivo";
	$fdata["GoodName"] = "arquivo";
	$fdata["ownerTable"] = "ger_arquivos_grupos";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_grupos","arquivo");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "arquivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "arquivo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
	



		$edata["IsRequired"] = true;

	
		$edata["UseTimestamp"] = true;

	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_grupos["arquivo"] = $fdata;
//	link_ger_grupos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "link_ger_grupos";
	$fdata["GoodName"] = "link_ger_grupos";
	$fdata["ownerTable"] = "ger_arquivos_grupos";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_grupos","link_ger_grupos");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_grupos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_grupos";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "nome";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_grupos["link_ger_grupos"] = $fdata;
//	link_ger_tipos_arquivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "link_ger_tipos_arquivo";
	$fdata["GoodName"] = "link_ger_tipos_arquivo";
	$fdata["ownerTable"] = "ger_arquivos_grupos";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_grupos","link_ger_tipos_arquivo");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_tipos_arquivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_tipos_arquivo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_tipodocs";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idTipodocs";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "tipodoc";

	
	$edata["LookupOrderBy"] = "tipodoc";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_grupos["link_ger_tipos_arquivo"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_arquivos_grupos";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_grupos","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_grupos["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_arquivos_grupos";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_grupos","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_grupos["ultimaalteracao"] = $fdata;


$tables_data["ger_arquivos_grupos"]=&$tdatager_arquivos_grupos;
$field_labels["ger_arquivos_grupos"] = &$fieldLabelsger_arquivos_grupos;
$fieldToolTips["ger_arquivos_grupos"] = &$fieldToolTipsger_arquivos_grupos;
$page_titles["ger_arquivos_grupos"] = &$pageTitlesger_arquivos_grupos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_arquivos_grupos"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_arquivos_grupos"] = array();


	
				$strOriginalDetailsTable="ger_grupo";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_grupo";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_grupo";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["ger_arquivos_grupos"][0] = $masterParams;
				$masterTablesData["ger_arquivos_grupos"][0]["masterKeys"] = array();
	$masterTablesData["ger_arquivos_grupos"][0]["masterKeys"][]="idGrupo";
				$masterTablesData["ger_arquivos_grupos"][0]["detailKeys"] = array();
	$masterTablesData["ger_arquivos_grupos"][0]["detailKeys"][]="link_ger_grupos";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_arquivos_grupos()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idArquivosGrupos,  	nomedoc,  	obs,  	arquivo,  	link_ger_grupos,  	link_ger_tipos_arquivo,  	ultimousuario,  	ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_arquivos_grupos";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idArquivosGrupos",
	"m_strTable" => "ger_arquivos_grupos",
	"m_srcTableName" => "ger_arquivos_grupos"
));

$proto6["m_sql"] = "idArquivosGrupos";
$proto6["m_srcTableName"] = "ger_arquivos_grupos";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nomedoc",
	"m_strTable" => "ger_arquivos_grupos",
	"m_srcTableName" => "ger_arquivos_grupos"
));

$proto8["m_sql"] = "nomedoc";
$proto8["m_srcTableName"] = "ger_arquivos_grupos";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_arquivos_grupos",
	"m_srcTableName" => "ger_arquivos_grupos"
));

$proto10["m_sql"] = "obs";
$proto10["m_srcTableName"] = "ger_arquivos_grupos";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "arquivo",
	"m_strTable" => "ger_arquivos_grupos",
	"m_srcTableName" => "ger_arquivos_grupos"
));

$proto12["m_sql"] = "arquivo";
$proto12["m_srcTableName"] = "ger_arquivos_grupos";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_grupos",
	"m_strTable" => "ger_arquivos_grupos",
	"m_srcTableName" => "ger_arquivos_grupos"
));

$proto14["m_sql"] = "link_ger_grupos";
$proto14["m_srcTableName"] = "ger_arquivos_grupos";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_tipos_arquivo",
	"m_strTable" => "ger_arquivos_grupos",
	"m_srcTableName" => "ger_arquivos_grupos"
));

$proto16["m_sql"] = "link_ger_tipos_arquivo";
$proto16["m_srcTableName"] = "ger_arquivos_grupos";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_arquivos_grupos",
	"m_srcTableName" => "ger_arquivos_grupos"
));

$proto18["m_sql"] = "ultimousuario";
$proto18["m_srcTableName"] = "ger_arquivos_grupos";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_arquivos_grupos",
	"m_srcTableName" => "ger_arquivos_grupos"
));

$proto20["m_sql"] = "ultimaalteracao";
$proto20["m_srcTableName"] = "ger_arquivos_grupos";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "ger_arquivos_grupos";
$proto23["m_srcTableName"] = "ger_arquivos_grupos";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "idArquivosGrupos";
$proto23["m_columns"][] = "nomedoc";
$proto23["m_columns"][] = "obs";
$proto23["m_columns"][] = "arquivo";
$proto23["m_columns"][] = "link_ger_grupos";
$proto23["m_columns"][] = "link_ger_tipos_arquivo";
$proto23["m_columns"][] = "ultimousuario";
$proto23["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "ger_arquivos_grupos";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "ger_arquivos_grupos";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_arquivos_grupos";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_arquivos_grupos = createSqlQuery_ger_arquivos_grupos();


	
		;

								

$tdatager_arquivos_grupos[".sqlquery"] = $queryData_ger_arquivos_grupos;

$tableEvents["ger_arquivos_grupos"] = new eventsBase;
$tdatager_arquivos_grupos[".hasEvents"] = false;

?>