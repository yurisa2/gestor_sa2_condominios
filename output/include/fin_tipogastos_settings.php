<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_tipogastos = array();
	$tdatafin_tipogastos[".truncateText"] = true;
	$tdatafin_tipogastos[".NumberOfChars"] = 80;
	$tdatafin_tipogastos[".ShortName"] = "fin_tipogastos";
	$tdatafin_tipogastos[".OwnerID"] = "";
	$tdatafin_tipogastos[".OriginalTable"] = "fin_tipogastos";

//	field labels
$fieldLabelsfin_tipogastos = array();
$fieldToolTipsfin_tipogastos = array();
$pageTitlesfin_tipogastos = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_tipogastos["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_tipogastos["Portuguese(Brazil)"] = array();
	$pageTitlesfin_tipogastos["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_tipogastos["Portuguese(Brazil)"]["idTipogasto"] = "Código";
	$fieldToolTipsfin_tipogastos["Portuguese(Brazil)"]["idTipogasto"] = "";
	$fieldLabelsfin_tipogastos["Portuguese(Brazil)"]["tipodegasto"] = "tipodegasto";
	$fieldToolTipsfin_tipogastos["Portuguese(Brazil)"]["tipodegasto"] = "";
	$fieldLabelsfin_tipogastos["Portuguese(Brazil)"]["obs"] = "obs";
	$fieldToolTipsfin_tipogastos["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsfin_tipogastos["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsfin_tipogastos["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_tipogastos["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsfin_tipogastos["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsfin_tipogastos["Portuguese(Brazil)"]))
		$tdatafin_tipogastos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_tipogastos[""] = array();
	$fieldToolTipsfin_tipogastos[""] = array();
	$pageTitlesfin_tipogastos[""] = array();
	if (count($fieldToolTipsfin_tipogastos[""]))
		$tdatafin_tipogastos[".isUseToolTips"] = true;
}


	$tdatafin_tipogastos[".NCSearch"] = true;



$tdatafin_tipogastos[".shortTableName"] = "fin_tipogastos";
$tdatafin_tipogastos[".nSecOptions"] = 0;
$tdatafin_tipogastos[".recsPerRowList"] = 1;
$tdatafin_tipogastos[".recsPerRowPrint"] = 1;
$tdatafin_tipogastos[".mainTableOwnerID"] = "";
$tdatafin_tipogastos[".moveNext"] = 1;
$tdatafin_tipogastos[".entityType"] = 0;

$tdatafin_tipogastos[".strOriginalTableName"] = "fin_tipogastos";





$tdatafin_tipogastos[".showAddInPopup"] = false;

$tdatafin_tipogastos[".showEditInPopup"] = false;

$tdatafin_tipogastos[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_tipogastos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_tipogastos[".fieldsForRegister"] = array();

$tdatafin_tipogastos[".listAjax"] = false;

	$tdatafin_tipogastos[".audit"] = true;

	$tdatafin_tipogastos[".locking"] = true;

$tdatafin_tipogastos[".edit"] = true;
$tdatafin_tipogastos[".afterEditAction"] = 1;
$tdatafin_tipogastos[".closePopupAfterEdit"] = 1;
$tdatafin_tipogastos[".afterEditActionDetTable"] = "";

$tdatafin_tipogastos[".add"] = true;
$tdatafin_tipogastos[".afterAddAction"] = 1;
$tdatafin_tipogastos[".closePopupAfterAdd"] = 1;
$tdatafin_tipogastos[".afterAddActionDetTable"] = "";

$tdatafin_tipogastos[".list"] = true;

$tdatafin_tipogastos[".view"] = true;


$tdatafin_tipogastos[".exportTo"] = true;

$tdatafin_tipogastos[".printFriendly"] = true;


$tdatafin_tipogastos[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_tipogastos[".searchSaving"] = false;
//

$tdatafin_tipogastos[".showSearchPanel"] = true;
		$tdatafin_tipogastos[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_tipogastos[".isUseAjaxSuggest"] = false;
else
	$tdatafin_tipogastos[".isUseAjaxSuggest"] = true;

$tdatafin_tipogastos[".rowHighlite"] = true;



$tdatafin_tipogastos[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_tipogastos[".isUseTimeForSearch"] = false;





$tdatafin_tipogastos[".allSearchFields"] = array();
$tdatafin_tipogastos[".filterFields"] = array();
$tdatafin_tipogastos[".requiredSearchFields"] = array();

$tdatafin_tipogastos[".allSearchFields"][] = "ultimousuario";
	$tdatafin_tipogastos[".allSearchFields"][] = "ultimaalteracao";
	$tdatafin_tipogastos[".allSearchFields"][] = "tipodegasto";
	$tdatafin_tipogastos[".allSearchFields"][] = "obs";
	

$tdatafin_tipogastos[".googleLikeFields"] = array();
$tdatafin_tipogastos[".googleLikeFields"][] = "tipodegasto";
$tdatafin_tipogastos[".googleLikeFields"][] = "obs";
$tdatafin_tipogastos[".googleLikeFields"][] = "ultimousuario";
$tdatafin_tipogastos[".googleLikeFields"][] = "ultimaalteracao";


$tdatafin_tipogastos[".advSearchFields"] = array();
$tdatafin_tipogastos[".advSearchFields"][] = "ultimousuario";
$tdatafin_tipogastos[".advSearchFields"][] = "ultimaalteracao";
$tdatafin_tipogastos[".advSearchFields"][] = "tipodegasto";
$tdatafin_tipogastos[".advSearchFields"][] = "obs";

$tdatafin_tipogastos[".tableType"] = "list";

$tdatafin_tipogastos[".printerPageOrientation"] = 0;
$tdatafin_tipogastos[".nPrinterPageScale"] = 100;

$tdatafin_tipogastos[".nPrinterSplitRecords"] = 40;

$tdatafin_tipogastos[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_tipogastos[".geocodingEnabled"] = false;





$tdatafin_tipogastos[".listGridLayout"] = 3;

$tdatafin_tipogastos[".isDisplayLoading"] = true;


$tdatafin_tipogastos[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatafin_tipogastos[".pageSize"] = 20;

$tdatafin_tipogastos[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_tipogastos[".strOrderBy"] = $tstrOrderBy;

$tdatafin_tipogastos[".orderindexes"] = array();

$tdatafin_tipogastos[".sqlHead"] = "select idTipogasto,  tipodegasto,  obs,  ultimousuario,  ultimaalteracao";
$tdatafin_tipogastos[".sqlFrom"] = "FROM fin_tipogastos";
$tdatafin_tipogastos[".sqlWhereExpr"] = "";
$tdatafin_tipogastos[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_tipogastos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_tipogastos[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_tipogastos[".highlightSearchResults"] = true;

$tableKeysfin_tipogastos = array();
$tableKeysfin_tipogastos[] = "idTipogasto";
$tdatafin_tipogastos[".Keys"] = $tableKeysfin_tipogastos;

$tdatafin_tipogastos[".listFields"] = array();
$tdatafin_tipogastos[".listFields"][] = "tipodegasto";
$tdatafin_tipogastos[".listFields"][] = "obs";

$tdatafin_tipogastos[".hideMobileList"] = array();


$tdatafin_tipogastos[".viewFields"] = array();
$tdatafin_tipogastos[".viewFields"][] = "ultimaalteracao";
$tdatafin_tipogastos[".viewFields"][] = "ultimousuario";
$tdatafin_tipogastos[".viewFields"][] = "tipodegasto";
$tdatafin_tipogastos[".viewFields"][] = "obs";

$tdatafin_tipogastos[".addFields"] = array();
$tdatafin_tipogastos[".addFields"][] = "tipodegasto";
$tdatafin_tipogastos[".addFields"][] = "obs";

$tdatafin_tipogastos[".masterListFields"] = array();

$tdatafin_tipogastos[".inlineAddFields"] = array();

$tdatafin_tipogastos[".editFields"] = array();
$tdatafin_tipogastos[".editFields"][] = "tipodegasto";
$tdatafin_tipogastos[".editFields"][] = "obs";

$tdatafin_tipogastos[".inlineEditFields"] = array();

$tdatafin_tipogastos[".exportFields"] = array();
$tdatafin_tipogastos[".exportFields"][] = "ultimaalteracao";
$tdatafin_tipogastos[".exportFields"][] = "ultimousuario";
$tdatafin_tipogastos[".exportFields"][] = "tipodegasto";
$tdatafin_tipogastos[".exportFields"][] = "obs";

$tdatafin_tipogastos[".importFields"] = array();
$tdatafin_tipogastos[".importFields"][] = "idTipogasto";
$tdatafin_tipogastos[".importFields"][] = "tipodegasto";
$tdatafin_tipogastos[".importFields"][] = "obs";
$tdatafin_tipogastos[".importFields"][] = "ultimousuario";
$tdatafin_tipogastos[".importFields"][] = "ultimaalteracao";

$tdatafin_tipogastos[".printFields"] = array();
$tdatafin_tipogastos[".printFields"][] = "tipodegasto";
$tdatafin_tipogastos[".printFields"][] = "obs";

//	idTipogasto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idTipogasto";
	$fdata["GoodName"] = "idTipogasto";
	$fdata["ownerTable"] = "fin_tipogastos";
	$fdata["Label"] = GetFieldLabel("fin_tipogastos","idTipogasto");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idTipogasto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idTipogasto";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_tipogastos["idTipogasto"] = $fdata;
//	tipodegasto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "tipodegasto";
	$fdata["GoodName"] = "tipodegasto";
	$fdata["ownerTable"] = "fin_tipogastos";
	$fdata["Label"] = GetFieldLabel("fin_tipogastos","tipodegasto");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipodegasto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipodegasto";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_tipogastos["tipodegasto"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "fin_tipogastos";
	$fdata["Label"] = GetFieldLabel("fin_tipogastos","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_tipogastos["obs"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_tipogastos";
	$fdata["Label"] = GetFieldLabel("fin_tipogastos","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_tipogastos["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_tipogastos";
	$fdata["Label"] = GetFieldLabel("fin_tipogastos","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_tipogastos["ultimaalteracao"] = $fdata;


$tables_data["fin_tipogastos"]=&$tdatafin_tipogastos;
$field_labels["fin_tipogastos"] = &$fieldLabelsfin_tipogastos;
$fieldToolTips["fin_tipogastos"] = &$fieldToolTipsfin_tipogastos;
$page_titles["fin_tipogastos"] = &$pageTitlesfin_tipogastos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_tipogastos"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_tipogastos"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_tipogastos()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idTipogasto,  tipodegasto,  obs,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM fin_tipogastos";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idTipogasto",
	"m_strTable" => "fin_tipogastos",
	"m_srcTableName" => "fin_tipogastos"
));

$proto6["m_sql"] = "idTipogasto";
$proto6["m_srcTableName"] = "fin_tipogastos";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "tipodegasto",
	"m_strTable" => "fin_tipogastos",
	"m_srcTableName" => "fin_tipogastos"
));

$proto8["m_sql"] = "tipodegasto";
$proto8["m_srcTableName"] = "fin_tipogastos";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "fin_tipogastos",
	"m_srcTableName" => "fin_tipogastos"
));

$proto10["m_sql"] = "obs";
$proto10["m_srcTableName"] = "fin_tipogastos";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_tipogastos",
	"m_srcTableName" => "fin_tipogastos"
));

$proto12["m_sql"] = "ultimousuario";
$proto12["m_srcTableName"] = "fin_tipogastos";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_tipogastos",
	"m_srcTableName" => "fin_tipogastos"
));

$proto14["m_sql"] = "ultimaalteracao";
$proto14["m_srcTableName"] = "fin_tipogastos";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "fin_tipogastos";
$proto17["m_srcTableName"] = "fin_tipogastos";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "idTipogasto";
$proto17["m_columns"][] = "tipodegasto";
$proto17["m_columns"][] = "obs";
$proto17["m_columns"][] = "ultimousuario";
$proto17["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "fin_tipogastos";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "fin_tipogastos";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="fin_tipogastos";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_tipogastos = createSqlQuery_fin_tipogastos();


	
		;

					

$tdatafin_tipogastos[".sqlquery"] = $queryData_fin_tipogastos;

include_once(getabspath("include/fin_tipogastos_events.php"));
$tableEvents["fin_tipogastos"] = new eventclass_fin_tipogastos;
$tdatafin_tipogastos[".hasEvents"] = true;

?>