<?php
include_once(getabspath("classes/printpage.php"));

function DisplayMasterTableInfoForPrint_ger_unidades($params)
{
	global $cman;
	
	$detailtable = $params["detailtable"];
	$keys = $params["keys"];
	
	$xt = new Xtempl();
	
	$tName = "ger_unidades";
	$xt->eventsObject = getEventObject($tName);

	$pageType = PAGE_PRINT;

	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = PRINT_MASTER;
	$mParams["pageType"] = $pageType;
	$mParams["tName"] = $tName;
	$masterPage = new PrintPage($mParams);
	
	$cipherer = new RunnerCipherer( $tName );
	$settings = new ProjectSettings($tName, $pageType);
	$connection = $cman->byTable( $tName );
	
	$masterQuery = $settings->getSQLQuery();
	$viewControls = new ViewControlsContainer($settings, $pageType, $masterPage);
	
	$where = "";
	$keysAssoc = array();
	$showKeys = "";

	if( $detailtable == "fin_cobranca" )
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("idUnidade", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("idUnidade", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}

	if( $detailtable == "ger_moradores" )
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("idUnidade", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("idUnidade", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}

	if( $detailtable == "fin_cobranca_atualizacao_aberto" )
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("idUnidade", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("idUnidade", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}

	if( $detailtable == "jur_processos" )
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("idUnidade", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("idUnidade", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}

	if( $detailtable == "ger_arquivos_unidades" )
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("idUnidade", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("idUnidade", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}
	
	if( !$where )
		return;
	
	$str = SecuritySQL("Export", $tName );
	if( strlen($str) )
		$where.= " and ".$str;
	
	$strWhere = whereAdd( $masterQuery->m_where->toSql($masterQuery), $where );
	if( strlen($strWhere) )
		$strWhere= " where ".$strWhere." ";
		
	$strSQL = $masterQuery->HeadToSql().' '.$masterQuery->FromToSql().$strWhere.$masterQuery->TailToSql();
	LogInfo($strSQL);
	
	$data = $cipherer->DecryptFetchedArray( $connection->query( $strSQL )->fetchAssoc() );
	if( !$data )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));	
	
	$keylink = "";
	$keylink.= "&key1=".runner_htmlspecialchars(rawurlencode(@$data["idUnidade"]));
	
	$xt->assign("grupounidade_mastervalue", $viewControls->showDBValue("grupounidade", $data, $keylink));
	$format = $settings->getViewFormat("grupounidade");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("grupounidade")))
		$class = ' rnr-field-number';
		
	$xt->assign("grupounidade_class", $class); // add class for field header as field value
	$xt->assign("link_ger_cadastro_mastervalue", $viewControls->showDBValue("link_ger_cadastro", $data, $keylink));
	$format = $settings->getViewFormat("link_ger_cadastro");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("link_ger_cadastro")))
		$class = ' rnr-field-number';
		
	$xt->assign("link_ger_cadastro_class", $class); // add class for field header as field value
	$xt->assign("dia_pref_venc_mastervalue", $viewControls->showDBValue("dia_pref_venc", $data, $keylink));
	$format = $settings->getViewFormat("dia_pref_venc");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("dia_pref_venc")))
		$class = ' rnr-field-number';
		
	$xt->assign("dia_pref_venc_class", $class); // add class for field header as field value
	$xt->assign("ativo_mastervalue", $viewControls->showDBValue("ativo", $data, $keylink));
	$format = $settings->getViewFormat("ativo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("ativo")))
		$class = ' rnr-field-number';
		
	$xt->assign("ativo_class", $class); // add class for field header as field value
	$xt->assign("obs_mastervalue", $viewControls->showDBValue("obs", $data, $keylink));
	$format = $settings->getViewFormat("obs");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("obs")))
		$class = ' rnr-field-number';
		
	$xt->assign("obs_class", $class); // add class for field header as field value
	$xt->assign("frideal_mastervalue", $viewControls->showDBValue("frideal", $data, $keylink));
	$format = $settings->getViewFormat("frideal");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("frideal")))
		$class = ' rnr-field-number';
		
	$xt->assign("frideal_class", $class); // add class for field header as field value

	$layout = GetPageLayout("ger_unidades", 'masterprint');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');

	$xt->displayPartial(GetTemplateName("ger_unidades", "masterprint"));
}

?>