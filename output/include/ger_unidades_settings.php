<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_unidades = array();
	$tdatager_unidades[".truncateText"] = true;
	$tdatager_unidades[".NumberOfChars"] = 80;
	$tdatager_unidades[".ShortName"] = "ger_unidades";
	$tdatager_unidades[".OwnerID"] = "";
	$tdatager_unidades[".OriginalTable"] = "ger_unidades";

//	field labels
$fieldLabelsger_unidades = array();
$fieldToolTipsger_unidades = array();
$pageTitlesger_unidades = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_unidades["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_unidades["Portuguese(Brazil)"] = array();
	$pageTitlesger_unidades["Portuguese(Brazil)"] = array();
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["idUnidade"] = "Código";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["idUnidade"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["grupo"] = "Grupo";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["grupo"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["unidade"] = "Unidade";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["unidade"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["grupounidade"] = "Unidade";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["grupounidade"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["link_ger_cadastro"] = "Cadastro";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["link_ger_cadastro"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["dia_pref_venc"] = "Dia de vcto";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["dia_pref_venc"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["ativo"] = "Ativo";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["ativo"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["frideal"] = "Fração Ideal";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["frideal"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_unidades["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsger_unidades["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_unidades["Portuguese(Brazil)"]))
		$tdatager_unidades[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_unidades[""] = array();
	$fieldToolTipsger_unidades[""] = array();
	$pageTitlesger_unidades[""] = array();
	if (count($fieldToolTipsger_unidades[""]))
		$tdatager_unidades[".isUseToolTips"] = true;
}


	$tdatager_unidades[".NCSearch"] = true;



$tdatager_unidades[".shortTableName"] = "ger_unidades";
$tdatager_unidades[".nSecOptions"] = 0;
$tdatager_unidades[".recsPerRowList"] = 1;
$tdatager_unidades[".recsPerRowPrint"] = 1;
$tdatager_unidades[".mainTableOwnerID"] = "";
$tdatager_unidades[".moveNext"] = 1;
$tdatager_unidades[".entityType"] = 0;

$tdatager_unidades[".strOriginalTableName"] = "ger_unidades";





$tdatager_unidades[".showAddInPopup"] = false;

$tdatager_unidades[".showEditInPopup"] = false;

$tdatager_unidades[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_unidades[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_unidades[".fieldsForRegister"] = array();

$tdatager_unidades[".listAjax"] = false;

	$tdatager_unidades[".audit"] = true;

	$tdatager_unidades[".locking"] = true;

$tdatager_unidades[".edit"] = true;
$tdatager_unidades[".afterEditAction"] = 1;
$tdatager_unidades[".closePopupAfterEdit"] = 1;
$tdatager_unidades[".afterEditActionDetTable"] = "";

$tdatager_unidades[".add"] = true;
$tdatager_unidades[".afterAddAction"] = 1;
$tdatager_unidades[".closePopupAfterAdd"] = 1;
$tdatager_unidades[".afterAddActionDetTable"] = "";

$tdatager_unidades[".list"] = true;

$tdatager_unidades[".view"] = true;


$tdatager_unidades[".exportTo"] = true;

$tdatager_unidades[".printFriendly"] = true;

$tdatager_unidades[".delete"] = true;

$tdatager_unidades[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_unidades[".searchSaving"] = false;
//

$tdatager_unidades[".showSearchPanel"] = true;
		$tdatager_unidades[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_unidades[".isUseAjaxSuggest"] = false;
else
	$tdatager_unidades[".isUseAjaxSuggest"] = true;

$tdatager_unidades[".rowHighlite"] = true;



$tdatager_unidades[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_unidades[".isUseTimeForSearch"] = false;



$tdatager_unidades[".badgeColor"] = "D2AF80";

$tdatager_unidades[".detailsLinksOnList"] = "1";

$tdatager_unidades[".allSearchFields"] = array();
$tdatager_unidades[".filterFields"] = array();
$tdatager_unidades[".requiredSearchFields"] = array();

$tdatager_unidades[".allSearchFields"][] = "idUnidade";
	$tdatager_unidades[".allSearchFields"][] = "grupounidade";
	$tdatager_unidades[".allSearchFields"][] = "link_ger_cadastro";
	$tdatager_unidades[".allSearchFields"][] = "dia_pref_venc";
	$tdatager_unidades[".allSearchFields"][] = "ativo";
	$tdatager_unidades[".allSearchFields"][] = "frideal";
	$tdatager_unidades[".allSearchFields"][] = "obs";
	$tdatager_unidades[".allSearchFields"][] = "ultimousuario";
	$tdatager_unidades[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_unidades[".googleLikeFields"] = array();
$tdatager_unidades[".googleLikeFields"][] = "idUnidade";
$tdatager_unidades[".googleLikeFields"][] = "grupounidade";
$tdatager_unidades[".googleLikeFields"][] = "link_ger_cadastro";
$tdatager_unidades[".googleLikeFields"][] = "dia_pref_venc";
$tdatager_unidades[".googleLikeFields"][] = "ativo";
$tdatager_unidades[".googleLikeFields"][] = "obs";
$tdatager_unidades[".googleLikeFields"][] = "frideal";
$tdatager_unidades[".googleLikeFields"][] = "ultimousuario";
$tdatager_unidades[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_unidades[".advSearchFields"] = array();
$tdatager_unidades[".advSearchFields"][] = "idUnidade";
$tdatager_unidades[".advSearchFields"][] = "grupounidade";
$tdatager_unidades[".advSearchFields"][] = "link_ger_cadastro";
$tdatager_unidades[".advSearchFields"][] = "dia_pref_venc";
$tdatager_unidades[".advSearchFields"][] = "ativo";
$tdatager_unidades[".advSearchFields"][] = "frideal";
$tdatager_unidades[".advSearchFields"][] = "obs";
$tdatager_unidades[".advSearchFields"][] = "ultimousuario";
$tdatager_unidades[".advSearchFields"][] = "ultimaalteracao";

$tdatager_unidades[".tableType"] = "list";

$tdatager_unidades[".printerPageOrientation"] = 0;
$tdatager_unidades[".nPrinterPageScale"] = 100;

$tdatager_unidades[".nPrinterSplitRecords"] = 40;

$tdatager_unidades[".nPrinterPDFSplitRecords"] = 40;



$tdatager_unidades[".geocodingEnabled"] = false;





$tdatager_unidades[".listGridLayout"] = 3;

$tdatager_unidades[".isDisplayLoading"] = true;


$tdatager_unidades[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_unidades[".pageSize"] = 20;

$tdatager_unidades[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_unidades[".strOrderBy"] = $tstrOrderBy;

$tdatager_unidades[".orderindexes"] = array();

$tdatager_unidades[".sqlHead"] = "select idUnidade,  grupo,  unidade,  idUnidade AS grupounidade,  link_ger_cadastro,  dia_pref_venc,  ativo,  obs,  frideal,  ultimousuario,  ultimaalteracao";
$tdatager_unidades[".sqlFrom"] = "FROM ger_unidades";
$tdatager_unidades[".sqlWhereExpr"] = "";
$tdatager_unidades[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_unidades[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_unidades[".arrGroupsPerPage"] = $arrGPP;

$tdatager_unidades[".highlightSearchResults"] = true;

$tableKeysger_unidades = array();
$tableKeysger_unidades[] = "idUnidade";
$tdatager_unidades[".Keys"] = $tableKeysger_unidades;

$tdatager_unidades[".listFields"] = array();
$tdatager_unidades[".listFields"][] = "grupounidade";
$tdatager_unidades[".listFields"][] = "link_ger_cadastro";
$tdatager_unidades[".listFields"][] = "dia_pref_venc";
$tdatager_unidades[".listFields"][] = "ativo";
$tdatager_unidades[".listFields"][] = "frideal";
$tdatager_unidades[".listFields"][] = "obs";

$tdatager_unidades[".hideMobileList"] = array();


$tdatager_unidades[".viewFields"] = array();
$tdatager_unidades[".viewFields"][] = "idUnidade";
$tdatager_unidades[".viewFields"][] = "grupounidade";
$tdatager_unidades[".viewFields"][] = "link_ger_cadastro";
$tdatager_unidades[".viewFields"][] = "dia_pref_venc";
$tdatager_unidades[".viewFields"][] = "ativo";
$tdatager_unidades[".viewFields"][] = "frideal";
$tdatager_unidades[".viewFields"][] = "obs";
$tdatager_unidades[".viewFields"][] = "ultimousuario";
$tdatager_unidades[".viewFields"][] = "ultimaalteracao";

$tdatager_unidades[".addFields"] = array();
$tdatager_unidades[".addFields"][] = "unidade";
$tdatager_unidades[".addFields"][] = "grupo";
$tdatager_unidades[".addFields"][] = "link_ger_cadastro";
$tdatager_unidades[".addFields"][] = "dia_pref_venc";
$tdatager_unidades[".addFields"][] = "ativo";
$tdatager_unidades[".addFields"][] = "frideal";
$tdatager_unidades[".addFields"][] = "obs";

$tdatager_unidades[".masterListFields"] = array();
$tdatager_unidades[".masterListFields"][] = "grupounidade";
$tdatager_unidades[".masterListFields"][] = "link_ger_cadastro";
$tdatager_unidades[".masterListFields"][] = "dia_pref_venc";
$tdatager_unidades[".masterListFields"][] = "ativo";
$tdatager_unidades[".masterListFields"][] = "frideal";
$tdatager_unidades[".masterListFields"][] = "obs";

$tdatager_unidades[".inlineAddFields"] = array();

$tdatager_unidades[".editFields"] = array();
$tdatager_unidades[".editFields"][] = "grupo";
$tdatager_unidades[".editFields"][] = "unidade";
$tdatager_unidades[".editFields"][] = "idUnidade";
$tdatager_unidades[".editFields"][] = "link_ger_cadastro";
$tdatager_unidades[".editFields"][] = "dia_pref_venc";
$tdatager_unidades[".editFields"][] = "ativo";
$tdatager_unidades[".editFields"][] = "frideal";
$tdatager_unidades[".editFields"][] = "obs";

$tdatager_unidades[".inlineEditFields"] = array();

$tdatager_unidades[".exportFields"] = array();
$tdatager_unidades[".exportFields"][] = "link_ger_cadastro";
$tdatager_unidades[".exportFields"][] = "dia_pref_venc";
$tdatager_unidades[".exportFields"][] = "ativo";
$tdatager_unidades[".exportFields"][] = "frideal";
$tdatager_unidades[".exportFields"][] = "obs";
$tdatager_unidades[".exportFields"][] = "ultimousuario";
$tdatager_unidades[".exportFields"][] = "ultimaalteracao";

$tdatager_unidades[".importFields"] = array();
$tdatager_unidades[".importFields"][] = "idUnidade";
$tdatager_unidades[".importFields"][] = "grupo";
$tdatager_unidades[".importFields"][] = "unidade";
$tdatager_unidades[".importFields"][] = "grupounidade";
$tdatager_unidades[".importFields"][] = "link_ger_cadastro";
$tdatager_unidades[".importFields"][] = "dia_pref_venc";
$tdatager_unidades[".importFields"][] = "ativo";
$tdatager_unidades[".importFields"][] = "obs";
$tdatager_unidades[".importFields"][] = "frideal";
$tdatager_unidades[".importFields"][] = "ultimousuario";
$tdatager_unidades[".importFields"][] = "ultimaalteracao";

$tdatager_unidades[".printFields"] = array();
$tdatager_unidades[".printFields"][] = "grupounidade";
$tdatager_unidades[".printFields"][] = "link_ger_cadastro";
$tdatager_unidades[".printFields"][] = "dia_pref_venc";
$tdatager_unidades[".printFields"][] = "ativo";
$tdatager_unidades[".printFields"][] = "frideal";
$tdatager_unidades[".printFields"][] = "obs";

//	idUnidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idUnidade";
	$fdata["GoodName"] = "idUnidade";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","idUnidade");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "idUnidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idUnidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_unidades["idUnidade"] = $fdata;
//	grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "grupo";
	$fdata["GoodName"] = "grupo";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","grupo");
	$fdata["FieldType"] = 3;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
	
	
	
		$fdata["strField"] = "grupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
			$edata["LookupUnique"] = true;

	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 83;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_unidades["grupo"] = $fdata;
//	unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "unidade";
	$fdata["GoodName"] = "unidade";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
	
	
	
		$fdata["strField"] = "unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_unidades["unidade"] = $fdata;
//	grupounidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "grupounidade";
	$fdata["GoodName"] = "grupounidade";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","grupounidade");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "idUnidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idUnidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_guni_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_unidades["grupounidade"] = $fdata;
//	link_ger_cadastro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "link_ger_cadastro";
	$fdata["GoodName"] = "link_ger_cadastro";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","link_ger_cadastro");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_cadastro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_cadastro";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_cadastro";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idCadastro";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "nome";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_unidades["link_ger_cadastro"] = $fdata;
//	dia_pref_venc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "dia_pref_venc";
	$fdata["GoodName"] = "dia_pref_venc";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","dia_pref_venc");
	$fdata["FieldType"] = 2;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "dia_pref_venc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dia_pref_venc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_unidades["dia_pref_venc"] = $fdata;
//	ativo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ativo";
	$fdata["GoodName"] = "ativo";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","ativo");
	$fdata["FieldType"] = 16;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ativo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ativo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_unidades["ativo"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_unidades["obs"] = $fdata;
//	frideal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "frideal";
	$fdata["GoodName"] = "frideal";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","frideal");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "frideal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "frideal";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Percent");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_unidades["frideal"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_unidades["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_unidades","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_unidades["ultimaalteracao"] = $fdata;


$tables_data["ger_unidades"]=&$tdatager_unidades;
$field_labels["ger_unidades"] = &$fieldLabelsger_unidades;
$fieldToolTips["ger_unidades"] = &$fieldToolTipsger_unidades;
$page_titles["ger_unidades"] = &$pageTitlesger_unidades;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_unidades"] = array();
//	fin_cobranca
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="fin_cobranca";
		$detailsParam["dOriginalTable"] = "fin_cobranca";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "fin_cobranca";
	$detailsParam["dCaptionTable"] = GetTableCaption("fin_cobranca");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_unidades"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"][]="idUnidade";

				$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"][]="link_ger_unidade";
//	ger_moradores
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="ger_moradores";
		$detailsParam["dOriginalTable"] = "ger_moradores";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "ger_moradores";
	$detailsParam["dCaptionTable"] = GetTableCaption("ger_moradores");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_unidades"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"][]="idUnidade";

				$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"][]="link_ger_unidades";
//	fin_cobranca_atualizacao_aberto
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="fin_cobranca_atualizacao_aberto";
		$detailsParam["dOriginalTable"] = "fin_cobranca_atualizacao_aberto";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "fin_cobranca_atualizacao_aberto";
	$detailsParam["dCaptionTable"] = GetTableCaption("fin_cobranca_atualizacao_aberto");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = true;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_unidades"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"][]="idUnidade";

				$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"][]="link_ger_unidade";
//	jur_processos
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="jur_processos";
		$detailsParam["dOriginalTable"] = "jur_processos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "jur_processos";
	$detailsParam["dCaptionTable"] = GetTableCaption("jur_processos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "0";
*/
	$detailsParam["dispChildCount"] = "0";
	
		$detailsParam["hideChild"] = true;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_unidades"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"][]="idUnidade";

				$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"][]="link_ger_unidade";
//	ger_arquivos_unidades
	
	

		$dIndex = 4;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="ger_arquivos_unidades";
		$detailsParam["dOriginalTable"] = "ger_arquivos_unidades";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "ger_arquivos_unidades";
	$detailsParam["dCaptionTable"] = GetTableCaption("ger_arquivos_unidades");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_unidades"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["masterKeys"][]="idUnidade";

				$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_unidades"][$dIndex]["detailKeys"][]="link_ger_unidade";

// tables which are master tables for current table (detail)
$masterTablesData["ger_unidades"] = array();


	
				$strOriginalDetailsTable="ger_cadastro";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_cadastro";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_cadastro";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["ger_unidades"][0] = $masterParams;
				$masterTablesData["ger_unidades"][0]["masterKeys"] = array();
	$masterTablesData["ger_unidades"][0]["masterKeys"][]="idCadastro";
				$masterTablesData["ger_unidades"][0]["detailKeys"] = array();
	$masterTablesData["ger_unidades"][0]["detailKeys"][]="link_ger_cadastro";
		
	
				$strOriginalDetailsTable="ger_grupo";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_grupo";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_grupo";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["ger_unidades"][1] = $masterParams;
				$masterTablesData["ger_unidades"][1]["masterKeys"] = array();
	$masterTablesData["ger_unidades"][1]["masterKeys"][]="idGrupo";
				$masterTablesData["ger_unidades"][1]["detailKeys"] = array();
	$masterTablesData["ger_unidades"][1]["detailKeys"][]="grupo";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_unidades()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idUnidade,  grupo,  unidade,  idUnidade AS grupounidade,  link_ger_cadastro,  dia_pref_venc,  ativo,  obs,  frideal,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_unidades";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idUnidade",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto6["m_sql"] = "idUnidade";
$proto6["m_srcTableName"] = "ger_unidades";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "grupo",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto8["m_sql"] = "grupo";
$proto8["m_srcTableName"] = "ger_unidades";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "unidade",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto10["m_sql"] = "unidade";
$proto10["m_srcTableName"] = "ger_unidades";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "idUnidade",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto12["m_sql"] = "idUnidade";
$proto12["m_srcTableName"] = "ger_unidades";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "grupounidade";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_cadastro",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto14["m_sql"] = "link_ger_cadastro";
$proto14["m_srcTableName"] = "ger_unidades";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "dia_pref_venc",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto16["m_sql"] = "dia_pref_venc";
$proto16["m_srcTableName"] = "ger_unidades";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ativo",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto18["m_sql"] = "ativo";
$proto18["m_srcTableName"] = "ger_unidades";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto20["m_sql"] = "obs";
$proto20["m_srcTableName"] = "ger_unidades";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "frideal",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto22["m_sql"] = "frideal";
$proto22["m_srcTableName"] = "ger_unidades";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto24["m_sql"] = "ultimousuario";
$proto24["m_srcTableName"] = "ger_unidades";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "ger_unidades"
));

$proto26["m_sql"] = "ultimaalteracao";
$proto26["m_srcTableName"] = "ger_unidades";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto28=array();
$proto28["m_link"] = "SQLL_MAIN";
			$proto29=array();
$proto29["m_strName"] = "ger_unidades";
$proto29["m_srcTableName"] = "ger_unidades";
$proto29["m_columns"] = array();
$proto29["m_columns"][] = "idUnidade";
$proto29["m_columns"][] = "grupo";
$proto29["m_columns"][] = "unidade";
$proto29["m_columns"][] = "link_ger_cadastro";
$proto29["m_columns"][] = "dia_pref_venc";
$proto29["m_columns"][] = "ativo";
$proto29["m_columns"][] = "obs";
$proto29["m_columns"][] = "frideal";
$proto29["m_columns"][] = "ultimousuario";
$proto29["m_columns"][] = "ultimaalteracao";
$proto29["m_columns"][] = "auth";
$obj = new SQLTable($proto29);

$proto28["m_table"] = $obj;
$proto28["m_sql"] = "ger_unidades";
$proto28["m_alias"] = "";
$proto28["m_srcTableName"] = "ger_unidades";
$proto30=array();
$proto30["m_sql"] = "";
$proto30["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto30["m_column"]=$obj;
$proto30["m_contained"] = array();
$proto30["m_strCase"] = "";
$proto30["m_havingmode"] = false;
$proto30["m_inBrackets"] = false;
$proto30["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto30);

$proto28["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto28);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_unidades";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_unidades = createSqlQuery_ger_unidades();


	
		;

											

$tdatager_unidades[".sqlquery"] = $queryData_ger_unidades;

include_once(getabspath("include/ger_unidades_events.php"));
$tableEvents["ger_unidades"] = new eventclass_ger_unidades;
$tdatager_unidades[".hasEvents"] = true;

?>