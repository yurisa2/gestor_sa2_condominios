<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_arquivos_geral = array();
	$tdatager_arquivos_geral[".truncateText"] = true;
	$tdatager_arquivos_geral[".NumberOfChars"] = 80;
	$tdatager_arquivos_geral[".ShortName"] = "ger_arquivos_geral";
	$tdatager_arquivos_geral[".OwnerID"] = "";
	$tdatager_arquivos_geral[".OriginalTable"] = "ger_arquivos_geral";

//	field labels
$fieldLabelsger_arquivos_geral = array();
$fieldToolTipsger_arquivos_geral = array();
$pageTitlesger_arquivos_geral = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_arquivos_geral["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"] = array();
	$pageTitlesger_arquivos_geral["Portuguese(Brazil)"] = array();
	$fieldLabelsger_arquivos_geral["Portuguese(Brazil)"]["idArquivosGeral"] = "Id Arquivos Geral";
	$fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"]["idArquivosGeral"] = "";
	$fieldLabelsger_arquivos_geral["Portuguese(Brazil)"]["nomedoc"] = "Nome do Documento";
	$fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"]["nomedoc"] = "";
	$fieldLabelsger_arquivos_geral["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsger_arquivos_geral["Portuguese(Brazil)"]["arquivo"] = "Arquivo";
	$fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"]["arquivo"] = "";
	$fieldLabelsger_arquivos_geral["Portuguese(Brazil)"]["link_ger_tipos_arquivo"] = "Tipo";
	$fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"]["link_ger_tipos_arquivo"] = "";
	$fieldLabelsger_arquivos_geral["Portuguese(Brazil)"]["ultimousuario"] = "Ultimo usuário";
	$fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_arquivos_geral["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima alteração";
	$fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_arquivos_geral["Portuguese(Brazil)"]))
		$tdatager_arquivos_geral[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_arquivos_geral[""] = array();
	$fieldToolTipsger_arquivos_geral[""] = array();
	$pageTitlesger_arquivos_geral[""] = array();
	if (count($fieldToolTipsger_arquivos_geral[""]))
		$tdatager_arquivos_geral[".isUseToolTips"] = true;
}


	$tdatager_arquivos_geral[".NCSearch"] = true;



$tdatager_arquivos_geral[".shortTableName"] = "ger_arquivos_geral";
$tdatager_arquivos_geral[".nSecOptions"] = 0;
$tdatager_arquivos_geral[".recsPerRowList"] = 1;
$tdatager_arquivos_geral[".recsPerRowPrint"] = 1;
$tdatager_arquivos_geral[".mainTableOwnerID"] = "";
$tdatager_arquivos_geral[".moveNext"] = 1;
$tdatager_arquivos_geral[".entityType"] = 0;

$tdatager_arquivos_geral[".strOriginalTableName"] = "ger_arquivos_geral";





$tdatager_arquivos_geral[".showAddInPopup"] = false;

$tdatager_arquivos_geral[".showEditInPopup"] = false;

$tdatager_arquivos_geral[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_arquivos_geral[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_arquivos_geral[".fieldsForRegister"] = array();

$tdatager_arquivos_geral[".listAjax"] = false;

	$tdatager_arquivos_geral[".audit"] = true;

	$tdatager_arquivos_geral[".locking"] = true;

$tdatager_arquivos_geral[".edit"] = true;
$tdatager_arquivos_geral[".afterEditAction"] = 1;
$tdatager_arquivos_geral[".closePopupAfterEdit"] = 1;
$tdatager_arquivos_geral[".afterEditActionDetTable"] = "";

$tdatager_arquivos_geral[".add"] = true;
$tdatager_arquivos_geral[".afterAddAction"] = 1;
$tdatager_arquivos_geral[".closePopupAfterAdd"] = 1;
$tdatager_arquivos_geral[".afterAddActionDetTable"] = "";

$tdatager_arquivos_geral[".list"] = true;

$tdatager_arquivos_geral[".view"] = true;

$tdatager_arquivos_geral[".import"] = true;

$tdatager_arquivos_geral[".exportTo"] = true;

$tdatager_arquivos_geral[".printFriendly"] = true;

$tdatager_arquivos_geral[".delete"] = true;

$tdatager_arquivos_geral[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_arquivos_geral[".searchSaving"] = false;
//

$tdatager_arquivos_geral[".showSearchPanel"] = true;
		$tdatager_arquivos_geral[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_arquivos_geral[".isUseAjaxSuggest"] = false;
else
	$tdatager_arquivos_geral[".isUseAjaxSuggest"] = true;

$tdatager_arquivos_geral[".rowHighlite"] = true;



$tdatager_arquivos_geral[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_arquivos_geral[".isUseTimeForSearch"] = false;





$tdatager_arquivos_geral[".allSearchFields"] = array();
$tdatager_arquivos_geral[".filterFields"] = array();
$tdatager_arquivos_geral[".requiredSearchFields"] = array();

$tdatager_arquivos_geral[".allSearchFields"][] = "nomedoc";
	$tdatager_arquivos_geral[".allSearchFields"][] = "obs";
	$tdatager_arquivos_geral[".allSearchFields"][] = "arquivo";
	$tdatager_arquivos_geral[".allSearchFields"][] = "link_ger_tipos_arquivo";
	$tdatager_arquivos_geral[".allSearchFields"][] = "ultimousuario";
	$tdatager_arquivos_geral[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_arquivos_geral[".googleLikeFields"] = array();
$tdatager_arquivos_geral[".googleLikeFields"][] = "nomedoc";
$tdatager_arquivos_geral[".googleLikeFields"][] = "obs";
$tdatager_arquivos_geral[".googleLikeFields"][] = "arquivo";
$tdatager_arquivos_geral[".googleLikeFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_geral[".googleLikeFields"][] = "ultimousuario";
$tdatager_arquivos_geral[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_arquivos_geral[".advSearchFields"] = array();
$tdatager_arquivos_geral[".advSearchFields"][] = "nomedoc";
$tdatager_arquivos_geral[".advSearchFields"][] = "obs";
$tdatager_arquivos_geral[".advSearchFields"][] = "arquivo";
$tdatager_arquivos_geral[".advSearchFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_geral[".advSearchFields"][] = "ultimousuario";
$tdatager_arquivos_geral[".advSearchFields"][] = "ultimaalteracao";

$tdatager_arquivos_geral[".tableType"] = "list";

$tdatager_arquivos_geral[".printerPageOrientation"] = 0;
$tdatager_arquivos_geral[".nPrinterPageScale"] = 100;

$tdatager_arquivos_geral[".nPrinterSplitRecords"] = 40;

$tdatager_arquivos_geral[".nPrinterPDFSplitRecords"] = 40;



$tdatager_arquivos_geral[".geocodingEnabled"] = false;





$tdatager_arquivos_geral[".listGridLayout"] = 3;

$tdatager_arquivos_geral[".isDisplayLoading"] = true;


$tdatager_arquivos_geral[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_arquivos_geral[".pageSize"] = 20;

$tdatager_arquivos_geral[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_arquivos_geral[".strOrderBy"] = $tstrOrderBy;

$tdatager_arquivos_geral[".orderindexes"] = array();

$tdatager_arquivos_geral[".sqlHead"] = "SELECT idArquivosGeral,  	nomedoc,  	obs,  	arquivo,  	link_ger_tipos_arquivo,  	ultimousuario,  	ultimaalteracao";
$tdatager_arquivos_geral[".sqlFrom"] = "FROM ger_arquivos_geral";
$tdatager_arquivos_geral[".sqlWhereExpr"] = "";
$tdatager_arquivos_geral[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_arquivos_geral[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_arquivos_geral[".arrGroupsPerPage"] = $arrGPP;

$tdatager_arquivos_geral[".highlightSearchResults"] = true;

$tableKeysger_arquivos_geral = array();
$tableKeysger_arquivos_geral[] = "idArquivosGeral";
$tdatager_arquivos_geral[".Keys"] = $tableKeysger_arquivos_geral;

$tdatager_arquivos_geral[".listFields"] = array();
$tdatager_arquivos_geral[".listFields"][] = "nomedoc";
$tdatager_arquivos_geral[".listFields"][] = "obs";
$tdatager_arquivos_geral[".listFields"][] = "arquivo";
$tdatager_arquivos_geral[".listFields"][] = "link_ger_tipos_arquivo";

$tdatager_arquivos_geral[".hideMobileList"] = array();


$tdatager_arquivos_geral[".viewFields"] = array();
$tdatager_arquivos_geral[".viewFields"][] = "nomedoc";
$tdatager_arquivos_geral[".viewFields"][] = "obs";
$tdatager_arquivos_geral[".viewFields"][] = "arquivo";
$tdatager_arquivos_geral[".viewFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_geral[".viewFields"][] = "ultimousuario";
$tdatager_arquivos_geral[".viewFields"][] = "ultimaalteracao";

$tdatager_arquivos_geral[".addFields"] = array();
$tdatager_arquivos_geral[".addFields"][] = "nomedoc";
$tdatager_arquivos_geral[".addFields"][] = "obs";
$tdatager_arquivos_geral[".addFields"][] = "arquivo";
$tdatager_arquivos_geral[".addFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_geral[".addFields"][] = "ultimousuario";
$tdatager_arquivos_geral[".addFields"][] = "ultimaalteracao";

$tdatager_arquivos_geral[".masterListFields"] = array();

$tdatager_arquivos_geral[".inlineAddFields"] = array();

$tdatager_arquivos_geral[".editFields"] = array();
$tdatager_arquivos_geral[".editFields"][] = "nomedoc";
$tdatager_arquivos_geral[".editFields"][] = "obs";
$tdatager_arquivos_geral[".editFields"][] = "arquivo";
$tdatager_arquivos_geral[".editFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_geral[".editFields"][] = "ultimousuario";
$tdatager_arquivos_geral[".editFields"][] = "ultimaalteracao";

$tdatager_arquivos_geral[".inlineEditFields"] = array();

$tdatager_arquivos_geral[".exportFields"] = array();
$tdatager_arquivos_geral[".exportFields"][] = "nomedoc";
$tdatager_arquivos_geral[".exportFields"][] = "obs";
$tdatager_arquivos_geral[".exportFields"][] = "arquivo";
$tdatager_arquivos_geral[".exportFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_geral[".exportFields"][] = "ultimousuario";
$tdatager_arquivos_geral[".exportFields"][] = "ultimaalteracao";

$tdatager_arquivos_geral[".importFields"] = array();
$tdatager_arquivos_geral[".importFields"][] = "idArquivosGeral";
$tdatager_arquivos_geral[".importFields"][] = "nomedoc";
$tdatager_arquivos_geral[".importFields"][] = "obs";
$tdatager_arquivos_geral[".importFields"][] = "arquivo";
$tdatager_arquivos_geral[".importFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_geral[".importFields"][] = "ultimousuario";
$tdatager_arquivos_geral[".importFields"][] = "ultimaalteracao";

$tdatager_arquivos_geral[".printFields"] = array();
$tdatager_arquivos_geral[".printFields"][] = "nomedoc";
$tdatager_arquivos_geral[".printFields"][] = "obs";
$tdatager_arquivos_geral[".printFields"][] = "arquivo";
$tdatager_arquivos_geral[".printFields"][] = "link_ger_tipos_arquivo";

//	idArquivosGeral
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idArquivosGeral";
	$fdata["GoodName"] = "idArquivosGeral";
	$fdata["ownerTable"] = "ger_arquivos_geral";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_geral","idArquivosGeral");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idArquivosGeral";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idArquivosGeral";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_arquivos_geral["idArquivosGeral"] = $fdata;
//	nomedoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nomedoc";
	$fdata["GoodName"] = "nomedoc";
	$fdata["ownerTable"] = "ger_arquivos_geral";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_geral","nomedoc");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nomedoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nomedoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=45";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_geral["nomedoc"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_arquivos_geral";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_geral","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_geral["obs"] = $fdata;
//	arquivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "arquivo";
	$fdata["GoodName"] = "arquivo";
	$fdata["ownerTable"] = "ger_arquivos_geral";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_geral","arquivo");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "arquivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "arquivo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "arquivos";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
	



		$edata["IsRequired"] = true;

	
		$edata["UseTimestamp"] = true;

	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_geral["arquivo"] = $fdata;
//	link_ger_tipos_arquivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "link_ger_tipos_arquivo";
	$fdata["GoodName"] = "link_ger_tipos_arquivo";
	$fdata["ownerTable"] = "ger_arquivos_geral";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_geral","link_ger_tipos_arquivo");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_tipos_arquivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_tipos_arquivo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_tipodocs";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idTipodocs";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "tipodoc";

	
	$edata["LookupOrderBy"] = "tipodoc";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_geral["link_ger_tipos_arquivo"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_arquivos_geral";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_geral","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_geral["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_arquivos_geral";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_geral","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_geral["ultimaalteracao"] = $fdata;


$tables_data["ger_arquivos_geral"]=&$tdatager_arquivos_geral;
$field_labels["ger_arquivos_geral"] = &$fieldLabelsger_arquivos_geral;
$fieldToolTips["ger_arquivos_geral"] = &$fieldToolTipsger_arquivos_geral;
$page_titles["ger_arquivos_geral"] = &$pageTitlesger_arquivos_geral;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_arquivos_geral"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_arquivos_geral"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_arquivos_geral()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idArquivosGeral,  	nomedoc,  	obs,  	arquivo,  	link_ger_tipos_arquivo,  	ultimousuario,  	ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_arquivos_geral";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idArquivosGeral",
	"m_strTable" => "ger_arquivos_geral",
	"m_srcTableName" => "ger_arquivos_geral"
));

$proto6["m_sql"] = "idArquivosGeral";
$proto6["m_srcTableName"] = "ger_arquivos_geral";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nomedoc",
	"m_strTable" => "ger_arquivos_geral",
	"m_srcTableName" => "ger_arquivos_geral"
));

$proto8["m_sql"] = "nomedoc";
$proto8["m_srcTableName"] = "ger_arquivos_geral";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_arquivos_geral",
	"m_srcTableName" => "ger_arquivos_geral"
));

$proto10["m_sql"] = "obs";
$proto10["m_srcTableName"] = "ger_arquivos_geral";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "arquivo",
	"m_strTable" => "ger_arquivos_geral",
	"m_srcTableName" => "ger_arquivos_geral"
));

$proto12["m_sql"] = "arquivo";
$proto12["m_srcTableName"] = "ger_arquivos_geral";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_tipos_arquivo",
	"m_strTable" => "ger_arquivos_geral",
	"m_srcTableName" => "ger_arquivos_geral"
));

$proto14["m_sql"] = "link_ger_tipos_arquivo";
$proto14["m_srcTableName"] = "ger_arquivos_geral";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_arquivos_geral",
	"m_srcTableName" => "ger_arquivos_geral"
));

$proto16["m_sql"] = "ultimousuario";
$proto16["m_srcTableName"] = "ger_arquivos_geral";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_arquivos_geral",
	"m_srcTableName" => "ger_arquivos_geral"
));

$proto18["m_sql"] = "ultimaalteracao";
$proto18["m_srcTableName"] = "ger_arquivos_geral";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "ger_arquivos_geral";
$proto21["m_srcTableName"] = "ger_arquivos_geral";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "idArquivosGeral";
$proto21["m_columns"][] = "nomedoc";
$proto21["m_columns"][] = "obs";
$proto21["m_columns"][] = "arquivo";
$proto21["m_columns"][] = "link_ger_tipos_arquivo";
$proto21["m_columns"][] = "ultimousuario";
$proto21["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "ger_arquivos_geral";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "ger_arquivos_geral";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_arquivos_geral";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_arquivos_geral = createSqlQuery_ger_arquivos_geral();


	
		;

							

$tdatager_arquivos_geral[".sqlquery"] = $queryData_ger_arquivos_geral;

$tableEvents["ger_arquivos_geral"] = new eventsBase;
$tdatager_arquivos_geral[".hasEvents"] = false;

?>