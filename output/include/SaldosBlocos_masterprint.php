<?php
include_once(getabspath("classes/printpage.php"));

function DisplayMasterTableInfoForPrint_SaldosBlocos($params)
{
	global $cman;
	
	$detailtable = $params["detailtable"];
	$keys = $params["keys"];
	
	$xt = new Xtempl();
	
	$tName = "SaldosBlocos";
	$xt->eventsObject = getEventObject($tName);

	$pageType = PAGE_PRINT;

	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = PRINT_MASTER;
	$mParams["pageType"] = $pageType;
	$mParams["tName"] = $tName;
	$masterPage = new PrintPage($mParams);
	
	$cipherer = new RunnerCipherer( $tName );
	$settings = new ProjectSettings($tName, $pageType);
	$connection = $cman->byTable( $tName );
	
	$masterQuery = $settings->getSQLQuery();
	$viewControls = new ViewControlsContainer($settings, $pageType, $masterPage);
	
	$where = "";
	$keysAssoc = array();
	$showKeys = "";

	if( $detailtable == "fin_bco_ext1" )
	{
		$keysAssoc["grupo"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("grupo", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("grupo", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("grupo", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("SaldosBlocos","grupo").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}
	
	if( !$where )
		return;
	
	$str = SecuritySQL("Export", $tName );
	if( strlen($str) )
		$where.= " and ".$str;
	
	$strWhere = whereAdd( $masterQuery->m_where->toSql($masterQuery), $where );
	if( strlen($strWhere) )
		$strWhere= " where ".$strWhere." ";
		
	$strSQL = $masterQuery->HeadToSql().' '.$masterQuery->FromToSql().$strWhere.$masterQuery->TailToSql();
	LogInfo($strSQL);
	
	$data = $cipherer->DecryptFetchedArray( $connection->query( $strSQL )->fetchAssoc() );
	if( !$data )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));	
	
	$keylink = "";
	
	$xt->assign("saldo_mastervalue", $viewControls->showDBValue("saldo", $data, $keylink));
	$format = $settings->getViewFormat("saldo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("saldo")))
		$class = ' rnr-field-number';
		
	$xt->assign("saldo_class", $class); // add class for field header as field value
	$xt->assign("primvalor_mastervalue", $viewControls->showDBValue("primvalor", $data, $keylink));
	$format = $settings->getViewFormat("primvalor");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("primvalor")))
		$class = ' rnr-field-number';
		
	$xt->assign("primvalor_class", $class); // add class for field header as field value
	$xt->assign("ultvalor_mastervalue", $viewControls->showDBValue("ultvalor", $data, $keylink));
	$format = $settings->getViewFormat("ultvalor");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("ultvalor")))
		$class = ' rnr-field-number';
		
	$xt->assign("ultvalor_class", $class); // add class for field header as field value
	$xt->assign("mediadegasto_mastervalue", $viewControls->showDBValue("mediadegasto", $data, $keylink));
	$format = $settings->getViewFormat("mediadegasto");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("mediadegasto")))
		$class = ' rnr-field-number';
		
	$xt->assign("mediadegasto_class", $class); // add class for field header as field value
	$xt->assign("grupo_mastervalue", $viewControls->showDBValue("grupo", $data, $keylink));
	$format = $settings->getViewFormat("grupo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("grupo")))
		$class = ' rnr-field-number';
		
	$xt->assign("grupo_class", $class); // add class for field header as field value

	$layout = GetPageLayout("SaldosBlocos", 'masterprint');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');

	$xt->displayPartial(GetTemplateName("SaldosBlocos", "masterprint"));
}

?>