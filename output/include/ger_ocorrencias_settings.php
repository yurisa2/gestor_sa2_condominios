<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_ocorrencias = array();
	$tdatager_ocorrencias[".truncateText"] = true;
	$tdatager_ocorrencias[".NumberOfChars"] = 80;
	$tdatager_ocorrencias[".ShortName"] = "ger_ocorrencias";
	$tdatager_ocorrencias[".OwnerID"] = "";
	$tdatager_ocorrencias[".OriginalTable"] = "ger_ocorrencias";

//	field labels
$fieldLabelsger_ocorrencias = array();
$fieldToolTipsger_ocorrencias = array();
$pageTitlesger_ocorrencias = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"] = array();
	$pageTitlesger_ocorrencias["Portuguese(Brazil)"] = array();
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"]["idOcorrencia"] = "Código";
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]["idOcorrencia"] = "";
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"]["link_ger_cadastro"] = "Cadastro";
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]["link_ger_cadastro"] = "";
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"]["data"] = "data";
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]["data"] = "";
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"]["link_ger_usuario"] = "link_ger_usuario";
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]["link_ger_usuario"] = "";
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"]["ultimousuario"] = "ultimousuario";
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"]["ultimaalteracao"] = "ultimaalteracao";
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"]["pendente"] = "Pendente";
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]["pendente"] = "";
	$fieldLabelsger_ocorrencias["Portuguese(Brazil)"]["ocorrencia"] = "Ocorrência";
	$fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]["ocorrencia"] = "";
	if (count($fieldToolTipsger_ocorrencias["Portuguese(Brazil)"]))
		$tdatager_ocorrencias[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_ocorrencias[""] = array();
	$fieldToolTipsger_ocorrencias[""] = array();
	$pageTitlesger_ocorrencias[""] = array();
	if (count($fieldToolTipsger_ocorrencias[""]))
		$tdatager_ocorrencias[".isUseToolTips"] = true;
}


	$tdatager_ocorrencias[".NCSearch"] = true;



$tdatager_ocorrencias[".shortTableName"] = "ger_ocorrencias";
$tdatager_ocorrencias[".nSecOptions"] = 0;
$tdatager_ocorrencias[".recsPerRowList"] = 1;
$tdatager_ocorrencias[".recsPerRowPrint"] = 1;
$tdatager_ocorrencias[".mainTableOwnerID"] = "";
$tdatager_ocorrencias[".moveNext"] = 1;
$tdatager_ocorrencias[".entityType"] = 0;

$tdatager_ocorrencias[".strOriginalTableName"] = "ger_ocorrencias";





$tdatager_ocorrencias[".showAddInPopup"] = false;

$tdatager_ocorrencias[".showEditInPopup"] = false;

$tdatager_ocorrencias[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_ocorrencias[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_ocorrencias[".fieldsForRegister"] = array();

$tdatager_ocorrencias[".listAjax"] = false;

	$tdatager_ocorrencias[".audit"] = true;

	$tdatager_ocorrencias[".locking"] = true;

$tdatager_ocorrencias[".edit"] = true;
$tdatager_ocorrencias[".afterEditAction"] = 1;
$tdatager_ocorrencias[".closePopupAfterEdit"] = 1;
$tdatager_ocorrencias[".afterEditActionDetTable"] = "";

$tdatager_ocorrencias[".add"] = true;
$tdatager_ocorrencias[".afterAddAction"] = 1;
$tdatager_ocorrencias[".closePopupAfterAdd"] = 1;
$tdatager_ocorrencias[".afterAddActionDetTable"] = "";

$tdatager_ocorrencias[".list"] = true;

$tdatager_ocorrencias[".view"] = true;

$tdatager_ocorrencias[".import"] = true;

$tdatager_ocorrencias[".exportTo"] = true;

$tdatager_ocorrencias[".printFriendly"] = true;

$tdatager_ocorrencias[".delete"] = true;

$tdatager_ocorrencias[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_ocorrencias[".searchSaving"] = false;
//

$tdatager_ocorrencias[".showSearchPanel"] = true;
		$tdatager_ocorrencias[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_ocorrencias[".isUseAjaxSuggest"] = false;
else
	$tdatager_ocorrencias[".isUseAjaxSuggest"] = true;

$tdatager_ocorrencias[".rowHighlite"] = true;



$tdatager_ocorrencias[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_ocorrencias[".isUseTimeForSearch"] = false;



$tdatager_ocorrencias[".badgeColor"] = "6DA5C8";


$tdatager_ocorrencias[".allSearchFields"] = array();
$tdatager_ocorrencias[".filterFields"] = array();
$tdatager_ocorrencias[".requiredSearchFields"] = array();

$tdatager_ocorrencias[".allSearchFields"][] = "pendente";
	$tdatager_ocorrencias[".allSearchFields"][] = "ocorrencia";
	$tdatager_ocorrencias[".allSearchFields"][] = "link_ger_cadastro";
	$tdatager_ocorrencias[".allSearchFields"][] = "ultimousuario";
	$tdatager_ocorrencias[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_ocorrencias[".googleLikeFields"] = array();
$tdatager_ocorrencias[".googleLikeFields"][] = "link_ger_cadastro";
$tdatager_ocorrencias[".googleLikeFields"][] = "ultimousuario";
$tdatager_ocorrencias[".googleLikeFields"][] = "ultimaalteracao";
$tdatager_ocorrencias[".googleLikeFields"][] = "pendente";
$tdatager_ocorrencias[".googleLikeFields"][] = "ocorrencia";


$tdatager_ocorrencias[".advSearchFields"] = array();
$tdatager_ocorrencias[".advSearchFields"][] = "pendente";
$tdatager_ocorrencias[".advSearchFields"][] = "ocorrencia";
$tdatager_ocorrencias[".advSearchFields"][] = "link_ger_cadastro";
$tdatager_ocorrencias[".advSearchFields"][] = "ultimousuario";
$tdatager_ocorrencias[".advSearchFields"][] = "ultimaalteracao";

$tdatager_ocorrencias[".tableType"] = "list";

$tdatager_ocorrencias[".printerPageOrientation"] = 0;
$tdatager_ocorrencias[".nPrinterPageScale"] = 100;

$tdatager_ocorrencias[".nPrinterSplitRecords"] = 40;

$tdatager_ocorrencias[".nPrinterPDFSplitRecords"] = 40;



$tdatager_ocorrencias[".geocodingEnabled"] = false;





$tdatager_ocorrencias[".listGridLayout"] = 3;

$tdatager_ocorrencias[".isDisplayLoading"] = true;


$tdatager_ocorrencias[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_ocorrencias[".pageSize"] = 20;

$tdatager_ocorrencias[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY pendente DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_ocorrencias[".strOrderBy"] = $tstrOrderBy;

$tdatager_ocorrencias[".orderindexes"] = array();
$tdatager_ocorrencias[".orderindexes"][] = array(7, (0 ? "ASC" : "DESC"), "pendente");

$tdatager_ocorrencias[".sqlHead"] = "SELECT idOcorrencia,  link_ger_cadastro,  `data`,  link_ger_usuario,  ultimousuario,  ultimaalteracao,  pendente,  ocorrencia";
$tdatager_ocorrencias[".sqlFrom"] = "FROM ger_ocorrencias";
$tdatager_ocorrencias[".sqlWhereExpr"] = "";
$tdatager_ocorrencias[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_ocorrencias[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_ocorrencias[".arrGroupsPerPage"] = $arrGPP;

$tdatager_ocorrencias[".highlightSearchResults"] = true;

$tableKeysger_ocorrencias = array();
$tableKeysger_ocorrencias[] = "idOcorrencia";
$tdatager_ocorrencias[".Keys"] = $tableKeysger_ocorrencias;

$tdatager_ocorrencias[".listFields"] = array();
$tdatager_ocorrencias[".listFields"][] = "pendente";
$tdatager_ocorrencias[".listFields"][] = "ocorrencia";
$tdatager_ocorrencias[".listFields"][] = "link_ger_cadastro";
$tdatager_ocorrencias[".listFields"][] = "ultimousuario";
$tdatager_ocorrencias[".listFields"][] = "ultimaalteracao";

$tdatager_ocorrencias[".hideMobileList"] = array();


$tdatager_ocorrencias[".viewFields"] = array();
$tdatager_ocorrencias[".viewFields"][] = "idOcorrencia";
$tdatager_ocorrencias[".viewFields"][] = "pendente";
$tdatager_ocorrencias[".viewFields"][] = "ocorrencia";
$tdatager_ocorrencias[".viewFields"][] = "link_ger_cadastro";
$tdatager_ocorrencias[".viewFields"][] = "ultimousuario";
$tdatager_ocorrencias[".viewFields"][] = "ultimaalteracao";

$tdatager_ocorrencias[".addFields"] = array();
$tdatager_ocorrencias[".addFields"][] = "pendente";
$tdatager_ocorrencias[".addFields"][] = "ocorrencia";
$tdatager_ocorrencias[".addFields"][] = "link_ger_cadastro";

$tdatager_ocorrencias[".masterListFields"] = array();

$tdatager_ocorrencias[".inlineAddFields"] = array();

$tdatager_ocorrencias[".editFields"] = array();
$tdatager_ocorrencias[".editFields"][] = "idOcorrencia";
$tdatager_ocorrencias[".editFields"][] = "pendente";
$tdatager_ocorrencias[".editFields"][] = "ocorrencia";
$tdatager_ocorrencias[".editFields"][] = "link_ger_cadastro";

$tdatager_ocorrencias[".inlineEditFields"] = array();

$tdatager_ocorrencias[".exportFields"] = array();
$tdatager_ocorrencias[".exportFields"][] = "pendente";
$tdatager_ocorrencias[".exportFields"][] = "ocorrencia";
$tdatager_ocorrencias[".exportFields"][] = "link_ger_cadastro";
$tdatager_ocorrencias[".exportFields"][] = "ultimousuario";
$tdatager_ocorrencias[".exportFields"][] = "ultimaalteracao";

$tdatager_ocorrencias[".importFields"] = array();
$tdatager_ocorrencias[".importFields"][] = "idOcorrencia";
$tdatager_ocorrencias[".importFields"][] = "link_ger_cadastro";
$tdatager_ocorrencias[".importFields"][] = "data";
$tdatager_ocorrencias[".importFields"][] = "link_ger_usuario";
$tdatager_ocorrencias[".importFields"][] = "ultimousuario";
$tdatager_ocorrencias[".importFields"][] = "ultimaalteracao";
$tdatager_ocorrencias[".importFields"][] = "pendente";
$tdatager_ocorrencias[".importFields"][] = "ocorrencia";

$tdatager_ocorrencias[".printFields"] = array();
$tdatager_ocorrencias[".printFields"][] = "pendente";
$tdatager_ocorrencias[".printFields"][] = "ocorrencia";
$tdatager_ocorrencias[".printFields"][] = "link_ger_cadastro";
$tdatager_ocorrencias[".printFields"][] = "ultimousuario";
$tdatager_ocorrencias[".printFields"][] = "ultimaalteracao";

//	idOcorrencia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idOcorrencia";
	$fdata["GoodName"] = "idOcorrencia";
	$fdata["ownerTable"] = "ger_ocorrencias";
	$fdata["Label"] = GetFieldLabel("ger_ocorrencias","idOcorrencia");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idOcorrencia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idOcorrencia";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_ocorrencias["idOcorrencia"] = $fdata;
//	link_ger_cadastro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "link_ger_cadastro";
	$fdata["GoodName"] = "link_ger_cadastro";
	$fdata["ownerTable"] = "ger_ocorrencias";
	$fdata["Label"] = GetFieldLabel("ger_ocorrencias","link_ger_cadastro");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_cadastro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_cadastro";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_cadastro";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idCadastro";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_ocorrencias["link_ger_cadastro"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "ger_ocorrencias";
	$fdata["Label"] = GetFieldLabel("ger_ocorrencias","data");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "data";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`data`";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_ocorrencias["data"] = $fdata;
//	link_ger_usuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "link_ger_usuario";
	$fdata["GoodName"] = "link_ger_usuario";
	$fdata["ownerTable"] = "ger_ocorrencias";
	$fdata["Label"] = GetFieldLabel("ger_ocorrencias","link_ger_usuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "link_ger_usuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_usuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_ocorrencias["link_ger_usuario"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_ocorrencias";
	$fdata["Label"] = GetFieldLabel("ger_ocorrencias","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_ocorrencias["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_ocorrencias";
	$fdata["Label"] = GetFieldLabel("ger_ocorrencias","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_ocorrencias["ultimaalteracao"] = $fdata;
//	pendente
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "pendente";
	$fdata["GoodName"] = "pendente";
	$fdata["ownerTable"] = "ger_ocorrencias";
	$fdata["Label"] = GetFieldLabel("ger_ocorrencias","pendente");
	$fdata["FieldType"] = 16;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "pendente";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pendente";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_ocorrencias["pendente"] = $fdata;
//	ocorrencia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ocorrencia";
	$fdata["GoodName"] = "ocorrencia";
	$fdata["ownerTable"] = "ger_ocorrencias";
	$fdata["Label"] = GetFieldLabel("ger_ocorrencias","ocorrencia");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ocorrencia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ocorrencia";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 250;
			$edata["nCols"] = 500;

	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_ocorrencias["ocorrencia"] = $fdata;


$tables_data["ger_ocorrencias"]=&$tdatager_ocorrencias;
$field_labels["ger_ocorrencias"] = &$fieldLabelsger_ocorrencias;
$fieldToolTips["ger_ocorrencias"] = &$fieldToolTipsger_ocorrencias;
$page_titles["ger_ocorrencias"] = &$pageTitlesger_ocorrencias;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_ocorrencias"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_ocorrencias"] = array();


	
				$strOriginalDetailsTable="ger_cadastro";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_cadastro";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_cadastro";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["ger_ocorrencias"][0] = $masterParams;
				$masterTablesData["ger_ocorrencias"][0]["masterKeys"] = array();
	$masterTablesData["ger_ocorrencias"][0]["masterKeys"][]="idCadastro";
				$masterTablesData["ger_ocorrencias"][0]["detailKeys"] = array();
	$masterTablesData["ger_ocorrencias"][0]["detailKeys"][]="link_ger_cadastro";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_ocorrencias()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idOcorrencia,  link_ger_cadastro,  `data`,  link_ger_usuario,  ultimousuario,  ultimaalteracao,  pendente,  ocorrencia";
$proto0["m_strFrom"] = "FROM ger_ocorrencias";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY pendente DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idOcorrencia",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto6["m_sql"] = "idOcorrencia";
$proto6["m_srcTableName"] = "ger_ocorrencias";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_cadastro",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto8["m_sql"] = "link_ger_cadastro";
$proto8["m_srcTableName"] = "ger_ocorrencias";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto10["m_sql"] = "`data`";
$proto10["m_srcTableName"] = "ger_ocorrencias";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_usuario",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto12["m_sql"] = "link_ger_usuario";
$proto12["m_srcTableName"] = "ger_ocorrencias";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto14["m_sql"] = "ultimousuario";
$proto14["m_srcTableName"] = "ger_ocorrencias";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto16["m_sql"] = "ultimaalteracao";
$proto16["m_srcTableName"] = "ger_ocorrencias";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "pendente",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto18["m_sql"] = "pendente";
$proto18["m_srcTableName"] = "ger_ocorrencias";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ocorrencia",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto20["m_sql"] = "ocorrencia";
$proto20["m_srcTableName"] = "ger_ocorrencias";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "ger_ocorrencias";
$proto23["m_srcTableName"] = "ger_ocorrencias";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "idOcorrencia";
$proto23["m_columns"][] = "ocorrencia";
$proto23["m_columns"][] = "link_ger_cadastro";
$proto23["m_columns"][] = "data";
$proto23["m_columns"][] = "link_ger_usuario";
$proto23["m_columns"][] = "ultimousuario";
$proto23["m_columns"][] = "ultimaalteracao";
$proto23["m_columns"][] = "pendente";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "ger_ocorrencias";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "ger_ocorrencias";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto26=array();
						$obj = new SQLField(array(
	"m_strName" => "pendente",
	"m_strTable" => "ger_ocorrencias",
	"m_srcTableName" => "ger_ocorrencias"
));

$proto26["m_column"]=$obj;
$proto26["m_bAsc"] = 0;
$proto26["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto26);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="ger_ocorrencias";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_ocorrencias = createSqlQuery_ger_ocorrencias();


	
		;

								

$tdatager_ocorrencias[".sqlquery"] = $queryData_ger_ocorrencias;

include_once(getabspath("include/ger_ocorrencias_events.php"));
$tableEvents["ger_ocorrencias"] = new eventclass_ger_ocorrencias;
$tdatager_ocorrencias[".hasEvents"] = true;

?>