<?php
require_once(getabspath("classes/cipherer.php"));




$tdataloginattempts = array();
	$tdataloginattempts[".truncateText"] = true;
	$tdataloginattempts[".NumberOfChars"] = 80;
	$tdataloginattempts[".ShortName"] = "loginattempts";
	$tdataloginattempts[".OwnerID"] = "";
	$tdataloginattempts[".OriginalTable"] = "loginattempts";

//	field labels
$fieldLabelsloginattempts = array();
$fieldToolTipsloginattempts = array();
$pageTitlesloginattempts = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsloginattempts["Portuguese(Brazil)"] = array();
	$fieldToolTipsloginattempts["Portuguese(Brazil)"] = array();
	$pageTitlesloginattempts["Portuguese(Brazil)"] = array();
	$fieldLabelsloginattempts["Portuguese(Brazil)"]["ip"] = "Ip";
	$fieldToolTipsloginattempts["Portuguese(Brazil)"]["ip"] = "";
	$fieldLabelsloginattempts["Portuguese(Brazil)"]["attempts"] = "Tentativas";
	$fieldToolTipsloginattempts["Portuguese(Brazil)"]["attempts"] = "";
	$fieldLabelsloginattempts["Portuguese(Brazil)"]["lastlogin"] = "Login";
	$fieldToolTipsloginattempts["Portuguese(Brazil)"]["lastlogin"] = "";
	if (count($fieldToolTipsloginattempts["Portuguese(Brazil)"]))
		$tdataloginattempts[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsloginattempts[""] = array();
	$fieldToolTipsloginattempts[""] = array();
	$pageTitlesloginattempts[""] = array();
	if (count($fieldToolTipsloginattempts[""]))
		$tdataloginattempts[".isUseToolTips"] = true;
}


	$tdataloginattempts[".NCSearch"] = true;



$tdataloginattempts[".shortTableName"] = "loginattempts";
$tdataloginattempts[".nSecOptions"] = 0;
$tdataloginattempts[".recsPerRowList"] = 1;
$tdataloginattempts[".recsPerRowPrint"] = 1;
$tdataloginattempts[".mainTableOwnerID"] = "";
$tdataloginattempts[".moveNext"] = 1;
$tdataloginattempts[".entityType"] = 0;

$tdataloginattempts[".strOriginalTableName"] = "loginattempts";





$tdataloginattempts[".showAddInPopup"] = false;

$tdataloginattempts[".showEditInPopup"] = false;

$tdataloginattempts[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataloginattempts[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataloginattempts[".fieldsForRegister"] = array();

$tdataloginattempts[".listAjax"] = false;

	$tdataloginattempts[".audit"] = true;

	$tdataloginattempts[".locking"] = true;



$tdataloginattempts[".list"] = true;




$tdataloginattempts[".printFriendly"] = true;

$tdataloginattempts[".delete"] = true;

$tdataloginattempts[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataloginattempts[".searchSaving"] = false;
//

$tdataloginattempts[".showSearchPanel"] = true;
		$tdataloginattempts[".flexibleSearch"] = true;

if (isMobile())
	$tdataloginattempts[".isUseAjaxSuggest"] = false;
else
	$tdataloginattempts[".isUseAjaxSuggest"] = true;

$tdataloginattempts[".rowHighlite"] = true;



$tdataloginattempts[".addPageEvents"] = false;

// use timepicker for search panel
$tdataloginattempts[".isUseTimeForSearch"] = false;





$tdataloginattempts[".allSearchFields"] = array();
$tdataloginattempts[".filterFields"] = array();
$tdataloginattempts[".requiredSearchFields"] = array();

$tdataloginattempts[".allSearchFields"][] = "ip";
	$tdataloginattempts[".allSearchFields"][] = "attempts";
	$tdataloginattempts[".allSearchFields"][] = "lastlogin";
	

$tdataloginattempts[".googleLikeFields"] = array();
$tdataloginattempts[".googleLikeFields"][] = "ip";
$tdataloginattempts[".googleLikeFields"][] = "attempts";
$tdataloginattempts[".googleLikeFields"][] = "lastlogin";

$tdataloginattempts[".panelSearchFields"] = array();
$tdataloginattempts[".searchPanelOptions"] = array();
$tdataloginattempts[".panelSearchFields"][] = "ip";
	$tdataloginattempts[".panelSearchFields"][] = "attempts";
	$tdataloginattempts[".panelSearchFields"][] = "lastlogin";
	
$tdataloginattempts[".advSearchFields"] = array();
$tdataloginattempts[".advSearchFields"][] = "ip";
$tdataloginattempts[".advSearchFields"][] = "attempts";
$tdataloginattempts[".advSearchFields"][] = "lastlogin";

$tdataloginattempts[".tableType"] = "list";

$tdataloginattempts[".printerPageOrientation"] = 0;
$tdataloginattempts[".nPrinterPageScale"] = 100;

$tdataloginattempts[".nPrinterSplitRecords"] = 40;

$tdataloginattempts[".nPrinterPDFSplitRecords"] = 40;



$tdataloginattempts[".geocodingEnabled"] = false;





$tdataloginattempts[".listGridLayout"] = 3;

$tdataloginattempts[".isDisplayLoading"] = true;


$tdataloginattempts[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdataloginattempts[".pageSize"] = 20;

$tdataloginattempts[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataloginattempts[".strOrderBy"] = $tstrOrderBy;

$tdataloginattempts[".orderindexes"] = array();

$tdataloginattempts[".sqlHead"] = "SELECT ip,  	attempts,  	lastlogin";
$tdataloginattempts[".sqlFrom"] = "FROM loginattempts";
$tdataloginattempts[".sqlWhereExpr"] = "";
$tdataloginattempts[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataloginattempts[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataloginattempts[".arrGroupsPerPage"] = $arrGPP;

$tdataloginattempts[".highlightSearchResults"] = true;

$tableKeysloginattempts = array();
$tableKeysloginattempts[] = "ip";
$tdataloginattempts[".Keys"] = $tableKeysloginattempts;

$tdataloginattempts[".listFields"] = array();
$tdataloginattempts[".listFields"][] = "ip";
$tdataloginattempts[".listFields"][] = "attempts";
$tdataloginattempts[".listFields"][] = "lastlogin";

$tdataloginattempts[".hideMobileList"] = array();


$tdataloginattempts[".viewFields"] = array();

$tdataloginattempts[".addFields"] = array();

$tdataloginattempts[".masterListFields"] = array();

$tdataloginattempts[".inlineAddFields"] = array();

$tdataloginattempts[".editFields"] = array();

$tdataloginattempts[".inlineEditFields"] = array();

$tdataloginattempts[".exportFields"] = array();

$tdataloginattempts[".importFields"] = array();
$tdataloginattempts[".importFields"][] = "ip";
$tdataloginattempts[".importFields"][] = "attempts";
$tdataloginattempts[".importFields"][] = "lastlogin";

$tdataloginattempts[".printFields"] = array();
$tdataloginattempts[".printFields"][] = "ip";
$tdataloginattempts[".printFields"][] = "attempts";
$tdataloginattempts[".printFields"][] = "lastlogin";

//	ip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ip";
	$fdata["GoodName"] = "ip";
	$fdata["ownerTable"] = "loginattempts";
	$fdata["Label"] = GetFieldLabel("loginattempts","ip");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "ip";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ip";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataloginattempts["ip"] = $fdata;
//	attempts
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "attempts";
	$fdata["GoodName"] = "attempts";
	$fdata["ownerTable"] = "loginattempts";
	$fdata["Label"] = GetFieldLabel("loginattempts","attempts");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "attempts";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "attempts";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataloginattempts["attempts"] = $fdata;
//	lastlogin
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "lastlogin";
	$fdata["GoodName"] = "lastlogin";
	$fdata["ownerTable"] = "loginattempts";
	$fdata["Label"] = GetFieldLabel("loginattempts","lastlogin");
	$fdata["FieldType"] = 135;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "lastlogin";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lastlogin";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataloginattempts["lastlogin"] = $fdata;


$tables_data["loginattempts"]=&$tdataloginattempts;
$field_labels["loginattempts"] = &$fieldLabelsloginattempts;
$fieldToolTips["loginattempts"] = &$fieldToolTipsloginattempts;
$page_titles["loginattempts"] = &$pageTitlesloginattempts;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["loginattempts"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["loginattempts"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_loginattempts()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ip,  	attempts,  	lastlogin";
$proto0["m_strFrom"] = "FROM loginattempts";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ip",
	"m_strTable" => "loginattempts",
	"m_srcTableName" => "loginattempts"
));

$proto6["m_sql"] = "ip";
$proto6["m_srcTableName"] = "loginattempts";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "attempts",
	"m_strTable" => "loginattempts",
	"m_srcTableName" => "loginattempts"
));

$proto8["m_sql"] = "attempts";
$proto8["m_srcTableName"] = "loginattempts";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "lastlogin",
	"m_strTable" => "loginattempts",
	"m_srcTableName" => "loginattempts"
));

$proto10["m_sql"] = "lastlogin";
$proto10["m_srcTableName"] = "loginattempts";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "loginattempts";
$proto13["m_srcTableName"] = "loginattempts";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "ip";
$proto13["m_columns"][] = "attempts";
$proto13["m_columns"][] = "lastlogin";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "loginattempts";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "loginattempts";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="loginattempts";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_loginattempts = createSqlQuery_loginattempts();


	
		;

			

$tdataloginattempts[".sqlquery"] = $queryData_loginattempts;

$tableEvents["loginattempts"] = new eventsBase;
$tdataloginattempts[".hasEvents"] = false;

?>