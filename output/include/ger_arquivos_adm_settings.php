<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_arquivos_adm = array();
	$tdatager_arquivos_adm[".truncateText"] = true;
	$tdatager_arquivos_adm[".NumberOfChars"] = 80;
	$tdatager_arquivos_adm[".ShortName"] = "ger_arquivos_adm";
	$tdatager_arquivos_adm[".OwnerID"] = "";
	$tdatager_arquivos_adm[".OriginalTable"] = "ger_arquivos_adm";

//	field labels
$fieldLabelsger_arquivos_adm = array();
$fieldToolTipsger_arquivos_adm = array();
$pageTitlesger_arquivos_adm = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_arquivos_adm["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"] = array();
	$pageTitlesger_arquivos_adm["Portuguese(Brazil)"] = array();
	$fieldLabelsger_arquivos_adm["Portuguese(Brazil)"]["idArquivosAdm"] = "ArquivoAdm";
	$fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"]["idArquivosAdm"] = "";
	$fieldLabelsger_arquivos_adm["Portuguese(Brazil)"]["nomedoc"] = "Nome";
	$fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"]["nomedoc"] = "";
	$fieldLabelsger_arquivos_adm["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsger_arquivos_adm["Portuguese(Brazil)"]["arquivo"] = "Arquivo";
	$fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"]["arquivo"] = "";
	$fieldLabelsger_arquivos_adm["Portuguese(Brazil)"]["link_ger_tipos_arquivo"] = "Tipo";
	$fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"]["link_ger_tipos_arquivo"] = "";
	$fieldLabelsger_arquivos_adm["Portuguese(Brazil)"]["ultimousuario"] = "Ultimo usuário";
	$fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_arquivos_adm["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima alteração";
	$fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_arquivos_adm["Portuguese(Brazil)"]))
		$tdatager_arquivos_adm[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_arquivos_adm[""] = array();
	$fieldToolTipsger_arquivos_adm[""] = array();
	$pageTitlesger_arquivos_adm[""] = array();
	if (count($fieldToolTipsger_arquivos_adm[""]))
		$tdatager_arquivos_adm[".isUseToolTips"] = true;
}


	$tdatager_arquivos_adm[".NCSearch"] = true;



$tdatager_arquivos_adm[".shortTableName"] = "ger_arquivos_adm";
$tdatager_arquivos_adm[".nSecOptions"] = 0;
$tdatager_arquivos_adm[".recsPerRowList"] = 1;
$tdatager_arquivos_adm[".recsPerRowPrint"] = 1;
$tdatager_arquivos_adm[".mainTableOwnerID"] = "";
$tdatager_arquivos_adm[".moveNext"] = 1;
$tdatager_arquivos_adm[".entityType"] = 0;

$tdatager_arquivos_adm[".strOriginalTableName"] = "ger_arquivos_adm";





$tdatager_arquivos_adm[".showAddInPopup"] = false;

$tdatager_arquivos_adm[".showEditInPopup"] = false;

$tdatager_arquivos_adm[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_arquivos_adm[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_arquivos_adm[".fieldsForRegister"] = array();

$tdatager_arquivos_adm[".listAjax"] = false;

	$tdatager_arquivos_adm[".audit"] = true;

	$tdatager_arquivos_adm[".locking"] = true;

$tdatager_arquivos_adm[".edit"] = true;
$tdatager_arquivos_adm[".afterEditAction"] = 1;
$tdatager_arquivos_adm[".closePopupAfterEdit"] = 1;
$tdatager_arquivos_adm[".afterEditActionDetTable"] = "";

$tdatager_arquivos_adm[".add"] = true;
$tdatager_arquivos_adm[".afterAddAction"] = 1;
$tdatager_arquivos_adm[".closePopupAfterAdd"] = 1;
$tdatager_arquivos_adm[".afterAddActionDetTable"] = "";

$tdatager_arquivos_adm[".list"] = true;

$tdatager_arquivos_adm[".view"] = true;


$tdatager_arquivos_adm[".exportTo"] = true;

$tdatager_arquivos_adm[".printFriendly"] = true;

$tdatager_arquivos_adm[".delete"] = true;

$tdatager_arquivos_adm[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_arquivos_adm[".searchSaving"] = false;
//

$tdatager_arquivos_adm[".showSearchPanel"] = true;
		$tdatager_arquivos_adm[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_arquivos_adm[".isUseAjaxSuggest"] = false;
else
	$tdatager_arquivos_adm[".isUseAjaxSuggest"] = true;

$tdatager_arquivos_adm[".rowHighlite"] = true;



$tdatager_arquivos_adm[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_arquivos_adm[".isUseTimeForSearch"] = false;





$tdatager_arquivos_adm[".allSearchFields"] = array();
$tdatager_arquivos_adm[".filterFields"] = array();
$tdatager_arquivos_adm[".requiredSearchFields"] = array();

$tdatager_arquivos_adm[".allSearchFields"][] = "nomedoc";
	$tdatager_arquivos_adm[".allSearchFields"][] = "obs";
	$tdatager_arquivos_adm[".allSearchFields"][] = "arquivo";
	$tdatager_arquivos_adm[".allSearchFields"][] = "link_ger_tipos_arquivo";
	$tdatager_arquivos_adm[".allSearchFields"][] = "ultimousuario";
	$tdatager_arquivos_adm[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_arquivos_adm[".googleLikeFields"] = array();
$tdatager_arquivos_adm[".googleLikeFields"][] = "nomedoc";
$tdatager_arquivos_adm[".googleLikeFields"][] = "obs";
$tdatager_arquivos_adm[".googleLikeFields"][] = "arquivo";
$tdatager_arquivos_adm[".googleLikeFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_adm[".googleLikeFields"][] = "ultimousuario";
$tdatager_arquivos_adm[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_arquivos_adm[".advSearchFields"] = array();
$tdatager_arquivos_adm[".advSearchFields"][] = "nomedoc";
$tdatager_arquivos_adm[".advSearchFields"][] = "obs";
$tdatager_arquivos_adm[".advSearchFields"][] = "arquivo";
$tdatager_arquivos_adm[".advSearchFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_adm[".advSearchFields"][] = "ultimousuario";
$tdatager_arquivos_adm[".advSearchFields"][] = "ultimaalteracao";

$tdatager_arquivos_adm[".tableType"] = "list";

$tdatager_arquivos_adm[".printerPageOrientation"] = 0;
$tdatager_arquivos_adm[".nPrinterPageScale"] = 100;

$tdatager_arquivos_adm[".nPrinterSplitRecords"] = 40;

$tdatager_arquivos_adm[".nPrinterPDFSplitRecords"] = 40;



$tdatager_arquivos_adm[".geocodingEnabled"] = false;





$tdatager_arquivos_adm[".listGridLayout"] = 3;

$tdatager_arquivos_adm[".isDisplayLoading"] = true;


$tdatager_arquivos_adm[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_arquivos_adm[".pageSize"] = 20;

$tdatager_arquivos_adm[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_arquivos_adm[".strOrderBy"] = $tstrOrderBy;

$tdatager_arquivos_adm[".orderindexes"] = array();

$tdatager_arquivos_adm[".sqlHead"] = "SELECT idArquivosAdm,  	nomedoc,  	obs,  	arquivo,  	link_ger_tipos_arquivo,  	ultimousuario,  	ultimaalteracao";
$tdatager_arquivos_adm[".sqlFrom"] = "FROM ger_arquivos_adm";
$tdatager_arquivos_adm[".sqlWhereExpr"] = "";
$tdatager_arquivos_adm[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_arquivos_adm[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_arquivos_adm[".arrGroupsPerPage"] = $arrGPP;

$tdatager_arquivos_adm[".highlightSearchResults"] = true;

$tableKeysger_arquivos_adm = array();
$tableKeysger_arquivos_adm[] = "idArquivosAdm";
$tdatager_arquivos_adm[".Keys"] = $tableKeysger_arquivos_adm;

$tdatager_arquivos_adm[".listFields"] = array();
$tdatager_arquivos_adm[".listFields"][] = "nomedoc";
$tdatager_arquivos_adm[".listFields"][] = "obs";
$tdatager_arquivos_adm[".listFields"][] = "arquivo";
$tdatager_arquivos_adm[".listFields"][] = "link_ger_tipos_arquivo";

$tdatager_arquivos_adm[".hideMobileList"] = array();


$tdatager_arquivos_adm[".viewFields"] = array();
$tdatager_arquivos_adm[".viewFields"][] = "nomedoc";
$tdatager_arquivos_adm[".viewFields"][] = "obs";
$tdatager_arquivos_adm[".viewFields"][] = "arquivo";
$tdatager_arquivos_adm[".viewFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_adm[".viewFields"][] = "ultimousuario";
$tdatager_arquivos_adm[".viewFields"][] = "ultimaalteracao";

$tdatager_arquivos_adm[".addFields"] = array();
$tdatager_arquivos_adm[".addFields"][] = "nomedoc";
$tdatager_arquivos_adm[".addFields"][] = "obs";
$tdatager_arquivos_adm[".addFields"][] = "arquivo";
$tdatager_arquivos_adm[".addFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_adm[".addFields"][] = "ultimousuario";
$tdatager_arquivos_adm[".addFields"][] = "ultimaalteracao";

$tdatager_arquivos_adm[".masterListFields"] = array();

$tdatager_arquivos_adm[".inlineAddFields"] = array();

$tdatager_arquivos_adm[".editFields"] = array();
$tdatager_arquivos_adm[".editFields"][] = "nomedoc";
$tdatager_arquivos_adm[".editFields"][] = "obs";
$tdatager_arquivos_adm[".editFields"][] = "arquivo";
$tdatager_arquivos_adm[".editFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_adm[".editFields"][] = "ultimousuario";
$tdatager_arquivos_adm[".editFields"][] = "ultimaalteracao";

$tdatager_arquivos_adm[".inlineEditFields"] = array();

$tdatager_arquivos_adm[".exportFields"] = array();
$tdatager_arquivos_adm[".exportFields"][] = "nomedoc";
$tdatager_arquivos_adm[".exportFields"][] = "obs";
$tdatager_arquivos_adm[".exportFields"][] = "arquivo";
$tdatager_arquivos_adm[".exportFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_adm[".exportFields"][] = "ultimousuario";
$tdatager_arquivos_adm[".exportFields"][] = "ultimaalteracao";

$tdatager_arquivos_adm[".importFields"] = array();
$tdatager_arquivos_adm[".importFields"][] = "idArquivosAdm";
$tdatager_arquivos_adm[".importFields"][] = "nomedoc";
$tdatager_arquivos_adm[".importFields"][] = "obs";
$tdatager_arquivos_adm[".importFields"][] = "arquivo";
$tdatager_arquivos_adm[".importFields"][] = "link_ger_tipos_arquivo";
$tdatager_arquivos_adm[".importFields"][] = "ultimousuario";
$tdatager_arquivos_adm[".importFields"][] = "ultimaalteracao";

$tdatager_arquivos_adm[".printFields"] = array();
$tdatager_arquivos_adm[".printFields"][] = "nomedoc";
$tdatager_arquivos_adm[".printFields"][] = "obs";
$tdatager_arquivos_adm[".printFields"][] = "arquivo";
$tdatager_arquivos_adm[".printFields"][] = "link_ger_tipos_arquivo";

//	idArquivosAdm
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idArquivosAdm";
	$fdata["GoodName"] = "idArquivosAdm";
	$fdata["ownerTable"] = "ger_arquivos_adm";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_adm","idArquivosAdm");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idArquivosAdm";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idArquivosAdm";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_arquivos_adm["idArquivosAdm"] = $fdata;
//	nomedoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nomedoc";
	$fdata["GoodName"] = "nomedoc";
	$fdata["ownerTable"] = "ger_arquivos_adm";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_adm","nomedoc");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nomedoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nomedoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=45";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_adm["nomedoc"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_arquivos_adm";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_adm","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_adm["obs"] = $fdata;
//	arquivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "arquivo";
	$fdata["GoodName"] = "arquivo";
	$fdata["ownerTable"] = "ger_arquivos_adm";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_adm","arquivo");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "arquivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "arquivo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "arquivos";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
	



		$edata["IsRequired"] = true;

	
		$edata["UseTimestamp"] = true;

	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_adm["arquivo"] = $fdata;
//	link_ger_tipos_arquivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "link_ger_tipos_arquivo";
	$fdata["GoodName"] = "link_ger_tipos_arquivo";
	$fdata["ownerTable"] = "ger_arquivos_adm";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_adm","link_ger_tipos_arquivo");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_tipos_arquivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_tipos_arquivo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_tipodocs";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idTipodocs";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "tipodoc";

	
	$edata["LookupOrderBy"] = "tipodoc";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_adm["link_ger_tipos_arquivo"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_arquivos_adm";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_adm","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_adm["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_arquivos_adm";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_adm","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_adm["ultimaalteracao"] = $fdata;


$tables_data["ger_arquivos_adm"]=&$tdatager_arquivos_adm;
$field_labels["ger_arquivos_adm"] = &$fieldLabelsger_arquivos_adm;
$fieldToolTips["ger_arquivos_adm"] = &$fieldToolTipsger_arquivos_adm;
$page_titles["ger_arquivos_adm"] = &$pageTitlesger_arquivos_adm;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_arquivos_adm"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_arquivos_adm"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_arquivos_adm()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idArquivosAdm,  	nomedoc,  	obs,  	arquivo,  	link_ger_tipos_arquivo,  	ultimousuario,  	ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_arquivos_adm";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idArquivosAdm",
	"m_strTable" => "ger_arquivos_adm",
	"m_srcTableName" => "ger_arquivos_adm"
));

$proto6["m_sql"] = "idArquivosAdm";
$proto6["m_srcTableName"] = "ger_arquivos_adm";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nomedoc",
	"m_strTable" => "ger_arquivos_adm",
	"m_srcTableName" => "ger_arquivos_adm"
));

$proto8["m_sql"] = "nomedoc";
$proto8["m_srcTableName"] = "ger_arquivos_adm";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_arquivos_adm",
	"m_srcTableName" => "ger_arquivos_adm"
));

$proto10["m_sql"] = "obs";
$proto10["m_srcTableName"] = "ger_arquivos_adm";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "arquivo",
	"m_strTable" => "ger_arquivos_adm",
	"m_srcTableName" => "ger_arquivos_adm"
));

$proto12["m_sql"] = "arquivo";
$proto12["m_srcTableName"] = "ger_arquivos_adm";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_tipos_arquivo",
	"m_strTable" => "ger_arquivos_adm",
	"m_srcTableName" => "ger_arquivos_adm"
));

$proto14["m_sql"] = "link_ger_tipos_arquivo";
$proto14["m_srcTableName"] = "ger_arquivos_adm";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_arquivos_adm",
	"m_srcTableName" => "ger_arquivos_adm"
));

$proto16["m_sql"] = "ultimousuario";
$proto16["m_srcTableName"] = "ger_arquivos_adm";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_arquivos_adm",
	"m_srcTableName" => "ger_arquivos_adm"
));

$proto18["m_sql"] = "ultimaalteracao";
$proto18["m_srcTableName"] = "ger_arquivos_adm";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "ger_arquivos_adm";
$proto21["m_srcTableName"] = "ger_arquivos_adm";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "idArquivosAdm";
$proto21["m_columns"][] = "nomedoc";
$proto21["m_columns"][] = "obs";
$proto21["m_columns"][] = "arquivo";
$proto21["m_columns"][] = "link_ger_tipos_arquivo";
$proto21["m_columns"][] = "ultimousuario";
$proto21["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "ger_arquivos_adm";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "ger_arquivos_adm";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_arquivos_adm";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_arquivos_adm = createSqlQuery_ger_arquivos_adm();


	
		;

							

$tdatager_arquivos_adm[".sqlquery"] = $queryData_ger_arquivos_adm;

$tableEvents["ger_arquivos_adm"] = new eventsBase;
$tdatager_arquivos_adm[".hasEvents"] = false;

?>