<?php

$dal_info = array();

/**
 * User API function
 * @param String sql
 */
function UsersTableName()
{
	global $cman;
	$connection = $cman->getForLogin();
	return $connection->addTableWrappers("ger_usuarios");
}

/**
 * User API function
 * It uses the default db connection
 * @param String dalSQL
 */
function CustomQuery($dalSQL)
{
	$connection = getDefaultConnection();
	$result = $connection->query( $dalSQL );
	if($result)
		return $result;
}

/**
 * User API function
 * It uses the default db connection 
 * @param String sql
 */
function DBLookup($sql)
{
	$connection = getDefaultConnection();
	$data = $connection->query( $sql )->fetchAssoc();

	if( $data )
		return reset($data);
	  
	return null;
}

/**
  * Data Access Layer.
  */
class tDAL
{
	var $tblTables__exp_ger_lista_logs;
	var $tblTables__exp_lista_foruns;
	var $tblTables__fin_bco_ext1;
	var $tblTables__fin_cheques;
	var $tblTables__fin_cobranca;
	var $tblTables__fin_cobranca_atualizacao_aberto;
	var $tblTables__fin_contas;
	var $tblTables__fin_correcao_monetaria;
	var $tblTables__fin_debitos_contador;
	var $tblTables__fin_debitos_resumo;
	var $tblTables__fin_despesas;
	var $tblTables__fin_fornecedores;
	var $tblTables__fin_meiospgto;
	var $tblTables__fin_tipodocs;
	var $tblTables__fin_tipogastos;
	var $tblTables__ger_arquivos_adm;
	var $tblTables__ger_arquivos_geral;
	var $tblTables__ger_arquivos_grupos;
	var $tblTables__ger_arquivos_unidades;
	var $tblTables__ger_cadastro;
	var $tblTables__ger_cadastro_moradores_plista;
	var $tblTables__ger_controle_de_acessos;
	var $tblTables__ger_grupo;
	var $tblTables__ger_lista_relacao_com_unidade;
	var $tblTables__ger_lista_uf;
	var $tblTables__ger_moradores;
	var $tblTables__ger_ocorrencias;
	var $tblTables__ger_op_inserirtdboleto;
	var $tblTables__ger_selecao_ger_unidades;
	var $tblTables__ger_selecao_guni_ger_unidades;
	var $tblTables__ger_unidades;
	var $tblTables__ger_usuarios;
	var $tblTables__ger_usuarios_uggroups;
	var $tblTables__ger_usuarios_ugmembers;
	var $tblTables__ger_usuarios_ugrights;
	var $tblTables__gestor_audit;
	var $tblTables__gestor_locking;
	var $tblTables__jur_processos;
	var $tblTables__loginattempts;
	var $lstTables;
	var $Table = array();

	function FillTablesList()
	{
		if($this->lstTables)
			return;
		$this->lstTables[] = array("name" => "exp_ger_lista_logs", "varname" => "Tables__exp_ger_lista_logs", "altvarname" => "exp_ger_lista_logs", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "exp_lista_foruns", "varname" => "Tables__exp_lista_foruns", "altvarname" => "exp_lista_foruns", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_bco_ext1", "varname" => "Tables__fin_bco_ext1", "altvarname" => "fin_bco_ext1", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_cheques", "varname" => "Tables__fin_cheques", "altvarname" => "fin_cheques", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_cobranca", "varname" => "Tables__fin_cobranca", "altvarname" => "fin_cobranca", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_cobranca_atualizacao_aberto", "varname" => "Tables__fin_cobranca_atualizacao_aberto", "altvarname" => "fin_cobranca_atualizacao_aberto", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_contas", "varname" => "Tables__fin_contas", "altvarname" => "fin_contas", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_correcao_monetaria", "varname" => "Tables__fin_correcao_monetaria", "altvarname" => "fin_correcao_monetaria", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_debitos_contador", "varname" => "Tables__fin_debitos_contador", "altvarname" => "fin_debitos_contador", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_debitos_resumo", "varname" => "Tables__fin_debitos_resumo", "altvarname" => "fin_debitos_resumo", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_despesas", "varname" => "Tables__fin_despesas", "altvarname" => "fin_despesas", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_fornecedores", "varname" => "Tables__fin_fornecedores", "altvarname" => "fin_fornecedores", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_meiospgto", "varname" => "Tables__fin_meiospgto", "altvarname" => "fin_meiospgto", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_tipodocs", "varname" => "Tables__fin_tipodocs", "altvarname" => "fin_tipodocs", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "fin_tipogastos", "varname" => "Tables__fin_tipogastos", "altvarname" => "fin_tipogastos", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_arquivos_adm", "varname" => "Tables__ger_arquivos_adm", "altvarname" => "ger_arquivos_adm", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_arquivos_geral", "varname" => "Tables__ger_arquivos_geral", "altvarname" => "ger_arquivos_geral", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_arquivos_grupos", "varname" => "Tables__ger_arquivos_grupos", "altvarname" => "ger_arquivos_grupos", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_arquivos_unidades", "varname" => "Tables__ger_arquivos_unidades", "altvarname" => "ger_arquivos_unidades", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_cadastro", "varname" => "Tables__ger_cadastro", "altvarname" => "ger_cadastro", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_cadastro_moradores_plista", "varname" => "Tables__ger_cadastro_moradores_plista", "altvarname" => "ger_cadastro_moradores_plista", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_controle_de_acessos", "varname" => "Tables__ger_controle_de_acessos", "altvarname" => "ger_controle_de_acessos", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_grupo", "varname" => "Tables__ger_grupo", "altvarname" => "ger_grupo", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_lista_relacao_com_unidade", "varname" => "Tables__ger_lista_relacao_com_unidade", "altvarname" => "ger_lista_relacao_com_unidade", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_lista_uf", "varname" => "Tables__ger_lista_uf", "altvarname" => "ger_lista_uf", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_moradores", "varname" => "Tables__ger_moradores", "altvarname" => "ger_moradores", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_ocorrencias", "varname" => "Tables__ger_ocorrencias", "altvarname" => "ger_ocorrencias", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_op_inserirtdboleto", "varname" => "Tables__ger_op_inserirtdboleto", "altvarname" => "ger_op_inserirtdboleto", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_selecao_ger_unidades", "varname" => "Tables__ger_selecao_ger_unidades", "altvarname" => "ger_selecao_ger_unidades", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_selecao_guni_ger_unidades", "varname" => "Tables__ger_selecao_guni_ger_unidades", "altvarname" => "ger_selecao_guni_ger_unidades", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_unidades", "varname" => "Tables__ger_unidades", "altvarname" => "ger_unidades", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_usuarios", "varname" => "Tables__ger_usuarios", "altvarname" => "ger_usuarios", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_usuarios_uggroups", "varname" => "Tables__ger_usuarios_uggroups", "altvarname" => "ger_usuarios_uggroups", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_usuarios_ugmembers", "varname" => "Tables__ger_usuarios_ugmembers", "altvarname" => "ger_usuarios_ugmembers", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "ger_usuarios_ugrights", "varname" => "Tables__ger_usuarios_ugrights", "altvarname" => "ger_usuarios_ugrights", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "gestor_audit", "varname" => "Tables__gestor_audit", "altvarname" => "gestor_audit", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "gestor_locking", "varname" => "Tables__gestor_locking", "altvarname" => "gestor_locking", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "jur_processos", "varname" => "Tables__jur_processos", "altvarname" => "jur_processos", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
		$this->lstTables[] = array("name" => "loginattempts", "varname" => "Tables__loginattempts", "altvarname" => "loginattempts", "connId" => "Tables", "schema" => "", "connName" => "MySQL");
	}

	/**
      * Returns table object by table name.
      * @intellisense
      */
	function & Table($strTable, $schema = "", $connName = "")
	{
		$this->FillTablesList();
		foreach($this->lstTables as $tbl)
		{
			if(strtoupper($strTable)==strtoupper($tbl["name"]) &&
				( schema == "" || strtoupper($schema) == strtoupper($tbl["schema"]) ) &&
				($connName == "" || strtoupper($connName) == strtoupper($tbl["connName"]) ) )
			{
				$this->CreateClass($tbl);
				return $this->{"tbl".$tbl["varname"]};
			}
		}
//	check table names without dbo. and other prefixes
		foreach($this->lstTables as $tbl)
		{
			if(strtoupper(cutprefix($strTable))==strtoupper(cutprefix($tbl["name"])))
			{
				$this->CreateClass($tbl);
				return $this->{"tbl".$tbl["varname"]};
			}
		}
		$dummy=null;
		return $dummy;
	}
	
	function CreateClass(&$tbl)
	{
		if($this->{"tbl".$tbl["varname"]})
			return;
//	load table info
		global $dal_info;
		include(getabspath("include/dal/".($tbl["varname"]).".php"));
//	create class and object

		$classname="class_".$tbl["varname"];
		$str = "class ".$classname." extends tDALTable  {";
		foreach($dal_info[$tbl["varname"]] as $fld)
		{
			$str.=' var $'.$fld["varname"].'; ';
		}
		
		$tableName = $tbl["name"];
		if( $tbl["schema"] )
			$tableName = $tbl["schema"] . "." . $tbl["name"];
		$str.=' function '.$classname.'()
			{
				$this->m_TableName = \''.escapesq( $tableName ).'\';
				$this->infoKey = \'' . $tbl["varname"] . '\';
				$this->setConnection('. $tbl["connId"] .');
			}
		};';
		eval($str);
		$this->{"tbl".$tbl["varname"]} = new $classname;
		$this->{$tbl["altvarname"]} = $this->{"tbl".$tbl["varname"]};
		$this->Table[$tbl["name"]]=&$this->{"tbl".$tbl["varname"]};
	}
	
	/**
      * Returns list of table names.
      * @intellisense
      */
	function GetTablesList()
	{
		$this->FillTablesList();
		$res=array();
		foreach($this->lstTables as $tbl)
			$res[]=$tbl["name"];
		return $res;
	}
	
	/**
      * Get list of table fields by table name.
      * @intellisense
      */
	function GetFieldsList($table)
	{
		$tbl = $this->Table($table);
		return $tbl->GetFieldsList();
	}
	
	/**
      * Get field type by table name and field name.
      * @intellisense
      */
	function GetFieldType($table,$field)
	{
		$tbl = $this->Table($table);
		return $tbl->GetFieldType($field);
	}

	/**
      * Get table key fields by table name.
      * @intellisense
      */
	function GetDBTableKeys($table)
	{
		$tbl = $this->Table($table);
		return $tbl->GetDBTableKeys();
	}
}

$dal = new tDAL;

/**
 * Data Access Layer table class.
 */ 
class tDALTable
{
	var $m_TableName;
	var $infoKey;
	var $Param = array();
	var $Value = array();
	/**
	 * @type Connection
	 */
	var $_connection;
	
	/**
	 * Set the connection property
	 * @param String connId
	 */
	function setConnection( $connId )
	{
		global $cman;
		$this->_connection = $cman->byId( $connId );
	}
	
	/**
      * Get table key fields.
      * @intellisense
      */
	function GetDBTableKeys()
	{
		global $dal_info;
		if( !array_key_exists($this->infoKey, $dal_info) || !is_array($dal_info[ $this->infoKey ]) )
			return array();
		
		$ret = array();
		foreach($dal_info[ $this->infoKey ] as $fname=>$f)
		{
			if( @$f["key"] )
				$ret[] = $fname;
		}
		return $ret;
	}
	
	/**
      * Get list of table fields.
      * @intellisense
      */
	function GetFieldsList()
	{
		global $dal_info;
		return array_keys( $dal_info[ $this->infoKey ] );
	}

	/**
      * Get field type.
      * @intellisense
      */
	function GetFieldType($field)
	{
		global $dal_info;
		
		if( !array_key_exists( $field, $dal_info[ $this->infoKey ]) )
			return 200;
			
		return $dal_info[ $this->infoKey ][ $field ]["type"];
	}
	
	/**
	 *
	 */
	function PrepareValue($value, $type)
	{
		if( $this->_connection->dbType == nDATABASE_Oracle || $this->_connection->dbType == nDATABASE_DB2 || $this->_connection->dbType == nDATABASE_Informix )
		{
			if( IsBinaryType($type) )
			{
				if( $this->_connection->dbType == nDATABASE_Oracle )
					return "EMPTY_BLOB()";
					
				return "?";
			}
			
			if( $this->_connection->dbType == nDATABASE_Informix  && IsTextType($type) )
				return "?";
		}
	
		if( IsDateFieldType($type) )
		{
			if( !$value )
				return "null";
			else
				$this->_connection->addDateQuotes( $value );
		}
		
		if( NeedQuotes($type) )
			return $this->_connection->prepareString( $value );

		return 0 + $value;
	}
	
	/**
      * Get table name.
      * @intellisense
      */
	function TableName()
	{
		return $this->_connection->addTableWrappers( $this->m_TableName );
	} 

	/**
	 * @param Array blobs
	 * @param String dalSQL
	 * @param Array tableinfo
	 */
	protected function Execute_Query($blobs, $dalSQL, $tableinfo)
	{		
		$blobTypes = array();
		if( $this->_connection->dbType == nDATABASE_Informix )
		{		
			foreach( $blobs as $fname => $fvalue )
			{
				$blobTypes[ $fname ] = $tableinfo[ $fname ]["type"];
			}
		}

		$this->_connection->execWithBlobProcessing( $dalSQL, $blobs, $blobTypes );
	}

	/**
      * Add new record to the table.
      * @intellisense
      */
	function Add() 
	{
		global $dal_info;
		
		$insertFields = "";
		$insertValues = "";
		$tableinfo = &$dal_info[ $this->infoKey ];
		$blobs = array();
		//	prepare parameters		
		foreach($tableinfo as $fieldname => $fld)
		{
			if( isset($this->{$fld['varname']}) )
			{
				$this->Value[ $fieldname ] = $this->{$fld['varname']};
			}
			
			foreach($this->Value as $field => $value)
			{
				if( strtoupper($field) != strtoupper($fieldname) )
					continue;
					
				$insertFields.= $this->_connection->addFieldWrappers( $fieldname ).",";
				$insertValues.= $this->PrepareValue($value,$fld["type"]) . ",";
				
				if( $this->_connection->dbType == nDATABASE_Oracle || $this->_connection->dbType == nDATABASE_DB2 || $this->_connection->dbType == nDATABASE_Informix )
				{
					if( IsBinaryType( $fld["type"] ) )
						$blobs[ $fieldname ] = $value;
						
					if( $this->_connection->dbType == nDATABASE_Informix && IsTextType( $fld["type"] ) )
						$blobs[ $fieldname ] = $value;
				}
				break;
			}
		}
		//	prepare and exec SQL
		if( $insertFields != "" && $insertValues != "" )		
		{
			$insertFields = substr($insertFields, 0, -1);
			$insertValues = substr($insertValues, 0, -1);
			$dalSQL = "insert into ".$this->_connection->addTableWrappers( $this->m_TableName )." (".$insertFields.") values (".$insertValues.")";
			$this->Execute_Query($blobs, $dalSQL, $tableinfo);
		}
		//	cleanup		
	    $this->Reset();
	}

	/**
      * Query all records from the table.
      * @intellisense
      */
	function QueryAll()
	{
		$dalSQL = "select * from ".$this->_connection->addTableWrappers( $this->m_TableName );
		return $this->_connection->query( $dalSQL );
	}

	/**
      * Do a custom query on the table.
      * @intellisense
      */
	function Query($swhere = "", $orderby = "")
	{
		if ($swhere)
			$swhere = " where ".$swhere;
			
		if ($orderby)
			$orderby = " order by ".$orderby;
			
		$dalSQL = "select * from ".$this->_connection->addTableWrappers( $this->m_TableName ).$swhere.$orderby;
		return $this->_connection->query( $dalSQL );
	}

	/**
      * Delete a record from the table.
      * @intellisense
      */
	function Delete()
	{
		global $dal_info;
		
		$deleteFields = "";
		$tableinfo = &$dal_info[ $this->infoKey ];
		//	prepare parameters		
		foreach($tableinfo as $fieldname => $fld)
		{
			if( isset($this->{$fld['varname']}) )
			{
				$this->Param[ $fieldname ] = $this->{$fld['varname']};
			}
			
			foreach($this->Param as $field => $value)
			{
				if( strtoupper($field) != strtoupper($fieldname) )
					continue;
					
				$deleteFields.= $this->_connection->addFieldWrappers( $fieldname )."=". $this->PrepareValue($value, $fld["type"]) . " and ";
				break;
			}
		}
		//	do delete
		if ($deleteFields)
		{
			$deleteFields = substr($deleteFields, 0, -5);
			$dalSQL = "delete from ".$this->_connection->addTableWrappers( $this->m_TableName )." where ".$deleteFields;
			$this->_connection->exec( $dalSQL );
		}
	
		//	cleanup
	    $this->Reset();
	}

	/**
      * Reset table object.
      * @intellisense
      */
	function Reset()
	{
		global $dal_info;
		
		$this->Value = array();
		$this->Param = array();
		
		$tableinfo = &$dal_info[ $this->infoKey ];
		//	prepare parameters		
		foreach($tableinfo as $fieldname => $fld)
		{
			unset($this->{$fld["varname"]});
		}
	}	

	/**
      * Update record in the table.
      * @intellisense
      */
	function Update()
	{
		global $dal_info;
		
		$tableinfo = &$dal_info[ $this->infoKey ];
		$updateParam = "";
		$updateValue = "";
		$blobs = array();

		foreach($tableinfo as $fieldname => $fld)
		{
			$command = 'if(isset($this->'.$fld['varname'].')) { ';
			if( $fld["key"] )
				$command.= '$this->Param[\''.escapesq($fieldname).'\'] = $this->'.$fld['varname'].';';
			else
				$command.= '$this->Value[\''.escapesq($fieldname).'\'] = $this->'.$fld['varname'].';';
			$command.= ' }';
			
			eval($command);
			
			if( !$fld["key"] && !array_key_exists( strtoupper($fieldname), array_change_key_case($this->Param, CASE_UPPER) ) )
			{
				foreach($this->Value as $field => $value)
				{
					if( strtoupper($field) != strtoupper($fieldname) )
						continue;
						
					$updateValue.= $this->_connection->addFieldWrappers( $fieldname )."=".$this->PrepareValue($value, $fld["type"]) . ", ";
					
					if( $this->_connection->dbType == nDATABASE_Oracle || $this->_connection->dbType == nDATABASE_DB2 || $this->_connection->dbType == nDATABASE_Informix )
					{
						if( IsBinaryType( $fld["type"] ) )
							$blobs[ $fieldname ] = $value;
							
						if( $this->_connection->dbType == nDATABASE_Informix && IsTextType( $fld["type"] ) )	
							$blobs[ $fieldname ] = $value;		
					}
					break;
				}
			}
			else
			{
				foreach($this->Param as $field=>$value)
				{
					if( strtoupper($field) != strtoupper($fieldname) )
						continue;
						
					$updateParam.= $this->_connection->addFieldWrappers( $fieldname )."=".$this->PrepareValue($value, $fld["type"]) . " and ";
					break;
				}
			}
		}

		//	construct SQL and do update	
		if ($updateParam)
			$updateParam = substr($updateParam, 0, -5);
		if ($updateValue)
			$updateValue = substr($updateValue, 0, -2);
			
		if ($updateValue && $updateParam)
		{
			$dalSQL = "update ".$this->_connection->addTableWrappers( $this->m_TableName )." set ".$updateValue." where ".$updateParam;
			$this->Execute_Query($blobs, $dalSQL, $tableinfo);
		}

		//	cleanup
		$this->Reset();
	}
	
	/**
	 * Select one or more records matching the condition
	 */
	function FetchByID()
	{
		global $dal_info;
		$tableinfo = &$dal_info[ $this->infoKey ];

		$dal_where = "";
		foreach($tableinfo as $fieldname => $fld)
		{
			$command = 'if(isset($this->'.$fld['varname'].')) { ';
			$command.= '$this->Value[\''.escapesq($fieldname).'\'] = $this->'.$fld['varname'].';';
			$command.= ' }';
			
			eval($command);
			
			foreach($this->Param as $field => $value)
			{
				if( strtoupper($field) != strtoupper($fieldname) )
					continue;
					
				$dal_where.= $this->_connection->addFieldWrappers( $fieldname )."=".$this->PrepareValue($value, $fld["type"]) . " and ";
				break;
			}
		}
		// cleanup
		$this->Reset();
		// construct and run SQL
		if ($dal_where)
			$dal_where = " where ".substr($dal_where, 0, -5);
			
		$dalSQL = "select * from ".$this->_connection->addTableWrappers( $this->m_TableName ).$dal_where;
		return $this->_connection->query( $dalSQL );
	}
}

function cutprefix($table)
{
	$pos = strpos($table,".");
	if( $pos === false )
		return $table;
		
	return substr($table, $pos + 1);
}

function escapesq($str)
{
	return str_replace(array("\\","'"),array("\\\\","\\'"),$str);
}

?>