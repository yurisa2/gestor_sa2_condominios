<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_cobranca = array();
	$tdatafin_cobranca[".truncateText"] = true;
	$tdatafin_cobranca[".NumberOfChars"] = 80;
	$tdatafin_cobranca[".ShortName"] = "fin_cobranca";
	$tdatafin_cobranca[".OwnerID"] = "auth";
	$tdatafin_cobranca[".OriginalTable"] = "fin_cobranca";

//	field labels
$fieldLabelsfin_cobranca = array();
$fieldToolTipsfin_cobranca = array();
$pageTitlesfin_cobranca = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"] = array();
	$pageTitlesfin_cobranca["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["idCobranca"] = "Código";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["idCobranca"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["Unidade"] = "Unidade";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["Unidade"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["vencimento"] = "Vencimento";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["vencimento"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["tipo"] = "Tipo";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["tipo"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["numdoc"] = "Número";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["numdoc"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["datadoc"] = "Data";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["datadoc"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["vlrdoc"] = "Valor do documento";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["vlrdoc"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["desc"] = "Desconto";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["desc"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["outded"] = "Outras deduções";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["outded"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["moramulta"] = "Mora/Multa";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["moramulta"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["outacr"] = "Outras Cobranças";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["outacr"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["env"] = "Enviado";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["env"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["imp"] = "Impresso";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["imp"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["conta"] = "Conta";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["conta"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["link_ger_origem"] = "Origem";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["link_ger_origem"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["tipodoc"] = "Tipo de Doc";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["tipodoc"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["liq"] = "Liquidado";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["liq"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["idUnidade"] = "Cod. Unidade";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["idUnidade"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["vlrcob"] = "Valor Cobrado";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["vlrcob"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["dtpagamento"] = "Data de pagamento";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["dtpagamento"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["baixado"] = "Baixado";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["baixado"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["motivobaixa"] = "Motivo Baixa";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["motivobaixa"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["acordo"] = "Acordo";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["acordo"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["dtacordo"] = "Data do acordo";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["dtacordo"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["detaacordo"] = "Detalhes do Acordo";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["detaacordo"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["link_ger_unidade"] = "Unidade";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["link_ger_unidade"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["acordoativo"] = "Acordo Ativo";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["acordoativo"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["numidacordo"] = "Identificação do Acordo";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["numidacordo"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["unidadeir"] = "Unidade";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["unidadeir"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["cadastro"] = "Cadastro";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["cadastro"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["auth"] = "Autorização";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["auth"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["jurosam"] = "Juros A.M.(%)";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["jurosam"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["multa"] = "Multa (%)";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["multa"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["bolItau"] = "Itau";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["bolItau"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["bolReal"] = "Real";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["bolReal"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["bolBB"] = "BB";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["bolBB"] = "";
	$fieldLabelsfin_cobranca["Portuguese(Brazil)"]["bolSantander"] = "Boleto";
	$fieldToolTipsfin_cobranca["Portuguese(Brazil)"]["bolSantander"] = "";
	if (count($fieldToolTipsfin_cobranca["Portuguese(Brazil)"]))
		$tdatafin_cobranca[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_cobranca[""] = array();
	$fieldToolTipsfin_cobranca[""] = array();
	$pageTitlesfin_cobranca[""] = array();
	if (count($fieldToolTipsfin_cobranca[""]))
		$tdatafin_cobranca[".isUseToolTips"] = true;
}


	$tdatafin_cobranca[".NCSearch"] = true;



$tdatafin_cobranca[".shortTableName"] = "fin_cobranca";
$tdatafin_cobranca[".nSecOptions"] = 1;
$tdatafin_cobranca[".recsPerRowList"] = 1;
$tdatafin_cobranca[".recsPerRowPrint"] = 1;
$tdatafin_cobranca[".mainTableOwnerID"] = "auth";
$tdatafin_cobranca[".moveNext"] = 1;
$tdatafin_cobranca[".entityType"] = 0;

$tdatafin_cobranca[".strOriginalTableName"] = "fin_cobranca";





$tdatafin_cobranca[".showAddInPopup"] = false;

$tdatafin_cobranca[".showEditInPopup"] = false;

$tdatafin_cobranca[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_cobranca[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_cobranca[".fieldsForRegister"] = array();

$tdatafin_cobranca[".listAjax"] = false;

	$tdatafin_cobranca[".audit"] = true;

	$tdatafin_cobranca[".locking"] = true;

$tdatafin_cobranca[".edit"] = true;
$tdatafin_cobranca[".afterEditAction"] = 1;
$tdatafin_cobranca[".closePopupAfterEdit"] = 1;
$tdatafin_cobranca[".afterEditActionDetTable"] = "";

$tdatafin_cobranca[".add"] = true;
$tdatafin_cobranca[".afterAddAction"] = 1;
$tdatafin_cobranca[".closePopupAfterAdd"] = 1;
$tdatafin_cobranca[".afterAddActionDetTable"] = "";

$tdatafin_cobranca[".list"] = true;

$tdatafin_cobranca[".view"] = true;


$tdatafin_cobranca[".exportTo"] = true;

$tdatafin_cobranca[".printFriendly"] = true;

$tdatafin_cobranca[".delete"] = true;

$tdatafin_cobranca[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_cobranca[".searchSaving"] = false;
//

$tdatafin_cobranca[".showSearchPanel"] = true;
		$tdatafin_cobranca[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_cobranca[".isUseAjaxSuggest"] = false;
else
	$tdatafin_cobranca[".isUseAjaxSuggest"] = true;

$tdatafin_cobranca[".rowHighlite"] = true;



$tdatafin_cobranca[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_cobranca[".isUseTimeForSearch"] = false;



$tdatafin_cobranca[".badgeColor"] = "008B8B";


$tdatafin_cobranca[".allSearchFields"] = array();
$tdatafin_cobranca[".filterFields"] = array();
$tdatafin_cobranca[".requiredSearchFields"] = array();

$tdatafin_cobranca[".allSearchFields"][] = "conta";
	$tdatafin_cobranca[".allSearchFields"][] = "tipo";
	$tdatafin_cobranca[".allSearchFields"][] = "liq";
	$tdatafin_cobranca[".allSearchFields"][] = "dtpagamento";
	$tdatafin_cobranca[".allSearchFields"][] = "link_ger_unidade";
	$tdatafin_cobranca[".allSearchFields"][] = "vencimento";
	$tdatafin_cobranca[".allSearchFields"][] = "vlrdoc";
	$tdatafin_cobranca[".allSearchFields"][] = "multa";
	$tdatafin_cobranca[".allSearchFields"][] = "jurosam";
	$tdatafin_cobranca[".allSearchFields"][] = "numdoc";
	$tdatafin_cobranca[".allSearchFields"][] = "desc";
	$tdatafin_cobranca[".allSearchFields"][] = "datadoc";
	$tdatafin_cobranca[".allSearchFields"][] = "outded";
	$tdatafin_cobranca[".allSearchFields"][] = "vlrcob";
	$tdatafin_cobranca[".allSearchFields"][] = "moramulta";
	$tdatafin_cobranca[".allSearchFields"][] = "obs";
	$tdatafin_cobranca[".allSearchFields"][] = "outacr";
	$tdatafin_cobranca[".allSearchFields"][] = "baixado";
	$tdatafin_cobranca[".allSearchFields"][] = "acordo";
	$tdatafin_cobranca[".allSearchFields"][] = "motivobaixa";
	$tdatafin_cobranca[".allSearchFields"][] = "dtacordo";
	$tdatafin_cobranca[".allSearchFields"][] = "numidacordo";
	$tdatafin_cobranca[".allSearchFields"][] = "ultimousuario";
	$tdatafin_cobranca[".allSearchFields"][] = "ultimaalteracao";
	$tdatafin_cobranca[".allSearchFields"][] = "detaacordo";
	

$tdatafin_cobranca[".googleLikeFields"] = array();
$tdatafin_cobranca[".googleLikeFields"][] = "vencimento";
$tdatafin_cobranca[".googleLikeFields"][] = "tipo";
$tdatafin_cobranca[".googleLikeFields"][] = "numdoc";
$tdatafin_cobranca[".googleLikeFields"][] = "datadoc";
$tdatafin_cobranca[".googleLikeFields"][] = "vlrdoc";
$tdatafin_cobranca[".googleLikeFields"][] = "desc";
$tdatafin_cobranca[".googleLikeFields"][] = "outded";
$tdatafin_cobranca[".googleLikeFields"][] = "moramulta";
$tdatafin_cobranca[".googleLikeFields"][] = "outacr";
$tdatafin_cobranca[".googleLikeFields"][] = "obs";
$tdatafin_cobranca[".googleLikeFields"][] = "conta";
$tdatafin_cobranca[".googleLikeFields"][] = "liq";
$tdatafin_cobranca[".googleLikeFields"][] = "vlrcob";
$tdatafin_cobranca[".googleLikeFields"][] = "dtpagamento";
$tdatafin_cobranca[".googleLikeFields"][] = "baixado";
$tdatafin_cobranca[".googleLikeFields"][] = "motivobaixa";
$tdatafin_cobranca[".googleLikeFields"][] = "acordo";
$tdatafin_cobranca[".googleLikeFields"][] = "dtacordo";
$tdatafin_cobranca[".googleLikeFields"][] = "detaacordo";
$tdatafin_cobranca[".googleLikeFields"][] = "ultimousuario";
$tdatafin_cobranca[".googleLikeFields"][] = "ultimaalteracao";
$tdatafin_cobranca[".googleLikeFields"][] = "link_ger_unidade";
$tdatafin_cobranca[".googleLikeFields"][] = "numidacordo";
$tdatafin_cobranca[".googleLikeFields"][] = "jurosam";
$tdatafin_cobranca[".googleLikeFields"][] = "multa";


$tdatafin_cobranca[".advSearchFields"] = array();
$tdatafin_cobranca[".advSearchFields"][] = "conta";
$tdatafin_cobranca[".advSearchFields"][] = "tipo";
$tdatafin_cobranca[".advSearchFields"][] = "liq";
$tdatafin_cobranca[".advSearchFields"][] = "dtpagamento";
$tdatafin_cobranca[".advSearchFields"][] = "link_ger_unidade";
$tdatafin_cobranca[".advSearchFields"][] = "vencimento";
$tdatafin_cobranca[".advSearchFields"][] = "vlrdoc";
$tdatafin_cobranca[".advSearchFields"][] = "multa";
$tdatafin_cobranca[".advSearchFields"][] = "jurosam";
$tdatafin_cobranca[".advSearchFields"][] = "numdoc";
$tdatafin_cobranca[".advSearchFields"][] = "desc";
$tdatafin_cobranca[".advSearchFields"][] = "datadoc";
$tdatafin_cobranca[".advSearchFields"][] = "outded";
$tdatafin_cobranca[".advSearchFields"][] = "vlrcob";
$tdatafin_cobranca[".advSearchFields"][] = "moramulta";
$tdatafin_cobranca[".advSearchFields"][] = "obs";
$tdatafin_cobranca[".advSearchFields"][] = "outacr";
$tdatafin_cobranca[".advSearchFields"][] = "baixado";
$tdatafin_cobranca[".advSearchFields"][] = "acordo";
$tdatafin_cobranca[".advSearchFields"][] = "motivobaixa";
$tdatafin_cobranca[".advSearchFields"][] = "dtacordo";
$tdatafin_cobranca[".advSearchFields"][] = "numidacordo";
$tdatafin_cobranca[".advSearchFields"][] = "ultimousuario";
$tdatafin_cobranca[".advSearchFields"][] = "ultimaalteracao";
$tdatafin_cobranca[".advSearchFields"][] = "detaacordo";

$tdatafin_cobranca[".tableType"] = "list";

$tdatafin_cobranca[".printerPageOrientation"] = 0;
$tdatafin_cobranca[".nPrinterPageScale"] = 100;


$tdatafin_cobranca[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_cobranca[".geocodingEnabled"] = false;





$tdatafin_cobranca[".listGridLayout"] = 3;

$tdatafin_cobranca[".isDisplayLoading"] = true;


$tdatafin_cobranca[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf

$tdatafin_cobranca[".totalsFields"] = array();
$tdatafin_cobranca[".totalsFields"][] = array(
	"fName" => "vlrcob",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Currency');

$tdatafin_cobranca[".pageSize"] = 20;

$tdatafin_cobranca[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY fin_cobranca.vencimento DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_cobranca[".strOrderBy"] = $tstrOrderBy;

$tdatafin_cobranca[".orderindexes"] = array();
$tdatafin_cobranca[".orderindexes"][] = array(3, (0 ? "ASC" : "DESC"), "fin_cobranca.vencimento");

$tdatafin_cobranca[".sqlHead"] = "select fin_cobranca.idCobranca,  ger_unidades.idUnidade AS Unidade,  fin_cobranca.vencimento,  fin_cobranca.tipo,  fin_cobranca.numdoc,  fin_cobranca.datadoc,  fin_cobranca.vlrdoc,  fin_cobranca.`desc`,  fin_cobranca.outded,  fin_cobranca.moramulta,  fin_cobranca.outacr,  fin_cobranca.env,  fin_cobranca.imp,  fin_cobranca.obs,  fin_cobranca.conta,  fin_cobranca.link_ger_origem,  fin_cobranca.tipodoc,  fin_cobranca.liq,  ger_unidades.idUnidade,  fin_cobranca.vlrcob,  fin_cobranca.dtpagamento,  fin_cobranca.baixado,  fin_cobranca.motivobaixa,  fin_cobranca.acordo,  fin_cobranca.dtacordo,  fin_cobranca.detaacordo,  fin_cobranca.ultimousuario,  fin_cobranca.ultimaalteracao,  fin_cobranca.link_ger_unidade,  fin_cobranca.acordoativo,  fin_cobranca.numidacordo,  fin_cobranca.link_ger_unidade AS unidadeir,  fin_cobranca.idCobranca AS bolSantander,  ger_unidades.link_ger_cadastro AS cadastro,  fin_cobranca.auth,  fin_cobranca.jurosam,  fin_cobranca.multa,  fin_cobranca.idCobranca AS bolItau,  fin_cobranca.idCobranca AS bolReal,  fin_cobranca.idCobranca AS bolBB";
$tdatafin_cobranca[".sqlFrom"] = "FROM fin_cobranca  INNER JOIN ger_unidades ON fin_cobranca.link_ger_unidade = ger_unidades.idUnidade";
$tdatafin_cobranca[".sqlWhereExpr"] = "";
$tdatafin_cobranca[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_cobranca[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_cobranca[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_cobranca[".highlightSearchResults"] = true;

$tableKeysfin_cobranca = array();
$tableKeysfin_cobranca[] = "idCobranca";
$tdatafin_cobranca[".Keys"] = $tableKeysfin_cobranca;

$tdatafin_cobranca[".listFields"] = array();
$tdatafin_cobranca[".listFields"][] = "bolSantander";
$tdatafin_cobranca[".listFields"][] = "link_ger_unidade";
$tdatafin_cobranca[".listFields"][] = "vencimento";
$tdatafin_cobranca[".listFields"][] = "cadastro";
$tdatafin_cobranca[".listFields"][] = "unidadeir";
$tdatafin_cobranca[".listFields"][] = "numdoc";
$tdatafin_cobranca[".listFields"][] = "datadoc";
$tdatafin_cobranca[".listFields"][] = "vlrcob";
$tdatafin_cobranca[".listFields"][] = "obs";
$tdatafin_cobranca[".listFields"][] = "liq";
$tdatafin_cobranca[".listFields"][] = "baixado";
$tdatafin_cobranca[".listFields"][] = "acordo";

$tdatafin_cobranca[".hideMobileList"] = array();


$tdatafin_cobranca[".viewFields"] = array();
$tdatafin_cobranca[".viewFields"][] = "auth";
$tdatafin_cobranca[".viewFields"][] = "conta";
$tdatafin_cobranca[".viewFields"][] = "tipo";
$tdatafin_cobranca[".viewFields"][] = "liq";
$tdatafin_cobranca[".viewFields"][] = "bolSantander";
$tdatafin_cobranca[".viewFields"][] = "dtpagamento";
$tdatafin_cobranca[".viewFields"][] = "link_ger_unidade";
$tdatafin_cobranca[".viewFields"][] = "vencimento";
$tdatafin_cobranca[".viewFields"][] = "vlrdoc";
$tdatafin_cobranca[".viewFields"][] = "cadastro";
$tdatafin_cobranca[".viewFields"][] = "unidadeir";
$tdatafin_cobranca[".viewFields"][] = "multa";
$tdatafin_cobranca[".viewFields"][] = "jurosam";
$tdatafin_cobranca[".viewFields"][] = "numdoc";
$tdatafin_cobranca[".viewFields"][] = "desc";
$tdatafin_cobranca[".viewFields"][] = "datadoc";
$tdatafin_cobranca[".viewFields"][] = "outded";
$tdatafin_cobranca[".viewFields"][] = "vlrcob";
$tdatafin_cobranca[".viewFields"][] = "moramulta";
$tdatafin_cobranca[".viewFields"][] = "obs";
$tdatafin_cobranca[".viewFields"][] = "outacr";
$tdatafin_cobranca[".viewFields"][] = "baixado";
$tdatafin_cobranca[".viewFields"][] = "acordo";
$tdatafin_cobranca[".viewFields"][] = "motivobaixa";
$tdatafin_cobranca[".viewFields"][] = "dtacordo";
$tdatafin_cobranca[".viewFields"][] = "numidacordo";
$tdatafin_cobranca[".viewFields"][] = "detaacordo";
$tdatafin_cobranca[".viewFields"][] = "ultimousuario";
$tdatafin_cobranca[".viewFields"][] = "ultimaalteracao";

$tdatafin_cobranca[".addFields"] = array();
$tdatafin_cobranca[".addFields"][] = "conta";
$tdatafin_cobranca[".addFields"][] = "tipo";
$tdatafin_cobranca[".addFields"][] = "liq";
$tdatafin_cobranca[".addFields"][] = "dtpagamento";
$tdatafin_cobranca[".addFields"][] = "link_ger_unidade";
$tdatafin_cobranca[".addFields"][] = "vencimento";
$tdatafin_cobranca[".addFields"][] = "vlrdoc";
$tdatafin_cobranca[".addFields"][] = "multa";
$tdatafin_cobranca[".addFields"][] = "jurosam";
$tdatafin_cobranca[".addFields"][] = "numdoc";
$tdatafin_cobranca[".addFields"][] = "desc";
$tdatafin_cobranca[".addFields"][] = "datadoc";
$tdatafin_cobranca[".addFields"][] = "outded";
$tdatafin_cobranca[".addFields"][] = "vlrcob";
$tdatafin_cobranca[".addFields"][] = "moramulta";
$tdatafin_cobranca[".addFields"][] = "obs";
$tdatafin_cobranca[".addFields"][] = "outacr";
$tdatafin_cobranca[".addFields"][] = "baixado";
$tdatafin_cobranca[".addFields"][] = "acordo";
$tdatafin_cobranca[".addFields"][] = "motivobaixa";
$tdatafin_cobranca[".addFields"][] = "dtacordo";
$tdatafin_cobranca[".addFields"][] = "numidacordo";
$tdatafin_cobranca[".addFields"][] = "detaacordo";

$tdatafin_cobranca[".masterListFields"] = array();

$tdatafin_cobranca[".inlineAddFields"] = array();

$tdatafin_cobranca[".editFields"] = array();
$tdatafin_cobranca[".editFields"][] = "conta";
$tdatafin_cobranca[".editFields"][] = "tipo";
$tdatafin_cobranca[".editFields"][] = "liq";
$tdatafin_cobranca[".editFields"][] = "dtpagamento";
$tdatafin_cobranca[".editFields"][] = "link_ger_unidade";
$tdatafin_cobranca[".editFields"][] = "vencimento";
$tdatafin_cobranca[".editFields"][] = "vlrdoc";
$tdatafin_cobranca[".editFields"][] = "multa";
$tdatafin_cobranca[".editFields"][] = "jurosam";
$tdatafin_cobranca[".editFields"][] = "numdoc";
$tdatafin_cobranca[".editFields"][] = "desc";
$tdatafin_cobranca[".editFields"][] = "datadoc";
$tdatafin_cobranca[".editFields"][] = "outded";
$tdatafin_cobranca[".editFields"][] = "vlrcob";
$tdatafin_cobranca[".editFields"][] = "moramulta";
$tdatafin_cobranca[".editFields"][] = "obs";
$tdatafin_cobranca[".editFields"][] = "outacr";
$tdatafin_cobranca[".editFields"][] = "baixado";
$tdatafin_cobranca[".editFields"][] = "acordo";
$tdatafin_cobranca[".editFields"][] = "motivobaixa";
$tdatafin_cobranca[".editFields"][] = "dtacordo";
$tdatafin_cobranca[".editFields"][] = "numidacordo";
$tdatafin_cobranca[".editFields"][] = "detaacordo";

$tdatafin_cobranca[".inlineEditFields"] = array();

$tdatafin_cobranca[".exportFields"] = array();
$tdatafin_cobranca[".exportFields"][] = "conta";
$tdatafin_cobranca[".exportFields"][] = "tipo";
$tdatafin_cobranca[".exportFields"][] = "liq";
$tdatafin_cobranca[".exportFields"][] = "dtpagamento";
$tdatafin_cobranca[".exportFields"][] = "link_ger_unidade";
$tdatafin_cobranca[".exportFields"][] = "vencimento";
$tdatafin_cobranca[".exportFields"][] = "vlrdoc";
$tdatafin_cobranca[".exportFields"][] = "cadastro";
$tdatafin_cobranca[".exportFields"][] = "unidadeir";
$tdatafin_cobranca[".exportFields"][] = "multa";
$tdatafin_cobranca[".exportFields"][] = "jurosam";
$tdatafin_cobranca[".exportFields"][] = "numdoc";
$tdatafin_cobranca[".exportFields"][] = "desc";
$tdatafin_cobranca[".exportFields"][] = "datadoc";
$tdatafin_cobranca[".exportFields"][] = "outded";
$tdatafin_cobranca[".exportFields"][] = "vlrcob";
$tdatafin_cobranca[".exportFields"][] = "moramulta";
$tdatafin_cobranca[".exportFields"][] = "obs";
$tdatafin_cobranca[".exportFields"][] = "outacr";
$tdatafin_cobranca[".exportFields"][] = "baixado";
$tdatafin_cobranca[".exportFields"][] = "acordo";
$tdatafin_cobranca[".exportFields"][] = "motivobaixa";
$tdatafin_cobranca[".exportFields"][] = "dtacordo";
$tdatafin_cobranca[".exportFields"][] = "ultimousuario";
$tdatafin_cobranca[".exportFields"][] = "numidacordo";
$tdatafin_cobranca[".exportFields"][] = "detaacordo";
$tdatafin_cobranca[".exportFields"][] = "ultimaalteracao";

$tdatafin_cobranca[".importFields"] = array();

$tdatafin_cobranca[".printFields"] = array();
$tdatafin_cobranca[".printFields"][] = "vencimento";
$tdatafin_cobranca[".printFields"][] = "vlrdoc";
$tdatafin_cobranca[".printFields"][] = "multa";
$tdatafin_cobranca[".printFields"][] = "jurosam";
$tdatafin_cobranca[".printFields"][] = "numdoc";
$tdatafin_cobranca[".printFields"][] = "desc";
$tdatafin_cobranca[".printFields"][] = "outded";
$tdatafin_cobranca[".printFields"][] = "moramulta";
$tdatafin_cobranca[".printFields"][] = "obs";
$tdatafin_cobranca[".printFields"][] = "outacr";

//	idCobranca
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idCobranca";
	$fdata["GoodName"] = "idCobranca";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","idCobranca");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["idCobranca"] = $fdata;
//	Unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Unidade";
	$fdata["GoodName"] = "Unidade";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","Unidade");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idUnidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ger_unidades.idUnidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_guni_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["Unidade"] = $fdata;
//	vencimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "vencimento";
	$fdata["GoodName"] = "vencimento";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","vencimento");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vencimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.vencimento";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["vencimento"] = $fdata;
//	tipo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "tipo";
	$fdata["GoodName"] = "tipo";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","tipo");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.tipo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Boleto";
	$edata["LookupValues"][] = "Depósito";
	$edata["LookupValues"][] = "Parcela de carnê";
	$edata["LookupValues"][] = "Pagamento em dinheiro";

	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["tipo"] = $fdata;
//	numdoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "numdoc";
	$fdata["GoodName"] = "numdoc";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","numdoc");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numdoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.numdoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["numdoc"] = $fdata;
//	datadoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "datadoc";
	$fdata["GoodName"] = "datadoc";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","datadoc");
	$fdata["FieldType"] = 135;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "datadoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.datadoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["datadoc"] = $fdata;
//	vlrdoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "vlrdoc";
	$fdata["GoodName"] = "vlrdoc";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","vlrdoc");
	$fdata["FieldType"] = 5;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vlrdoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.vlrdoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Currency");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 83;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["vlrdoc"] = $fdata;
//	desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "desc";
	$fdata["GoodName"] = "desc";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","desc");
	$fdata["FieldType"] = 5;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "desc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.`desc`";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Currency");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["desc"] = $fdata;
//	outded
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "outded";
	$fdata["GoodName"] = "outded";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","outded");
	$fdata["FieldType"] = 5;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "outded";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.outded";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Currency");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["outded"] = $fdata;
//	moramulta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "moramulta";
	$fdata["GoodName"] = "moramulta";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","moramulta");
	$fdata["FieldType"] = 5;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "moramulta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.moramulta";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["moramulta"] = $fdata;
//	outacr
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "outacr";
	$fdata["GoodName"] = "outacr";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","outacr");
	$fdata["FieldType"] = 5;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "outacr";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.outacr";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["outacr"] = $fdata;
//	env
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "env";
	$fdata["GoodName"] = "env";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","env");
	$fdata["FieldType"] = 16;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "env";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.env";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["env"] = $fdata;
//	imp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "imp";
	$fdata["GoodName"] = "imp";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","imp");
	$fdata["FieldType"] = 16;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "imp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.imp";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["imp"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 250;

	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["obs"] = $fdata;
//	conta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "conta";
	$fdata["GoodName"] = "conta";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","conta");
	$fdata["FieldType"] = 3;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "conta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.conta";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_contas";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idConta";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["conta"] = $fdata;
//	link_ger_origem
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "link_ger_origem";
	$fdata["GoodName"] = "link_ger_origem";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","link_ger_origem");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "link_ger_origem";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.link_ger_origem";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["link_ger_origem"] = $fdata;
//	tipodoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "tipodoc";
	$fdata["GoodName"] = "tipodoc";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","tipodoc");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "tipodoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.tipodoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_tipodocs";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idTipodocs";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "tipodoc";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["tipodoc"] = $fdata;
//	liq
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "liq";
	$fdata["GoodName"] = "liq";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","liq");
	$fdata["FieldType"] = 16;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "liq";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.liq";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["liq"] = $fdata;
//	idUnidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "idUnidade";
	$fdata["GoodName"] = "idUnidade";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","idUnidade");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idUnidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ger_unidades.idUnidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["idUnidade"] = $fdata;
//	vlrcob
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "vlrcob";
	$fdata["GoodName"] = "vlrcob";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","vlrcob");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vlrcob";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.vlrcob";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Currency");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 83;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["vlrcob"] = $fdata;
//	dtpagamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "dtpagamento";
	$fdata["GoodName"] = "dtpagamento";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","dtpagamento");
	$fdata["FieldType"] = 7;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "dtpagamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.dtpagamento";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["dtpagamento"] = $fdata;
//	baixado
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "baixado";
	$fdata["GoodName"] = "baixado";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","baixado");
	$fdata["FieldType"] = 16;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "baixado";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.baixado";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["baixado"] = $fdata;
//	motivobaixa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "motivobaixa";
	$fdata["GoodName"] = "motivobaixa";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","motivobaixa");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "motivobaixa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.motivobaixa";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["motivobaixa"] = $fdata;
//	acordo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "acordo";
	$fdata["GoodName"] = "acordo";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","acordo");
	$fdata["FieldType"] = 16;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "acordo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.acordo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["acordo"] = $fdata;
//	dtacordo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "dtacordo";
	$fdata["GoodName"] = "dtacordo";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","dtacordo");
	$fdata["FieldType"] = 7;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "dtacordo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.dtacordo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["dtacordo"] = $fdata;
//	detaacordo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "detaacordo";
	$fdata["GoodName"] = "detaacordo";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","detaacordo");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "detaacordo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.detaacordo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 100;
			$edata["nCols"] = 250;

	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["detaacordo"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 28;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["ultimaalteracao"] = $fdata;
//	link_ger_unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 29;
	$fdata["strName"] = "link_ger_unidade";
	$fdata["GoodName"] = "link_ger_unidade";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","link_ger_unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "grupounidade";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cobranca["link_ger_unidade"] = $fdata;
//	acordoativo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 30;
	$fdata["strName"] = "acordoativo";
	$fdata["GoodName"] = "acordoativo";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","acordoativo");
	$fdata["FieldType"] = 16;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "acordoativo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.acordoativo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["acordoativo"] = $fdata;
//	numidacordo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 31;
	$fdata["strName"] = "numidacordo";
	$fdata["GoodName"] = "numidacordo";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","numidacordo");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numidacordo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.numidacordo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["numidacordo"] = $fdata;
//	unidadeir
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 32;
	$fdata["strName"] = "unidadeir";
	$fdata["GoodName"] = "unidadeir";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","unidadeir");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["unidadeir"] = $fdata;
//	bolSantander
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 33;
	$fdata["strName"] = "bolSantander";
	$fdata["GoodName"] = "bolSantander";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","bolSantander");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "boletophp/boleto.php?nnum1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="boletophp/boleto.php?nnum1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Boleto";
	$vdata["hlTitleField"] = "Boleto";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["bolSantander"] = $fdata;
//	cadastro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 34;
	$fdata["strName"] = "cadastro";
	$fdata["GoodName"] = "cadastro";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","cadastro");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_cadastro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ger_unidades.link_ger_cadastro";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "ger_cadastro_view.php?editid1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="ger_cadastro_view.php?editid1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Cadastro";
	$vdata["hlTitleField"] = "Cadastro";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["cadastro"] = $fdata;
//	auth
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 35;
	$fdata["strName"] = "auth";
	$fdata["GoodName"] = "auth";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","auth");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "auth";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.auth";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["auth"] = $fdata;
//	jurosam
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 36;
	$fdata["strName"] = "jurosam";
	$fdata["GoodName"] = "jurosam";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","jurosam");
	$fdata["FieldType"] = 5;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "jurosam";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.jurosam";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["jurosam"] = $fdata;
//	multa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 37;
	$fdata["strName"] = "multa";
	$fdata["GoodName"] = "multa";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","multa");
	$fdata["FieldType"] = 5;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "multa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.multa";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cobranca["multa"] = $fdata;
//	bolItau
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 38;
	$fdata["strName"] = "bolItau";
	$fdata["GoodName"] = "bolItau";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","bolItau");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "boletophp/boleto_itau.php?nnum1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="boletophp/boleto_itau.php?nnum1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Itaú";
	$vdata["hlTitleField"] = "Itaú";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["bolItau"] = $fdata;
//	bolReal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 39;
	$fdata["strName"] = "bolReal";
	$fdata["GoodName"] = "bolReal";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","bolReal");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "boletophp/boleto_real.php?nnum1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="boletophp/boleto_real.php?nnum1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Real";
	$vdata["hlTitleField"] = "Real";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["bolReal"] = $fdata;
//	bolBB
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 40;
	$fdata["strName"] = "bolBB";
	$fdata["GoodName"] = "bolBB";
	$fdata["ownerTable"] = "fin_cobranca";
	$fdata["Label"] = GetFieldLabel("fin_cobranca","bolBB");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_cobranca.idCobranca";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "boletophp/boleto_bb.php?nnum1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="boletophp/boleto_bb.php?nnum1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "BB";
	$vdata["hlTitleField"] = "BB";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_cobranca["bolBB"] = $fdata;


$tables_data["fin_cobranca"]=&$tdatafin_cobranca;
$field_labels["fin_cobranca"] = &$fieldLabelsfin_cobranca;
$fieldToolTips["fin_cobranca"] = &$fieldToolTipsfin_cobranca;
$page_titles["fin_cobranca"] = &$pageTitlesfin_cobranca;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_cobranca"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_cobranca"] = array();


	
				$strOriginalDetailsTable="ger_unidades";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_unidades";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_unidades";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["fin_cobranca"][0] = $masterParams;
				$masterTablesData["fin_cobranca"][0]["masterKeys"] = array();
	$masterTablesData["fin_cobranca"][0]["masterKeys"][]="idUnidade";
				$masterTablesData["fin_cobranca"][0]["detailKeys"] = array();
	$masterTablesData["fin_cobranca"][0]["detailKeys"][]="link_ger_unidade";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_cobranca()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "fin_cobranca.idCobranca,  ger_unidades.idUnidade AS Unidade,  fin_cobranca.vencimento,  fin_cobranca.tipo,  fin_cobranca.numdoc,  fin_cobranca.datadoc,  fin_cobranca.vlrdoc,  fin_cobranca.`desc`,  fin_cobranca.outded,  fin_cobranca.moramulta,  fin_cobranca.outacr,  fin_cobranca.env,  fin_cobranca.imp,  fin_cobranca.obs,  fin_cobranca.conta,  fin_cobranca.link_ger_origem,  fin_cobranca.tipodoc,  fin_cobranca.liq,  ger_unidades.idUnidade,  fin_cobranca.vlrcob,  fin_cobranca.dtpagamento,  fin_cobranca.baixado,  fin_cobranca.motivobaixa,  fin_cobranca.acordo,  fin_cobranca.dtacordo,  fin_cobranca.detaacordo,  fin_cobranca.ultimousuario,  fin_cobranca.ultimaalteracao,  fin_cobranca.link_ger_unidade,  fin_cobranca.acordoativo,  fin_cobranca.numidacordo,  fin_cobranca.link_ger_unidade AS unidadeir,  fin_cobranca.idCobranca AS bolSantander,  ger_unidades.link_ger_cadastro AS cadastro,  fin_cobranca.auth,  fin_cobranca.jurosam,  fin_cobranca.multa,  fin_cobranca.idCobranca AS bolItau,  fin_cobranca.idCobranca AS bolReal,  fin_cobranca.idCobranca AS bolBB";
$proto0["m_strFrom"] = "FROM fin_cobranca  INNER JOIN ger_unidades ON fin_cobranca.link_ger_unidade = ger_unidades.idUnidade";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY fin_cobranca.vencimento DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto6["m_sql"] = "fin_cobranca.idCobranca";
$proto6["m_srcTableName"] = "fin_cobranca";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "idUnidade",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "fin_cobranca"
));

$proto8["m_sql"] = "ger_unidades.idUnidade";
$proto8["m_srcTableName"] = "fin_cobranca";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "Unidade";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "vencimento",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto10["m_sql"] = "fin_cobranca.vencimento";
$proto10["m_srcTableName"] = "fin_cobranca";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "tipo",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto12["m_sql"] = "fin_cobranca.tipo";
$proto12["m_srcTableName"] = "fin_cobranca";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "numdoc",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto14["m_sql"] = "fin_cobranca.numdoc";
$proto14["m_srcTableName"] = "fin_cobranca";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "datadoc",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto16["m_sql"] = "fin_cobranca.datadoc";
$proto16["m_srcTableName"] = "fin_cobranca";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "vlrdoc",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto18["m_sql"] = "fin_cobranca.vlrdoc";
$proto18["m_srcTableName"] = "fin_cobranca";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "desc",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto20["m_sql"] = "fin_cobranca.`desc`";
$proto20["m_srcTableName"] = "fin_cobranca";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "outded",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto22["m_sql"] = "fin_cobranca.outded";
$proto22["m_srcTableName"] = "fin_cobranca";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "moramulta",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto24["m_sql"] = "fin_cobranca.moramulta";
$proto24["m_srcTableName"] = "fin_cobranca";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "outacr",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto26["m_sql"] = "fin_cobranca.outacr";
$proto26["m_srcTableName"] = "fin_cobranca";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "env",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto28["m_sql"] = "fin_cobranca.env";
$proto28["m_srcTableName"] = "fin_cobranca";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "imp",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto30["m_sql"] = "fin_cobranca.imp";
$proto30["m_srcTableName"] = "fin_cobranca";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto32["m_sql"] = "fin_cobranca.obs";
$proto32["m_srcTableName"] = "fin_cobranca";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "conta",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto34["m_sql"] = "fin_cobranca.conta";
$proto34["m_srcTableName"] = "fin_cobranca";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_origem",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto36["m_sql"] = "fin_cobranca.link_ger_origem";
$proto36["m_srcTableName"] = "fin_cobranca";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "tipodoc",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto38["m_sql"] = "fin_cobranca.tipodoc";
$proto38["m_srcTableName"] = "fin_cobranca";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "liq",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto40["m_sql"] = "fin_cobranca.liq";
$proto40["m_srcTableName"] = "fin_cobranca";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "idUnidade",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "fin_cobranca"
));

$proto42["m_sql"] = "ger_unidades.idUnidade";
$proto42["m_srcTableName"] = "fin_cobranca";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "vlrcob",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto44["m_sql"] = "fin_cobranca.vlrcob";
$proto44["m_srcTableName"] = "fin_cobranca";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "dtpagamento",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto46["m_sql"] = "fin_cobranca.dtpagamento";
$proto46["m_srcTableName"] = "fin_cobranca";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "baixado",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto48["m_sql"] = "fin_cobranca.baixado";
$proto48["m_srcTableName"] = "fin_cobranca";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "motivobaixa",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto50["m_sql"] = "fin_cobranca.motivobaixa";
$proto50["m_srcTableName"] = "fin_cobranca";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "acordo",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto52["m_sql"] = "fin_cobranca.acordo";
$proto52["m_srcTableName"] = "fin_cobranca";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$obj = new SQLField(array(
	"m_strName" => "dtacordo",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto54["m_sql"] = "fin_cobranca.dtacordo";
$proto54["m_srcTableName"] = "fin_cobranca";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto56=array();
			$obj = new SQLField(array(
	"m_strName" => "detaacordo",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto56["m_sql"] = "fin_cobranca.detaacordo";
$proto56["m_srcTableName"] = "fin_cobranca";
$proto56["m_expr"]=$obj;
$proto56["m_alias"] = "";
$obj = new SQLFieldListItem($proto56);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto58["m_sql"] = "fin_cobranca.ultimousuario";
$proto58["m_srcTableName"] = "fin_cobranca";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
						$proto60=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto60["m_sql"] = "fin_cobranca.ultimaalteracao";
$proto60["m_srcTableName"] = "fin_cobranca";
$proto60["m_expr"]=$obj;
$proto60["m_alias"] = "";
$obj = new SQLFieldListItem($proto60);

$proto0["m_fieldlist"][]=$obj;
						$proto62=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto62["m_sql"] = "fin_cobranca.link_ger_unidade";
$proto62["m_srcTableName"] = "fin_cobranca";
$proto62["m_expr"]=$obj;
$proto62["m_alias"] = "";
$obj = new SQLFieldListItem($proto62);

$proto0["m_fieldlist"][]=$obj;
						$proto64=array();
			$obj = new SQLField(array(
	"m_strName" => "acordoativo",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto64["m_sql"] = "fin_cobranca.acordoativo";
$proto64["m_srcTableName"] = "fin_cobranca";
$proto64["m_expr"]=$obj;
$proto64["m_alias"] = "";
$obj = new SQLFieldListItem($proto64);

$proto0["m_fieldlist"][]=$obj;
						$proto66=array();
			$obj = new SQLField(array(
	"m_strName" => "numidacordo",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto66["m_sql"] = "fin_cobranca.numidacordo";
$proto66["m_srcTableName"] = "fin_cobranca";
$proto66["m_expr"]=$obj;
$proto66["m_alias"] = "";
$obj = new SQLFieldListItem($proto66);

$proto0["m_fieldlist"][]=$obj;
						$proto68=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto68["m_sql"] = "fin_cobranca.link_ger_unidade";
$proto68["m_srcTableName"] = "fin_cobranca";
$proto68["m_expr"]=$obj;
$proto68["m_alias"] = "unidadeir";
$obj = new SQLFieldListItem($proto68);

$proto0["m_fieldlist"][]=$obj;
						$proto70=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto70["m_sql"] = "fin_cobranca.idCobranca";
$proto70["m_srcTableName"] = "fin_cobranca";
$proto70["m_expr"]=$obj;
$proto70["m_alias"] = "bolSantander";
$obj = new SQLFieldListItem($proto70);

$proto0["m_fieldlist"][]=$obj;
						$proto72=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_cadastro",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "fin_cobranca"
));

$proto72["m_sql"] = "ger_unidades.link_ger_cadastro";
$proto72["m_srcTableName"] = "fin_cobranca";
$proto72["m_expr"]=$obj;
$proto72["m_alias"] = "cadastro";
$obj = new SQLFieldListItem($proto72);

$proto0["m_fieldlist"][]=$obj;
						$proto74=array();
			$obj = new SQLField(array(
	"m_strName" => "auth",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto74["m_sql"] = "fin_cobranca.auth";
$proto74["m_srcTableName"] = "fin_cobranca";
$proto74["m_expr"]=$obj;
$proto74["m_alias"] = "";
$obj = new SQLFieldListItem($proto74);

$proto0["m_fieldlist"][]=$obj;
						$proto76=array();
			$obj = new SQLField(array(
	"m_strName" => "jurosam",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto76["m_sql"] = "fin_cobranca.jurosam";
$proto76["m_srcTableName"] = "fin_cobranca";
$proto76["m_expr"]=$obj;
$proto76["m_alias"] = "";
$obj = new SQLFieldListItem($proto76);

$proto0["m_fieldlist"][]=$obj;
						$proto78=array();
			$obj = new SQLField(array(
	"m_strName" => "multa",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto78["m_sql"] = "fin_cobranca.multa";
$proto78["m_srcTableName"] = "fin_cobranca";
$proto78["m_expr"]=$obj;
$proto78["m_alias"] = "";
$obj = new SQLFieldListItem($proto78);

$proto0["m_fieldlist"][]=$obj;
						$proto80=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto80["m_sql"] = "fin_cobranca.idCobranca";
$proto80["m_srcTableName"] = "fin_cobranca";
$proto80["m_expr"]=$obj;
$proto80["m_alias"] = "bolItau";
$obj = new SQLFieldListItem($proto80);

$proto0["m_fieldlist"][]=$obj;
						$proto82=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto82["m_sql"] = "fin_cobranca.idCobranca";
$proto82["m_srcTableName"] = "fin_cobranca";
$proto82["m_expr"]=$obj;
$proto82["m_alias"] = "bolReal";
$obj = new SQLFieldListItem($proto82);

$proto0["m_fieldlist"][]=$obj;
						$proto84=array();
			$obj = new SQLField(array(
	"m_strName" => "idCobranca",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto84["m_sql"] = "fin_cobranca.idCobranca";
$proto84["m_srcTableName"] = "fin_cobranca";
$proto84["m_expr"]=$obj;
$proto84["m_alias"] = "bolBB";
$obj = new SQLFieldListItem($proto84);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto86=array();
$proto86["m_link"] = "SQLL_MAIN";
			$proto87=array();
$proto87["m_strName"] = "fin_cobranca";
$proto87["m_srcTableName"] = "fin_cobranca";
$proto87["m_columns"] = array();
$proto87["m_columns"][] = "idCobranca";
$proto87["m_columns"][] = "vencimento";
$proto87["m_columns"][] = "tipo";
$proto87["m_columns"][] = "numdoc";
$proto87["m_columns"][] = "datadoc";
$proto87["m_columns"][] = "vlrdoc";
$proto87["m_columns"][] = "desc";
$proto87["m_columns"][] = "outded";
$proto87["m_columns"][] = "moramulta";
$proto87["m_columns"][] = "outacr";
$proto87["m_columns"][] = "vlrcob";
$proto87["m_columns"][] = "link_ger_unidade";
$proto87["m_columns"][] = "liq";
$proto87["m_columns"][] = "env";
$proto87["m_columns"][] = "imp";
$proto87["m_columns"][] = "obs";
$proto87["m_columns"][] = "conta";
$proto87["m_columns"][] = "link_ger_origem";
$proto87["m_columns"][] = "tipodoc";
$proto87["m_columns"][] = "dtpagamento";
$proto87["m_columns"][] = "baixado";
$proto87["m_columns"][] = "motivobaixa";
$proto87["m_columns"][] = "acordo";
$proto87["m_columns"][] = "dtacordo";
$proto87["m_columns"][] = "detaacordo";
$proto87["m_columns"][] = "ultimousuario";
$proto87["m_columns"][] = "ultimaalteracao";
$proto87["m_columns"][] = "acordoativo";
$proto87["m_columns"][] = "numidacordo";
$proto87["m_columns"][] = "cartas";
$proto87["m_columns"][] = "auth";
$proto87["m_columns"][] = "multa";
$proto87["m_columns"][] = "jurosam";
$obj = new SQLTable($proto87);

$proto86["m_table"] = $obj;
$proto86["m_sql"] = "fin_cobranca";
$proto86["m_alias"] = "";
$proto86["m_srcTableName"] = "fin_cobranca";
$proto88=array();
$proto88["m_sql"] = "";
$proto88["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto88["m_column"]=$obj;
$proto88["m_contained"] = array();
$proto88["m_strCase"] = "";
$proto88["m_havingmode"] = false;
$proto88["m_inBrackets"] = false;
$proto88["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto88);

$proto86["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto86);

$proto0["m_fromlist"][]=$obj;
												$proto90=array();
$proto90["m_link"] = "SQLL_INNERJOIN";
			$proto91=array();
$proto91["m_strName"] = "ger_unidades";
$proto91["m_srcTableName"] = "fin_cobranca";
$proto91["m_columns"] = array();
$proto91["m_columns"][] = "idUnidade";
$proto91["m_columns"][] = "grupo";
$proto91["m_columns"][] = "unidade";
$proto91["m_columns"][] = "link_ger_cadastro";
$proto91["m_columns"][] = "dia_pref_venc";
$proto91["m_columns"][] = "ativo";
$proto91["m_columns"][] = "obs";
$proto91["m_columns"][] = "frideal";
$proto91["m_columns"][] = "ultimousuario";
$proto91["m_columns"][] = "ultimaalteracao";
$proto91["m_columns"][] = "auth";
$obj = new SQLTable($proto91);

$proto90["m_table"] = $obj;
$proto90["m_sql"] = "INNER JOIN ger_unidades ON fin_cobranca.link_ger_unidade = ger_unidades.idUnidade";
$proto90["m_alias"] = "";
$proto90["m_srcTableName"] = "fin_cobranca";
$proto92=array();
$proto92["m_sql"] = "fin_cobranca.link_ger_unidade = ger_unidades.idUnidade";
$proto92["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto92["m_column"]=$obj;
$proto92["m_contained"] = array();
$proto92["m_strCase"] = "= ger_unidades.idUnidade";
$proto92["m_havingmode"] = false;
$proto92["m_inBrackets"] = false;
$proto92["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto92);

$proto90["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto90);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto94=array();
						$obj = new SQLField(array(
	"m_strName" => "vencimento",
	"m_strTable" => "fin_cobranca",
	"m_srcTableName" => "fin_cobranca"
));

$proto94["m_column"]=$obj;
$proto94["m_bAsc"] = 0;
$proto94["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto94);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="fin_cobranca";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_cobranca = createSqlQuery_fin_cobranca();


	
		;

																																								

$tdatafin_cobranca[".sqlquery"] = $queryData_fin_cobranca;

include_once(getabspath("include/fin_cobranca_events.php"));
$tableEvents["fin_cobranca"] = new eventclass_fin_cobranca;
$tdatafin_cobranca[".hasEvents"] = true;

?>