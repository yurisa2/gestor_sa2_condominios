<?php
function DisplayMasterTableInfo_SaldosBlocos($params)
{
	$keys = $params["keys"];
	$detailtable = $params["detailtable"];
	$data = $params["masterRecordData"];
	
	$xt = new Xtempl();
	$tName = "SaldosBlocos";
	$xt->eventsObject = getEventObject($tName);
	
	include_once(getabspath('classes/listpage.php'));
	include_once(getabspath('classes/listpage_simple.php'));
	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = LIST_MASTER;
	$mParams["pageType"] = PAGE_LIST;
	$mParams["flyId"] = $params["recId"];
	$masterPage = ListPage::createListPage($tName, $mParams);
	
	$settings = $masterPage->pSet;
	$viewControls = new ViewControlsContainer($settings, PAGE_LIST, $masterPage);
	
	$keysAssoc = array();
	$showKeys = "";	

	if($detailtable == "fin_bco_ext1")
	{
		$keysAssoc["grupo"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("grupo", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("SaldosBlocos","grupo").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if( !$data || !count($data) )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));
	
	$keylink = "";
	
	$xt->assign("saldo_mastervalue", $viewControls->showDBValue("saldo", $data, $keylink));
	$format = $settings->getViewFormat("saldo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("saldo")))
		$class = ' rnr-field-number';
		
	$xt->assign("saldo_class", $class); // add class for field header as field value
	$xt->assign("primvalor_mastervalue", $viewControls->showDBValue("primvalor", $data, $keylink));
	$format = $settings->getViewFormat("primvalor");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("primvalor")))
		$class = ' rnr-field-number';
		
	$xt->assign("primvalor_class", $class); // add class for field header as field value
	$xt->assign("ultvalor_mastervalue", $viewControls->showDBValue("ultvalor", $data, $keylink));
	$format = $settings->getViewFormat("ultvalor");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("ultvalor")))
		$class = ' rnr-field-number';
		
	$xt->assign("ultvalor_class", $class); // add class for field header as field value
	$xt->assign("mediadegasto_mastervalue", $viewControls->showDBValue("mediadegasto", $data, $keylink));
	$format = $settings->getViewFormat("mediadegasto");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("mediadegasto")))
		$class = ' rnr-field-number';
		
	$xt->assign("mediadegasto_class", $class); // add class for field header as field value
	$xt->assign("grupo_mastervalue", $viewControls->showDBValue("grupo", $data, $keylink));
	$format = $settings->getViewFormat("grupo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("grupo")))
		$class = ' rnr-field-number';
		
	$xt->assign("grupo_class", $class); // add class for field header as field value

	$layout = GetPageLayout("SaldosBlocos", 'masterlist');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');
	
	$xt->displayPartial(GetTemplateName("SaldosBlocos", "masterlist"));
}

?>