<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_meiospgto = array();
	$tdatafin_meiospgto[".truncateText"] = true;
	$tdatafin_meiospgto[".NumberOfChars"] = 80;
	$tdatafin_meiospgto[".ShortName"] = "fin_meiospgto";
	$tdatafin_meiospgto[".OwnerID"] = "";
	$tdatafin_meiospgto[".OriginalTable"] = "fin_meiospgto";

//	field labels
$fieldLabelsfin_meiospgto = array();
$fieldToolTipsfin_meiospgto = array();
$pageTitlesfin_meiospgto = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_meiospgto["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_meiospgto["Portuguese(Brazil)"] = array();
	$pageTitlesfin_meiospgto["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_meiospgto["Portuguese(Brazil)"]["idMeiospgto"] = "Cdigo";
	$fieldToolTipsfin_meiospgto["Portuguese(Brazil)"]["idMeiospgto"] = "";
	$fieldLabelsfin_meiospgto["Portuguese(Brazil)"]["meiopgto"] = "Meio de pagamento";
	$fieldToolTipsfin_meiospgto["Portuguese(Brazil)"]["meiopgto"] = "";
	$fieldLabelsfin_meiospgto["Portuguese(Brazil)"]["obs"] = "Obs.";
	$fieldToolTipsfin_meiospgto["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsfin_meiospgto["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsfin_meiospgto["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_meiospgto["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsfin_meiospgto["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsfin_meiospgto["Portuguese(Brazil)"]))
		$tdatafin_meiospgto[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_meiospgto[""] = array();
	$fieldToolTipsfin_meiospgto[""] = array();
	$pageTitlesfin_meiospgto[""] = array();
	if (count($fieldToolTipsfin_meiospgto[""]))
		$tdatafin_meiospgto[".isUseToolTips"] = true;
}


	$tdatafin_meiospgto[".NCSearch"] = true;



$tdatafin_meiospgto[".shortTableName"] = "fin_meiospgto";
$tdatafin_meiospgto[".nSecOptions"] = 0;
$tdatafin_meiospgto[".recsPerRowList"] = 1;
$tdatafin_meiospgto[".recsPerRowPrint"] = 1;
$tdatafin_meiospgto[".mainTableOwnerID"] = "";
$tdatafin_meiospgto[".moveNext"] = 1;
$tdatafin_meiospgto[".entityType"] = 0;

$tdatafin_meiospgto[".strOriginalTableName"] = "fin_meiospgto";





$tdatafin_meiospgto[".showAddInPopup"] = false;

$tdatafin_meiospgto[".showEditInPopup"] = false;

$tdatafin_meiospgto[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_meiospgto[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_meiospgto[".fieldsForRegister"] = array();

$tdatafin_meiospgto[".listAjax"] = false;

	$tdatafin_meiospgto[".audit"] = true;

	$tdatafin_meiospgto[".locking"] = true;

$tdatafin_meiospgto[".edit"] = true;
$tdatafin_meiospgto[".afterEditAction"] = 1;
$tdatafin_meiospgto[".closePopupAfterEdit"] = 1;
$tdatafin_meiospgto[".afterEditActionDetTable"] = "";

$tdatafin_meiospgto[".add"] = true;
$tdatafin_meiospgto[".afterAddAction"] = 1;
$tdatafin_meiospgto[".closePopupAfterAdd"] = 1;
$tdatafin_meiospgto[".afterAddActionDetTable"] = "";

$tdatafin_meiospgto[".list"] = true;

$tdatafin_meiospgto[".view"] = true;


$tdatafin_meiospgto[".exportTo"] = true;

$tdatafin_meiospgto[".printFriendly"] = true;


$tdatafin_meiospgto[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_meiospgto[".searchSaving"] = false;
//

$tdatafin_meiospgto[".showSearchPanel"] = true;
		$tdatafin_meiospgto[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_meiospgto[".isUseAjaxSuggest"] = false;
else
	$tdatafin_meiospgto[".isUseAjaxSuggest"] = true;

$tdatafin_meiospgto[".rowHighlite"] = true;



$tdatafin_meiospgto[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_meiospgto[".isUseTimeForSearch"] = false;





$tdatafin_meiospgto[".allSearchFields"] = array();
$tdatafin_meiospgto[".filterFields"] = array();
$tdatafin_meiospgto[".requiredSearchFields"] = array();

$tdatafin_meiospgto[".allSearchFields"][] = "meiopgto";
	$tdatafin_meiospgto[".allSearchFields"][] = "obs";
	$tdatafin_meiospgto[".allSearchFields"][] = "ultimousuario";
	$tdatafin_meiospgto[".allSearchFields"][] = "ultimaalteracao";
	

$tdatafin_meiospgto[".googleLikeFields"] = array();
$tdatafin_meiospgto[".googleLikeFields"][] = "meiopgto";
$tdatafin_meiospgto[".googleLikeFields"][] = "obs";
$tdatafin_meiospgto[".googleLikeFields"][] = "ultimousuario";
$tdatafin_meiospgto[".googleLikeFields"][] = "ultimaalteracao";


$tdatafin_meiospgto[".advSearchFields"] = array();
$tdatafin_meiospgto[".advSearchFields"][] = "meiopgto";
$tdatafin_meiospgto[".advSearchFields"][] = "obs";
$tdatafin_meiospgto[".advSearchFields"][] = "ultimousuario";
$tdatafin_meiospgto[".advSearchFields"][] = "ultimaalteracao";

$tdatafin_meiospgto[".tableType"] = "list";

$tdatafin_meiospgto[".printerPageOrientation"] = 0;
$tdatafin_meiospgto[".nPrinterPageScale"] = 100;

$tdatafin_meiospgto[".nPrinterSplitRecords"] = 40;

$tdatafin_meiospgto[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_meiospgto[".geocodingEnabled"] = false;





$tdatafin_meiospgto[".listGridLayout"] = 3;

$tdatafin_meiospgto[".isDisplayLoading"] = true;


$tdatafin_meiospgto[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatafin_meiospgto[".pageSize"] = 20;

$tdatafin_meiospgto[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_meiospgto[".strOrderBy"] = $tstrOrderBy;

$tdatafin_meiospgto[".orderindexes"] = array();

$tdatafin_meiospgto[".sqlHead"] = "select idMeiospgto,  meiopgto,  obs,  ultimousuario,  ultimaalteracao";
$tdatafin_meiospgto[".sqlFrom"] = "FROM fin_meiospgto";
$tdatafin_meiospgto[".sqlWhereExpr"] = "";
$tdatafin_meiospgto[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_meiospgto[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_meiospgto[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_meiospgto[".highlightSearchResults"] = true;

$tableKeysfin_meiospgto = array();
$tableKeysfin_meiospgto[] = "idMeiospgto";
$tdatafin_meiospgto[".Keys"] = $tableKeysfin_meiospgto;

$tdatafin_meiospgto[".listFields"] = array();
$tdatafin_meiospgto[".listFields"][] = "meiopgto";
$tdatafin_meiospgto[".listFields"][] = "obs";

$tdatafin_meiospgto[".hideMobileList"] = array();


$tdatafin_meiospgto[".viewFields"] = array();
$tdatafin_meiospgto[".viewFields"][] = "meiopgto";
$tdatafin_meiospgto[".viewFields"][] = "obs";
$tdatafin_meiospgto[".viewFields"][] = "ultimousuario";
$tdatafin_meiospgto[".viewFields"][] = "ultimaalteracao";

$tdatafin_meiospgto[".addFields"] = array();
$tdatafin_meiospgto[".addFields"][] = "meiopgto";
$tdatafin_meiospgto[".addFields"][] = "obs";

$tdatafin_meiospgto[".masterListFields"] = array();

$tdatafin_meiospgto[".inlineAddFields"] = array();

$tdatafin_meiospgto[".editFields"] = array();
$tdatafin_meiospgto[".editFields"][] = "meiopgto";
$tdatafin_meiospgto[".editFields"][] = "obs";

$tdatafin_meiospgto[".inlineEditFields"] = array();

$tdatafin_meiospgto[".exportFields"] = array();
$tdatafin_meiospgto[".exportFields"][] = "meiopgto";
$tdatafin_meiospgto[".exportFields"][] = "obs";
$tdatafin_meiospgto[".exportFields"][] = "ultimousuario";
$tdatafin_meiospgto[".exportFields"][] = "ultimaalteracao";

$tdatafin_meiospgto[".importFields"] = array();
$tdatafin_meiospgto[".importFields"][] = "idMeiospgto";
$tdatafin_meiospgto[".importFields"][] = "meiopgto";
$tdatafin_meiospgto[".importFields"][] = "obs";
$tdatafin_meiospgto[".importFields"][] = "ultimousuario";
$tdatafin_meiospgto[".importFields"][] = "ultimaalteracao";

$tdatafin_meiospgto[".printFields"] = array();
$tdatafin_meiospgto[".printFields"][] = "meiopgto";
$tdatafin_meiospgto[".printFields"][] = "obs";

//	idMeiospgto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idMeiospgto";
	$fdata["GoodName"] = "idMeiospgto";
	$fdata["ownerTable"] = "fin_meiospgto";
	$fdata["Label"] = GetFieldLabel("fin_meiospgto","idMeiospgto");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idMeiospgto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idMeiospgto";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_meiospgto["idMeiospgto"] = $fdata;
//	meiopgto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "meiopgto";
	$fdata["GoodName"] = "meiopgto";
	$fdata["ownerTable"] = "fin_meiospgto";
	$fdata["Label"] = GetFieldLabel("fin_meiospgto","meiopgto");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "meiopgto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meiopgto";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_meiospgto["meiopgto"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "fin_meiospgto";
	$fdata["Label"] = GetFieldLabel("fin_meiospgto","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_meiospgto["obs"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_meiospgto";
	$fdata["Label"] = GetFieldLabel("fin_meiospgto","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_meiospgto["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_meiospgto";
	$fdata["Label"] = GetFieldLabel("fin_meiospgto","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_meiospgto["ultimaalteracao"] = $fdata;


$tables_data["fin_meiospgto"]=&$tdatafin_meiospgto;
$field_labels["fin_meiospgto"] = &$fieldLabelsfin_meiospgto;
$fieldToolTips["fin_meiospgto"] = &$fieldToolTipsfin_meiospgto;
$page_titles["fin_meiospgto"] = &$pageTitlesfin_meiospgto;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_meiospgto"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_meiospgto"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_meiospgto()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idMeiospgto,  meiopgto,  obs,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM fin_meiospgto";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idMeiospgto",
	"m_strTable" => "fin_meiospgto",
	"m_srcTableName" => "fin_meiospgto"
));

$proto6["m_sql"] = "idMeiospgto";
$proto6["m_srcTableName"] = "fin_meiospgto";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "meiopgto",
	"m_strTable" => "fin_meiospgto",
	"m_srcTableName" => "fin_meiospgto"
));

$proto8["m_sql"] = "meiopgto";
$proto8["m_srcTableName"] = "fin_meiospgto";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "fin_meiospgto",
	"m_srcTableName" => "fin_meiospgto"
));

$proto10["m_sql"] = "obs";
$proto10["m_srcTableName"] = "fin_meiospgto";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_meiospgto",
	"m_srcTableName" => "fin_meiospgto"
));

$proto12["m_sql"] = "ultimousuario";
$proto12["m_srcTableName"] = "fin_meiospgto";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_meiospgto",
	"m_srcTableName" => "fin_meiospgto"
));

$proto14["m_sql"] = "ultimaalteracao";
$proto14["m_srcTableName"] = "fin_meiospgto";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "fin_meiospgto";
$proto17["m_srcTableName"] = "fin_meiospgto";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "idMeiospgto";
$proto17["m_columns"][] = "meiopgto";
$proto17["m_columns"][] = "obs";
$proto17["m_columns"][] = "ultimaalteracao";
$proto17["m_columns"][] = "ultimousuario";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "fin_meiospgto";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "fin_meiospgto";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="fin_meiospgto";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_meiospgto = createSqlQuery_fin_meiospgto();


	
		;

					

$tdatafin_meiospgto[".sqlquery"] = $queryData_fin_meiospgto;

include_once(getabspath("include/fin_meiospgto_events.php"));
$tableEvents["fin_meiospgto"] = new eventclass_fin_meiospgto;
$tdatafin_meiospgto[".hasEvents"] = true;

?>