<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_controle_de_acessos = array();
	$tdatager_controle_de_acessos[".truncateText"] = true;
	$tdatager_controle_de_acessos[".NumberOfChars"] = 80;
	$tdatager_controle_de_acessos[".ShortName"] = "ger_controle_de_acessos";
	$tdatager_controle_de_acessos[".OwnerID"] = "";
	$tdatager_controle_de_acessos[".OriginalTable"] = "ger_controle_de_acessos";

//	field labels
$fieldLabelsger_controle_de_acessos = array();
$fieldToolTipsger_controle_de_acessos = array();
$pageTitlesger_controle_de_acessos = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_controle_de_acessos["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_controle_de_acessos["Portuguese(Brazil)"] = array();
	$pageTitlesger_controle_de_acessos["Portuguese(Brazil)"] = array();
	$fieldLabelsger_controle_de_acessos["Portuguese(Brazil)"]["idControle"] = "N. Acesso";
	$fieldToolTipsger_controle_de_acessos["Portuguese(Brazil)"]["idControle"] = "";
	$fieldLabelsger_controle_de_acessos["Portuguese(Brazil)"]["usuario"] = "Usuario";
	$fieldToolTipsger_controle_de_acessos["Portuguese(Brazil)"]["usuario"] = "";
	$fieldLabelsger_controle_de_acessos["Portuguese(Brazil)"]["cod_conex"] = "Cód. Conexão";
	$fieldToolTipsger_controle_de_acessos["Portuguese(Brazil)"]["cod_conex"] = "";
	$fieldLabelsger_controle_de_acessos["Portuguese(Brazil)"]["hora"] = "Hora";
	$fieldToolTipsger_controle_de_acessos["Portuguese(Brazil)"]["hora"] = "";
	if (count($fieldToolTipsger_controle_de_acessos["Portuguese(Brazil)"]))
		$tdatager_controle_de_acessos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_controle_de_acessos[""] = array();
	$fieldToolTipsger_controle_de_acessos[""] = array();
	$pageTitlesger_controle_de_acessos[""] = array();
	if (count($fieldToolTipsger_controle_de_acessos[""]))
		$tdatager_controle_de_acessos[".isUseToolTips"] = true;
}


	$tdatager_controle_de_acessos[".NCSearch"] = true;



$tdatager_controle_de_acessos[".shortTableName"] = "ger_controle_de_acessos";
$tdatager_controle_de_acessos[".nSecOptions"] = 0;
$tdatager_controle_de_acessos[".recsPerRowList"] = 1;
$tdatager_controle_de_acessos[".recsPerRowPrint"] = 1;
$tdatager_controle_de_acessos[".mainTableOwnerID"] = "";
$tdatager_controle_de_acessos[".moveNext"] = 1;
$tdatager_controle_de_acessos[".entityType"] = 0;

$tdatager_controle_de_acessos[".strOriginalTableName"] = "ger_controle_de_acessos";





$tdatager_controle_de_acessos[".showAddInPopup"] = false;

$tdatager_controle_de_acessos[".showEditInPopup"] = false;

$tdatager_controle_de_acessos[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_controle_de_acessos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_controle_de_acessos[".fieldsForRegister"] = array();

$tdatager_controle_de_acessos[".listAjax"] = false;

	$tdatager_controle_de_acessos[".audit"] = true;

	$tdatager_controle_de_acessos[".locking"] = true;



$tdatager_controle_de_acessos[".list"] = true;

$tdatager_controle_de_acessos[".view"] = true;


$tdatager_controle_de_acessos[".exportTo"] = true;

$tdatager_controle_de_acessos[".printFriendly"] = true;


$tdatager_controle_de_acessos[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_controle_de_acessos[".searchSaving"] = false;
//

$tdatager_controle_de_acessos[".showSearchPanel"] = true;
		$tdatager_controle_de_acessos[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_controle_de_acessos[".isUseAjaxSuggest"] = false;
else
	$tdatager_controle_de_acessos[".isUseAjaxSuggest"] = true;

$tdatager_controle_de_acessos[".rowHighlite"] = true;



$tdatager_controle_de_acessos[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_controle_de_acessos[".isUseTimeForSearch"] = false;





$tdatager_controle_de_acessos[".allSearchFields"] = array();
$tdatager_controle_de_acessos[".filterFields"] = array();
$tdatager_controle_de_acessos[".requiredSearchFields"] = array();

$tdatager_controle_de_acessos[".allSearchFields"][] = "usuario";
	$tdatager_controle_de_acessos[".allSearchFields"][] = "cod_conex";
	$tdatager_controle_de_acessos[".allSearchFields"][] = "hora";
	

$tdatager_controle_de_acessos[".googleLikeFields"] = array();
$tdatager_controle_de_acessos[".googleLikeFields"][] = "usuario";
$tdatager_controle_de_acessos[".googleLikeFields"][] = "cod_conex";
$tdatager_controle_de_acessos[".googleLikeFields"][] = "hora";


$tdatager_controle_de_acessos[".advSearchFields"] = array();
$tdatager_controle_de_acessos[".advSearchFields"][] = "usuario";
$tdatager_controle_de_acessos[".advSearchFields"][] = "cod_conex";
$tdatager_controle_de_acessos[".advSearchFields"][] = "hora";

$tdatager_controle_de_acessos[".tableType"] = "list";

$tdatager_controle_de_acessos[".printerPageOrientation"] = 0;
$tdatager_controle_de_acessos[".nPrinterPageScale"] = 100;

$tdatager_controle_de_acessos[".nPrinterSplitRecords"] = 40;

$tdatager_controle_de_acessos[".nPrinterPDFSplitRecords"] = 40;



$tdatager_controle_de_acessos[".geocodingEnabled"] = false;





$tdatager_controle_de_acessos[".listGridLayout"] = 3;

$tdatager_controle_de_acessos[".isDisplayLoading"] = true;


$tdatager_controle_de_acessos[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_controle_de_acessos[".pageSize"] = 20;

$tdatager_controle_de_acessos[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_controle_de_acessos[".strOrderBy"] = $tstrOrderBy;

$tdatager_controle_de_acessos[".orderindexes"] = array();

$tdatager_controle_de_acessos[".sqlHead"] = "SELECT idControle,  usuario,  cod_conex,  hora";
$tdatager_controle_de_acessos[".sqlFrom"] = "FROM ger_controle_de_acessos";
$tdatager_controle_de_acessos[".sqlWhereExpr"] = "";
$tdatager_controle_de_acessos[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_controle_de_acessos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_controle_de_acessos[".arrGroupsPerPage"] = $arrGPP;

$tdatager_controle_de_acessos[".highlightSearchResults"] = true;

$tableKeysger_controle_de_acessos = array();
$tableKeysger_controle_de_acessos[] = "idControle";
$tdatager_controle_de_acessos[".Keys"] = $tableKeysger_controle_de_acessos;

$tdatager_controle_de_acessos[".listFields"] = array();
$tdatager_controle_de_acessos[".listFields"][] = "usuario";
$tdatager_controle_de_acessos[".listFields"][] = "cod_conex";
$tdatager_controle_de_acessos[".listFields"][] = "hora";

$tdatager_controle_de_acessos[".hideMobileList"] = array();


$tdatager_controle_de_acessos[".viewFields"] = array();
$tdatager_controle_de_acessos[".viewFields"][] = "usuario";
$tdatager_controle_de_acessos[".viewFields"][] = "cod_conex";
$tdatager_controle_de_acessos[".viewFields"][] = "hora";

$tdatager_controle_de_acessos[".addFields"] = array();

$tdatager_controle_de_acessos[".masterListFields"] = array();

$tdatager_controle_de_acessos[".inlineAddFields"] = array();

$tdatager_controle_de_acessos[".editFields"] = array();

$tdatager_controle_de_acessos[".inlineEditFields"] = array();

$tdatager_controle_de_acessos[".exportFields"] = array();
$tdatager_controle_de_acessos[".exportFields"][] = "usuario";
$tdatager_controle_de_acessos[".exportFields"][] = "cod_conex";
$tdatager_controle_de_acessos[".exportFields"][] = "hora";

$tdatager_controle_de_acessos[".importFields"] = array();
$tdatager_controle_de_acessos[".importFields"][] = "idControle";
$tdatager_controle_de_acessos[".importFields"][] = "usuario";
$tdatager_controle_de_acessos[".importFields"][] = "cod_conex";
$tdatager_controle_de_acessos[".importFields"][] = "hora";

$tdatager_controle_de_acessos[".printFields"] = array();
$tdatager_controle_de_acessos[".printFields"][] = "usuario";
$tdatager_controle_de_acessos[".printFields"][] = "cod_conex";
$tdatager_controle_de_acessos[".printFields"][] = "hora";

//	idControle
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idControle";
	$fdata["GoodName"] = "idControle";
	$fdata["ownerTable"] = "ger_controle_de_acessos";
	$fdata["Label"] = GetFieldLabel("ger_controle_de_acessos","idControle");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idControle";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idControle";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_controle_de_acessos["idControle"] = $fdata;
//	usuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "usuario";
	$fdata["GoodName"] = "usuario";
	$fdata["ownerTable"] = "ger_controle_de_acessos";
	$fdata["Label"] = GetFieldLabel("ger_controle_de_acessos","usuario");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "usuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "usuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=45";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_controle_de_acessos["usuario"] = $fdata;
//	cod_conex
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "cod_conex";
	$fdata["GoodName"] = "cod_conex";
	$fdata["ownerTable"] = "ger_controle_de_acessos";
	$fdata["Label"] = GetFieldLabel("ger_controle_de_acessos","cod_conex");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cod_conex";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_conex";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=45";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_controle_de_acessos["cod_conex"] = $fdata;
//	hora
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "hora";
	$fdata["GoodName"] = "hora";
	$fdata["ownerTable"] = "ger_controle_de_acessos";
	$fdata["Label"] = GetFieldLabel("ger_controle_de_acessos","hora");
	$fdata["FieldType"] = 135;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "hora";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "hora";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_controle_de_acessos["hora"] = $fdata;


$tables_data["ger_controle_de_acessos"]=&$tdatager_controle_de_acessos;
$field_labels["ger_controle_de_acessos"] = &$fieldLabelsger_controle_de_acessos;
$fieldToolTips["ger_controle_de_acessos"] = &$fieldToolTipsger_controle_de_acessos;
$page_titles["ger_controle_de_acessos"] = &$pageTitlesger_controle_de_acessos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_controle_de_acessos"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_controle_de_acessos"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_controle_de_acessos()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idControle,  usuario,  cod_conex,  hora";
$proto0["m_strFrom"] = "FROM ger_controle_de_acessos";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idControle",
	"m_strTable" => "ger_controle_de_acessos",
	"m_srcTableName" => "ger_controle_de_acessos"
));

$proto6["m_sql"] = "idControle";
$proto6["m_srcTableName"] = "ger_controle_de_acessos";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "usuario",
	"m_strTable" => "ger_controle_de_acessos",
	"m_srcTableName" => "ger_controle_de_acessos"
));

$proto8["m_sql"] = "usuario";
$proto8["m_srcTableName"] = "ger_controle_de_acessos";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_conex",
	"m_strTable" => "ger_controle_de_acessos",
	"m_srcTableName" => "ger_controle_de_acessos"
));

$proto10["m_sql"] = "cod_conex";
$proto10["m_srcTableName"] = "ger_controle_de_acessos";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "hora",
	"m_strTable" => "ger_controle_de_acessos",
	"m_srcTableName" => "ger_controle_de_acessos"
));

$proto12["m_sql"] = "hora";
$proto12["m_srcTableName"] = "ger_controle_de_acessos";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "ger_controle_de_acessos";
$proto15["m_srcTableName"] = "ger_controle_de_acessos";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "idControle";
$proto15["m_columns"][] = "usuario";
$proto15["m_columns"][] = "cod_conex";
$proto15["m_columns"][] = "hora";
$proto15["m_columns"][] = "ip";
$proto15["m_columns"][] = "browser";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "ger_controle_de_acessos";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "ger_controle_de_acessos";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_controle_de_acessos";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_controle_de_acessos = createSqlQuery_ger_controle_de_acessos();


	
		;

				

$tdatager_controle_de_acessos[".sqlquery"] = $queryData_ger_controle_de_acessos;

$tableEvents["ger_controle_de_acessos"] = new eventsBase;
$tdatager_controle_de_acessos[".hasEvents"] = false;

?>