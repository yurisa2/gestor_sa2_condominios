<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_lista_uf = array();
	$tdatager_lista_uf[".truncateText"] = true;
	$tdatager_lista_uf[".NumberOfChars"] = 80;
	$tdatager_lista_uf[".ShortName"] = "ger_lista_uf";
	$tdatager_lista_uf[".OwnerID"] = "";
	$tdatager_lista_uf[".OriginalTable"] = "ger_lista_uf";

//	field labels
$fieldLabelsger_lista_uf = array();
$fieldToolTipsger_lista_uf = array();
$pageTitlesger_lista_uf = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_lista_uf["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_lista_uf["Portuguese(Brazil)"] = array();
	$pageTitlesger_lista_uf["Portuguese(Brazil)"] = array();
	$fieldLabelsger_lista_uf["Portuguese(Brazil)"]["idUf"] = "Código";
	$fieldToolTipsger_lista_uf["Portuguese(Brazil)"]["idUf"] = "";
	$fieldLabelsger_lista_uf["Portuguese(Brazil)"]["uf"] = "UF";
	$fieldToolTipsger_lista_uf["Portuguese(Brazil)"]["uf"] = "";
	if (count($fieldToolTipsger_lista_uf["Portuguese(Brazil)"]))
		$tdatager_lista_uf[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_lista_uf[""] = array();
	$fieldToolTipsger_lista_uf[""] = array();
	$pageTitlesger_lista_uf[""] = array();
	if (count($fieldToolTipsger_lista_uf[""]))
		$tdatager_lista_uf[".isUseToolTips"] = true;
}


	$tdatager_lista_uf[".NCSearch"] = true;



$tdatager_lista_uf[".shortTableName"] = "ger_lista_uf";
$tdatager_lista_uf[".nSecOptions"] = 0;
$tdatager_lista_uf[".recsPerRowList"] = 1;
$tdatager_lista_uf[".recsPerRowPrint"] = 1;
$tdatager_lista_uf[".mainTableOwnerID"] = "";
$tdatager_lista_uf[".moveNext"] = 1;
$tdatager_lista_uf[".entityType"] = 0;

$tdatager_lista_uf[".strOriginalTableName"] = "ger_lista_uf";





$tdatager_lista_uf[".showAddInPopup"] = false;

$tdatager_lista_uf[".showEditInPopup"] = false;

$tdatager_lista_uf[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_lista_uf[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_lista_uf[".fieldsForRegister"] = array();

$tdatager_lista_uf[".listAjax"] = false;

	$tdatager_lista_uf[".audit"] = true;

	$tdatager_lista_uf[".locking"] = true;

$tdatager_lista_uf[".edit"] = true;
$tdatager_lista_uf[".afterEditAction"] = 1;
$tdatager_lista_uf[".closePopupAfterEdit"] = 1;
$tdatager_lista_uf[".afterEditActionDetTable"] = "";

$tdatager_lista_uf[".add"] = true;
$tdatager_lista_uf[".afterAddAction"] = 1;
$tdatager_lista_uf[".closePopupAfterAdd"] = 1;
$tdatager_lista_uf[".afterAddActionDetTable"] = "";

$tdatager_lista_uf[".list"] = true;





$tdatager_lista_uf[".delete"] = true;

$tdatager_lista_uf[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_lista_uf[".searchSaving"] = false;
//

$tdatager_lista_uf[".showSearchPanel"] = true;
		$tdatager_lista_uf[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_lista_uf[".isUseAjaxSuggest"] = false;
else
	$tdatager_lista_uf[".isUseAjaxSuggest"] = true;

$tdatager_lista_uf[".rowHighlite"] = true;



$tdatager_lista_uf[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_lista_uf[".isUseTimeForSearch"] = false;





$tdatager_lista_uf[".allSearchFields"] = array();
$tdatager_lista_uf[".filterFields"] = array();
$tdatager_lista_uf[".requiredSearchFields"] = array();

$tdatager_lista_uf[".allSearchFields"][] = "uf";
	

$tdatager_lista_uf[".googleLikeFields"] = array();
$tdatager_lista_uf[".googleLikeFields"][] = "uf";


$tdatager_lista_uf[".advSearchFields"] = array();
$tdatager_lista_uf[".advSearchFields"][] = "uf";

$tdatager_lista_uf[".tableType"] = "list";

$tdatager_lista_uf[".printerPageOrientation"] = 0;
$tdatager_lista_uf[".nPrinterPageScale"] = 100;

$tdatager_lista_uf[".nPrinterSplitRecords"] = 40;

$tdatager_lista_uf[".nPrinterPDFSplitRecords"] = 40;



$tdatager_lista_uf[".geocodingEnabled"] = false;





$tdatager_lista_uf[".listGridLayout"] = 3;

$tdatager_lista_uf[".isDisplayLoading"] = true;


$tdatager_lista_uf[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_lista_uf[".pageSize"] = 20;

$tdatager_lista_uf[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_lista_uf[".strOrderBy"] = $tstrOrderBy;

$tdatager_lista_uf[".orderindexes"] = array();

$tdatager_lista_uf[".sqlHead"] = "select idUf,  uf";
$tdatager_lista_uf[".sqlFrom"] = "FROM ger_lista_uf";
$tdatager_lista_uf[".sqlWhereExpr"] = "";
$tdatager_lista_uf[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_lista_uf[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_lista_uf[".arrGroupsPerPage"] = $arrGPP;

$tdatager_lista_uf[".highlightSearchResults"] = true;

$tableKeysger_lista_uf = array();
$tableKeysger_lista_uf[] = "idUf";
$tdatager_lista_uf[".Keys"] = $tableKeysger_lista_uf;

$tdatager_lista_uf[".listFields"] = array();
$tdatager_lista_uf[".listFields"][] = "uf";

$tdatager_lista_uf[".hideMobileList"] = array();


$tdatager_lista_uf[".viewFields"] = array();

$tdatager_lista_uf[".addFields"] = array();
$tdatager_lista_uf[".addFields"][] = "uf";

$tdatager_lista_uf[".masterListFields"] = array();

$tdatager_lista_uf[".inlineAddFields"] = array();

$tdatager_lista_uf[".editFields"] = array();
$tdatager_lista_uf[".editFields"][] = "uf";

$tdatager_lista_uf[".inlineEditFields"] = array();

$tdatager_lista_uf[".exportFields"] = array();

$tdatager_lista_uf[".importFields"] = array();
$tdatager_lista_uf[".importFields"][] = "idUf";
$tdatager_lista_uf[".importFields"][] = "uf";

$tdatager_lista_uf[".printFields"] = array();

//	idUf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idUf";
	$fdata["GoodName"] = "idUf";
	$fdata["ownerTable"] = "ger_lista_uf";
	$fdata["Label"] = GetFieldLabel("ger_lista_uf","idUf");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idUf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idUf";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_lista_uf["idUf"] = $fdata;
//	uf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "uf";
	$fdata["GoodName"] = "uf";
	$fdata["ownerTable"] = "ger_lista_uf";
	$fdata["Label"] = GetFieldLabel("ger_lista_uf","uf");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "uf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "uf";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=2";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_lista_uf["uf"] = $fdata;


$tables_data["ger_lista_uf"]=&$tdatager_lista_uf;
$field_labels["ger_lista_uf"] = &$fieldLabelsger_lista_uf;
$fieldToolTips["ger_lista_uf"] = &$fieldToolTipsger_lista_uf;
$page_titles["ger_lista_uf"] = &$pageTitlesger_lista_uf;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_lista_uf"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_lista_uf"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_lista_uf()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idUf,  uf";
$proto0["m_strFrom"] = "FROM ger_lista_uf";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idUf",
	"m_strTable" => "ger_lista_uf",
	"m_srcTableName" => "ger_lista_uf"
));

$proto6["m_sql"] = "idUf";
$proto6["m_srcTableName"] = "ger_lista_uf";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "uf",
	"m_strTable" => "ger_lista_uf",
	"m_srcTableName" => "ger_lista_uf"
));

$proto8["m_sql"] = "uf";
$proto8["m_srcTableName"] = "ger_lista_uf";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "ger_lista_uf";
$proto11["m_srcTableName"] = "ger_lista_uf";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "idUf";
$proto11["m_columns"][] = "uf";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "ger_lista_uf";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "ger_lista_uf";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_lista_uf";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_lista_uf = createSqlQuery_ger_lista_uf();


	
		;

		

$tdatager_lista_uf[".sqlquery"] = $queryData_ger_lista_uf;

include_once(getabspath("include/ger_lista_uf_events.php"));
$tableEvents["ger_lista_uf"] = new eventclass_ger_lista_uf;
$tdatager_lista_uf[".hasEvents"] = true;

?>