<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_fornecedores = array();
	$tdatafin_fornecedores[".truncateText"] = true;
	$tdatafin_fornecedores[".NumberOfChars"] = 80;
	$tdatafin_fornecedores[".ShortName"] = "fin_fornecedores";
	$tdatafin_fornecedores[".OwnerID"] = "";
	$tdatafin_fornecedores[".OriginalTable"] = "fin_fornecedores";

//	field labels
$fieldLabelsfin_fornecedores = array();
$fieldToolTipsfin_fornecedores = array();
$pageTitlesfin_fornecedores = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"] = array();
	$pageTitlesfin_fornecedores["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["idForn"] = "Código";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["idForn"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["razaosocial"] = "Razão Social";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["razaosocial"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["nomefantasia"] = "Nome fantasia";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["nomefantasia"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["cnpjcpf"] = "CNPJ/CPF";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["cnpjcpf"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["ie"] = "I.E.";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["ie"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["log"] = "log";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["log"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["end"] = "Endereçoo";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["end"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["num"] = "N.";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["num"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["comp"] = "Com.";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["comp"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["bairro"] = "Bairro";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["bairro"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["cidade"] = "Cidade";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["cidade"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["uf"] = "UF";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["uf"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["cep"] = "Cep";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["cep"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["telefone"] = "Telefone";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["telefone"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["fax"] = "Fax";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["fax"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["email"] = "Email";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["email"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["site"] = "Site";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["site"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["tipofornecimento"] = "Tipo de fornecimento";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["tipofornecimento"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["celular"] = "Celular";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["celular"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_fornecedores["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsfin_fornecedores["Portuguese(Brazil)"]))
		$tdatafin_fornecedores[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_fornecedores[""] = array();
	$fieldToolTipsfin_fornecedores[""] = array();
	$pageTitlesfin_fornecedores[""] = array();
	if (count($fieldToolTipsfin_fornecedores[""]))
		$tdatafin_fornecedores[".isUseToolTips"] = true;
}


	$tdatafin_fornecedores[".NCSearch"] = true;



$tdatafin_fornecedores[".shortTableName"] = "fin_fornecedores";
$tdatafin_fornecedores[".nSecOptions"] = 0;
$tdatafin_fornecedores[".recsPerRowList"] = 1;
$tdatafin_fornecedores[".recsPerRowPrint"] = 1;
$tdatafin_fornecedores[".mainTableOwnerID"] = "";
$tdatafin_fornecedores[".moveNext"] = 1;
$tdatafin_fornecedores[".entityType"] = 0;

$tdatafin_fornecedores[".strOriginalTableName"] = "fin_fornecedores";





$tdatafin_fornecedores[".showAddInPopup"] = false;

$tdatafin_fornecedores[".showEditInPopup"] = false;

$tdatafin_fornecedores[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_fornecedores[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_fornecedores[".fieldsForRegister"] = array();

$tdatafin_fornecedores[".listAjax"] = false;

	$tdatafin_fornecedores[".audit"] = true;

	$tdatafin_fornecedores[".locking"] = true;

$tdatafin_fornecedores[".edit"] = true;
$tdatafin_fornecedores[".afterEditAction"] = 1;
$tdatafin_fornecedores[".closePopupAfterEdit"] = 1;
$tdatafin_fornecedores[".afterEditActionDetTable"] = "";

$tdatafin_fornecedores[".add"] = true;
$tdatafin_fornecedores[".afterAddAction"] = 1;
$tdatafin_fornecedores[".closePopupAfterAdd"] = 1;
$tdatafin_fornecedores[".afterAddActionDetTable"] = "";

$tdatafin_fornecedores[".list"] = true;

$tdatafin_fornecedores[".copy"] = true;
$tdatafin_fornecedores[".view"] = true;



$tdatafin_fornecedores[".printFriendly"] = true;

$tdatafin_fornecedores[".delete"] = true;

$tdatafin_fornecedores[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_fornecedores[".searchSaving"] = false;
//

$tdatafin_fornecedores[".showSearchPanel"] = true;
		$tdatafin_fornecedores[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_fornecedores[".isUseAjaxSuggest"] = false;
else
	$tdatafin_fornecedores[".isUseAjaxSuggest"] = true;

$tdatafin_fornecedores[".rowHighlite"] = true;



$tdatafin_fornecedores[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_fornecedores[".isUseTimeForSearch"] = false;





$tdatafin_fornecedores[".allSearchFields"] = array();
$tdatafin_fornecedores[".filterFields"] = array();
$tdatafin_fornecedores[".requiredSearchFields"] = array();

$tdatafin_fornecedores[".allSearchFields"][] = "ultimousuario";
	$tdatafin_fornecedores[".allSearchFields"][] = "ultimaalteracao";
	$tdatafin_fornecedores[".allSearchFields"][] = "razaosocial";
	$tdatafin_fornecedores[".allSearchFields"][] = "nomefantasia";
	$tdatafin_fornecedores[".allSearchFields"][] = "cnpjcpf";
	$tdatafin_fornecedores[".allSearchFields"][] = "ie";
	$tdatafin_fornecedores[".allSearchFields"][] = "log";
	$tdatafin_fornecedores[".allSearchFields"][] = "end";
	$tdatafin_fornecedores[".allSearchFields"][] = "num";
	$tdatafin_fornecedores[".allSearchFields"][] = "comp";
	$tdatafin_fornecedores[".allSearchFields"][] = "bairro";
	$tdatafin_fornecedores[".allSearchFields"][] = "cidade";
	$tdatafin_fornecedores[".allSearchFields"][] = "uf";
	$tdatafin_fornecedores[".allSearchFields"][] = "cep";
	$tdatafin_fornecedores[".allSearchFields"][] = "obs";
	$tdatafin_fornecedores[".allSearchFields"][] = "telefone";
	$tdatafin_fornecedores[".allSearchFields"][] = "fax";
	$tdatafin_fornecedores[".allSearchFields"][] = "email";
	$tdatafin_fornecedores[".allSearchFields"][] = "site";
	$tdatafin_fornecedores[".allSearchFields"][] = "tipofornecimento";
	$tdatafin_fornecedores[".allSearchFields"][] = "celular";
	

$tdatafin_fornecedores[".googleLikeFields"] = array();
$tdatafin_fornecedores[".googleLikeFields"][] = "razaosocial";
$tdatafin_fornecedores[".googleLikeFields"][] = "nomefantasia";
$tdatafin_fornecedores[".googleLikeFields"][] = "cnpjcpf";
$tdatafin_fornecedores[".googleLikeFields"][] = "ie";
$tdatafin_fornecedores[".googleLikeFields"][] = "log";
$tdatafin_fornecedores[".googleLikeFields"][] = "end";
$tdatafin_fornecedores[".googleLikeFields"][] = "num";
$tdatafin_fornecedores[".googleLikeFields"][] = "comp";
$tdatafin_fornecedores[".googleLikeFields"][] = "bairro";
$tdatafin_fornecedores[".googleLikeFields"][] = "cidade";
$tdatafin_fornecedores[".googleLikeFields"][] = "uf";
$tdatafin_fornecedores[".googleLikeFields"][] = "cep";
$tdatafin_fornecedores[".googleLikeFields"][] = "obs";
$tdatafin_fornecedores[".googleLikeFields"][] = "telefone";
$tdatafin_fornecedores[".googleLikeFields"][] = "fax";
$tdatafin_fornecedores[".googleLikeFields"][] = "email";
$tdatafin_fornecedores[".googleLikeFields"][] = "site";
$tdatafin_fornecedores[".googleLikeFields"][] = "tipofornecimento";
$tdatafin_fornecedores[".googleLikeFields"][] = "celular";
$tdatafin_fornecedores[".googleLikeFields"][] = "ultimousuario";
$tdatafin_fornecedores[".googleLikeFields"][] = "ultimaalteracao";


$tdatafin_fornecedores[".advSearchFields"] = array();
$tdatafin_fornecedores[".advSearchFields"][] = "ultimousuario";
$tdatafin_fornecedores[".advSearchFields"][] = "ultimaalteracao";
$tdatafin_fornecedores[".advSearchFields"][] = "razaosocial";
$tdatafin_fornecedores[".advSearchFields"][] = "nomefantasia";
$tdatafin_fornecedores[".advSearchFields"][] = "cnpjcpf";
$tdatafin_fornecedores[".advSearchFields"][] = "ie";
$tdatafin_fornecedores[".advSearchFields"][] = "log";
$tdatafin_fornecedores[".advSearchFields"][] = "end";
$tdatafin_fornecedores[".advSearchFields"][] = "num";
$tdatafin_fornecedores[".advSearchFields"][] = "comp";
$tdatafin_fornecedores[".advSearchFields"][] = "bairro";
$tdatafin_fornecedores[".advSearchFields"][] = "cidade";
$tdatafin_fornecedores[".advSearchFields"][] = "uf";
$tdatafin_fornecedores[".advSearchFields"][] = "cep";
$tdatafin_fornecedores[".advSearchFields"][] = "obs";
$tdatafin_fornecedores[".advSearchFields"][] = "telefone";
$tdatafin_fornecedores[".advSearchFields"][] = "fax";
$tdatafin_fornecedores[".advSearchFields"][] = "email";
$tdatafin_fornecedores[".advSearchFields"][] = "site";
$tdatafin_fornecedores[".advSearchFields"][] = "tipofornecimento";
$tdatafin_fornecedores[".advSearchFields"][] = "celular";

$tdatafin_fornecedores[".tableType"] = "list";

$tdatafin_fornecedores[".printerPageOrientation"] = 0;
$tdatafin_fornecedores[".nPrinterPageScale"] = 100;

$tdatafin_fornecedores[".nPrinterSplitRecords"] = 40;

$tdatafin_fornecedores[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_fornecedores[".geocodingEnabled"] = false;





$tdatafin_fornecedores[".listGridLayout"] = 3;

$tdatafin_fornecedores[".isDisplayLoading"] = true;


$tdatafin_fornecedores[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatafin_fornecedores[".pageSize"] = 20;

$tdatafin_fornecedores[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_fornecedores[".strOrderBy"] = $tstrOrderBy;

$tdatafin_fornecedores[".orderindexes"] = array();

$tdatafin_fornecedores[".sqlHead"] = "select idForn,  razaosocial,  nomefantasia,  cnpjcpf,  ie,  log,  `end`,  num,  comp,  bairro,  cidade,  uf,  cep,  obs,  telefone,  fax,  email,  site,  tipofornecimento,  celular,  ultimousuario,  ultimaalteracao";
$tdatafin_fornecedores[".sqlFrom"] = "FROM fin_fornecedores";
$tdatafin_fornecedores[".sqlWhereExpr"] = "";
$tdatafin_fornecedores[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_fornecedores[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_fornecedores[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_fornecedores[".highlightSearchResults"] = true;

$tableKeysfin_fornecedores = array();
$tableKeysfin_fornecedores[] = "idForn";
$tdatafin_fornecedores[".Keys"] = $tableKeysfin_fornecedores;

$tdatafin_fornecedores[".listFields"] = array();
$tdatafin_fornecedores[".listFields"][] = "razaosocial";
$tdatafin_fornecedores[".listFields"][] = "nomefantasia";
$tdatafin_fornecedores[".listFields"][] = "cnpjcpf";
$tdatafin_fornecedores[".listFields"][] = "ie";
$tdatafin_fornecedores[".listFields"][] = "log";
$tdatafin_fornecedores[".listFields"][] = "end";
$tdatafin_fornecedores[".listFields"][] = "num";
$tdatafin_fornecedores[".listFields"][] = "comp";
$tdatafin_fornecedores[".listFields"][] = "bairro";
$tdatafin_fornecedores[".listFields"][] = "cidade";
$tdatafin_fornecedores[".listFields"][] = "uf";
$tdatafin_fornecedores[".listFields"][] = "cep";
$tdatafin_fornecedores[".listFields"][] = "obs";
$tdatafin_fornecedores[".listFields"][] = "telefone";
$tdatafin_fornecedores[".listFields"][] = "fax";
$tdatafin_fornecedores[".listFields"][] = "email";
$tdatafin_fornecedores[".listFields"][] = "site";
$tdatafin_fornecedores[".listFields"][] = "tipofornecimento";
$tdatafin_fornecedores[".listFields"][] = "celular";

$tdatafin_fornecedores[".hideMobileList"] = array();


$tdatafin_fornecedores[".viewFields"] = array();
$tdatafin_fornecedores[".viewFields"][] = "idForn";
$tdatafin_fornecedores[".viewFields"][] = "razaosocial";
$tdatafin_fornecedores[".viewFields"][] = "nomefantasia";
$tdatafin_fornecedores[".viewFields"][] = "cnpjcpf";
$tdatafin_fornecedores[".viewFields"][] = "ie";
$tdatafin_fornecedores[".viewFields"][] = "log";
$tdatafin_fornecedores[".viewFields"][] = "end";
$tdatafin_fornecedores[".viewFields"][] = "num";
$tdatafin_fornecedores[".viewFields"][] = "comp";
$tdatafin_fornecedores[".viewFields"][] = "bairro";
$tdatafin_fornecedores[".viewFields"][] = "cidade";
$tdatafin_fornecedores[".viewFields"][] = "uf";
$tdatafin_fornecedores[".viewFields"][] = "cep";
$tdatafin_fornecedores[".viewFields"][] = "obs";
$tdatafin_fornecedores[".viewFields"][] = "telefone";
$tdatafin_fornecedores[".viewFields"][] = "fax";
$tdatafin_fornecedores[".viewFields"][] = "email";
$tdatafin_fornecedores[".viewFields"][] = "site";
$tdatafin_fornecedores[".viewFields"][] = "tipofornecimento";
$tdatafin_fornecedores[".viewFields"][] = "celular";
$tdatafin_fornecedores[".viewFields"][] = "ultimousuario";
$tdatafin_fornecedores[".viewFields"][] = "ultimaalteracao";

$tdatafin_fornecedores[".addFields"] = array();
$tdatafin_fornecedores[".addFields"][] = "razaosocial";
$tdatafin_fornecedores[".addFields"][] = "nomefantasia";
$tdatafin_fornecedores[".addFields"][] = "cnpjcpf";
$tdatafin_fornecedores[".addFields"][] = "ie";
$tdatafin_fornecedores[".addFields"][] = "log";
$tdatafin_fornecedores[".addFields"][] = "end";
$tdatafin_fornecedores[".addFields"][] = "num";
$tdatafin_fornecedores[".addFields"][] = "comp";
$tdatafin_fornecedores[".addFields"][] = "bairro";
$tdatafin_fornecedores[".addFields"][] = "cidade";
$tdatafin_fornecedores[".addFields"][] = "uf";
$tdatafin_fornecedores[".addFields"][] = "cep";
$tdatafin_fornecedores[".addFields"][] = "obs";
$tdatafin_fornecedores[".addFields"][] = "telefone";
$tdatafin_fornecedores[".addFields"][] = "fax";
$tdatafin_fornecedores[".addFields"][] = "email";
$tdatafin_fornecedores[".addFields"][] = "site";
$tdatafin_fornecedores[".addFields"][] = "tipofornecimento";
$tdatafin_fornecedores[".addFields"][] = "celular";

$tdatafin_fornecedores[".masterListFields"] = array();

$tdatafin_fornecedores[".inlineAddFields"] = array();

$tdatafin_fornecedores[".editFields"] = array();
$tdatafin_fornecedores[".editFields"][] = "idForn";
$tdatafin_fornecedores[".editFields"][] = "razaosocial";
$tdatafin_fornecedores[".editFields"][] = "nomefantasia";
$tdatafin_fornecedores[".editFields"][] = "cnpjcpf";
$tdatafin_fornecedores[".editFields"][] = "ie";
$tdatafin_fornecedores[".editFields"][] = "log";
$tdatafin_fornecedores[".editFields"][] = "end";
$tdatafin_fornecedores[".editFields"][] = "num";
$tdatafin_fornecedores[".editFields"][] = "comp";
$tdatafin_fornecedores[".editFields"][] = "bairro";
$tdatafin_fornecedores[".editFields"][] = "cidade";
$tdatafin_fornecedores[".editFields"][] = "uf";
$tdatafin_fornecedores[".editFields"][] = "cep";
$tdatafin_fornecedores[".editFields"][] = "obs";
$tdatafin_fornecedores[".editFields"][] = "telefone";
$tdatafin_fornecedores[".editFields"][] = "fax";
$tdatafin_fornecedores[".editFields"][] = "email";
$tdatafin_fornecedores[".editFields"][] = "site";
$tdatafin_fornecedores[".editFields"][] = "tipofornecimento";
$tdatafin_fornecedores[".editFields"][] = "celular";

$tdatafin_fornecedores[".inlineEditFields"] = array();

$tdatafin_fornecedores[".exportFields"] = array();

$tdatafin_fornecedores[".importFields"] = array();
$tdatafin_fornecedores[".importFields"][] = "idForn";
$tdatafin_fornecedores[".importFields"][] = "razaosocial";
$tdatafin_fornecedores[".importFields"][] = "nomefantasia";
$tdatafin_fornecedores[".importFields"][] = "cnpjcpf";
$tdatafin_fornecedores[".importFields"][] = "ie";
$tdatafin_fornecedores[".importFields"][] = "log";
$tdatafin_fornecedores[".importFields"][] = "end";
$tdatafin_fornecedores[".importFields"][] = "num";
$tdatafin_fornecedores[".importFields"][] = "comp";
$tdatafin_fornecedores[".importFields"][] = "bairro";
$tdatafin_fornecedores[".importFields"][] = "cidade";
$tdatafin_fornecedores[".importFields"][] = "uf";
$tdatafin_fornecedores[".importFields"][] = "cep";
$tdatafin_fornecedores[".importFields"][] = "obs";
$tdatafin_fornecedores[".importFields"][] = "telefone";
$tdatafin_fornecedores[".importFields"][] = "fax";
$tdatafin_fornecedores[".importFields"][] = "email";
$tdatafin_fornecedores[".importFields"][] = "site";
$tdatafin_fornecedores[".importFields"][] = "tipofornecimento";
$tdatafin_fornecedores[".importFields"][] = "celular";
$tdatafin_fornecedores[".importFields"][] = "ultimousuario";
$tdatafin_fornecedores[".importFields"][] = "ultimaalteracao";

$tdatafin_fornecedores[".printFields"] = array();
$tdatafin_fornecedores[".printFields"][] = "razaosocial";
$tdatafin_fornecedores[".printFields"][] = "nomefantasia";
$tdatafin_fornecedores[".printFields"][] = "cnpjcpf";
$tdatafin_fornecedores[".printFields"][] = "ie";
$tdatafin_fornecedores[".printFields"][] = "log";
$tdatafin_fornecedores[".printFields"][] = "end";
$tdatafin_fornecedores[".printFields"][] = "num";
$tdatafin_fornecedores[".printFields"][] = "comp";
$tdatafin_fornecedores[".printFields"][] = "bairro";
$tdatafin_fornecedores[".printFields"][] = "cidade";
$tdatafin_fornecedores[".printFields"][] = "uf";
$tdatafin_fornecedores[".printFields"][] = "cep";
$tdatafin_fornecedores[".printFields"][] = "obs";
$tdatafin_fornecedores[".printFields"][] = "telefone";
$tdatafin_fornecedores[".printFields"][] = "fax";
$tdatafin_fornecedores[".printFields"][] = "email";
$tdatafin_fornecedores[".printFields"][] = "site";
$tdatafin_fornecedores[".printFields"][] = "tipofornecimento";
$tdatafin_fornecedores[".printFields"][] = "celular";

//	idForn
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idForn";
	$fdata["GoodName"] = "idForn";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","idForn");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idForn";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idForn";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_fornecedores["idForn"] = $fdata;
//	razaosocial
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "razaosocial";
	$fdata["GoodName"] = "razaosocial";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","razaosocial");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "razaosocial";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "razaosocial";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["razaosocial"] = $fdata;
//	nomefantasia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nomefantasia";
	$fdata["GoodName"] = "nomefantasia";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","nomefantasia");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "nomefantasia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nomefantasia";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["nomefantasia"] = $fdata;
//	cnpjcpf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "cnpjcpf";
	$fdata["GoodName"] = "cnpjcpf";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","cnpjcpf");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "cnpjcpf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cnpjcpf";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["cnpjcpf"] = $fdata;
//	ie
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ie";
	$fdata["GoodName"] = "ie";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","ie");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "ie";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ie";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["ie"] = $fdata;
//	log
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "log";
	$fdata["GoodName"] = "log";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","log");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "log";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "log";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "exp_ger_lista_logs";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "log";
	$edata["LinkFieldType"] = 129;
	$edata["DisplayField"] = "log";

	
	$edata["LookupOrderBy"] = "log";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["log"] = $fdata;
//	end
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "end";
	$fdata["GoodName"] = "end";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","end");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "end";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`end`";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["end"] = $fdata;
//	num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "num";
	$fdata["GoodName"] = "num";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","num");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "num";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "num";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["num"] = $fdata;
//	comp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "comp";
	$fdata["GoodName"] = "comp";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","comp");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "comp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comp";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["comp"] = $fdata;
//	bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "bairro";
	$fdata["GoodName"] = "bairro";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","bairro");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["bairro"] = $fdata;
//	cidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "cidade";
	$fdata["GoodName"] = "cidade";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","cidade");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "cidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["cidade"] = $fdata;
//	uf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "uf";
	$fdata["GoodName"] = "uf";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","uf");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "uf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "uf";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_lista_uf";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "uf";
	$edata["LinkFieldType"] = 129;
	$edata["DisplayField"] = "uf";

	
	$edata["LookupOrderBy"] = "uf";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["uf"] = $fdata;
//	cep
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "cep";
	$fdata["GoodName"] = "cep";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","cep");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "cep";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cep";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=9";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["cep"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["obs"] = $fdata;
//	telefone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "telefone";
	$fdata["GoodName"] = "telefone";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","telefone");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "telefone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "telefone";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["telefone"] = $fdata;
//	fax
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "fax";
	$fdata["GoodName"] = "fax";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","fax");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "fax";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fax";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["fax"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","email");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["email"] = $fdata;
//	site
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "site";
	$fdata["GoodName"] = "site";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","site");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "site";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "site";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["site"] = $fdata;
//	tipofornecimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "tipofornecimento";
	$fdata["GoodName"] = "tipofornecimento";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","tipofornecimento");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "tipofornecimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipofornecimento";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["tipofornecimento"] = $fdata;
//	celular
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "celular";
	$fdata["GoodName"] = "celular";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","celular");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "celular";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "celular";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["celular"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_fornecedores["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_fornecedores";
	$fdata["Label"] = GetFieldLabel("fin_fornecedores","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_fornecedores["ultimaalteracao"] = $fdata;


$tables_data["fin_fornecedores"]=&$tdatafin_fornecedores;
$field_labels["fin_fornecedores"] = &$fieldLabelsfin_fornecedores;
$fieldToolTips["fin_fornecedores"] = &$fieldToolTipsfin_fornecedores;
$page_titles["fin_fornecedores"] = &$pageTitlesfin_fornecedores;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_fornecedores"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_fornecedores"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_fornecedores()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idForn,  razaosocial,  nomefantasia,  cnpjcpf,  ie,  log,  `end`,  num,  comp,  bairro,  cidade,  uf,  cep,  obs,  telefone,  fax,  email,  site,  tipofornecimento,  celular,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM fin_fornecedores";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idForn",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto6["m_sql"] = "idForn";
$proto6["m_srcTableName"] = "fin_fornecedores";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "razaosocial",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto8["m_sql"] = "razaosocial";
$proto8["m_srcTableName"] = "fin_fornecedores";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nomefantasia",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto10["m_sql"] = "nomefantasia";
$proto10["m_srcTableName"] = "fin_fornecedores";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "cnpjcpf",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto12["m_sql"] = "cnpjcpf";
$proto12["m_srcTableName"] = "fin_fornecedores";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ie",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto14["m_sql"] = "ie";
$proto14["m_srcTableName"] = "fin_fornecedores";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "log",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto16["m_sql"] = "log";
$proto16["m_srcTableName"] = "fin_fornecedores";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "end",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto18["m_sql"] = "`end`";
$proto18["m_srcTableName"] = "fin_fornecedores";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "num",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto20["m_sql"] = "num";
$proto20["m_srcTableName"] = "fin_fornecedores";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "comp",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto22["m_sql"] = "comp";
$proto22["m_srcTableName"] = "fin_fornecedores";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto24["m_sql"] = "bairro";
$proto24["m_srcTableName"] = "fin_fornecedores";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "cidade",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto26["m_sql"] = "cidade";
$proto26["m_srcTableName"] = "fin_fornecedores";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "uf",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto28["m_sql"] = "uf";
$proto28["m_srcTableName"] = "fin_fornecedores";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "cep",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto30["m_sql"] = "cep";
$proto30["m_srcTableName"] = "fin_fornecedores";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto32["m_sql"] = "obs";
$proto32["m_srcTableName"] = "fin_fornecedores";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "telefone",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto34["m_sql"] = "telefone";
$proto34["m_srcTableName"] = "fin_fornecedores";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "fax",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto36["m_sql"] = "fax";
$proto36["m_srcTableName"] = "fin_fornecedores";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto38["m_sql"] = "email";
$proto38["m_srcTableName"] = "fin_fornecedores";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "site",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto40["m_sql"] = "site";
$proto40["m_srcTableName"] = "fin_fornecedores";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "tipofornecimento",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto42["m_sql"] = "tipofornecimento";
$proto42["m_srcTableName"] = "fin_fornecedores";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "celular",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto44["m_sql"] = "celular";
$proto44["m_srcTableName"] = "fin_fornecedores";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto46["m_sql"] = "ultimousuario";
$proto46["m_srcTableName"] = "fin_fornecedores";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_fornecedores",
	"m_srcTableName" => "fin_fornecedores"
));

$proto48["m_sql"] = "ultimaalteracao";
$proto48["m_srcTableName"] = "fin_fornecedores";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto50=array();
$proto50["m_link"] = "SQLL_MAIN";
			$proto51=array();
$proto51["m_strName"] = "fin_fornecedores";
$proto51["m_srcTableName"] = "fin_fornecedores";
$proto51["m_columns"] = array();
$proto51["m_columns"][] = "idForn";
$proto51["m_columns"][] = "razaosocial";
$proto51["m_columns"][] = "nomefantasia";
$proto51["m_columns"][] = "cnpjcpf";
$proto51["m_columns"][] = "ie";
$proto51["m_columns"][] = "log";
$proto51["m_columns"][] = "end";
$proto51["m_columns"][] = "num";
$proto51["m_columns"][] = "comp";
$proto51["m_columns"][] = "bairro";
$proto51["m_columns"][] = "cidade";
$proto51["m_columns"][] = "uf";
$proto51["m_columns"][] = "cep";
$proto51["m_columns"][] = "obs";
$proto51["m_columns"][] = "telefone";
$proto51["m_columns"][] = "fax";
$proto51["m_columns"][] = "email";
$proto51["m_columns"][] = "site";
$proto51["m_columns"][] = "tipofornecimento";
$proto51["m_columns"][] = "celular";
$proto51["m_columns"][] = "ultimousuario";
$proto51["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto51);

$proto50["m_table"] = $obj;
$proto50["m_sql"] = "fin_fornecedores";
$proto50["m_alias"] = "";
$proto50["m_srcTableName"] = "fin_fornecedores";
$proto52=array();
$proto52["m_sql"] = "";
$proto52["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto52["m_column"]=$obj;
$proto52["m_contained"] = array();
$proto52["m_strCase"] = "";
$proto52["m_havingmode"] = false;
$proto52["m_inBrackets"] = false;
$proto52["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto52);

$proto50["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto50);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="fin_fornecedores";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_fornecedores = createSqlQuery_fin_fornecedores();


	
		;

																						

$tdatafin_fornecedores[".sqlquery"] = $queryData_fin_fornecedores;

include_once(getabspath("include/fin_fornecedores_events.php"));
$tableEvents["fin_fornecedores"] = new eventclass_fin_fornecedores;
$tdatafin_fornecedores[".hasEvents"] = true;

?>