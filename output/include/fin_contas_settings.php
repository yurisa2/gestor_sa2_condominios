<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_contas = array();
	$tdatafin_contas[".truncateText"] = true;
	$tdatafin_contas[".NumberOfChars"] = 80;
	$tdatafin_contas[".ShortName"] = "fin_contas";
	$tdatafin_contas[".OwnerID"] = "";
	$tdatafin_contas[".OriginalTable"] = "fin_contas";

//	field labels
$fieldLabelsfin_contas = array();
$fieldToolTipsfin_contas = array();
$pageTitlesfin_contas = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_contas["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_contas["Portuguese(Brazil)"] = array();
	$pageTitlesfin_contas["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["idConta"] = "Código";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["idConta"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["numero"] = "Número";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["numero"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["agencia"] = "Agência";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["agencia"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["banco"] = "Banco";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["banco"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["correntista"] = "Correntista";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["correntista"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["senha1"] = "Senha 1";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["senha1"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["comsenha1"] = "Complemento 1";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["comsenha1"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["senha2"] = "Senha 2";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["senha2"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["comsenha2"] = "Complemento 2";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["comsenha2"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["senha3"] = "Senha 3";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["senha3"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["comsenha3"] = "Complemento 3";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["comsenha3"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["ultimousuario"] = "ultimousuario";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_contas["Portuguese(Brazil)"]["ultimaalteracao"] = "ultimaalteracao";
	$fieldToolTipsfin_contas["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsfin_contas["Portuguese(Brazil)"]))
		$tdatafin_contas[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_contas[""] = array();
	$fieldToolTipsfin_contas[""] = array();
	$pageTitlesfin_contas[""] = array();
	if (count($fieldToolTipsfin_contas[""]))
		$tdatafin_contas[".isUseToolTips"] = true;
}


	$tdatafin_contas[".NCSearch"] = true;



$tdatafin_contas[".shortTableName"] = "fin_contas";
$tdatafin_contas[".nSecOptions"] = 0;
$tdatafin_contas[".recsPerRowList"] = 1;
$tdatafin_contas[".recsPerRowPrint"] = 1;
$tdatafin_contas[".mainTableOwnerID"] = "";
$tdatafin_contas[".moveNext"] = 1;
$tdatafin_contas[".entityType"] = 0;

$tdatafin_contas[".strOriginalTableName"] = "fin_contas";





$tdatafin_contas[".showAddInPopup"] = false;

$tdatafin_contas[".showEditInPopup"] = false;

$tdatafin_contas[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_contas[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_contas[".fieldsForRegister"] = array();

$tdatafin_contas[".listAjax"] = false;

	$tdatafin_contas[".audit"] = true;

	$tdatafin_contas[".locking"] = true;

$tdatafin_contas[".edit"] = true;
$tdatafin_contas[".afterEditAction"] = 1;
$tdatafin_contas[".closePopupAfterEdit"] = 1;
$tdatafin_contas[".afterEditActionDetTable"] = "";

$tdatafin_contas[".add"] = true;
$tdatafin_contas[".afterAddAction"] = 1;
$tdatafin_contas[".closePopupAfterAdd"] = 1;
$tdatafin_contas[".afterAddActionDetTable"] = "";

$tdatafin_contas[".list"] = true;

$tdatafin_contas[".view"] = true;

$tdatafin_contas[".import"] = true;

$tdatafin_contas[".exportTo"] = true;

$tdatafin_contas[".printFriendly"] = true;

$tdatafin_contas[".delete"] = true;

$tdatafin_contas[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_contas[".searchSaving"] = false;
//

$tdatafin_contas[".showSearchPanel"] = true;
		$tdatafin_contas[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_contas[".isUseAjaxSuggest"] = false;
else
	$tdatafin_contas[".isUseAjaxSuggest"] = true;

$tdatafin_contas[".rowHighlite"] = true;



$tdatafin_contas[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_contas[".isUseTimeForSearch"] = false;





$tdatafin_contas[".allSearchFields"] = array();
$tdatafin_contas[".filterFields"] = array();
$tdatafin_contas[".requiredSearchFields"] = array();

$tdatafin_contas[".allSearchFields"][] = "numero";
	$tdatafin_contas[".allSearchFields"][] = "agencia";
	$tdatafin_contas[".allSearchFields"][] = "nome";
	$tdatafin_contas[".allSearchFields"][] = "banco";
	$tdatafin_contas[".allSearchFields"][] = "correntista";
	$tdatafin_contas[".allSearchFields"][] = "senha1";
	$tdatafin_contas[".allSearchFields"][] = "comsenha1";
	$tdatafin_contas[".allSearchFields"][] = "senha2";
	$tdatafin_contas[".allSearchFields"][] = "comsenha2";
	$tdatafin_contas[".allSearchFields"][] = "senha3";
	$tdatafin_contas[".allSearchFields"][] = "comsenha3";
	$tdatafin_contas[".allSearchFields"][] = "ultimousuario";
	$tdatafin_contas[".allSearchFields"][] = "ultimaalteracao";
	

$tdatafin_contas[".googleLikeFields"] = array();
$tdatafin_contas[".googleLikeFields"][] = "numero";
$tdatafin_contas[".googleLikeFields"][] = "agencia";
$tdatafin_contas[".googleLikeFields"][] = "nome";
$tdatafin_contas[".googleLikeFields"][] = "banco";
$tdatafin_contas[".googleLikeFields"][] = "correntista";
$tdatafin_contas[".googleLikeFields"][] = "senha1";
$tdatafin_contas[".googleLikeFields"][] = "comsenha1";
$tdatafin_contas[".googleLikeFields"][] = "senha2";
$tdatafin_contas[".googleLikeFields"][] = "comsenha2";
$tdatafin_contas[".googleLikeFields"][] = "senha3";
$tdatafin_contas[".googleLikeFields"][] = "comsenha3";
$tdatafin_contas[".googleLikeFields"][] = "ultimousuario";
$tdatafin_contas[".googleLikeFields"][] = "ultimaalteracao";


$tdatafin_contas[".advSearchFields"] = array();
$tdatafin_contas[".advSearchFields"][] = "numero";
$tdatafin_contas[".advSearchFields"][] = "agencia";
$tdatafin_contas[".advSearchFields"][] = "nome";
$tdatafin_contas[".advSearchFields"][] = "banco";
$tdatafin_contas[".advSearchFields"][] = "correntista";
$tdatafin_contas[".advSearchFields"][] = "senha1";
$tdatafin_contas[".advSearchFields"][] = "comsenha1";
$tdatafin_contas[".advSearchFields"][] = "senha2";
$tdatafin_contas[".advSearchFields"][] = "comsenha2";
$tdatafin_contas[".advSearchFields"][] = "senha3";
$tdatafin_contas[".advSearchFields"][] = "comsenha3";
$tdatafin_contas[".advSearchFields"][] = "ultimousuario";
$tdatafin_contas[".advSearchFields"][] = "ultimaalteracao";

$tdatafin_contas[".tableType"] = "list";

$tdatafin_contas[".printerPageOrientation"] = 0;
$tdatafin_contas[".nPrinterPageScale"] = 100;

$tdatafin_contas[".nPrinterSplitRecords"] = 40;

$tdatafin_contas[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_contas[".geocodingEnabled"] = false;





$tdatafin_contas[".listGridLayout"] = 3;

$tdatafin_contas[".isDisplayLoading"] = true;


$tdatafin_contas[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatafin_contas[".pageSize"] = 20;

$tdatafin_contas[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_contas[".strOrderBy"] = $tstrOrderBy;

$tdatafin_contas[".orderindexes"] = array();

$tdatafin_contas[".sqlHead"] = "SELECT idConta,  	numero,  	agencia,  	nome,  	banco,  	correntista,  	senha1,  	comsenha1,  	senha2,  	comsenha2,  	senha3,  	comsenha3,  	ultimousuario,  	ultimaalteracao";
$tdatafin_contas[".sqlFrom"] = "FROM fin_contas";
$tdatafin_contas[".sqlWhereExpr"] = "";
$tdatafin_contas[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_contas[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_contas[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_contas[".highlightSearchResults"] = true;

$tableKeysfin_contas = array();
$tableKeysfin_contas[] = "idConta";
$tdatafin_contas[".Keys"] = $tableKeysfin_contas;

$tdatafin_contas[".listFields"] = array();
$tdatafin_contas[".listFields"][] = "numero";
$tdatafin_contas[".listFields"][] = "agencia";
$tdatafin_contas[".listFields"][] = "nome";
$tdatafin_contas[".listFields"][] = "banco";
$tdatafin_contas[".listFields"][] = "correntista";
$tdatafin_contas[".listFields"][] = "senha1";
$tdatafin_contas[".listFields"][] = "comsenha1";
$tdatafin_contas[".listFields"][] = "senha2";
$tdatafin_contas[".listFields"][] = "comsenha2";
$tdatafin_contas[".listFields"][] = "senha3";
$tdatafin_contas[".listFields"][] = "comsenha3";

$tdatafin_contas[".hideMobileList"] = array();


$tdatafin_contas[".viewFields"] = array();
$tdatafin_contas[".viewFields"][] = "idConta";
$tdatafin_contas[".viewFields"][] = "numero";
$tdatafin_contas[".viewFields"][] = "agencia";
$tdatafin_contas[".viewFields"][] = "nome";
$tdatafin_contas[".viewFields"][] = "banco";
$tdatafin_contas[".viewFields"][] = "correntista";
$tdatafin_contas[".viewFields"][] = "senha1";
$tdatafin_contas[".viewFields"][] = "comsenha1";
$tdatafin_contas[".viewFields"][] = "senha2";
$tdatafin_contas[".viewFields"][] = "comsenha2";
$tdatafin_contas[".viewFields"][] = "senha3";
$tdatafin_contas[".viewFields"][] = "comsenha3";
$tdatafin_contas[".viewFields"][] = "ultimousuario";
$tdatafin_contas[".viewFields"][] = "ultimaalteracao";

$tdatafin_contas[".addFields"] = array();
$tdatafin_contas[".addFields"][] = "numero";
$tdatafin_contas[".addFields"][] = "agencia";
$tdatafin_contas[".addFields"][] = "nome";
$tdatafin_contas[".addFields"][] = "banco";
$tdatafin_contas[".addFields"][] = "correntista";
$tdatafin_contas[".addFields"][] = "senha1";
$tdatafin_contas[".addFields"][] = "comsenha1";
$tdatafin_contas[".addFields"][] = "senha2";
$tdatafin_contas[".addFields"][] = "comsenha2";
$tdatafin_contas[".addFields"][] = "senha3";
$tdatafin_contas[".addFields"][] = "comsenha3";

$tdatafin_contas[".masterListFields"] = array();

$tdatafin_contas[".inlineAddFields"] = array();

$tdatafin_contas[".editFields"] = array();
$tdatafin_contas[".editFields"][] = "idConta";
$tdatafin_contas[".editFields"][] = "numero";
$tdatafin_contas[".editFields"][] = "agencia";
$tdatafin_contas[".editFields"][] = "nome";
$tdatafin_contas[".editFields"][] = "banco";
$tdatafin_contas[".editFields"][] = "correntista";
$tdatafin_contas[".editFields"][] = "senha1";
$tdatafin_contas[".editFields"][] = "comsenha1";
$tdatafin_contas[".editFields"][] = "senha2";
$tdatafin_contas[".editFields"][] = "comsenha2";
$tdatafin_contas[".editFields"][] = "senha3";
$tdatafin_contas[".editFields"][] = "comsenha3";

$tdatafin_contas[".inlineEditFields"] = array();

$tdatafin_contas[".exportFields"] = array();
$tdatafin_contas[".exportFields"][] = "numero";
$tdatafin_contas[".exportFields"][] = "agencia";
$tdatafin_contas[".exportFields"][] = "nome";
$tdatafin_contas[".exportFields"][] = "banco";
$tdatafin_contas[".exportFields"][] = "correntista";
$tdatafin_contas[".exportFields"][] = "senha1";
$tdatafin_contas[".exportFields"][] = "comsenha1";
$tdatafin_contas[".exportFields"][] = "senha2";
$tdatafin_contas[".exportFields"][] = "comsenha2";
$tdatafin_contas[".exportFields"][] = "senha3";
$tdatafin_contas[".exportFields"][] = "comsenha3";
$tdatafin_contas[".exportFields"][] = "ultimousuario";
$tdatafin_contas[".exportFields"][] = "ultimaalteracao";

$tdatafin_contas[".importFields"] = array();
$tdatafin_contas[".importFields"][] = "idConta";
$tdatafin_contas[".importFields"][] = "numero";
$tdatafin_contas[".importFields"][] = "agencia";
$tdatafin_contas[".importFields"][] = "nome";
$tdatafin_contas[".importFields"][] = "banco";
$tdatafin_contas[".importFields"][] = "correntista";
$tdatafin_contas[".importFields"][] = "senha1";
$tdatafin_contas[".importFields"][] = "comsenha1";
$tdatafin_contas[".importFields"][] = "senha2";
$tdatafin_contas[".importFields"][] = "comsenha2";
$tdatafin_contas[".importFields"][] = "senha3";
$tdatafin_contas[".importFields"][] = "comsenha3";
$tdatafin_contas[".importFields"][] = "ultimousuario";
$tdatafin_contas[".importFields"][] = "ultimaalteracao";

$tdatafin_contas[".printFields"] = array();
$tdatafin_contas[".printFields"][] = "numero";
$tdatafin_contas[".printFields"][] = "agencia";
$tdatafin_contas[".printFields"][] = "nome";
$tdatafin_contas[".printFields"][] = "banco";
$tdatafin_contas[".printFields"][] = "correntista";
$tdatafin_contas[".printFields"][] = "senha1";
$tdatafin_contas[".printFields"][] = "comsenha1";
$tdatafin_contas[".printFields"][] = "senha2";
$tdatafin_contas[".printFields"][] = "comsenha2";
$tdatafin_contas[".printFields"][] = "senha3";
$tdatafin_contas[".printFields"][] = "comsenha3";

//	idConta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idConta";
	$fdata["GoodName"] = "idConta";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","idConta");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idConta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idConta";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_contas["idConta"] = $fdata;
//	numero
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "numero";
	$fdata["GoodName"] = "numero";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","numero");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numero";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "numero";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["numero"] = $fdata;
//	agencia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "agencia";
	$fdata["GoodName"] = "agencia";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","agencia");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "agencia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "agencia";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["agencia"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["nome"] = $fdata;
//	banco
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "banco";
	$fdata["GoodName"] = "banco";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","banco");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "banco";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "banco";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["banco"] = $fdata;
//	correntista
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "correntista";
	$fdata["GoodName"] = "correntista";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","correntista");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "correntista";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "correntista";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=60";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["correntista"] = $fdata;
//	senha1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "senha1";
	$fdata["GoodName"] = "senha1";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","senha1");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "senha1";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "senha1";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=40";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["senha1"] = $fdata;
//	comsenha1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "comsenha1";
	$fdata["GoodName"] = "comsenha1";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","comsenha1");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "comsenha1";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comsenha1";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=40";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["comsenha1"] = $fdata;
//	senha2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "senha2";
	$fdata["GoodName"] = "senha2";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","senha2");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "senha2";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "senha2";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=40";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["senha2"] = $fdata;
//	comsenha2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "comsenha2";
	$fdata["GoodName"] = "comsenha2";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","comsenha2");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "comsenha2";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comsenha2";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=40";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["comsenha2"] = $fdata;
//	senha3
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "senha3";
	$fdata["GoodName"] = "senha3";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","senha3");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "senha3";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "senha3";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=40";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["senha3"] = $fdata;
//	comsenha3
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "comsenha3";
	$fdata["GoodName"] = "comsenha3";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","comsenha3");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "comsenha3";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comsenha3";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=40";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["comsenha3"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_contas";
	$fdata["Label"] = GetFieldLabel("fin_contas","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_contas["ultimaalteracao"] = $fdata;


$tables_data["fin_contas"]=&$tdatafin_contas;
$field_labels["fin_contas"] = &$fieldLabelsfin_contas;
$fieldToolTips["fin_contas"] = &$fieldToolTipsfin_contas;
$page_titles["fin_contas"] = &$pageTitlesfin_contas;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_contas"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_contas"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_contas()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idConta,  	numero,  	agencia,  	nome,  	banco,  	correntista,  	senha1,  	comsenha1,  	senha2,  	comsenha2,  	senha3,  	comsenha3,  	ultimousuario,  	ultimaalteracao";
$proto0["m_strFrom"] = "FROM fin_contas";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idConta",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto6["m_sql"] = "idConta";
$proto6["m_srcTableName"] = "fin_contas";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "numero",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto8["m_sql"] = "numero";
$proto8["m_srcTableName"] = "fin_contas";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "agencia",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto10["m_sql"] = "agencia";
$proto10["m_srcTableName"] = "fin_contas";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto12["m_sql"] = "nome";
$proto12["m_srcTableName"] = "fin_contas";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "banco",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto14["m_sql"] = "banco";
$proto14["m_srcTableName"] = "fin_contas";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "correntista",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto16["m_sql"] = "correntista";
$proto16["m_srcTableName"] = "fin_contas";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "senha1",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto18["m_sql"] = "senha1";
$proto18["m_srcTableName"] = "fin_contas";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "comsenha1",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto20["m_sql"] = "comsenha1";
$proto20["m_srcTableName"] = "fin_contas";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "senha2",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto22["m_sql"] = "senha2";
$proto22["m_srcTableName"] = "fin_contas";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "comsenha2",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto24["m_sql"] = "comsenha2";
$proto24["m_srcTableName"] = "fin_contas";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "senha3",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto26["m_sql"] = "senha3";
$proto26["m_srcTableName"] = "fin_contas";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "comsenha3",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto28["m_sql"] = "comsenha3";
$proto28["m_srcTableName"] = "fin_contas";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto30["m_sql"] = "ultimousuario";
$proto30["m_srcTableName"] = "fin_contas";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_contas",
	"m_srcTableName" => "fin_contas"
));

$proto32["m_sql"] = "ultimaalteracao";
$proto32["m_srcTableName"] = "fin_contas";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto34=array();
$proto34["m_link"] = "SQLL_MAIN";
			$proto35=array();
$proto35["m_strName"] = "fin_contas";
$proto35["m_srcTableName"] = "fin_contas";
$proto35["m_columns"] = array();
$proto35["m_columns"][] = "idConta";
$proto35["m_columns"][] = "numero";
$proto35["m_columns"][] = "agencia";
$proto35["m_columns"][] = "nome";
$proto35["m_columns"][] = "banco";
$proto35["m_columns"][] = "correntista";
$proto35["m_columns"][] = "senha1";
$proto35["m_columns"][] = "comsenha1";
$proto35["m_columns"][] = "senha2";
$proto35["m_columns"][] = "comsenha2";
$proto35["m_columns"][] = "senha3";
$proto35["m_columns"][] = "comsenha3";
$proto35["m_columns"][] = "ultimousuario";
$proto35["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto35);

$proto34["m_table"] = $obj;
$proto34["m_sql"] = "fin_contas";
$proto34["m_alias"] = "";
$proto34["m_srcTableName"] = "fin_contas";
$proto36=array();
$proto36["m_sql"] = "";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = false;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto34["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto34);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="fin_contas";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_contas = createSqlQuery_fin_contas();


	
		;

														

$tdatafin_contas[".sqlquery"] = $queryData_fin_contas;

include_once(getabspath("include/fin_contas_events.php"));
$tableEvents["fin_contas"] = new eventclass_fin_contas;
$tdatafin_contas[".hasEvents"] = true;

?>