<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_cadastro_moradores_plista = array();
	$tdatager_cadastro_moradores_plista[".truncateText"] = true;
	$tdatager_cadastro_moradores_plista[".NumberOfChars"] = 80;
	$tdatager_cadastro_moradores_plista[".ShortName"] = "ger_cadastro_moradores_plista";
	$tdatager_cadastro_moradores_plista[".OwnerID"] = "";
	$tdatager_cadastro_moradores_plista[".OriginalTable"] = "ger_cadastro_moradores_plista";

//	field labels
$fieldLabelsger_cadastro_moradores_plista = array();
$fieldToolTipsger_cadastro_moradores_plista = array();
$pageTitlesger_cadastro_moradores_plista = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_cadastro_moradores_plista["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_cadastro_moradores_plista["Portuguese(Brazil)"] = array();
	$pageTitlesger_cadastro_moradores_plista["Portuguese(Brazil)"] = array();
	$fieldLabelsger_cadastro_moradores_plista["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsger_cadastro_moradores_plista["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsger_cadastro_moradores_plista["Portuguese(Brazil)"]["grupo"] = "grupo";
	$fieldToolTipsger_cadastro_moradores_plista["Portuguese(Brazil)"]["grupo"] = "";
	$fieldLabelsger_cadastro_moradores_plista["Portuguese(Brazil)"]["unidade"] = "unidade";
	$fieldToolTipsger_cadastro_moradores_plista["Portuguese(Brazil)"]["unidade"] = "";
	$fieldLabelsger_cadastro_moradores_plista["Portuguese(Brazil)"]["assinatura"] = "Assinatura";
	$fieldToolTipsger_cadastro_moradores_plista["Portuguese(Brazil)"]["assinatura"] = "";
	if (count($fieldToolTipsger_cadastro_moradores_plista["Portuguese(Brazil)"]))
		$tdatager_cadastro_moradores_plista[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_cadastro_moradores_plista[""] = array();
	$fieldToolTipsger_cadastro_moradores_plista[""] = array();
	$pageTitlesger_cadastro_moradores_plista[""] = array();
	if (count($fieldToolTipsger_cadastro_moradores_plista[""]))
		$tdatager_cadastro_moradores_plista[".isUseToolTips"] = true;
}


	$tdatager_cadastro_moradores_plista[".NCSearch"] = true;



$tdatager_cadastro_moradores_plista[".shortTableName"] = "ger_cadastro_moradores_plista";
$tdatager_cadastro_moradores_plista[".nSecOptions"] = 0;
$tdatager_cadastro_moradores_plista[".recsPerRowList"] = 1;
$tdatager_cadastro_moradores_plista[".recsPerRowPrint"] = 1;
$tdatager_cadastro_moradores_plista[".mainTableOwnerID"] = "";
$tdatager_cadastro_moradores_plista[".moveNext"] = 1;
$tdatager_cadastro_moradores_plista[".entityType"] = 0;

$tdatager_cadastro_moradores_plista[".strOriginalTableName"] = "ger_cadastro_moradores_plista";





$tdatager_cadastro_moradores_plista[".showAddInPopup"] = false;

$tdatager_cadastro_moradores_plista[".showEditInPopup"] = false;

$tdatager_cadastro_moradores_plista[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_cadastro_moradores_plista[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_cadastro_moradores_plista[".fieldsForRegister"] = array();

$tdatager_cadastro_moradores_plista[".listAjax"] = false;

	$tdatager_cadastro_moradores_plista[".audit"] = true;

	$tdatager_cadastro_moradores_plista[".locking"] = true;



$tdatager_cadastro_moradores_plista[".list"] = true;



$tdatager_cadastro_moradores_plista[".exportTo"] = true;

$tdatager_cadastro_moradores_plista[".printFriendly"] = true;


$tdatager_cadastro_moradores_plista[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_cadastro_moradores_plista[".searchSaving"] = false;
//

$tdatager_cadastro_moradores_plista[".showSearchPanel"] = true;
		$tdatager_cadastro_moradores_plista[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_cadastro_moradores_plista[".isUseAjaxSuggest"] = false;
else
	$tdatager_cadastro_moradores_plista[".isUseAjaxSuggest"] = true;

$tdatager_cadastro_moradores_plista[".rowHighlite"] = true;



$tdatager_cadastro_moradores_plista[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_cadastro_moradores_plista[".isUseTimeForSearch"] = false;





$tdatager_cadastro_moradores_plista[".allSearchFields"] = array();
$tdatager_cadastro_moradores_plista[".filterFields"] = array();
$tdatager_cadastro_moradores_plista[".requiredSearchFields"] = array();

$tdatager_cadastro_moradores_plista[".allSearchFields"][] = "grupo";
	$tdatager_cadastro_moradores_plista[".allSearchFields"][] = "unidade";
	$tdatager_cadastro_moradores_plista[".allSearchFields"][] = "nome";
	

$tdatager_cadastro_moradores_plista[".googleLikeFields"] = array();
$tdatager_cadastro_moradores_plista[".googleLikeFields"][] = "nome";
$tdatager_cadastro_moradores_plista[".googleLikeFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".googleLikeFields"][] = "unidade";


$tdatager_cadastro_moradores_plista[".advSearchFields"] = array();
$tdatager_cadastro_moradores_plista[".advSearchFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".advSearchFields"][] = "unidade";
$tdatager_cadastro_moradores_plista[".advSearchFields"][] = "nome";

$tdatager_cadastro_moradores_plista[".tableType"] = "list";

$tdatager_cadastro_moradores_plista[".printerPageOrientation"] = 0;
$tdatager_cadastro_moradores_plista[".nPrinterPageScale"] = 100;

$tdatager_cadastro_moradores_plista[".nPrinterSplitRecords"] = 40;

$tdatager_cadastro_moradores_plista[".nPrinterPDFSplitRecords"] = 40;



$tdatager_cadastro_moradores_plista[".geocodingEnabled"] = false;





$tdatager_cadastro_moradores_plista[".listGridLayout"] = 3;

$tdatager_cadastro_moradores_plista[".isDisplayLoading"] = true;


$tdatager_cadastro_moradores_plista[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_cadastro_moradores_plista[".pageSize"] = 20;

$tdatager_cadastro_moradores_plista[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_cadastro_moradores_plista[".strOrderBy"] = $tstrOrderBy;

$tdatager_cadastro_moradores_plista[".orderindexes"] = array();

$tdatager_cadastro_moradores_plista[".sqlHead"] = "SELECT nome,  grupo,  unidade,  \"Ass.:\" AS assinatura";
$tdatager_cadastro_moradores_plista[".sqlFrom"] = "FROM ger_cadastro_moradores_plista";
$tdatager_cadastro_moradores_plista[".sqlWhereExpr"] = "";
$tdatager_cadastro_moradores_plista[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_cadastro_moradores_plista[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_cadastro_moradores_plista[".arrGroupsPerPage"] = $arrGPP;

$tdatager_cadastro_moradores_plista[".highlightSearchResults"] = true;

$tableKeysger_cadastro_moradores_plista = array();
$tdatager_cadastro_moradores_plista[".Keys"] = $tableKeysger_cadastro_moradores_plista;

$tdatager_cadastro_moradores_plista[".listFields"] = array();
$tdatager_cadastro_moradores_plista[".listFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".listFields"][] = "unidade";
$tdatager_cadastro_moradores_plista[".listFields"][] = "nome";
$tdatager_cadastro_moradores_plista[".listFields"][] = "assinatura";

$tdatager_cadastro_moradores_plista[".hideMobileList"] = array();


$tdatager_cadastro_moradores_plista[".viewFields"] = array();
$tdatager_cadastro_moradores_plista[".viewFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".viewFields"][] = "unidade";
$tdatager_cadastro_moradores_plista[".viewFields"][] = "assinatura";

$tdatager_cadastro_moradores_plista[".addFields"] = array();
$tdatager_cadastro_moradores_plista[".addFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".addFields"][] = "unidade";

$tdatager_cadastro_moradores_plista[".masterListFields"] = array();

$tdatager_cadastro_moradores_plista[".inlineAddFields"] = array();

$tdatager_cadastro_moradores_plista[".editFields"] = array();
$tdatager_cadastro_moradores_plista[".editFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".editFields"][] = "unidade";

$tdatager_cadastro_moradores_plista[".inlineEditFields"] = array();

$tdatager_cadastro_moradores_plista[".exportFields"] = array();
$tdatager_cadastro_moradores_plista[".exportFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".exportFields"][] = "unidade";
$tdatager_cadastro_moradores_plista[".exportFields"][] = "nome";
$tdatager_cadastro_moradores_plista[".exportFields"][] = "assinatura";

$tdatager_cadastro_moradores_plista[".importFields"] = array();
$tdatager_cadastro_moradores_plista[".importFields"][] = "nome";
$tdatager_cadastro_moradores_plista[".importFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".importFields"][] = "unidade";
$tdatager_cadastro_moradores_plista[".importFields"][] = "assinatura";

$tdatager_cadastro_moradores_plista[".printFields"] = array();
$tdatager_cadastro_moradores_plista[".printFields"][] = "grupo";
$tdatager_cadastro_moradores_plista[".printFields"][] = "unidade";
$tdatager_cadastro_moradores_plista[".printFields"][] = "nome";
$tdatager_cadastro_moradores_plista[".printFields"][] = "assinatura";

//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "ger_cadastro_moradores_plista";
	$fdata["Label"] = GetFieldLabel("ger_cadastro_moradores_plista","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro_moradores_plista["nome"] = $fdata;
//	grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "grupo";
	$fdata["GoodName"] = "grupo";
	$fdata["ownerTable"] = "ger_cadastro_moradores_plista";
	$fdata["Label"] = GetFieldLabel("ger_cadastro_moradores_plista","grupo");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "grupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro_moradores_plista["grupo"] = $fdata;
//	unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "unidade";
	$fdata["GoodName"] = "unidade";
	$fdata["ownerTable"] = "ger_cadastro_moradores_plista";
	$fdata["Label"] = GetFieldLabel("ger_cadastro_moradores_plista","unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro_moradores_plista["unidade"] = $fdata;
//	assinatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "assinatura";
	$fdata["GoodName"] = "assinatura";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("ger_cadastro_moradores_plista","assinatura");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Ass.:\"";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_cadastro_moradores_plista["assinatura"] = $fdata;


$tables_data["ger_cadastro_moradores_plista"]=&$tdatager_cadastro_moradores_plista;
$field_labels["ger_cadastro_moradores_plista"] = &$fieldLabelsger_cadastro_moradores_plista;
$fieldToolTips["ger_cadastro_moradores_plista"] = &$fieldToolTipsger_cadastro_moradores_plista;
$page_titles["ger_cadastro_moradores_plista"] = &$pageTitlesger_cadastro_moradores_plista;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_cadastro_moradores_plista"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_cadastro_moradores_plista"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_cadastro_moradores_plista()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "nome,  grupo,  unidade,  \"Ass.:\" AS assinatura";
$proto0["m_strFrom"] = "FROM ger_cadastro_moradores_plista";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_cadastro_moradores_plista",
	"m_srcTableName" => "ger_cadastro_moradores_plista"
));

$proto6["m_sql"] = "nome";
$proto6["m_srcTableName"] = "ger_cadastro_moradores_plista";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "grupo",
	"m_strTable" => "ger_cadastro_moradores_plista",
	"m_srcTableName" => "ger_cadastro_moradores_plista"
));

$proto8["m_sql"] = "grupo";
$proto8["m_srcTableName"] = "ger_cadastro_moradores_plista";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "unidade",
	"m_strTable" => "ger_cadastro_moradores_plista",
	"m_srcTableName" => "ger_cadastro_moradores_plista"
));

$proto10["m_sql"] = "unidade";
$proto10["m_srcTableName"] = "ger_cadastro_moradores_plista";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "\"Ass.:\""
));

$proto12["m_sql"] = "\"Ass.:\"";
$proto12["m_srcTableName"] = "ger_cadastro_moradores_plista";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "assinatura";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "ger_cadastro_moradores_plista";
$proto15["m_srcTableName"] = "ger_cadastro_moradores_plista";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "nome";
$proto15["m_columns"][] = "grupo";
$proto15["m_columns"][] = "unidade";
$proto15["m_columns"][] = "link_ger_cadastro";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "ger_cadastro_moradores_plista";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "ger_cadastro_moradores_plista";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_cadastro_moradores_plista";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_cadastro_moradores_plista = createSqlQuery_ger_cadastro_moradores_plista();


	
		;

				

$tdatager_cadastro_moradores_plista[".sqlquery"] = $queryData_ger_cadastro_moradores_plista;

include_once(getabspath("include/ger_cadastro_moradores_plista_events.php"));
$tableEvents["ger_cadastro_moradores_plista"] = new eventclass_ger_cadastro_moradores_plista;
$tdatager_cadastro_moradores_plista[".hasEvents"] = true;

?>