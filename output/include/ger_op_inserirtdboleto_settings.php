<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_op_inserirtdboleto = array();
	$tdatager_op_inserirtdboleto[".truncateText"] = true;
	$tdatager_op_inserirtdboleto[".NumberOfChars"] = 80;
	$tdatager_op_inserirtdboleto[".ShortName"] = "ger_op_inserirtdboleto";
	$tdatager_op_inserirtdboleto[".OwnerID"] = "";
	$tdatager_op_inserirtdboleto[".OriginalTable"] = "ger_op_inserirtdboleto";

//	field labels
$fieldLabelsger_op_inserirtdboleto = array();
$fieldToolTipsger_op_inserirtdboleto = array();
$pageTitlesger_op_inserirtdboleto = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"] = array();
	$pageTitlesger_op_inserirtdboleto["Portuguese(Brazil)"] = array();
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["idInsert"] = "idInsert";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["idInsert"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["mes"] = "Mês (ex. 05)";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["mes"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["ano"] = "Ano (ex. 2008)";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["ano"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["vlrdoc"] = "Valor do Documento";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["vlrdoc"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["vlrcob"] = "Valor Cobrado";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["vlrcob"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["link_fin_conta"] = "Conta de crédito";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["link_fin_conta"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["outded"] = "Outros Débitos";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["outded"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["moramulta"] = "Mora/Multa";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["moramulta"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["outacr"] = "Outras Cobrancas";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["outacr"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["tipo"] = "Tipo";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["tipo"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["ultimousuario"] = "ultimousuario";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["numdoc"] = "Numero Inicial";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["numdoc"] = "";
	$fieldLabelsger_op_inserirtdboleto["Portuguese(Brazil)"]["grupo"] = "Grupo";
	$fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]["grupo"] = "";
	if (count($fieldToolTipsger_op_inserirtdboleto["Portuguese(Brazil)"]))
		$tdatager_op_inserirtdboleto[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_op_inserirtdboleto[""] = array();
	$fieldToolTipsger_op_inserirtdboleto[""] = array();
	$pageTitlesger_op_inserirtdboleto[""] = array();
	if (count($fieldToolTipsger_op_inserirtdboleto[""]))
		$tdatager_op_inserirtdboleto[".isUseToolTips"] = true;
}


	$tdatager_op_inserirtdboleto[".NCSearch"] = true;



$tdatager_op_inserirtdboleto[".shortTableName"] = "ger_op_inserirtdboleto";
$tdatager_op_inserirtdboleto[".nSecOptions"] = 0;
$tdatager_op_inserirtdboleto[".recsPerRowList"] = 1;
$tdatager_op_inserirtdboleto[".recsPerRowPrint"] = 1;
$tdatager_op_inserirtdboleto[".mainTableOwnerID"] = "";
$tdatager_op_inserirtdboleto[".moveNext"] = 1;
$tdatager_op_inserirtdboleto[".entityType"] = 0;

$tdatager_op_inserirtdboleto[".strOriginalTableName"] = "ger_op_inserirtdboleto";





$tdatager_op_inserirtdboleto[".showAddInPopup"] = false;

$tdatager_op_inserirtdboleto[".showEditInPopup"] = false;

$tdatager_op_inserirtdboleto[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_op_inserirtdboleto[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_op_inserirtdboleto[".fieldsForRegister"] = array();

$tdatager_op_inserirtdboleto[".listAjax"] = false;

	$tdatager_op_inserirtdboleto[".audit"] = true;

	$tdatager_op_inserirtdboleto[".locking"] = true;


$tdatager_op_inserirtdboleto[".add"] = true;
$tdatager_op_inserirtdboleto[".afterAddAction"] = 1;
$tdatager_op_inserirtdboleto[".closePopupAfterAdd"] = 1;
$tdatager_op_inserirtdboleto[".afterAddActionDetTable"] = "";







$tdatager_op_inserirtdboleto[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_op_inserirtdboleto[".searchSaving"] = false;
//

$tdatager_op_inserirtdboleto[".showSearchPanel"] = true;
		$tdatager_op_inserirtdboleto[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_op_inserirtdboleto[".isUseAjaxSuggest"] = false;
else
	$tdatager_op_inserirtdboleto[".isUseAjaxSuggest"] = true;

$tdatager_op_inserirtdboleto[".rowHighlite"] = true;



$tdatager_op_inserirtdboleto[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_op_inserirtdboleto[".isUseTimeForSearch"] = false;





$tdatager_op_inserirtdboleto[".allSearchFields"] = array();
$tdatager_op_inserirtdboleto[".filterFields"] = array();
$tdatager_op_inserirtdboleto[".requiredSearchFields"] = array();



$tdatager_op_inserirtdboleto[".googleLikeFields"] = array();
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "idInsert";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "mes";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "ano";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "vlrdoc";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "vlrcob";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "link_fin_conta";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "obs";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "outded";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "moramulta";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "outacr";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "tipo";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "ultimousuario";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "numdoc";
$tdatager_op_inserirtdboleto[".googleLikeFields"][] = "grupo";


$tdatager_op_inserirtdboleto[".advSearchFields"] = array();
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "idInsert";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "mes";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "ano";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "vlrdoc";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "vlrcob";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "link_fin_conta";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "obs";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "outded";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "moramulta";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "outacr";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "tipo";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "ultimousuario";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "numdoc";
$tdatager_op_inserirtdboleto[".advSearchFields"][] = "grupo";

$tdatager_op_inserirtdboleto[".tableType"] = "list";

$tdatager_op_inserirtdboleto[".printerPageOrientation"] = 0;
$tdatager_op_inserirtdboleto[".nPrinterPageScale"] = 100;

$tdatager_op_inserirtdboleto[".nPrinterSplitRecords"] = 40;

$tdatager_op_inserirtdboleto[".nPrinterPDFSplitRecords"] = 40;



$tdatager_op_inserirtdboleto[".geocodingEnabled"] = false;





$tdatager_op_inserirtdboleto[".listGridLayout"] = 3;

$tdatager_op_inserirtdboleto[".isDisplayLoading"] = true;


$tdatager_op_inserirtdboleto[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_op_inserirtdboleto[".pageSize"] = 20;

$tdatager_op_inserirtdboleto[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_op_inserirtdboleto[".strOrderBy"] = $tstrOrderBy;

$tdatager_op_inserirtdboleto[".orderindexes"] = array();

$tdatager_op_inserirtdboleto[".sqlHead"] = "SELECT idInsert,  	mes,  	ano,  	vlrdoc,  	vlrcob,  	link_fin_conta,  	obs,  	outded,  	moramulta,  	outacr,  	tipo,  	ultimousuario,  	numdoc,  	grupo";
$tdatager_op_inserirtdboleto[".sqlFrom"] = "FROM ger_op_inserirtdboleto";
$tdatager_op_inserirtdboleto[".sqlWhereExpr"] = "";
$tdatager_op_inserirtdboleto[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_op_inserirtdboleto[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_op_inserirtdboleto[".arrGroupsPerPage"] = $arrGPP;

$tdatager_op_inserirtdboleto[".highlightSearchResults"] = true;

$tableKeysger_op_inserirtdboleto = array();
$tableKeysger_op_inserirtdboleto[] = "idInsert";
$tdatager_op_inserirtdboleto[".Keys"] = $tableKeysger_op_inserirtdboleto;

$tdatager_op_inserirtdboleto[".listFields"] = array();

$tdatager_op_inserirtdboleto[".hideMobileList"] = array();


$tdatager_op_inserirtdboleto[".viewFields"] = array();

$tdatager_op_inserirtdboleto[".addFields"] = array();
$tdatager_op_inserirtdboleto[".addFields"][] = "tipo";
$tdatager_op_inserirtdboleto[".addFields"][] = "link_fin_conta";
$tdatager_op_inserirtdboleto[".addFields"][] = "grupo";
$tdatager_op_inserirtdboleto[".addFields"][] = "mes";
$tdatager_op_inserirtdboleto[".addFields"][] = "ano";
$tdatager_op_inserirtdboleto[".addFields"][] = "vlrdoc";
$tdatager_op_inserirtdboleto[".addFields"][] = "outded";
$tdatager_op_inserirtdboleto[".addFields"][] = "moramulta";
$tdatager_op_inserirtdboleto[".addFields"][] = "outacr";
$tdatager_op_inserirtdboleto[".addFields"][] = "vlrcob";
$tdatager_op_inserirtdboleto[".addFields"][] = "obs";

$tdatager_op_inserirtdboleto[".masterListFields"] = array();

$tdatager_op_inserirtdboleto[".inlineAddFields"] = array();

$tdatager_op_inserirtdboleto[".editFields"] = array();

$tdatager_op_inserirtdboleto[".inlineEditFields"] = array();

$tdatager_op_inserirtdboleto[".exportFields"] = array();

$tdatager_op_inserirtdboleto[".importFields"] = array();
$tdatager_op_inserirtdboleto[".importFields"][] = "idInsert";
$tdatager_op_inserirtdboleto[".importFields"][] = "mes";
$tdatager_op_inserirtdboleto[".importFields"][] = "ano";
$tdatager_op_inserirtdboleto[".importFields"][] = "vlrdoc";
$tdatager_op_inserirtdboleto[".importFields"][] = "vlrcob";
$tdatager_op_inserirtdboleto[".importFields"][] = "link_fin_conta";
$tdatager_op_inserirtdboleto[".importFields"][] = "obs";
$tdatager_op_inserirtdboleto[".importFields"][] = "outded";
$tdatager_op_inserirtdboleto[".importFields"][] = "moramulta";
$tdatager_op_inserirtdboleto[".importFields"][] = "outacr";
$tdatager_op_inserirtdboleto[".importFields"][] = "tipo";
$tdatager_op_inserirtdboleto[".importFields"][] = "ultimousuario";
$tdatager_op_inserirtdboleto[".importFields"][] = "numdoc";
$tdatager_op_inserirtdboleto[".importFields"][] = "grupo";

$tdatager_op_inserirtdboleto[".printFields"] = array();

//	idInsert
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idInsert";
	$fdata["GoodName"] = "idInsert";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","idInsert");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idInsert";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idInsert";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["idInsert"] = $fdata;
//	mes
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "mes";
	$fdata["GoodName"] = "mes";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","mes");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "mes";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "mes";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=2";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["mes"] = $fdata;
//	ano
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ano";
	$fdata["GoodName"] = "ano";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","ano");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "ano";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ano";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=4";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["ano"] = $fdata;
//	vlrdoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "vlrdoc";
	$fdata["GoodName"] = "vlrdoc";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","vlrdoc");
	$fdata["FieldType"] = 14;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "vlrdoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vlrdoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["vlrdoc"] = $fdata;
//	vlrcob
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "vlrcob";
	$fdata["GoodName"] = "vlrcob";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","vlrcob");
	$fdata["FieldType"] = 14;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "vlrcob";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vlrcob";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["vlrcob"] = $fdata;
//	link_fin_conta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "link_fin_conta";
	$fdata["GoodName"] = "link_fin_conta";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","link_fin_conta");
	$fdata["FieldType"] = 3;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "link_fin_conta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_fin_conta";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_contas";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idConta";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "nome";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["link_fin_conta"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 250;
			$edata["nCols"] = 500;

	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["obs"] = $fdata;
//	outded
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "outded";
	$fdata["GoodName"] = "outded";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","outded");
	$fdata["FieldType"] = 14;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "outded";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "outded";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["outded"] = $fdata;
//	moramulta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "moramulta";
	$fdata["GoodName"] = "moramulta";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","moramulta");
	$fdata["FieldType"] = 14;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "moramulta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "moramulta";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["moramulta"] = $fdata;
//	outacr
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "outacr";
	$fdata["GoodName"] = "outacr";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","outacr");
	$fdata["FieldType"] = 14;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "outacr";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "outacr";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["outacr"] = $fdata;
//	tipo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "tipo";
	$fdata["GoodName"] = "tipo";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","tipo");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "tipo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Boleto";
	$edata["LookupValues"][] = "Depósito";
	$edata["LookupValues"][] = "Parcela de carnê";
	$edata["LookupValues"][] = "Pagamento em dinheiro";

	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["tipo"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["ultimousuario"] = $fdata;
//	numdoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "numdoc";
	$fdata["GoodName"] = "numdoc";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","numdoc");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "numdoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "numdoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=7";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["numdoc"] = $fdata;
//	grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "grupo";
	$fdata["GoodName"] = "grupo";
	$fdata["ownerTable"] = "ger_op_inserirtdboleto";
	$fdata["Label"] = GetFieldLabel("ger_op_inserirtdboleto","grupo");
	$fdata["FieldType"] = 3;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "grupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_op_inserirtdboleto["grupo"] = $fdata;


$tables_data["ger_op_inserirtdboleto"]=&$tdatager_op_inserirtdboleto;
$field_labels["ger_op_inserirtdboleto"] = &$fieldLabelsger_op_inserirtdboleto;
$fieldToolTips["ger_op_inserirtdboleto"] = &$fieldToolTipsger_op_inserirtdboleto;
$page_titles["ger_op_inserirtdboleto"] = &$pageTitlesger_op_inserirtdboleto;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_op_inserirtdboleto"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_op_inserirtdboleto"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_op_inserirtdboleto()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idInsert,  	mes,  	ano,  	vlrdoc,  	vlrcob,  	link_fin_conta,  	obs,  	outded,  	moramulta,  	outacr,  	tipo,  	ultimousuario,  	numdoc,  	grupo";
$proto0["m_strFrom"] = "FROM ger_op_inserirtdboleto";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idInsert",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto6["m_sql"] = "idInsert";
$proto6["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "mes",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto8["m_sql"] = "mes";
$proto8["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ano",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto10["m_sql"] = "ano";
$proto10["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "vlrdoc",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto12["m_sql"] = "vlrdoc";
$proto12["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "vlrcob",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto14["m_sql"] = "vlrcob";
$proto14["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "link_fin_conta",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto16["m_sql"] = "link_fin_conta";
$proto16["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto18["m_sql"] = "obs";
$proto18["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "outded",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto20["m_sql"] = "outded";
$proto20["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "moramulta",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto22["m_sql"] = "moramulta";
$proto22["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "outacr",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto24["m_sql"] = "outacr";
$proto24["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "tipo",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto26["m_sql"] = "tipo";
$proto26["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto28["m_sql"] = "ultimousuario";
$proto28["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "numdoc",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto30["m_sql"] = "numdoc";
$proto30["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "grupo",
	"m_strTable" => "ger_op_inserirtdboleto",
	"m_srcTableName" => "ger_op_inserirtdboleto"
));

$proto32["m_sql"] = "grupo";
$proto32["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto34=array();
$proto34["m_link"] = "SQLL_MAIN";
			$proto35=array();
$proto35["m_strName"] = "ger_op_inserirtdboleto";
$proto35["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto35["m_columns"] = array();
$proto35["m_columns"][] = "idInsert";
$proto35["m_columns"][] = "mes";
$proto35["m_columns"][] = "ano";
$proto35["m_columns"][] = "vlrdoc";
$proto35["m_columns"][] = "vlrcob";
$proto35["m_columns"][] = "link_fin_conta";
$proto35["m_columns"][] = "obs";
$proto35["m_columns"][] = "outded";
$proto35["m_columns"][] = "moramulta";
$proto35["m_columns"][] = "outacr";
$proto35["m_columns"][] = "tipo";
$proto35["m_columns"][] = "ultimousuario";
$proto35["m_columns"][] = "numdoc";
$proto35["m_columns"][] = "grupo";
$obj = new SQLTable($proto35);

$proto34["m_table"] = $obj;
$proto34["m_sql"] = "ger_op_inserirtdboleto";
$proto34["m_alias"] = "";
$proto34["m_srcTableName"] = "ger_op_inserirtdboleto";
$proto36=array();
$proto36["m_sql"] = "";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = false;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto34["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto34);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_op_inserirtdboleto";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_op_inserirtdboleto = createSqlQuery_ger_op_inserirtdboleto();


	
		;

														

$tdatager_op_inserirtdboleto[".sqlquery"] = $queryData_ger_op_inserirtdboleto;

include_once(getabspath("include/ger_op_inserirtdboleto_events.php"));
$tableEvents["ger_op_inserirtdboleto"] = new eventclass_ger_op_inserirtdboleto;
$tdatager_op_inserirtdboleto[".hasEvents"] = true;

?>