<?php
require_once(getabspath("classes/cipherer.php"));




$tdataSaldosBlocos = array();
	$tdataSaldosBlocos[".truncateText"] = true;
	$tdataSaldosBlocos[".NumberOfChars"] = 80;
	$tdataSaldosBlocos[".ShortName"] = "SaldosBlocos";
	$tdataSaldosBlocos[".OwnerID"] = "grupo";
	$tdataSaldosBlocos[".OriginalTable"] = "fin_bco_ext1";

//	field labels
$fieldLabelsSaldosBlocos = array();
$fieldToolTipsSaldosBlocos = array();
$pageTitlesSaldosBlocos = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsSaldosBlocos["Portuguese(Brazil)"] = array();
	$fieldToolTipsSaldosBlocos["Portuguese(Brazil)"] = array();
	$pageTitlesSaldosBlocos["Portuguese(Brazil)"] = array();
	$fieldLabelsSaldosBlocos["Portuguese(Brazil)"]["grupo"] = "Grupo";
	$fieldToolTipsSaldosBlocos["Portuguese(Brazil)"]["grupo"] = "";
	$fieldLabelsSaldosBlocos["Portuguese(Brazil)"]["saldo"] = "Saldo";
	$fieldToolTipsSaldosBlocos["Portuguese(Brazil)"]["saldo"] = "";
	$fieldLabelsSaldosBlocos["Portuguese(Brazil)"]["primvalor"] = "Início";
	$fieldToolTipsSaldosBlocos["Portuguese(Brazil)"]["primvalor"] = "";
	$fieldLabelsSaldosBlocos["Portuguese(Brazil)"]["ultvalor"] = "Fim";
	$fieldToolTipsSaldosBlocos["Portuguese(Brazil)"]["ultvalor"] = "";
	$fieldLabelsSaldosBlocos["Portuguese(Brazil)"]["mediadegasto"] = "Média de valor";
	$fieldToolTipsSaldosBlocos["Portuguese(Brazil)"]["mediadegasto"] = "";
	if (count($fieldToolTipsSaldosBlocos["Portuguese(Brazil)"]))
		$tdataSaldosBlocos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsSaldosBlocos[""] = array();
	$fieldToolTipsSaldosBlocos[""] = array();
	$pageTitlesSaldosBlocos[""] = array();
	if (count($fieldToolTipsSaldosBlocos[""]))
		$tdataSaldosBlocos[".isUseToolTips"] = true;
}


	$tdataSaldosBlocos[".NCSearch"] = true;



$tdataSaldosBlocos[".shortTableName"] = "SaldosBlocos";
$tdataSaldosBlocos[".nSecOptions"] = 1;
$tdataSaldosBlocos[".recsPerRowList"] = 1;
$tdataSaldosBlocos[".recsPerRowPrint"] = 1;
$tdataSaldosBlocos[".mainTableOwnerID"] = "grupo";
$tdataSaldosBlocos[".moveNext"] = 1;
$tdataSaldosBlocos[".entityType"] = 1;

$tdataSaldosBlocos[".strOriginalTableName"] = "fin_bco_ext1";





$tdataSaldosBlocos[".showAddInPopup"] = false;

$tdataSaldosBlocos[".showEditInPopup"] = false;

$tdataSaldosBlocos[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataSaldosBlocos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataSaldosBlocos[".fieldsForRegister"] = array();

$tdataSaldosBlocos[".listAjax"] = false;

	$tdataSaldosBlocos[".audit"] = true;

	$tdataSaldosBlocos[".locking"] = true;



$tdataSaldosBlocos[".list"] = true;


$tdataSaldosBlocos[".import"] = true;

$tdataSaldosBlocos[".exportTo"] = true;

$tdataSaldosBlocos[".printFriendly"] = true;


$tdataSaldosBlocos[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataSaldosBlocos[".searchSaving"] = false;
//

$tdataSaldosBlocos[".showSearchPanel"] = true;
		$tdataSaldosBlocos[".flexibleSearch"] = true;

if (isMobile())
	$tdataSaldosBlocos[".isUseAjaxSuggest"] = false;
else
	$tdataSaldosBlocos[".isUseAjaxSuggest"] = true;

$tdataSaldosBlocos[".rowHighlite"] = true;



$tdataSaldosBlocos[".addPageEvents"] = false;

// use timepicker for search panel
$tdataSaldosBlocos[".isUseTimeForSearch"] = false;



$tdataSaldosBlocos[".badgeColor"] = "8FBC8B";

$tdataSaldosBlocos[".detailsLinksOnList"] = "1";

$tdataSaldosBlocos[".allSearchFields"] = array();
$tdataSaldosBlocos[".filterFields"] = array();
$tdataSaldosBlocos[".requiredSearchFields"] = array();

$tdataSaldosBlocos[".allSearchFields"][] = "grupo";
	

$tdataSaldosBlocos[".googleLikeFields"] = array();
$tdataSaldosBlocos[".googleLikeFields"][] = "grupo";


$tdataSaldosBlocos[".advSearchFields"] = array();
$tdataSaldosBlocos[".advSearchFields"][] = "grupo";

$tdataSaldosBlocos[".tableType"] = "list";

$tdataSaldosBlocos[".printerPageOrientation"] = 0;
$tdataSaldosBlocos[".nPrinterPageScale"] = 100;

$tdataSaldosBlocos[".nPrinterSplitRecords"] = 40;

$tdataSaldosBlocos[".nPrinterPDFSplitRecords"] = 40;



$tdataSaldosBlocos[".geocodingEnabled"] = false;





$tdataSaldosBlocos[".listGridLayout"] = 3;

$tdataSaldosBlocos[".isDisplayLoading"] = true;


$tdataSaldosBlocos[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf

$tdataSaldosBlocos[".totalsFields"] = array();
$tdataSaldosBlocos[".totalsFields"][] = array(
	"fName" => "grupo",
	"numRows" => 0,
	"totalsType" => "COUNT",
	"viewFormat" => '');
$tdataSaldosBlocos[".totalsFields"][] = array(
	"fName" => "mediadegasto",
	"numRows" => 0,
	"totalsType" => "AVERAGE",
	"viewFormat" => 'Number');
$tdataSaldosBlocos[".totalsFields"][] = array(
	"fName" => "saldo",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Custom');

$tdataSaldosBlocos[".pageSize"] = 20;

$tdataSaldosBlocos[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY dtpagamento DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataSaldosBlocos[".strOrderBy"] = $tstrOrderBy;

$tdataSaldosBlocos[".orderindexes"] = array();
$tdataSaldosBlocos[".orderindexes"][] = array(2, (0 ? "ASC" : "DESC"), "dtpagamento");

$tdataSaldosBlocos[".sqlHead"] = "SELECT SUM(vlrcob) AS saldo,  MIN(dtpagamento) AS primvalor,  MAX(dtpagamento) AS ultvalor,  AVG(vlrcob) AS mediadegasto,  grupo";
$tdataSaldosBlocos[".sqlFrom"] = "FROM fin_bco_ext1";
$tdataSaldosBlocos[".sqlWhereExpr"] = "dtpagamento <> 0";
$tdataSaldosBlocos[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataSaldosBlocos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataSaldosBlocos[".arrGroupsPerPage"] = $arrGPP;

$tdataSaldosBlocos[".highlightSearchResults"] = true;

$tableKeysSaldosBlocos = array();
$tdataSaldosBlocos[".Keys"] = $tableKeysSaldosBlocos;

$tdataSaldosBlocos[".listFields"] = array();
$tdataSaldosBlocos[".listFields"][] = "grupo";
$tdataSaldosBlocos[".listFields"][] = "primvalor";
$tdataSaldosBlocos[".listFields"][] = "ultvalor";
$tdataSaldosBlocos[".listFields"][] = "mediadegasto";
$tdataSaldosBlocos[".listFields"][] = "saldo";

$tdataSaldosBlocos[".hideMobileList"] = array();


$tdataSaldosBlocos[".viewFields"] = array();

$tdataSaldosBlocos[".addFields"] = array();

$tdataSaldosBlocos[".masterListFields"] = array();
$tdataSaldosBlocos[".masterListFields"][] = "grupo";
$tdataSaldosBlocos[".masterListFields"][] = "primvalor";
$tdataSaldosBlocos[".masterListFields"][] = "ultvalor";
$tdataSaldosBlocos[".masterListFields"][] = "mediadegasto";
$tdataSaldosBlocos[".masterListFields"][] = "saldo";

$tdataSaldosBlocos[".inlineAddFields"] = array();

$tdataSaldosBlocos[".editFields"] = array();

$tdataSaldosBlocos[".inlineEditFields"] = array();

$tdataSaldosBlocos[".exportFields"] = array();
$tdataSaldosBlocos[".exportFields"][] = "grupo";
$tdataSaldosBlocos[".exportFields"][] = "primvalor";
$tdataSaldosBlocos[".exportFields"][] = "ultvalor";
$tdataSaldosBlocos[".exportFields"][] = "mediadegasto";
$tdataSaldosBlocos[".exportFields"][] = "saldo";

$tdataSaldosBlocos[".importFields"] = array();
$tdataSaldosBlocos[".importFields"][] = "saldo";
$tdataSaldosBlocos[".importFields"][] = "primvalor";
$tdataSaldosBlocos[".importFields"][] = "ultvalor";
$tdataSaldosBlocos[".importFields"][] = "mediadegasto";
$tdataSaldosBlocos[".importFields"][] = "grupo";

$tdataSaldosBlocos[".printFields"] = array();
$tdataSaldosBlocos[".printFields"][] = "grupo";
$tdataSaldosBlocos[".printFields"][] = "primvalor";
$tdataSaldosBlocos[".printFields"][] = "ultvalor";
$tdataSaldosBlocos[".printFields"][] = "mediadegasto";
$tdataSaldosBlocos[".printFields"][] = "saldo";

//	saldo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "saldo";
	$fdata["GoodName"] = "saldo";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("SaldosBlocos","saldo");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "SUM(vlrcob)";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataSaldosBlocos["saldo"] = $fdata;
//	primvalor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "primvalor";
	$fdata["GoodName"] = "primvalor";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("SaldosBlocos","primvalor");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "MIN(dtpagamento)";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataSaldosBlocos["primvalor"] = $fdata;
//	ultvalor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ultvalor";
	$fdata["GoodName"] = "ultvalor";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("SaldosBlocos","ultvalor");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "MAX(dtpagamento)";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataSaldosBlocos["ultvalor"] = $fdata;
//	mediadegasto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "mediadegasto";
	$fdata["GoodName"] = "mediadegasto";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("SaldosBlocos","mediadegasto");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "AVG(vlrcob)";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataSaldosBlocos["mediadegasto"] = $fdata;
//	grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "grupo";
	$fdata["GoodName"] = "grupo";
	$fdata["ownerTable"] = "fin_bco_ext1";
	$fdata["Label"] = GetFieldLabel("SaldosBlocos","grupo");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "grupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdataSaldosBlocos["grupo"] = $fdata;


$tables_data["SaldosBlocos"]=&$tdataSaldosBlocos;
$field_labels["SaldosBlocos"] = &$fieldLabelsSaldosBlocos;
$fieldToolTips["SaldosBlocos"] = &$fieldToolTipsSaldosBlocos;
$page_titles["SaldosBlocos"] = &$pageTitlesSaldosBlocos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["SaldosBlocos"] = array();
//	fin_bco_ext1
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="fin_bco_ext1";
		$detailsParam["dOriginalTable"] = "fin_bco_ext1";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "fin_bco_ext1";
	$detailsParam["dCaptionTable"] = GetTableCaption("fin_bco_ext1");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "0";
*/
	$detailsParam["dispChildCount"] = "0";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["SaldosBlocos"][$dIndex] = $detailsParam;

	
		$detailsTablesData["SaldosBlocos"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["SaldosBlocos"][$dIndex]["masterKeys"][]="grupo";

				$detailsTablesData["SaldosBlocos"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["SaldosBlocos"][$dIndex]["detailKeys"][]="grupo";

// tables which are master tables for current table (detail)
$masterTablesData["SaldosBlocos"] = array();


	
				$strOriginalDetailsTable="ger_grupo";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_grupo";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_grupo";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["SaldosBlocos"][0] = $masterParams;
				$masterTablesData["SaldosBlocos"][0]["masterKeys"] = array();
	$masterTablesData["SaldosBlocos"][0]["masterKeys"][]="idGrupo";
				$masterTablesData["SaldosBlocos"][0]["detailKeys"] = array();
	$masterTablesData["SaldosBlocos"][0]["detailKeys"][]="grupo";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_SaldosBlocos()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "SUM(vlrcob) AS saldo,  MIN(dtpagamento) AS primvalor,  MAX(dtpagamento) AS ultvalor,  AVG(vlrcob) AS mediadegasto,  grupo";
$proto0["m_strFrom"] = "FROM fin_bco_ext1";
$proto0["m_strWhere"] = "dtpagamento <> 0";
$proto0["m_strOrderBy"] = "ORDER BY dtpagamento DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "dtpagamento <> 0";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "dtpagamento",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "SaldosBlocos"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "<> 0";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$proto7=array();
$proto7["m_functiontype"] = "SQLF_SUM";
$proto7["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "vlrcob",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "SaldosBlocos"
));

$proto7["m_arguments"][]=$obj;
$proto7["m_strFunctionName"] = "SUM";
$obj = new SQLFunctionCall($proto7);

$proto6["m_sql"] = "SUM(vlrcob)";
$proto6["m_srcTableName"] = "SaldosBlocos";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "saldo";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$proto10=array();
$proto10["m_functiontype"] = "SQLF_MIN";
$proto10["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "dtpagamento",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "SaldosBlocos"
));

$proto10["m_arguments"][]=$obj;
$proto10["m_strFunctionName"] = "MIN";
$obj = new SQLFunctionCall($proto10);

$proto9["m_sql"] = "MIN(dtpagamento)";
$proto9["m_srcTableName"] = "SaldosBlocos";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "primvalor";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$proto13=array();
$proto13["m_functiontype"] = "SQLF_MAX";
$proto13["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "dtpagamento",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "SaldosBlocos"
));

$proto13["m_arguments"][]=$obj;
$proto13["m_strFunctionName"] = "MAX";
$obj = new SQLFunctionCall($proto13);

$proto12["m_sql"] = "MAX(dtpagamento)";
$proto12["m_srcTableName"] = "SaldosBlocos";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "ultvalor";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$proto16=array();
$proto16["m_functiontype"] = "SQLF_AVG";
$proto16["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "vlrcob",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "SaldosBlocos"
));

$proto16["m_arguments"][]=$obj;
$proto16["m_strFunctionName"] = "AVG";
$obj = new SQLFunctionCall($proto16);

$proto15["m_sql"] = "AVG(vlrcob)";
$proto15["m_srcTableName"] = "SaldosBlocos";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "mediadegasto";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "grupo",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "SaldosBlocos"
));

$proto18["m_sql"] = "grupo";
$proto18["m_srcTableName"] = "SaldosBlocos";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "fin_bco_ext1";
$proto21["m_srcTableName"] = "SaldosBlocos";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "operacao";
$proto21["m_columns"][] = "numdoc";
$proto21["m_columns"][] = "vlrcob";
$proto21["m_columns"][] = "dtpagamento";
$proto21["m_columns"][] = "obs";
$proto21["m_columns"][] = "grupo";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "fin_bco_ext1";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "SaldosBlocos";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
												$proto24=array();
						$obj = new SQLField(array(
	"m_strName" => "grupo",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "SaldosBlocos"
));

$proto24["m_column"]=$obj;
$obj = new SQLGroupByItem($proto24);

$proto0["m_groupby"][]=$obj;
$proto0["m_orderby"] = array();
												$proto26=array();
						$obj = new SQLField(array(
	"m_strName" => "dtpagamento",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "SaldosBlocos"
));

$proto26["m_column"]=$obj;
$proto26["m_bAsc"] = 0;
$proto26["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto26);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="SaldosBlocos";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_SaldosBlocos = createSqlQuery_SaldosBlocos();


	
		;

					

$tdataSaldosBlocos[".sqlquery"] = $queryData_SaldosBlocos;

$tableEvents["SaldosBlocos"] = new eventsBase;
$tdataSaldosBlocos[".hasEvents"] = false;

?>