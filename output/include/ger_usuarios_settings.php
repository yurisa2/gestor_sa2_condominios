<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_usuarios = array();
	$tdatager_usuarios[".truncateText"] = true;
	$tdatager_usuarios[".NumberOfChars"] = 80;
	$tdatager_usuarios[".ShortName"] = "ger_usuarios";
	$tdatager_usuarios[".OwnerID"] = "";
	$tdatager_usuarios[".OriginalTable"] = "ger_usuarios";

//	field labels
$fieldLabelsger_usuarios = array();
$fieldToolTipsger_usuarios = array();
$pageTitlesger_usuarios = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_usuarios["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"] = array();
	$pageTitlesger_usuarios["Portuguese(Brazil)"] = array();
	$fieldLabelsger_usuarios["Portuguese(Brazil)"]["idUsu"] = "Código";
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"]["idUsu"] = "";
	$fieldLabelsger_usuarios["Portuguese(Brazil)"]["login"] = "Login";
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"]["login"] = "";
	$fieldLabelsger_usuarios["Portuguese(Brazil)"]["senha"] = "Senha";
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"]["senha"] = "";
	$fieldLabelsger_usuarios["Portuguese(Brazil)"]["grupo"] = "Grupo";
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"]["grupo"] = "";
	$fieldLabelsger_usuarios["Portuguese(Brazil)"]["email"] = "email";
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"]["email"] = "";
	$fieldLabelsger_usuarios["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsger_usuarios["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_usuarios["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsger_usuarios["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_usuarios["Portuguese(Brazil)"]))
		$tdatager_usuarios[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_usuarios[""] = array();
	$fieldToolTipsger_usuarios[""] = array();
	$pageTitlesger_usuarios[""] = array();
	if (count($fieldToolTipsger_usuarios[""]))
		$tdatager_usuarios[".isUseToolTips"] = true;
}


	$tdatager_usuarios[".NCSearch"] = true;



$tdatager_usuarios[".shortTableName"] = "ger_usuarios";
$tdatager_usuarios[".nSecOptions"] = 0;
$tdatager_usuarios[".recsPerRowList"] = 1;
$tdatager_usuarios[".recsPerRowPrint"] = 1;
$tdatager_usuarios[".mainTableOwnerID"] = "";
$tdatager_usuarios[".moveNext"] = 1;
$tdatager_usuarios[".entityType"] = 0;

$tdatager_usuarios[".strOriginalTableName"] = "ger_usuarios";





$tdatager_usuarios[".showAddInPopup"] = false;

$tdatager_usuarios[".showEditInPopup"] = false;

$tdatager_usuarios[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_usuarios[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_usuarios[".fieldsForRegister"] = array();

$tdatager_usuarios[".listAjax"] = false;

	$tdatager_usuarios[".audit"] = true;

	$tdatager_usuarios[".locking"] = true;



$tdatager_usuarios[".list"] = true;

$tdatager_usuarios[".view"] = true;



$tdatager_usuarios[".printFriendly"] = true;

$tdatager_usuarios[".delete"] = true;

$tdatager_usuarios[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_usuarios[".searchSaving"] = false;
//

$tdatager_usuarios[".showSearchPanel"] = true;
		$tdatager_usuarios[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_usuarios[".isUseAjaxSuggest"] = false;
else
	$tdatager_usuarios[".isUseAjaxSuggest"] = true;

$tdatager_usuarios[".rowHighlite"] = true;



$tdatager_usuarios[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_usuarios[".isUseTimeForSearch"] = false;





$tdatager_usuarios[".allSearchFields"] = array();
$tdatager_usuarios[".filterFields"] = array();
$tdatager_usuarios[".requiredSearchFields"] = array();

$tdatager_usuarios[".allSearchFields"][] = "idUsu";
	$tdatager_usuarios[".allSearchFields"][] = "login";
	$tdatager_usuarios[".allSearchFields"][] = "senha";
	$tdatager_usuarios[".allSearchFields"][] = "grupo";
	$tdatager_usuarios[".allSearchFields"][] = "email";
	$tdatager_usuarios[".allSearchFields"][] = "nome";
	$tdatager_usuarios[".allSearchFields"][] = "ultimousuario";
	$tdatager_usuarios[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_usuarios[".googleLikeFields"] = array();
$tdatager_usuarios[".googleLikeFields"][] = "idUsu";
$tdatager_usuarios[".googleLikeFields"][] = "login";
$tdatager_usuarios[".googleLikeFields"][] = "senha";
$tdatager_usuarios[".googleLikeFields"][] = "grupo";
$tdatager_usuarios[".googleLikeFields"][] = "email";
$tdatager_usuarios[".googleLikeFields"][] = "nome";
$tdatager_usuarios[".googleLikeFields"][] = "ultimousuario";
$tdatager_usuarios[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_usuarios[".advSearchFields"] = array();
$tdatager_usuarios[".advSearchFields"][] = "idUsu";
$tdatager_usuarios[".advSearchFields"][] = "login";
$tdatager_usuarios[".advSearchFields"][] = "senha";
$tdatager_usuarios[".advSearchFields"][] = "grupo";
$tdatager_usuarios[".advSearchFields"][] = "email";
$tdatager_usuarios[".advSearchFields"][] = "nome";
$tdatager_usuarios[".advSearchFields"][] = "ultimousuario";
$tdatager_usuarios[".advSearchFields"][] = "ultimaalteracao";

$tdatager_usuarios[".tableType"] = "list";

$tdatager_usuarios[".printerPageOrientation"] = 0;
$tdatager_usuarios[".nPrinterPageScale"] = 100;

$tdatager_usuarios[".nPrinterSplitRecords"] = 40;

$tdatager_usuarios[".nPrinterPDFSplitRecords"] = 40;



$tdatager_usuarios[".geocodingEnabled"] = false;





$tdatager_usuarios[".listGridLayout"] = 3;

$tdatager_usuarios[".isDisplayLoading"] = true;


$tdatager_usuarios[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_usuarios[".pageSize"] = 20;

$tdatager_usuarios[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_usuarios[".strOrderBy"] = $tstrOrderBy;

$tdatager_usuarios[".orderindexes"] = array();

$tdatager_usuarios[".sqlHead"] = "SELECT idUsu,  	login,  	senha,  	grupo,  	email,  	nome,  	ultimousuario,  	ultimaalteracao";
$tdatager_usuarios[".sqlFrom"] = "FROM ger_usuarios";
$tdatager_usuarios[".sqlWhereExpr"] = "";
$tdatager_usuarios[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_usuarios[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_usuarios[".arrGroupsPerPage"] = $arrGPP;

$tdatager_usuarios[".highlightSearchResults"] = true;

$tableKeysger_usuarios = array();
$tableKeysger_usuarios[] = "idUsu";
$tdatager_usuarios[".Keys"] = $tableKeysger_usuarios;

$tdatager_usuarios[".listFields"] = array();
$tdatager_usuarios[".listFields"][] = "idUsu";
$tdatager_usuarios[".listFields"][] = "login";
$tdatager_usuarios[".listFields"][] = "senha";
$tdatager_usuarios[".listFields"][] = "grupo";
$tdatager_usuarios[".listFields"][] = "email";
$tdatager_usuarios[".listFields"][] = "nome";

$tdatager_usuarios[".hideMobileList"] = array();


$tdatager_usuarios[".viewFields"] = array();
$tdatager_usuarios[".viewFields"][] = "idUsu";
$tdatager_usuarios[".viewFields"][] = "login";
$tdatager_usuarios[".viewFields"][] = "senha";
$tdatager_usuarios[".viewFields"][] = "grupo";
$tdatager_usuarios[".viewFields"][] = "email";
$tdatager_usuarios[".viewFields"][] = "nome";
$tdatager_usuarios[".viewFields"][] = "ultimousuario";
$tdatager_usuarios[".viewFields"][] = "ultimaalteracao";

$tdatager_usuarios[".addFields"] = array();

$tdatager_usuarios[".masterListFields"] = array();

$tdatager_usuarios[".inlineAddFields"] = array();

$tdatager_usuarios[".editFields"] = array();

$tdatager_usuarios[".inlineEditFields"] = array();

$tdatager_usuarios[".exportFields"] = array();

$tdatager_usuarios[".importFields"] = array();
$tdatager_usuarios[".importFields"][] = "idUsu";
$tdatager_usuarios[".importFields"][] = "login";
$tdatager_usuarios[".importFields"][] = "senha";
$tdatager_usuarios[".importFields"][] = "grupo";
$tdatager_usuarios[".importFields"][] = "email";
$tdatager_usuarios[".importFields"][] = "nome";
$tdatager_usuarios[".importFields"][] = "ultimousuario";
$tdatager_usuarios[".importFields"][] = "ultimaalteracao";

$tdatager_usuarios[".printFields"] = array();
$tdatager_usuarios[".printFields"][] = "idUsu";
$tdatager_usuarios[".printFields"][] = "login";
$tdatager_usuarios[".printFields"][] = "senha";
$tdatager_usuarios[".printFields"][] = "grupo";
$tdatager_usuarios[".printFields"][] = "email";
$tdatager_usuarios[".printFields"][] = "nome";

//	idUsu
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idUsu";
	$fdata["GoodName"] = "idUsu";
	$fdata["ownerTable"] = "ger_usuarios";
	$fdata["Label"] = GetFieldLabel("ger_usuarios","idUsu");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "idUsu";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idUsu";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_usuarios["idUsu"] = $fdata;
//	login
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "login";
	$fdata["GoodName"] = "login";
	$fdata["ownerTable"] = "ger_usuarios";
	$fdata["Label"] = GetFieldLabel("ger_usuarios","login");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "login";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "login";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_usuarios["login"] = $fdata;
//	senha
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "senha";
	$fdata["GoodName"] = "senha";
	$fdata["ownerTable"] = "ger_usuarios";
	$fdata["Label"] = GetFieldLabel("ger_usuarios","senha");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "senha";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "senha";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_usuarios["senha"] = $fdata;
//	grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "grupo";
	$fdata["GoodName"] = "grupo";
	$fdata["ownerTable"] = "ger_usuarios";
	$fdata["Label"] = GetFieldLabel("ger_usuarios","grupo");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "grupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "nome";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_usuarios["grupo"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "ger_usuarios";
	$fdata["Label"] = GetFieldLabel("ger_usuarios","email");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_usuarios["email"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "ger_usuarios";
	$fdata["Label"] = GetFieldLabel("ger_usuarios","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=60";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_usuarios["nome"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_usuarios";
	$fdata["Label"] = GetFieldLabel("ger_usuarios","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_usuarios["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_usuarios";
	$fdata["Label"] = GetFieldLabel("ger_usuarios","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_usuarios["ultimaalteracao"] = $fdata;


$tables_data["ger_usuarios"]=&$tdatager_usuarios;
$field_labels["ger_usuarios"] = &$fieldLabelsger_usuarios;
$fieldToolTips["ger_usuarios"] = &$fieldToolTipsger_usuarios;
$page_titles["ger_usuarios"] = &$pageTitlesger_usuarios;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_usuarios"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_usuarios"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_usuarios()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idUsu,  	login,  	senha,  	grupo,  	email,  	nome,  	ultimousuario,  	ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_usuarios";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idUsu",
	"m_strTable" => "ger_usuarios",
	"m_srcTableName" => "ger_usuarios"
));

$proto6["m_sql"] = "idUsu";
$proto6["m_srcTableName"] = "ger_usuarios";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "login",
	"m_strTable" => "ger_usuarios",
	"m_srcTableName" => "ger_usuarios"
));

$proto8["m_sql"] = "login";
$proto8["m_srcTableName"] = "ger_usuarios";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "senha",
	"m_strTable" => "ger_usuarios",
	"m_srcTableName" => "ger_usuarios"
));

$proto10["m_sql"] = "senha";
$proto10["m_srcTableName"] = "ger_usuarios";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "grupo",
	"m_strTable" => "ger_usuarios",
	"m_srcTableName" => "ger_usuarios"
));

$proto12["m_sql"] = "grupo";
$proto12["m_srcTableName"] = "ger_usuarios";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "ger_usuarios",
	"m_srcTableName" => "ger_usuarios"
));

$proto14["m_sql"] = "email";
$proto14["m_srcTableName"] = "ger_usuarios";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_usuarios",
	"m_srcTableName" => "ger_usuarios"
));

$proto16["m_sql"] = "nome";
$proto16["m_srcTableName"] = "ger_usuarios";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_usuarios",
	"m_srcTableName" => "ger_usuarios"
));

$proto18["m_sql"] = "ultimousuario";
$proto18["m_srcTableName"] = "ger_usuarios";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_usuarios",
	"m_srcTableName" => "ger_usuarios"
));

$proto20["m_sql"] = "ultimaalteracao";
$proto20["m_srcTableName"] = "ger_usuarios";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "ger_usuarios";
$proto23["m_srcTableName"] = "ger_usuarios";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "idUsu";
$proto23["m_columns"][] = "login";
$proto23["m_columns"][] = "senha";
$proto23["m_columns"][] = "grupo";
$proto23["m_columns"][] = "email";
$proto23["m_columns"][] = "nome";
$proto23["m_columns"][] = "ultimousuario";
$proto23["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "ger_usuarios";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "ger_usuarios";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_usuarios";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_usuarios = createSqlQuery_ger_usuarios();


	
		;

								

$tdatager_usuarios[".sqlquery"] = $queryData_ger_usuarios;

$tableEvents["ger_usuarios"] = new eventsBase;
$tdatager_usuarios[".hasEvents"] = false;

?>