<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_despesas = array();
	$tdatafin_despesas[".truncateText"] = true;
	$tdatafin_despesas[".NumberOfChars"] = 80;
	$tdatafin_despesas[".ShortName"] = "fin_despesas";
	$tdatafin_despesas[".OwnerID"] = "";
	$tdatafin_despesas[".OriginalTable"] = "fin_despesas";

//	field labels
$fieldLabelsfin_despesas = array();
$fieldToolTipsfin_despesas = array();
$pageTitlesfin_despesas = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_despesas["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"] = array();
	$pageTitlesfin_despesas["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["idDespesa"] = "Código";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["idDespesa"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["vencimento"] = "Vencimento";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["vencimento"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["link_fin_meiospgto"] = "Meio de pgto";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["link_fin_meiospgto"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["anotado"] = "Anotado";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["anotado"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["link_fin_tipogasto"] = "Gasto";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["link_fin_tipogasto"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["link_fin_tipodoc"] = "Tipo de doc";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["link_fin_tipodoc"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["ndoc"] = "Numero do doc";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["ndoc"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["link_fin_fornecedor"] = "Fornecedor";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["link_fin_fornecedor"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["valor"] = "Valor";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["valor"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["link_fin_conta"] = "Conta";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["link_fin_conta"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["pagamento"] = "Data do pagamento";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["pagamento"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["ultimaalteracao"] = "Último usuário";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["ultimousuario"] = "Ultima Alteração";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["link_ger_grupo"] = "Grupo/Bloco";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["link_ger_grupo"] = "";
	$fieldLabelsfin_despesas["Portuguese(Brazil)"]["documento"] = "Documento";
	$fieldToolTipsfin_despesas["Portuguese(Brazil)"]["documento"] = "";
	if (count($fieldToolTipsfin_despesas["Portuguese(Brazil)"]))
		$tdatafin_despesas[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_despesas[""] = array();
	$fieldToolTipsfin_despesas[""] = array();
	$pageTitlesfin_despesas[""] = array();
	if (count($fieldToolTipsfin_despesas[""]))
		$tdatafin_despesas[".isUseToolTips"] = true;
}


	$tdatafin_despesas[".NCSearch"] = true;



$tdatafin_despesas[".shortTableName"] = "fin_despesas";
$tdatafin_despesas[".nSecOptions"] = 0;
$tdatafin_despesas[".recsPerRowList"] = 1;
$tdatafin_despesas[".recsPerRowPrint"] = 1;
$tdatafin_despesas[".mainTableOwnerID"] = "";
$tdatafin_despesas[".moveNext"] = 1;
$tdatafin_despesas[".entityType"] = 0;

$tdatafin_despesas[".strOriginalTableName"] = "fin_despesas";





$tdatafin_despesas[".showAddInPopup"] = false;

$tdatafin_despesas[".showEditInPopup"] = false;

$tdatafin_despesas[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_despesas[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_despesas[".fieldsForRegister"] = array();

$tdatafin_despesas[".listAjax"] = false;

	$tdatafin_despesas[".audit"] = true;

	$tdatafin_despesas[".locking"] = true;

$tdatafin_despesas[".edit"] = true;
$tdatafin_despesas[".afterEditAction"] = 1;
$tdatafin_despesas[".closePopupAfterEdit"] = 1;
$tdatafin_despesas[".afterEditActionDetTable"] = "";

$tdatafin_despesas[".add"] = true;
$tdatafin_despesas[".afterAddAction"] = 1;
$tdatafin_despesas[".closePopupAfterAdd"] = 1;
$tdatafin_despesas[".afterAddActionDetTable"] = "";

$tdatafin_despesas[".list"] = true;

$tdatafin_despesas[".view"] = true;



$tdatafin_despesas[".printFriendly"] = true;

$tdatafin_despesas[".delete"] = true;

$tdatafin_despesas[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_despesas[".searchSaving"] = false;
//

$tdatafin_despesas[".showSearchPanel"] = true;
		$tdatafin_despesas[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_despesas[".isUseAjaxSuggest"] = false;
else
	$tdatafin_despesas[".isUseAjaxSuggest"] = true;

$tdatafin_despesas[".rowHighlite"] = true;



$tdatafin_despesas[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_despesas[".isUseTimeForSearch"] = false;



$tdatafin_despesas[".badgeColor"] = "6493EA";

$tdatafin_despesas[".detailsLinksOnList"] = "1";

$tdatafin_despesas[".allSearchFields"] = array();
$tdatafin_despesas[".filterFields"] = array();
$tdatafin_despesas[".requiredSearchFields"] = array();

$tdatafin_despesas[".allSearchFields"][] = "link_ger_grupo";
	$tdatafin_despesas[".allSearchFields"][] = "link_fin_tipogasto";
	$tdatafin_despesas[".allSearchFields"][] = "documento";
	$tdatafin_despesas[".allSearchFields"][] = "anotado";
	$tdatafin_despesas[".allSearchFields"][] = "link_fin_fornecedor";
	$tdatafin_despesas[".allSearchFields"][] = "vencimento";
	$tdatafin_despesas[".allSearchFields"][] = "link_fin_meiospgto";
	$tdatafin_despesas[".allSearchFields"][] = "link_fin_tipodoc";
	$tdatafin_despesas[".allSearchFields"][] = "ndoc";
	$tdatafin_despesas[".allSearchFields"][] = "link_fin_conta";
	$tdatafin_despesas[".allSearchFields"][] = "valor";
	$tdatafin_despesas[".allSearchFields"][] = "pagamento";
	$tdatafin_despesas[".allSearchFields"][] = "obs";
	$tdatafin_despesas[".allSearchFields"][] = "ultimousuario";
	$tdatafin_despesas[".allSearchFields"][] = "ultimaalteracao";
	

$tdatafin_despesas[".googleLikeFields"] = array();
$tdatafin_despesas[".googleLikeFields"][] = "vencimento";
$tdatafin_despesas[".googleLikeFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".googleLikeFields"][] = "anotado";
$tdatafin_despesas[".googleLikeFields"][] = "obs";
$tdatafin_despesas[".googleLikeFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".googleLikeFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".googleLikeFields"][] = "ndoc";
$tdatafin_despesas[".googleLikeFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".googleLikeFields"][] = "valor";
$tdatafin_despesas[".googleLikeFields"][] = "link_fin_conta";
$tdatafin_despesas[".googleLikeFields"][] = "pagamento";
$tdatafin_despesas[".googleLikeFields"][] = "ultimaalteracao";
$tdatafin_despesas[".googleLikeFields"][] = "ultimousuario";
$tdatafin_despesas[".googleLikeFields"][] = "link_ger_grupo";
$tdatafin_despesas[".googleLikeFields"][] = "documento";


$tdatafin_despesas[".advSearchFields"] = array();
$tdatafin_despesas[".advSearchFields"][] = "link_ger_grupo";
$tdatafin_despesas[".advSearchFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".advSearchFields"][] = "documento";
$tdatafin_despesas[".advSearchFields"][] = "anotado";
$tdatafin_despesas[".advSearchFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".advSearchFields"][] = "vencimento";
$tdatafin_despesas[".advSearchFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".advSearchFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".advSearchFields"][] = "ndoc";
$tdatafin_despesas[".advSearchFields"][] = "link_fin_conta";
$tdatafin_despesas[".advSearchFields"][] = "valor";
$tdatafin_despesas[".advSearchFields"][] = "pagamento";
$tdatafin_despesas[".advSearchFields"][] = "obs";
$tdatafin_despesas[".advSearchFields"][] = "ultimousuario";
$tdatafin_despesas[".advSearchFields"][] = "ultimaalteracao";

$tdatafin_despesas[".tableType"] = "list";

$tdatafin_despesas[".printerPageOrientation"] = 0;
$tdatafin_despesas[".nPrinterPageScale"] = 100;

$tdatafin_despesas[".nPrinterSplitRecords"] = 40;

$tdatafin_despesas[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_despesas[".geocodingEnabled"] = false;




$tdatafin_despesas[".printGridLayout"] = 1;

$tdatafin_despesas[".listGridLayout"] = 3;

$tdatafin_despesas[".isDisplayLoading"] = true;


$tdatafin_despesas[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf

$tdatafin_despesas[".totalsFields"] = array();
$tdatafin_despesas[".totalsFields"][] = array(
	"fName" => "valor",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Currency');

$tdatafin_despesas[".pageSize"] = 20;

$tdatafin_despesas[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY vencimento DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_despesas[".strOrderBy"] = $tstrOrderBy;

$tdatafin_despesas[".orderindexes"] = array();
$tdatafin_despesas[".orderindexes"][] = array(2, (0 ? "ASC" : "DESC"), "vencimento");

$tdatafin_despesas[".sqlHead"] = "select idDespesa,  vencimento,  link_fin_meiospgto,  anotado,  obs,  link_fin_tipogasto,  link_fin_tipodoc,  ndoc,  link_fin_fornecedor,  valor,  link_fin_conta,  pagamento,  ultimaalteracao,  ultimousuario,  link_ger_grupo,  documento";
$tdatafin_despesas[".sqlFrom"] = "FROM fin_despesas";
$tdatafin_despesas[".sqlWhereExpr"] = "";
$tdatafin_despesas[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_despesas[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_despesas[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_despesas[".highlightSearchResults"] = true;

$tableKeysfin_despesas = array();
$tableKeysfin_despesas[] = "idDespesa";
$tdatafin_despesas[".Keys"] = $tableKeysfin_despesas;

$tdatafin_despesas[".listFields"] = array();
$tdatafin_despesas[".listFields"][] = "documento";
$tdatafin_despesas[".listFields"][] = "link_ger_grupo";
$tdatafin_despesas[".listFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".listFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".listFields"][] = "vencimento";
$tdatafin_despesas[".listFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".listFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".listFields"][] = "ndoc";
$tdatafin_despesas[".listFields"][] = "link_fin_conta";
$tdatafin_despesas[".listFields"][] = "valor";
$tdatafin_despesas[".listFields"][] = "pagamento";
$tdatafin_despesas[".listFields"][] = "obs";

$tdatafin_despesas[".hideMobileList"] = array();


$tdatafin_despesas[".viewFields"] = array();
$tdatafin_despesas[".viewFields"][] = "link_ger_grupo";
$tdatafin_despesas[".viewFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".viewFields"][] = "documento";
$tdatafin_despesas[".viewFields"][] = "idDespesa";
$tdatafin_despesas[".viewFields"][] = "anotado";
$tdatafin_despesas[".viewFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".viewFields"][] = "vencimento";
$tdatafin_despesas[".viewFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".viewFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".viewFields"][] = "ndoc";
$tdatafin_despesas[".viewFields"][] = "link_fin_conta";
$tdatafin_despesas[".viewFields"][] = "valor";
$tdatafin_despesas[".viewFields"][] = "pagamento";
$tdatafin_despesas[".viewFields"][] = "obs";
$tdatafin_despesas[".viewFields"][] = "ultimousuario";
$tdatafin_despesas[".viewFields"][] = "ultimaalteracao";

$tdatafin_despesas[".addFields"] = array();
$tdatafin_despesas[".addFields"][] = "link_ger_grupo";
$tdatafin_despesas[".addFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".addFields"][] = "documento";
$tdatafin_despesas[".addFields"][] = "anotado";
$tdatafin_despesas[".addFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".addFields"][] = "vencimento";
$tdatafin_despesas[".addFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".addFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".addFields"][] = "ndoc";
$tdatafin_despesas[".addFields"][] = "link_fin_conta";
$tdatafin_despesas[".addFields"][] = "valor";
$tdatafin_despesas[".addFields"][] = "pagamento";
$tdatafin_despesas[".addFields"][] = "obs";

$tdatafin_despesas[".masterListFields"] = array();
$tdatafin_despesas[".masterListFields"][] = "documento";
$tdatafin_despesas[".masterListFields"][] = "link_ger_grupo";
$tdatafin_despesas[".masterListFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".masterListFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".masterListFields"][] = "vencimento";
$tdatafin_despesas[".masterListFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".masterListFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".masterListFields"][] = "ndoc";
$tdatafin_despesas[".masterListFields"][] = "link_fin_conta";
$tdatafin_despesas[".masterListFields"][] = "valor";
$tdatafin_despesas[".masterListFields"][] = "pagamento";
$tdatafin_despesas[".masterListFields"][] = "obs";

$tdatafin_despesas[".inlineAddFields"] = array();

$tdatafin_despesas[".editFields"] = array();
$tdatafin_despesas[".editFields"][] = "link_ger_grupo";
$tdatafin_despesas[".editFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".editFields"][] = "documento";
$tdatafin_despesas[".editFields"][] = "idDespesa";
$tdatafin_despesas[".editFields"][] = "anotado";
$tdatafin_despesas[".editFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".editFields"][] = "vencimento";
$tdatafin_despesas[".editFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".editFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".editFields"][] = "ndoc";
$tdatafin_despesas[".editFields"][] = "link_fin_conta";
$tdatafin_despesas[".editFields"][] = "valor";
$tdatafin_despesas[".editFields"][] = "pagamento";
$tdatafin_despesas[".editFields"][] = "obs";

$tdatafin_despesas[".inlineEditFields"] = array();

$tdatafin_despesas[".exportFields"] = array();

$tdatafin_despesas[".importFields"] = array();
$tdatafin_despesas[".importFields"][] = "idDespesa";
$tdatafin_despesas[".importFields"][] = "vencimento";
$tdatafin_despesas[".importFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".importFields"][] = "anotado";
$tdatafin_despesas[".importFields"][] = "obs";
$tdatafin_despesas[".importFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".importFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".importFields"][] = "ndoc";
$tdatafin_despesas[".importFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".importFields"][] = "valor";
$tdatafin_despesas[".importFields"][] = "link_fin_conta";
$tdatafin_despesas[".importFields"][] = "pagamento";
$tdatafin_despesas[".importFields"][] = "ultimaalteracao";
$tdatafin_despesas[".importFields"][] = "ultimousuario";
$tdatafin_despesas[".importFields"][] = "link_ger_grupo";
$tdatafin_despesas[".importFields"][] = "documento";

$tdatafin_despesas[".printFields"] = array();
$tdatafin_despesas[".printFields"][] = "link_ger_grupo";
$tdatafin_despesas[".printFields"][] = "link_fin_tipogasto";
$tdatafin_despesas[".printFields"][] = "documento";
$tdatafin_despesas[".printFields"][] = "link_fin_fornecedor";
$tdatafin_despesas[".printFields"][] = "vencimento";
$tdatafin_despesas[".printFields"][] = "link_fin_meiospgto";
$tdatafin_despesas[".printFields"][] = "link_fin_tipodoc";
$tdatafin_despesas[".printFields"][] = "ndoc";
$tdatafin_despesas[".printFields"][] = "link_fin_conta";
$tdatafin_despesas[".printFields"][] = "valor";
$tdatafin_despesas[".printFields"][] = "pagamento";
$tdatafin_despesas[".printFields"][] = "obs";

//	idDespesa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idDespesa";
	$fdata["GoodName"] = "idDespesa";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","idDespesa");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idDespesa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idDespesa";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_despesas["idDespesa"] = $fdata;
//	vencimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "vencimento";
	$fdata["GoodName"] = "vencimento";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","vencimento");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "vencimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vencimento";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_despesas["vencimento"] = $fdata;
//	link_fin_meiospgto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "link_fin_meiospgto";
	$fdata["GoodName"] = "link_fin_meiospgto";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","link_fin_meiospgto");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "link_fin_meiospgto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_fin_meiospgto";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_meiospgto";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idMeiospgto";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "meiopgto";

	
	$edata["LookupOrderBy"] = "meiopgto";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_despesas["link_fin_meiospgto"] = $fdata;
//	anotado
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "anotado";
	$fdata["GoodName"] = "anotado";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","anotado");
	$fdata["FieldType"] = 135;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "anotado";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "anotado";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_despesas["anotado"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","obs");
	$fdata["FieldType"] = 201;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 250;
			$edata["nCols"] = 500;

	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_despesas["obs"] = $fdata;
//	link_fin_tipogasto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "link_fin_tipogasto";
	$fdata["GoodName"] = "link_fin_tipogasto";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","link_fin_tipogasto");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "link_fin_tipogasto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_fin_tipogasto";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_tipogastos";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "tipodegasto";
	$edata["LinkFieldType"] = 129;
	$edata["DisplayField"] = "tipodegasto";

	
	$edata["LookupOrderBy"] = "tipodegasto";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_despesas["link_fin_tipogasto"] = $fdata;
//	link_fin_tipodoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "link_fin_tipodoc";
	$fdata["GoodName"] = "link_fin_tipodoc";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","link_fin_tipodoc");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "link_fin_tipodoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_fin_tipodoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_tipodocs";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idTipodocs";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "tipodoc";

	
	$edata["LookupOrderBy"] = "";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_despesas["link_fin_tipodoc"] = $fdata;
//	ndoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ndoc";
	$fdata["GoodName"] = "ndoc";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","ndoc");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "ndoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ndoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_despesas["ndoc"] = $fdata;
//	link_fin_fornecedor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "link_fin_fornecedor";
	$fdata["GoodName"] = "link_fin_fornecedor";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","link_fin_fornecedor");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "link_fin_fornecedor";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_fin_fornecedor";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_fornecedores";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idForn";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nomefantasia";

	
	$edata["LookupOrderBy"] = "";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_despesas["link_fin_fornecedor"] = $fdata;
//	valor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "valor";
	$fdata["GoodName"] = "valor";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","valor");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "valor";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "valor";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Currency");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_despesas["valor"] = $fdata;
//	link_fin_conta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "link_fin_conta";
	$fdata["GoodName"] = "link_fin_conta";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","link_fin_conta");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "link_fin_conta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_fin_conta";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_contas";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idConta";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "nome";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_despesas["link_fin_conta"] = $fdata;
//	pagamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "pagamento";
	$fdata["GoodName"] = "pagamento";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","pagamento");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "pagamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pagamento";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_despesas["pagamento"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_despesas["ultimaalteracao"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_despesas["ultimousuario"] = $fdata;
//	link_ger_grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "link_ger_grupo";
	$fdata["GoodName"] = "link_ger_grupo";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","link_ger_grupo");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "link_ger_grupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_grupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "nome";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_despesas["link_ger_grupo"] = $fdata;
//	documento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "documento";
	$fdata["GoodName"] = "documento";
	$fdata["ownerTable"] = "fin_despesas";
	$fdata["Label"] = GetFieldLabel("fin_despesas","documento");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "documento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "documento";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "arquivos";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
	



	
	
		$edata["UseTimestamp"] = true;

	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_despesas["documento"] = $fdata;


$tables_data["fin_despesas"]=&$tdatafin_despesas;
$field_labels["fin_despesas"] = &$fieldLabelsfin_despesas;
$fieldToolTips["fin_despesas"] = &$fieldToolTipsfin_despesas;
$page_titles["fin_despesas"] = &$pageTitlesfin_despesas;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_despesas"] = array();
//	fin_cheques
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="fin_cheques";
		$detailsParam["dOriginalTable"] = "fin_cheques";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "fin_cheques";
	$detailsParam["dCaptionTable"] = GetTableCaption("fin_cheques");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["fin_despesas"][$dIndex] = $detailsParam;

	
		$detailsTablesData["fin_despesas"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["fin_despesas"][$dIndex]["masterKeys"][]="idDespesa";

				$detailsTablesData["fin_despesas"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["fin_despesas"][$dIndex]["detailKeys"][]="link_fin_despesa";

// tables which are master tables for current table (detail)
$masterTablesData["fin_despesas"] = array();


	
				$strOriginalDetailsTable="ger_grupo";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_grupo";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_grupo";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["fin_despesas"][0] = $masterParams;
				$masterTablesData["fin_despesas"][0]["masterKeys"] = array();
	$masterTablesData["fin_despesas"][0]["masterKeys"][]="idGrupo";
				$masterTablesData["fin_despesas"][0]["detailKeys"] = array();
	$masterTablesData["fin_despesas"][0]["detailKeys"][]="link_ger_grupo";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_despesas()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idDespesa,  vencimento,  link_fin_meiospgto,  anotado,  obs,  link_fin_tipogasto,  link_fin_tipodoc,  ndoc,  link_fin_fornecedor,  valor,  link_fin_conta,  pagamento,  ultimaalteracao,  ultimousuario,  link_ger_grupo,  documento";
$proto0["m_strFrom"] = "FROM fin_despesas";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY vencimento DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idDespesa",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto6["m_sql"] = "idDespesa";
$proto6["m_srcTableName"] = "fin_despesas";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "vencimento",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto8["m_sql"] = "vencimento";
$proto8["m_srcTableName"] = "fin_despesas";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "link_fin_meiospgto",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto10["m_sql"] = "link_fin_meiospgto";
$proto10["m_srcTableName"] = "fin_despesas";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "anotado",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto12["m_sql"] = "anotado";
$proto12["m_srcTableName"] = "fin_despesas";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto14["m_sql"] = "obs";
$proto14["m_srcTableName"] = "fin_despesas";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "link_fin_tipogasto",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto16["m_sql"] = "link_fin_tipogasto";
$proto16["m_srcTableName"] = "fin_despesas";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "link_fin_tipodoc",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto18["m_sql"] = "link_fin_tipodoc";
$proto18["m_srcTableName"] = "fin_despesas";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ndoc",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto20["m_sql"] = "ndoc";
$proto20["m_srcTableName"] = "fin_despesas";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "link_fin_fornecedor",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto22["m_sql"] = "link_fin_fornecedor";
$proto22["m_srcTableName"] = "fin_despesas";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "valor",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto24["m_sql"] = "valor";
$proto24["m_srcTableName"] = "fin_despesas";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "link_fin_conta",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto26["m_sql"] = "link_fin_conta";
$proto26["m_srcTableName"] = "fin_despesas";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "pagamento",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto28["m_sql"] = "pagamento";
$proto28["m_srcTableName"] = "fin_despesas";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto30["m_sql"] = "ultimaalteracao";
$proto30["m_srcTableName"] = "fin_despesas";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto32["m_sql"] = "ultimousuario";
$proto32["m_srcTableName"] = "fin_despesas";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_grupo",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto34["m_sql"] = "link_ger_grupo";
$proto34["m_srcTableName"] = "fin_despesas";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "documento",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto36["m_sql"] = "documento";
$proto36["m_srcTableName"] = "fin_despesas";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto38=array();
$proto38["m_link"] = "SQLL_MAIN";
			$proto39=array();
$proto39["m_strName"] = "fin_despesas";
$proto39["m_srcTableName"] = "fin_despesas";
$proto39["m_columns"] = array();
$proto39["m_columns"][] = "idDespesa";
$proto39["m_columns"][] = "vencimento";
$proto39["m_columns"][] = "link_fin_meiospgto";
$proto39["m_columns"][] = "anotado";
$proto39["m_columns"][] = "obs";
$proto39["m_columns"][] = "link_fin_tipogasto";
$proto39["m_columns"][] = "link_fin_tipodoc";
$proto39["m_columns"][] = "ndoc";
$proto39["m_columns"][] = "link_fin_fornecedor";
$proto39["m_columns"][] = "valor";
$proto39["m_columns"][] = "link_fin_conta";
$proto39["m_columns"][] = "pagamento";
$proto39["m_columns"][] = "ultimousuario";
$proto39["m_columns"][] = "ultimaalteracao";
$proto39["m_columns"][] = "link_ger_grupo";
$proto39["m_columns"][] = "documento";
$obj = new SQLTable($proto39);

$proto38["m_table"] = $obj;
$proto38["m_sql"] = "fin_despesas";
$proto38["m_alias"] = "";
$proto38["m_srcTableName"] = "fin_despesas";
$proto40=array();
$proto40["m_sql"] = "";
$proto40["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto40["m_column"]=$obj;
$proto40["m_contained"] = array();
$proto40["m_strCase"] = "";
$proto40["m_havingmode"] = false;
$proto40["m_inBrackets"] = false;
$proto40["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto40);

$proto38["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto38);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto42=array();
						$obj = new SQLField(array(
	"m_strName" => "vencimento",
	"m_strTable" => "fin_despesas",
	"m_srcTableName" => "fin_despesas"
));

$proto42["m_column"]=$obj;
$proto42["m_bAsc"] = 0;
$proto42["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto42);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="fin_despesas";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_despesas = createSqlQuery_fin_despesas();


	
		;

																

$tdatafin_despesas[".sqlquery"] = $queryData_fin_despesas;

include_once(getabspath("include/fin_despesas_events.php"));
$tableEvents["fin_despesas"] = new eventclass_fin_despesas;
$tdatafin_despesas[".hasEvents"] = true;

?>