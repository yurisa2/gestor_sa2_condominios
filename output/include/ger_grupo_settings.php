<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_grupo = array();
	$tdatager_grupo[".truncateText"] = true;
	$tdatager_grupo[".NumberOfChars"] = 80;
	$tdatager_grupo[".ShortName"] = "ger_grupo";
	$tdatager_grupo[".OwnerID"] = "nome";
	$tdatager_grupo[".OriginalTable"] = "ger_grupo";

//	field labels
$fieldLabelsger_grupo = array();
$fieldToolTipsger_grupo = array();
$pageTitlesger_grupo = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_grupo["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_grupo["Portuguese(Brazil)"] = array();
	$pageTitlesger_grupo["Portuguese(Brazil)"] = array();
	$fieldLabelsger_grupo["Portuguese(Brazil)"]["idGrupo"] = "Cód";
	$fieldToolTipsger_grupo["Portuguese(Brazil)"]["idGrupo"] = "";
	$fieldLabelsger_grupo["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsger_grupo["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsger_grupo["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsger_grupo["Portuguese(Brazil)"]["obs"] = "";
	if (count($fieldToolTipsger_grupo["Portuguese(Brazil)"]))
		$tdatager_grupo[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_grupo[""] = array();
	$fieldToolTipsger_grupo[""] = array();
	$pageTitlesger_grupo[""] = array();
	if (count($fieldToolTipsger_grupo[""]))
		$tdatager_grupo[".isUseToolTips"] = true;
}


	$tdatager_grupo[".NCSearch"] = true;



$tdatager_grupo[".shortTableName"] = "ger_grupo";
$tdatager_grupo[".nSecOptions"] = 1;
$tdatager_grupo[".recsPerRowList"] = 1;
$tdatager_grupo[".recsPerRowPrint"] = 1;
$tdatager_grupo[".mainTableOwnerID"] = "nome";
$tdatager_grupo[".moveNext"] = 1;
$tdatager_grupo[".entityType"] = 0;

$tdatager_grupo[".strOriginalTableName"] = "ger_grupo";





$tdatager_grupo[".showAddInPopup"] = false;

$tdatager_grupo[".showEditInPopup"] = false;

$tdatager_grupo[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_grupo[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_grupo[".fieldsForRegister"] = array();

$tdatager_grupo[".listAjax"] = false;

	$tdatager_grupo[".audit"] = true;

	$tdatager_grupo[".locking"] = true;

$tdatager_grupo[".edit"] = true;
$tdatager_grupo[".afterEditAction"] = 1;
$tdatager_grupo[".closePopupAfterEdit"] = 1;
$tdatager_grupo[".afterEditActionDetTable"] = "";

$tdatager_grupo[".add"] = true;
$tdatager_grupo[".afterAddAction"] = 1;
$tdatager_grupo[".closePopupAfterAdd"] = 1;
$tdatager_grupo[".afterAddActionDetTable"] = "";

$tdatager_grupo[".list"] = true;

$tdatager_grupo[".view"] = true;

$tdatager_grupo[".import"] = true;

$tdatager_grupo[".exportTo"] = true;

$tdatager_grupo[".printFriendly"] = true;

$tdatager_grupo[".delete"] = true;

$tdatager_grupo[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_grupo[".searchSaving"] = false;
//

$tdatager_grupo[".showSearchPanel"] = true;
		$tdatager_grupo[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_grupo[".isUseAjaxSuggest"] = false;
else
	$tdatager_grupo[".isUseAjaxSuggest"] = true;

$tdatager_grupo[".rowHighlite"] = true;



$tdatager_grupo[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_grupo[".isUseTimeForSearch"] = false;




$tdatager_grupo[".detailsLinksOnList"] = "1";

$tdatager_grupo[".allSearchFields"] = array();
$tdatager_grupo[".filterFields"] = array();
$tdatager_grupo[".requiredSearchFields"] = array();

$tdatager_grupo[".allSearchFields"][] = "nome";
	$tdatager_grupo[".allSearchFields"][] = "obs";
	

$tdatager_grupo[".googleLikeFields"] = array();
$tdatager_grupo[".googleLikeFields"][] = "nome";
$tdatager_grupo[".googleLikeFields"][] = "obs";


$tdatager_grupo[".advSearchFields"] = array();
$tdatager_grupo[".advSearchFields"][] = "nome";
$tdatager_grupo[".advSearchFields"][] = "obs";

$tdatager_grupo[".tableType"] = "list";

$tdatager_grupo[".printerPageOrientation"] = 0;
$tdatager_grupo[".nPrinterPageScale"] = 100;

$tdatager_grupo[".nPrinterSplitRecords"] = 40;

$tdatager_grupo[".nPrinterPDFSplitRecords"] = 40;



$tdatager_grupo[".geocodingEnabled"] = false;





$tdatager_grupo[".listGridLayout"] = 3;

$tdatager_grupo[".isDisplayLoading"] = true;


$tdatager_grupo[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_grupo[".pageSize"] = 20;

$tdatager_grupo[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_grupo[".strOrderBy"] = $tstrOrderBy;

$tdatager_grupo[".orderindexes"] = array();

$tdatager_grupo[".sqlHead"] = "SELECT idGrupo,  	nome,  	obs";
$tdatager_grupo[".sqlFrom"] = "FROM ger_grupo";
$tdatager_grupo[".sqlWhereExpr"] = "";
$tdatager_grupo[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_grupo[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_grupo[".arrGroupsPerPage"] = $arrGPP;

$tdatager_grupo[".highlightSearchResults"] = true;

$tableKeysger_grupo = array();
$tableKeysger_grupo[] = "idGrupo";
$tdatager_grupo[".Keys"] = $tableKeysger_grupo;

$tdatager_grupo[".listFields"] = array();
$tdatager_grupo[".listFields"][] = "nome";
$tdatager_grupo[".listFields"][] = "idGrupo";
$tdatager_grupo[".listFields"][] = "obs";

$tdatager_grupo[".hideMobileList"] = array();


$tdatager_grupo[".viewFields"] = array();
$tdatager_grupo[".viewFields"][] = "nome";
$tdatager_grupo[".viewFields"][] = "obs";

$tdatager_grupo[".addFields"] = array();
$tdatager_grupo[".addFields"][] = "nome";
$tdatager_grupo[".addFields"][] = "obs";

$tdatager_grupo[".masterListFields"] = array();
$tdatager_grupo[".masterListFields"][] = "nome";
$tdatager_grupo[".masterListFields"][] = "idGrupo";
$tdatager_grupo[".masterListFields"][] = "obs";

$tdatager_grupo[".inlineAddFields"] = array();

$tdatager_grupo[".editFields"] = array();
$tdatager_grupo[".editFields"][] = "nome";
$tdatager_grupo[".editFields"][] = "obs";

$tdatager_grupo[".inlineEditFields"] = array();

$tdatager_grupo[".exportFields"] = array();
$tdatager_grupo[".exportFields"][] = "nome";
$tdatager_grupo[".exportFields"][] = "obs";

$tdatager_grupo[".importFields"] = array();
$tdatager_grupo[".importFields"][] = "idGrupo";
$tdatager_grupo[".importFields"][] = "nome";
$tdatager_grupo[".importFields"][] = "obs";

$tdatager_grupo[".printFields"] = array();
$tdatager_grupo[".printFields"][] = "nome";
$tdatager_grupo[".printFields"][] = "obs";

//	idGrupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idGrupo";
	$fdata["GoodName"] = "idGrupo";
	$fdata["ownerTable"] = "ger_grupo";
	$fdata["Label"] = GetFieldLabel("ger_grupo","idGrupo");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
	
	
		$fdata["strField"] = "idGrupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idGrupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_grupo["idGrupo"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "ger_grupo";
	$fdata["Label"] = GetFieldLabel("ger_grupo","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=45";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_grupo["nome"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_grupo";
	$fdata["Label"] = GetFieldLabel("ger_grupo","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=45";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_grupo["obs"] = $fdata;


$tables_data["ger_grupo"]=&$tdatager_grupo;
$field_labels["ger_grupo"] = &$fieldLabelsger_grupo;
$fieldToolTips["ger_grupo"] = &$fieldToolTipsger_grupo;
$page_titles["ger_grupo"] = &$pageTitlesger_grupo;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_grupo"] = array();
//	fin_despesas
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="fin_despesas";
		$detailsParam["dOriginalTable"] = "fin_despesas";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "fin_despesas";
	$detailsParam["dCaptionTable"] = GetTableCaption("fin_despesas");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_grupo"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"][]="idGrupo";

				$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"][]="link_ger_grupo";
//	ger_unidades
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="ger_unidades";
		$detailsParam["dOriginalTable"] = "ger_unidades";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "ger_unidades";
	$detailsParam["dCaptionTable"] = GetTableCaption("ger_unidades");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_grupo"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"][]="idGrupo";

				$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"][]="grupo";
//	fin_bco_ext1
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="fin_bco_ext1";
		$detailsParam["dOriginalTable"] = "fin_bco_ext1";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "fin_bco_ext1";
	$detailsParam["dCaptionTable"] = GetTableCaption("fin_bco_ext1");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "0";
*/
	$detailsParam["dispChildCount"] = "0";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_grupo"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"][]="idGrupo";

				$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"][]="grupo";
//	SaldosBlocos
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="SaldosBlocos";
		$detailsParam["dOriginalTable"] = "fin_bco_ext1";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "SaldosBlocos";
	$detailsParam["dCaptionTable"] = GetTableCaption("SaldosBlocos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "0";
*/
	$detailsParam["dispChildCount"] = "0";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_grupo"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"][]="idGrupo";

				$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"][]="grupo";
//	ger_arquivos_grupos
	
	

		$dIndex = 4;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="ger_arquivos_grupos";
		$detailsParam["dOriginalTable"] = "ger_arquivos_grupos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "ger_arquivos_grupos";
	$detailsParam["dCaptionTable"] = GetTableCaption("ger_arquivos_grupos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_grupo"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["masterKeys"][]="idGrupo";

				$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_grupo"][$dIndex]["detailKeys"][]="link_ger_grupos";

// tables which are master tables for current table (detail)
$masterTablesData["ger_grupo"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_grupo()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idGrupo,  	nome,  	obs";
$proto0["m_strFrom"] = "FROM ger_grupo";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idGrupo",
	"m_strTable" => "ger_grupo",
	"m_srcTableName" => "ger_grupo"
));

$proto6["m_sql"] = "idGrupo";
$proto6["m_srcTableName"] = "ger_grupo";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_grupo",
	"m_srcTableName" => "ger_grupo"
));

$proto8["m_sql"] = "nome";
$proto8["m_srcTableName"] = "ger_grupo";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_grupo",
	"m_srcTableName" => "ger_grupo"
));

$proto10["m_sql"] = "obs";
$proto10["m_srcTableName"] = "ger_grupo";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "ger_grupo";
$proto13["m_srcTableName"] = "ger_grupo";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "idGrupo";
$proto13["m_columns"][] = "nome";
$proto13["m_columns"][] = "obs";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "ger_grupo";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "ger_grupo";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_grupo";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_grupo = createSqlQuery_ger_grupo();


	
		;

			

$tdatager_grupo[".sqlquery"] = $queryData_ger_grupo;

$tableEvents["ger_grupo"] = new eventsBase;
$tdatager_grupo[".hasEvents"] = false;

?>