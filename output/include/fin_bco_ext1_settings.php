<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_bco_ext1 = array();
	$tdatafin_bco_ext1[".truncateText"] = true;
	$tdatafin_bco_ext1[".NumberOfChars"] = 80;
	$tdatafin_bco_ext1[".ShortName"] = "fin_bco_ext1";
	$tdatafin_bco_ext1[".OwnerID"] = "grupo";
	$tdatafin_bco_ext1[".OriginalTable"] = "fin_bco_ext1";

//	field labels
$fieldLabelsfin_bco_ext1 = array();
$fieldToolTipsfin_bco_ext1 = array();
$pageTitlesfin_bco_ext1 = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_bco_ext1["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_bco_ext1["Portuguese(Brazil)"] = array();
	$pageTitlesfin_bco_ext1["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_bco_ext1["Portuguese(Brazil)"]["dtpagamento"] = "Data";
	$fieldToolTipsfin_bco_ext1["Portuguese(Brazil)"]["dtpagamento"] = "";
	$fieldLabelsfin_bco_ext1["Portuguese(Brazil)"]["operacao"] = "Operação";
	$fieldToolTipsfin_bco_ext1["Portuguese(Brazil)"]["operacao"] = "";
	$fieldLabelsfin_bco_ext1["Portuguese(Brazil)"]["numdoc"] = "Número";
	$fieldToolTipsfin_bco_ext1["Portuguese(Brazil)"]["numdoc"] = "";
	$fieldLabelsfin_bco_ext1["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsfin_bco_ext1["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsfin_bco_ext1["Portuguese(Brazil)"]["grupo"] = "Grupo";
	$fieldToolTipsfin_bco_ext1["Portuguese(Brazil)"]["grupo"] = "";
	$fieldLabelsfin_bco_ext1["Portuguese(Brazil)"]["vlrcob"] = "Valor";
	$fieldToolTipsfin_bco_ext1["Portuguese(Brazil)"]["vlrcob"] = "";
	if (count($fieldToolTipsfin_bco_ext1["Portuguese(Brazil)"]))
		$tdatafin_bco_ext1[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_bco_ext1[""] = array();
	$fieldToolTipsfin_bco_ext1[""] = array();
	$pageTitlesfin_bco_ext1[""] = array();
	if (count($fieldToolTipsfin_bco_ext1[""]))
		$tdatafin_bco_ext1[".isUseToolTips"] = true;
}


	$tdatafin_bco_ext1[".NCSearch"] = true;



$tdatafin_bco_ext1[".shortTableName"] = "fin_bco_ext1";
$tdatafin_bco_ext1[".nSecOptions"] = 1;
$tdatafin_bco_ext1[".recsPerRowList"] = 1;
$tdatafin_bco_ext1[".recsPerRowPrint"] = 1;
$tdatafin_bco_ext1[".mainTableOwnerID"] = "grupo";
$tdatafin_bco_ext1[".moveNext"] = 1;
$tdatafin_bco_ext1[".entityType"] = 0;

$tdatafin_bco_ext1[".strOriginalTableName"] = "fin_bco_ext1";





$tdatafin_bco_ext1[".showAddInPopup"] = false;

$tdatafin_bco_ext1[".showEditInPopup"] = false;

$tdatafin_bco_ext1[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_bco_ext1[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_bco_ext1[".fieldsForRegister"] = array();

$tdatafin_bco_ext1[".listAjax"] = false;

	$tdatafin_bco_ext1[".audit"] = true;

	$tdatafin_bco_ext1[".locking"] = true;



$tdatafin_bco_ext1[".list"] = true;


$tdatafin_bco_ext1[".import"] = true;

$tdatafin_bco_ext1[".exportTo"] = true;

$tdatafin_bco_ext1[".printFriendly"] = true;


$tdatafin_bco_ext1[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_bco_ext1[".searchSaving"] = false;
//

$tdatafin_bco_ext1[".showSearchPanel"] = true;
		$tdatafin_bco_ext1[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_bco_ext1[".isUseAjaxSuggest"] = false;
else
	$tdatafin_bco_ext1[".isUseAjaxSuggest"] = true;

$tdatafin_bco_ext1[".rowHighlite"] = true;



$tdatafin_bco_ext1[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_bco_ext1[".isUseTimeForSearch"] = false;



$tdatafin_bco_ext1[".badgeColor"] = "4169E1";


$tdatafin_bco_ext1[".allSearchFields"] = array();
$tdatafin_bco_ext1[".filterFields"] = array();
$tdatafin_bco_ext1[".requiredSearchFields"] = array();

$tdatafin_bco_ext1[".allSearchFields"][] = "dtpagamento";
	$tdatafin_bco_ext1[".allSearchFields"][] = "grupo";
	$tdatafin_bco_ext1[".allSearchFields"][] = "operacao";
	$tdatafin_bco_ext1[".allSearchFields"][] = "obs";
	$tdatafin_bco_ext1[".allSearchFields"][] = "numdoc";
	$tdatafin_bco_ext1[".allSearchFields"][] = "vlrcob";
	

$tdatafin_bco_ext1[".googleLikeFields"] = array();
$tdatafin_bco_ext1[".googleLikeFields"][] = "operacao";
$tdatafin_bco_ext1[".googleLikeFields"][] = "numdoc";
$tdatafin_bco_ext1[".googleLikeFields"][] = "vlrcob";
$tdatafin_bco_ext1[".googleLikeFields"][] = "dtpagamento";
$tdatafin_bco_ext1[".googleLikeFields"][] = "obs";
$tdatafin_bco_ext1[".googleLikeFields"][] = "grupo";


$tdatafin_bco_ext1[".advSearchFields"] = array();
$tdatafin_bco_ext1[".advSearchFields"][] = "dtpagamento";
$tdatafin_bco_ext1[".advSearchFields"][] = "grupo";
$tdatafin_bco_ext1[".advSearchFields"][] = "operacao";
$tdatafin_bco_ext1[".advSearchFields"][] = "obs";
$tdatafin_bco_ext1[".advSearchFields"][] = "numdoc";
$tdatafin_bco_ext1[".advSearchFields"][] = "vlrcob";

$tdatafin_bco_ext1[".tableType"] = "list";

$tdatafin_bco_ext1[".printerPageOrientation"] = 0;
$tdatafin_bco_ext1[".nPrinterPageScale"] = 100;

$tdatafin_bco_ext1[".nPrinterSplitRecords"] = 40;

$tdatafin_bco_ext1[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_bco_ext1[".geocodingEnabled"] = false;





$tdatafin_bco_ext1[".listGridLayout"] = 3;

$tdatafin_bco_ext1[".isDisplayLoading"] = true;


$tdatafin_bco_ext1[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf

$tdatafin_bco_ext1[".totalsFields"] = array();
$tdatafin_bco_ext1[".totalsFields"][] = array(
	"fName" => "dtpagamento",
	"numRows" => 0,
	"totalsType" => "COUNT",
	"viewFormat" => 'Short Date');
$tdatafin_bco_ext1[".totalsFields"][] = array(
	"fName" => "vlrcob",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Custom');

$tdatafin_bco_ext1[".pageSize"] = 20;

$tdatafin_bco_ext1[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY dtpagamento";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_bco_ext1[".strOrderBy"] = $tstrOrderBy;

$tdatafin_bco_ext1[".orderindexes"] = array();
$tdatafin_bco_ext1[".orderindexes"][] = array(4, (1 ? "ASC" : "DESC"), "dtpagamento");

$tdatafin_bco_ext1[".sqlHead"] = "SELECT operacao,  numdoc,  vlrcob,  dtpagamento,  obs,  grupo";
$tdatafin_bco_ext1[".sqlFrom"] = "FROM fin_bco_ext1";
$tdatafin_bco_ext1[".sqlWhereExpr"] = "dtpagamento <> 0";
$tdatafin_bco_ext1[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_bco_ext1[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_bco_ext1[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_bco_ext1[".highlightSearchResults"] = true;

$tableKeysfin_bco_ext1 = array();
$tdatafin_bco_ext1[".Keys"] = $tableKeysfin_bco_ext1;

$tdatafin_bco_ext1[".listFields"] = array();
$tdatafin_bco_ext1[".listFields"][] = "dtpagamento";
$tdatafin_bco_ext1[".listFields"][] = "grupo";
$tdatafin_bco_ext1[".listFields"][] = "operacao";
$tdatafin_bco_ext1[".listFields"][] = "obs";
$tdatafin_bco_ext1[".listFields"][] = "numdoc";
$tdatafin_bco_ext1[".listFields"][] = "vlrcob";

$tdatafin_bco_ext1[".hideMobileList"] = array();


$tdatafin_bco_ext1[".viewFields"] = array();

$tdatafin_bco_ext1[".addFields"] = array();

$tdatafin_bco_ext1[".masterListFields"] = array();

$tdatafin_bco_ext1[".inlineAddFields"] = array();

$tdatafin_bco_ext1[".editFields"] = array();

$tdatafin_bco_ext1[".inlineEditFields"] = array();

$tdatafin_bco_ext1[".exportFields"] = array();
$tdatafin_bco_ext1[".exportFields"][] = "dtpagamento";
$tdatafin_bco_ext1[".exportFields"][] = "grupo";
$tdatafin_bco_ext1[".exportFields"][] = "operacao";
$tdatafin_bco_ext1[".exportFields"][] = "obs";
$tdatafin_bco_ext1[".exportFields"][] = "numdoc";
$tdatafin_bco_ext1[".exportFields"][] = "vlrcob";

$tdatafin_bco_ext1[".importFields"] = array();
$tdatafin_bco_ext1[".importFields"][] = "operacao";
$tdatafin_bco_ext1[".importFields"][] = "numdoc";
$tdatafin_bco_ext1[".importFields"][] = "vlrcob";
$tdatafin_bco_ext1[".importFields"][] = "dtpagamento";
$tdatafin_bco_ext1[".importFields"][] = "obs";
$tdatafin_bco_ext1[".importFields"][] = "grupo";

$tdatafin_bco_ext1[".printFields"] = array();
$tdatafin_bco_ext1[".printFields"][] = "dtpagamento";
$tdatafin_bco_ext1[".printFields"][] = "grupo";
$tdatafin_bco_ext1[".printFields"][] = "operacao";
$tdatafin_bco_ext1[".printFields"][] = "obs";
$tdatafin_bco_ext1[".printFields"][] = "numdoc";
$tdatafin_bco_ext1[".printFields"][] = "vlrcob";

//	operacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "operacao";
	$fdata["GoodName"] = "operacao";
	$fdata["ownerTable"] = "fin_bco_ext1";
	$fdata["Label"] = GetFieldLabel("fin_bco_ext1","operacao");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "operacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "operacao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_bco_ext1["operacao"] = $fdata;
//	numdoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "numdoc";
	$fdata["GoodName"] = "numdoc";
	$fdata["ownerTable"] = "fin_bco_ext1";
	$fdata["Label"] = GetFieldLabel("fin_bco_ext1","numdoc");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numdoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "numdoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_bco_ext1["numdoc"] = $fdata;
//	vlrcob
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "vlrcob";
	$fdata["GoodName"] = "vlrcob";
	$fdata["ownerTable"] = "fin_bco_ext1";
	$fdata["Label"] = GetFieldLabel("fin_bco_ext1","vlrcob");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vlrcob";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vlrcob";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_bco_ext1["vlrcob"] = $fdata;
//	dtpagamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "dtpagamento";
	$fdata["GoodName"] = "dtpagamento";
	$fdata["ownerTable"] = "fin_bco_ext1";
	$fdata["Label"] = GetFieldLabel("fin_bco_ext1","dtpagamento");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "dtpagamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dtpagamento";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_bco_ext1["dtpagamento"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "fin_bco_ext1";
	$fdata["Label"] = GetFieldLabel("fin_bco_ext1","obs");
	$fdata["FieldType"] = 201;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text area");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
				$edata["nRows"] = 250;
			$edata["nCols"] = 500;

	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_bco_ext1["obs"] = $fdata;
//	grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "grupo";
	$fdata["GoodName"] = "grupo";
	$fdata["ownerTable"] = "fin_bco_ext1";
	$fdata["Label"] = GetFieldLabel("fin_bco_ext1","grupo");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "grupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_bco_ext1["grupo"] = $fdata;


$tables_data["fin_bco_ext1"]=&$tdatafin_bco_ext1;
$field_labels["fin_bco_ext1"] = &$fieldLabelsfin_bco_ext1;
$fieldToolTips["fin_bco_ext1"] = &$fieldToolTipsfin_bco_ext1;
$page_titles["fin_bco_ext1"] = &$pageTitlesfin_bco_ext1;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_bco_ext1"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_bco_ext1"] = array();


	
				$strOriginalDetailsTable="fin_bco_ext1";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="SaldosBlocos";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "SaldosBlocos";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["fin_bco_ext1"][0] = $masterParams;
				$masterTablesData["fin_bco_ext1"][0]["masterKeys"] = array();
	$masterTablesData["fin_bco_ext1"][0]["masterKeys"][]="grupo";
				$masterTablesData["fin_bco_ext1"][0]["detailKeys"] = array();
	$masterTablesData["fin_bco_ext1"][0]["detailKeys"][]="grupo";
		
	
				$strOriginalDetailsTable="ger_grupo";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_grupo";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_grupo";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["fin_bco_ext1"][1] = $masterParams;
				$masterTablesData["fin_bco_ext1"][1]["masterKeys"] = array();
	$masterTablesData["fin_bco_ext1"][1]["masterKeys"][]="idGrupo";
				$masterTablesData["fin_bco_ext1"][1]["detailKeys"] = array();
	$masterTablesData["fin_bco_ext1"][1]["detailKeys"][]="grupo";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_bco_ext1()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "operacao,  numdoc,  vlrcob,  dtpagamento,  obs,  grupo";
$proto0["m_strFrom"] = "FROM fin_bco_ext1";
$proto0["m_strWhere"] = "dtpagamento <> 0";
$proto0["m_strOrderBy"] = "ORDER BY dtpagamento";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "dtpagamento <> 0";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "dtpagamento",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "fin_bco_ext1"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "<> 0";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "operacao",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "fin_bco_ext1"
));

$proto6["m_sql"] = "operacao";
$proto6["m_srcTableName"] = "fin_bco_ext1";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "numdoc",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "fin_bco_ext1"
));

$proto8["m_sql"] = "numdoc";
$proto8["m_srcTableName"] = "fin_bco_ext1";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "vlrcob",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "fin_bco_ext1"
));

$proto10["m_sql"] = "vlrcob";
$proto10["m_srcTableName"] = "fin_bco_ext1";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "dtpagamento",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "fin_bco_ext1"
));

$proto12["m_sql"] = "dtpagamento";
$proto12["m_srcTableName"] = "fin_bco_ext1";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "fin_bco_ext1"
));

$proto14["m_sql"] = "obs";
$proto14["m_srcTableName"] = "fin_bco_ext1";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "grupo",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "fin_bco_ext1"
));

$proto16["m_sql"] = "grupo";
$proto16["m_srcTableName"] = "fin_bco_ext1";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "fin_bco_ext1";
$proto19["m_srcTableName"] = "fin_bco_ext1";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "operacao";
$proto19["m_columns"][] = "numdoc";
$proto19["m_columns"][] = "vlrcob";
$proto19["m_columns"][] = "dtpagamento";
$proto19["m_columns"][] = "obs";
$proto19["m_columns"][] = "grupo";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "fin_bco_ext1";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "fin_bco_ext1";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto22=array();
						$obj = new SQLField(array(
	"m_strName" => "dtpagamento",
	"m_strTable" => "fin_bco_ext1",
	"m_srcTableName" => "fin_bco_ext1"
));

$proto22["m_column"]=$obj;
$proto22["m_bAsc"] = 1;
$proto22["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto22);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="fin_bco_ext1";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_bco_ext1 = createSqlQuery_fin_bco_ext1();


	
		;

						

$tdatafin_bco_ext1[".sqlquery"] = $queryData_fin_bco_ext1;

$tableEvents["fin_bco_ext1"] = new eventsBase;
$tdatafin_bco_ext1[".hasEvents"] = false;

?>