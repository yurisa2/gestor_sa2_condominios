<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_debitos_contador = array();
	$tdatafin_debitos_contador[".truncateText"] = true;
	$tdatafin_debitos_contador[".NumberOfChars"] = 80;
	$tdatafin_debitos_contador[".ShortName"] = "fin_debitos_contador";
	$tdatafin_debitos_contador[".OwnerID"] = "";
	$tdatafin_debitos_contador[".OriginalTable"] = "fin_debitos_contador";

//	field labels
$fieldLabelsfin_debitos_contador = array();
$fieldToolTipsfin_debitos_contador = array();
$pageTitlesfin_debitos_contador = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_debitos_contador["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_debitos_contador["Portuguese(Brazil)"] = array();
	$pageTitlesfin_debitos_contador["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_debitos_contador["Portuguese(Brazil)"]["link_ger_unidade"] = "Unidade";
	$fieldToolTipsfin_debitos_contador["Portuguese(Brazil)"]["link_ger_unidade"] = "";
	$fieldLabelsfin_debitos_contador["Portuguese(Brazil)"]["contador1"] = "Débitos";
	$fieldToolTipsfin_debitos_contador["Portuguese(Brazil)"]["contador1"] = "";
	$fieldLabelsfin_debitos_contador["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsfin_debitos_contador["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsfin_debitos_contador["Portuguese(Brazil)"]["assinatura"] = "Assinatura";
	$fieldToolTipsfin_debitos_contador["Portuguese(Brazil)"]["assinatura"] = "";
	if (count($fieldToolTipsfin_debitos_contador["Portuguese(Brazil)"]))
		$tdatafin_debitos_contador[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_debitos_contador[""] = array();
	$fieldToolTipsfin_debitos_contador[""] = array();
	$pageTitlesfin_debitos_contador[""] = array();
	if (count($fieldToolTipsfin_debitos_contador[""]))
		$tdatafin_debitos_contador[".isUseToolTips"] = true;
}


	$tdatafin_debitos_contador[".NCSearch"] = true;



$tdatafin_debitos_contador[".shortTableName"] = "fin_debitos_contador";
$tdatafin_debitos_contador[".nSecOptions"] = 0;
$tdatafin_debitos_contador[".recsPerRowList"] = 1;
$tdatafin_debitos_contador[".recsPerRowPrint"] = 1;
$tdatafin_debitos_contador[".mainTableOwnerID"] = "";
$tdatafin_debitos_contador[".moveNext"] = 1;
$tdatafin_debitos_contador[".entityType"] = 0;

$tdatafin_debitos_contador[".strOriginalTableName"] = "fin_debitos_contador";





$tdatafin_debitos_contador[".showAddInPopup"] = false;

$tdatafin_debitos_contador[".showEditInPopup"] = false;

$tdatafin_debitos_contador[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_debitos_contador[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_debitos_contador[".fieldsForRegister"] = array();

$tdatafin_debitos_contador[".listAjax"] = false;

	$tdatafin_debitos_contador[".audit"] = true;

	$tdatafin_debitos_contador[".locking"] = true;



$tdatafin_debitos_contador[".list"] = true;



$tdatafin_debitos_contador[".exportTo"] = true;

$tdatafin_debitos_contador[".printFriendly"] = true;


$tdatafin_debitos_contador[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_debitos_contador[".searchSaving"] = false;
//

$tdatafin_debitos_contador[".showSearchPanel"] = true;
		$tdatafin_debitos_contador[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_debitos_contador[".isUseAjaxSuggest"] = false;
else
	$tdatafin_debitos_contador[".isUseAjaxSuggest"] = true;

$tdatafin_debitos_contador[".rowHighlite"] = true;



$tdatafin_debitos_contador[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_debitos_contador[".isUseTimeForSearch"] = false;





$tdatafin_debitos_contador[".allSearchFields"] = array();
$tdatafin_debitos_contador[".filterFields"] = array();
$tdatafin_debitos_contador[".requiredSearchFields"] = array();

$tdatafin_debitos_contador[".allSearchFields"][] = "link_ger_unidade";
	$tdatafin_debitos_contador[".allSearchFields"][] = "nome";
	$tdatafin_debitos_contador[".allSearchFields"][] = "contador1";
	

$tdatafin_debitos_contador[".googleLikeFields"] = array();
$tdatafin_debitos_contador[".googleLikeFields"][] = "link_ger_unidade";
$tdatafin_debitos_contador[".googleLikeFields"][] = "contador1";
$tdatafin_debitos_contador[".googleLikeFields"][] = "nome";


$tdatafin_debitos_contador[".advSearchFields"] = array();
$tdatafin_debitos_contador[".advSearchFields"][] = "link_ger_unidade";
$tdatafin_debitos_contador[".advSearchFields"][] = "nome";
$tdatafin_debitos_contador[".advSearchFields"][] = "contador1";

$tdatafin_debitos_contador[".tableType"] = "list";

$tdatafin_debitos_contador[".printerPageOrientation"] = 0;
$tdatafin_debitos_contador[".nPrinterPageScale"] = 100;

$tdatafin_debitos_contador[".nPrinterSplitRecords"] = 40;

$tdatafin_debitos_contador[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_debitos_contador[".geocodingEnabled"] = false;





$tdatafin_debitos_contador[".listGridLayout"] = 3;

$tdatafin_debitos_contador[".isDisplayLoading"] = true;


$tdatafin_debitos_contador[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatafin_debitos_contador[".pageSize"] = 20;

$tdatafin_debitos_contador[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY ger_cadastro.nome";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_debitos_contador[".strOrderBy"] = $tstrOrderBy;

$tdatafin_debitos_contador[".orderindexes"] = array();
$tdatafin_debitos_contador[".orderindexes"][] = array(3, (1 ? "ASC" : "DESC"), "ger_cadastro.nome");

$tdatafin_debitos_contador[".sqlHead"] = "SELECT fin_debitos_contador.link_ger_unidade,  fin_debitos_contador.contador1,  ger_cadastro.nome,  \"Ass.:\" AS assinatura";
$tdatafin_debitos_contador[".sqlFrom"] = "FROM fin_debitos_contador  INNER JOIN ger_unidades ON fin_debitos_contador.link_ger_unidade = ger_unidades.idUnidade  INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$tdatafin_debitos_contador[".sqlWhereExpr"] = "";
$tdatafin_debitos_contador[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_debitos_contador[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_debitos_contador[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_debitos_contador[".highlightSearchResults"] = true;

$tableKeysfin_debitos_contador = array();
$tdatafin_debitos_contador[".Keys"] = $tableKeysfin_debitos_contador;

$tdatafin_debitos_contador[".listFields"] = array();
$tdatafin_debitos_contador[".listFields"][] = "link_ger_unidade";
$tdatafin_debitos_contador[".listFields"][] = "nome";
$tdatafin_debitos_contador[".listFields"][] = "contador1";
$tdatafin_debitos_contador[".listFields"][] = "assinatura";

$tdatafin_debitos_contador[".hideMobileList"] = array();


$tdatafin_debitos_contador[".viewFields"] = array();

$tdatafin_debitos_contador[".addFields"] = array();

$tdatafin_debitos_contador[".masterListFields"] = array();

$tdatafin_debitos_contador[".inlineAddFields"] = array();

$tdatafin_debitos_contador[".editFields"] = array();

$tdatafin_debitos_contador[".inlineEditFields"] = array();

$tdatafin_debitos_contador[".exportFields"] = array();
$tdatafin_debitos_contador[".exportFields"][] = "link_ger_unidade";
$tdatafin_debitos_contador[".exportFields"][] = "nome";
$tdatafin_debitos_contador[".exportFields"][] = "contador1";
$tdatafin_debitos_contador[".exportFields"][] = "assinatura";

$tdatafin_debitos_contador[".importFields"] = array();

$tdatafin_debitos_contador[".printFields"] = array();
$tdatafin_debitos_contador[".printFields"][] = "link_ger_unidade";
$tdatafin_debitos_contador[".printFields"][] = "nome";
$tdatafin_debitos_contador[".printFields"][] = "contador1";
$tdatafin_debitos_contador[".printFields"][] = "assinatura";

//	link_ger_unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "link_ger_unidade";
	$fdata["GoodName"] = "link_ger_unidade";
	$fdata["ownerTable"] = "fin_debitos_contador";
	$fdata["Label"] = GetFieldLabel("fin_debitos_contador","link_ger_unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_contador.link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_guni_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_debitos_contador["link_ger_unidade"] = $fdata;
//	contador1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "contador1";
	$fdata["GoodName"] = "contador1";
	$fdata["ownerTable"] = "fin_debitos_contador";
	$fdata["Label"] = GetFieldLabel("fin_debitos_contador","contador1");
	$fdata["FieldType"] = 20;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "contador1";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_contador.contador1";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_contador["contador1"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("fin_debitos_contador","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ger_cadastro.nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_contador["nome"] = $fdata;
//	assinatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "assinatura";
	$fdata["GoodName"] = "assinatura";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("fin_debitos_contador","assinatura");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Ass.:\"";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_debitos_contador["assinatura"] = $fdata;


$tables_data["fin_debitos_contador"]=&$tdatafin_debitos_contador;
$field_labels["fin_debitos_contador"] = &$fieldLabelsfin_debitos_contador;
$fieldToolTips["fin_debitos_contador"] = &$fieldToolTipsfin_debitos_contador;
$page_titles["fin_debitos_contador"] = &$pageTitlesfin_debitos_contador;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_debitos_contador"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_debitos_contador"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_debitos_contador()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "fin_debitos_contador.link_ger_unidade,  fin_debitos_contador.contador1,  ger_cadastro.nome,  \"Ass.:\" AS assinatura";
$proto0["m_strFrom"] = "FROM fin_debitos_contador  INNER JOIN ger_unidades ON fin_debitos_contador.link_ger_unidade = ger_unidades.idUnidade  INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY ger_cadastro.nome";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_debitos_contador",
	"m_srcTableName" => "fin_debitos_contador"
));

$proto6["m_sql"] = "fin_debitos_contador.link_ger_unidade";
$proto6["m_srcTableName"] = "fin_debitos_contador";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "contador1",
	"m_strTable" => "fin_debitos_contador",
	"m_srcTableName" => "fin_debitos_contador"
));

$proto8["m_sql"] = "fin_debitos_contador.contador1";
$proto8["m_srcTableName"] = "fin_debitos_contador";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "fin_debitos_contador"
));

$proto10["m_sql"] = "ger_cadastro.nome";
$proto10["m_srcTableName"] = "fin_debitos_contador";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLNonParsed(array(
	"m_sql" => "\"Ass.:\""
));

$proto12["m_sql"] = "\"Ass.:\"";
$proto12["m_srcTableName"] = "fin_debitos_contador";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "assinatura";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "fin_debitos_contador";
$proto15["m_srcTableName"] = "fin_debitos_contador";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "link_ger_unidade";
$proto15["m_columns"][] = "contador1";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "fin_debitos_contador";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "fin_debitos_contador";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
												$proto18=array();
$proto18["m_link"] = "SQLL_INNERJOIN";
			$proto19=array();
$proto19["m_strName"] = "ger_unidades";
$proto19["m_srcTableName"] = "fin_debitos_contador";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "idUnidade";
$proto19["m_columns"][] = "grupo";
$proto19["m_columns"][] = "unidade";
$proto19["m_columns"][] = "link_ger_cadastro";
$proto19["m_columns"][] = "dia_pref_venc";
$proto19["m_columns"][] = "ativo";
$proto19["m_columns"][] = "obs";
$proto19["m_columns"][] = "frideal";
$proto19["m_columns"][] = "ultimousuario";
$proto19["m_columns"][] = "ultimaalteracao";
$proto19["m_columns"][] = "auth";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "INNER JOIN ger_unidades ON fin_debitos_contador.link_ger_unidade = ger_unidades.idUnidade";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "fin_debitos_contador";
$proto20=array();
$proto20["m_sql"] = "fin_debitos_contador.link_ger_unidade = ger_unidades.idUnidade";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_debitos_contador",
	"m_srcTableName" => "fin_debitos_contador"
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "= ger_unidades.idUnidade";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
												$proto22=array();
$proto22["m_link"] = "SQLL_INNERJOIN";
			$proto23=array();
$proto23["m_strName"] = "ger_cadastro";
$proto23["m_srcTableName"] = "fin_debitos_contador";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "idCadastro";
$proto23["m_columns"][] = "nome";
$proto23["m_columns"][] = "rg";
$proto23["m_columns"][] = "cpf";
$proto23["m_columns"][] = "TelPrim";
$proto23["m_columns"][] = "CompPrim";
$proto23["m_columns"][] = "TelSec";
$proto23["m_columns"][] = "CompSec";
$proto23["m_columns"][] = "TelTerc";
$proto23["m_columns"][] = "CompTerc";
$proto23["m_columns"][] = "log";
$proto23["m_columns"][] = "end";
$proto23["m_columns"][] = "num";
$proto23["m_columns"][] = "comp";
$proto23["m_columns"][] = "bairro";
$proto23["m_columns"][] = "cep";
$proto23["m_columns"][] = "cidade";
$proto23["m_columns"][] = "uf";
$proto23["m_columns"][] = "obs";
$proto23["m_columns"][] = "situacao";
$proto23["m_columns"][] = "morador";
$proto23["m_columns"][] = "cepnet";
$proto23["m_columns"][] = "ativo";
$proto23["m_columns"][] = "processado";
$proto23["m_columns"][] = "email";
$proto23["m_columns"][] = "webuser";
$proto23["m_columns"][] = "ultimousuario";
$proto23["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "fin_debitos_contador";
$proto24=array();
$proto24["m_sql"] = "ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "link_ger_cadastro",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "fin_debitos_contador"
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "= ger_cadastro.idCadastro";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto26=array();
						$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "fin_debitos_contador"
));

$proto26["m_column"]=$obj;
$proto26["m_bAsc"] = 1;
$proto26["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto26);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="fin_debitos_contador";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_debitos_contador = createSqlQuery_fin_debitos_contador();


	
		;

				

$tdatafin_debitos_contador[".sqlquery"] = $queryData_fin_debitos_contador;

include_once(getabspath("include/fin_debitos_contador_events.php"));
$tableEvents["fin_debitos_contador"] = new eventclass_fin_debitos_contador;
$tdatafin_debitos_contador[".hasEvents"] = true;

?>