<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_tipodocs = array();
	$tdatafin_tipodocs[".truncateText"] = true;
	$tdatafin_tipodocs[".NumberOfChars"] = 80;
	$tdatafin_tipodocs[".ShortName"] = "fin_tipodocs";
	$tdatafin_tipodocs[".OwnerID"] = "";
	$tdatafin_tipodocs[".OriginalTable"] = "fin_tipodocs";

//	field labels
$fieldLabelsfin_tipodocs = array();
$fieldToolTipsfin_tipodocs = array();
$pageTitlesfin_tipodocs = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_tipodocs["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_tipodocs["Portuguese(Brazil)"] = array();
	$pageTitlesfin_tipodocs["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_tipodocs["Portuguese(Brazil)"]["idTipodocs"] = "Código";
	$fieldToolTipsfin_tipodocs["Portuguese(Brazil)"]["idTipodocs"] = "";
	$fieldLabelsfin_tipodocs["Portuguese(Brazil)"]["tipodoc"] = "tipodoc";
	$fieldToolTipsfin_tipodocs["Portuguese(Brazil)"]["tipodoc"] = "";
	$fieldLabelsfin_tipodocs["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsfin_tipodocs["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsfin_tipodocs["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsfin_tipodocs["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_tipodocs["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsfin_tipodocs["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsfin_tipodocs["Portuguese(Brazil)"]))
		$tdatafin_tipodocs[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_tipodocs[""] = array();
	$fieldToolTipsfin_tipodocs[""] = array();
	$pageTitlesfin_tipodocs[""] = array();
	if (count($fieldToolTipsfin_tipodocs[""]))
		$tdatafin_tipodocs[".isUseToolTips"] = true;
}


	$tdatafin_tipodocs[".NCSearch"] = true;



$tdatafin_tipodocs[".shortTableName"] = "fin_tipodocs";
$tdatafin_tipodocs[".nSecOptions"] = 0;
$tdatafin_tipodocs[".recsPerRowList"] = 1;
$tdatafin_tipodocs[".recsPerRowPrint"] = 1;
$tdatafin_tipodocs[".mainTableOwnerID"] = "";
$tdatafin_tipodocs[".moveNext"] = 1;
$tdatafin_tipodocs[".entityType"] = 0;

$tdatafin_tipodocs[".strOriginalTableName"] = "fin_tipodocs";





$tdatafin_tipodocs[".showAddInPopup"] = false;

$tdatafin_tipodocs[".showEditInPopup"] = false;

$tdatafin_tipodocs[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_tipodocs[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_tipodocs[".fieldsForRegister"] = array();

$tdatafin_tipodocs[".listAjax"] = false;

	$tdatafin_tipodocs[".audit"] = true;

	$tdatafin_tipodocs[".locking"] = true;

$tdatafin_tipodocs[".edit"] = true;
$tdatafin_tipodocs[".afterEditAction"] = 1;
$tdatafin_tipodocs[".closePopupAfterEdit"] = 1;
$tdatafin_tipodocs[".afterEditActionDetTable"] = "";

$tdatafin_tipodocs[".add"] = true;
$tdatafin_tipodocs[".afterAddAction"] = 1;
$tdatafin_tipodocs[".closePopupAfterAdd"] = 1;
$tdatafin_tipodocs[".afterAddActionDetTable"] = "";

$tdatafin_tipodocs[".list"] = true;

$tdatafin_tipodocs[".view"] = true;


$tdatafin_tipodocs[".exportTo"] = true;

$tdatafin_tipodocs[".printFriendly"] = true;


$tdatafin_tipodocs[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_tipodocs[".searchSaving"] = false;
//

$tdatafin_tipodocs[".showSearchPanel"] = true;
		$tdatafin_tipodocs[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_tipodocs[".isUseAjaxSuggest"] = false;
else
	$tdatafin_tipodocs[".isUseAjaxSuggest"] = true;

$tdatafin_tipodocs[".rowHighlite"] = true;



$tdatafin_tipodocs[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_tipodocs[".isUseTimeForSearch"] = false;





$tdatafin_tipodocs[".allSearchFields"] = array();
$tdatafin_tipodocs[".filterFields"] = array();
$tdatafin_tipodocs[".requiredSearchFields"] = array();

$tdatafin_tipodocs[".allSearchFields"][] = "tipodoc";
	$tdatafin_tipodocs[".allSearchFields"][] = "obs";
	$tdatafin_tipodocs[".allSearchFields"][] = "ultimousuario";
	$tdatafin_tipodocs[".allSearchFields"][] = "ultimaalteracao";
	

$tdatafin_tipodocs[".googleLikeFields"] = array();
$tdatafin_tipodocs[".googleLikeFields"][] = "tipodoc";
$tdatafin_tipodocs[".googleLikeFields"][] = "obs";
$tdatafin_tipodocs[".googleLikeFields"][] = "ultimousuario";
$tdatafin_tipodocs[".googleLikeFields"][] = "ultimaalteracao";


$tdatafin_tipodocs[".advSearchFields"] = array();
$tdatafin_tipodocs[".advSearchFields"][] = "tipodoc";
$tdatafin_tipodocs[".advSearchFields"][] = "obs";
$tdatafin_tipodocs[".advSearchFields"][] = "ultimousuario";
$tdatafin_tipodocs[".advSearchFields"][] = "ultimaalteracao";

$tdatafin_tipodocs[".tableType"] = "list";

$tdatafin_tipodocs[".printerPageOrientation"] = 0;
$tdatafin_tipodocs[".nPrinterPageScale"] = 100;

$tdatafin_tipodocs[".nPrinterSplitRecords"] = 40;

$tdatafin_tipodocs[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_tipodocs[".geocodingEnabled"] = false;





$tdatafin_tipodocs[".listGridLayout"] = 3;

$tdatafin_tipodocs[".isDisplayLoading"] = true;


$tdatafin_tipodocs[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatafin_tipodocs[".pageSize"] = 20;

$tdatafin_tipodocs[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_tipodocs[".strOrderBy"] = $tstrOrderBy;

$tdatafin_tipodocs[".orderindexes"] = array();

$tdatafin_tipodocs[".sqlHead"] = "select idTipodocs,  tipodoc,  obs,  ultimousuario,  ultimaalteracao";
$tdatafin_tipodocs[".sqlFrom"] = "FROM fin_tipodocs";
$tdatafin_tipodocs[".sqlWhereExpr"] = "";
$tdatafin_tipodocs[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_tipodocs[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_tipodocs[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_tipodocs[".highlightSearchResults"] = true;

$tableKeysfin_tipodocs = array();
$tableKeysfin_tipodocs[] = "idTipodocs";
$tdatafin_tipodocs[".Keys"] = $tableKeysfin_tipodocs;

$tdatafin_tipodocs[".listFields"] = array();
$tdatafin_tipodocs[".listFields"][] = "tipodoc";
$tdatafin_tipodocs[".listFields"][] = "obs";

$tdatafin_tipodocs[".hideMobileList"] = array();


$tdatafin_tipodocs[".viewFields"] = array();
$tdatafin_tipodocs[".viewFields"][] = "tipodoc";
$tdatafin_tipodocs[".viewFields"][] = "obs";
$tdatafin_tipodocs[".viewFields"][] = "ultimousuario";
$tdatafin_tipodocs[".viewFields"][] = "ultimaalteracao";

$tdatafin_tipodocs[".addFields"] = array();
$tdatafin_tipodocs[".addFields"][] = "tipodoc";
$tdatafin_tipodocs[".addFields"][] = "obs";

$tdatafin_tipodocs[".masterListFields"] = array();

$tdatafin_tipodocs[".inlineAddFields"] = array();

$tdatafin_tipodocs[".editFields"] = array();
$tdatafin_tipodocs[".editFields"][] = "tipodoc";
$tdatafin_tipodocs[".editFields"][] = "obs";

$tdatafin_tipodocs[".inlineEditFields"] = array();

$tdatafin_tipodocs[".exportFields"] = array();
$tdatafin_tipodocs[".exportFields"][] = "tipodoc";
$tdatafin_tipodocs[".exportFields"][] = "obs";
$tdatafin_tipodocs[".exportFields"][] = "ultimousuario";
$tdatafin_tipodocs[".exportFields"][] = "ultimaalteracao";

$tdatafin_tipodocs[".importFields"] = array();
$tdatafin_tipodocs[".importFields"][] = "idTipodocs";
$tdatafin_tipodocs[".importFields"][] = "tipodoc";
$tdatafin_tipodocs[".importFields"][] = "obs";
$tdatafin_tipodocs[".importFields"][] = "ultimousuario";
$tdatafin_tipodocs[".importFields"][] = "ultimaalteracao";

$tdatafin_tipodocs[".printFields"] = array();
$tdatafin_tipodocs[".printFields"][] = "tipodoc";
$tdatafin_tipodocs[".printFields"][] = "obs";

//	idTipodocs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idTipodocs";
	$fdata["GoodName"] = "idTipodocs";
	$fdata["ownerTable"] = "fin_tipodocs";
	$fdata["Label"] = GetFieldLabel("fin_tipodocs","idTipodocs");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idTipodocs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idTipodocs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_tipodocs["idTipodocs"] = $fdata;
//	tipodoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "tipodoc";
	$fdata["GoodName"] = "tipodoc";
	$fdata["ownerTable"] = "fin_tipodocs";
	$fdata["Label"] = GetFieldLabel("fin_tipodocs","tipodoc");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipodoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipodoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_tipodocs["tipodoc"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "fin_tipodocs";
	$fdata["Label"] = GetFieldLabel("fin_tipodocs","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_tipodocs["obs"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_tipodocs";
	$fdata["Label"] = GetFieldLabel("fin_tipodocs","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_tipodocs["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_tipodocs";
	$fdata["Label"] = GetFieldLabel("fin_tipodocs","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_tipodocs["ultimaalteracao"] = $fdata;


$tables_data["fin_tipodocs"]=&$tdatafin_tipodocs;
$field_labels["fin_tipodocs"] = &$fieldLabelsfin_tipodocs;
$fieldToolTips["fin_tipodocs"] = &$fieldToolTipsfin_tipodocs;
$page_titles["fin_tipodocs"] = &$pageTitlesfin_tipodocs;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_tipodocs"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_tipodocs"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_tipodocs()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idTipodocs,  tipodoc,  obs,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM fin_tipodocs";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idTipodocs",
	"m_strTable" => "fin_tipodocs",
	"m_srcTableName" => "fin_tipodocs"
));

$proto6["m_sql"] = "idTipodocs";
$proto6["m_srcTableName"] = "fin_tipodocs";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "tipodoc",
	"m_strTable" => "fin_tipodocs",
	"m_srcTableName" => "fin_tipodocs"
));

$proto8["m_sql"] = "tipodoc";
$proto8["m_srcTableName"] = "fin_tipodocs";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "fin_tipodocs",
	"m_srcTableName" => "fin_tipodocs"
));

$proto10["m_sql"] = "obs";
$proto10["m_srcTableName"] = "fin_tipodocs";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_tipodocs",
	"m_srcTableName" => "fin_tipodocs"
));

$proto12["m_sql"] = "ultimousuario";
$proto12["m_srcTableName"] = "fin_tipodocs";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_tipodocs",
	"m_srcTableName" => "fin_tipodocs"
));

$proto14["m_sql"] = "ultimaalteracao";
$proto14["m_srcTableName"] = "fin_tipodocs";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "fin_tipodocs";
$proto17["m_srcTableName"] = "fin_tipodocs";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "idTipodocs";
$proto17["m_columns"][] = "tipodoc";
$proto17["m_columns"][] = "obs";
$proto17["m_columns"][] = "ultimousuario";
$proto17["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "fin_tipodocs";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "fin_tipodocs";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="fin_tipodocs";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_tipodocs = createSqlQuery_fin_tipodocs();


	
		;

					

$tdatafin_tipodocs[".sqlquery"] = $queryData_fin_tipodocs;

include_once(getabspath("include/fin_tipodocs_events.php"));
$tableEvents["fin_tipodocs"] = new eventclass_fin_tipodocs;
$tdatafin_tipodocs[".hasEvents"] = true;

?>