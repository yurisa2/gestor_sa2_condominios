<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_debitos_resumo = array();
	$tdatafin_debitos_resumo[".truncateText"] = true;
	$tdatafin_debitos_resumo[".NumberOfChars"] = 80;
	$tdatafin_debitos_resumo[".ShortName"] = "fin_debitos_resumo";
	$tdatafin_debitos_resumo[".OwnerID"] = "";
	$tdatafin_debitos_resumo[".OriginalTable"] = "fin_debitos_resumo";

//	field labels
$fieldLabelsfin_debitos_resumo = array();
$fieldToolTipsfin_debitos_resumo = array();
$pageTitlesfin_debitos_resumo = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"] = array();
	$pageTitlesfin_debitos_resumo["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["link_ger_unidade"] = "Unidade";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["link_ger_unidade"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["contador2"] = "Débitos em aberto";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["contador2"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["debitooriginal"] = "Valor inicial";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["debitooriginal"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["multatotal"] = "Multa";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["multatotal"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["jurosttl"] = "Juros";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["jurosttl"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["corrmon"] = "Correção monetária";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["corrmon"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["debitototal"] = "Total";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["debitototal"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["processado"] = "Processado";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["processado"] = "";
	$fieldLabelsfin_debitos_resumo["Portuguese(Brazil)"]["idUnidade"] = "Unidade";
	$fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]["idUnidade"] = "";
	if (count($fieldToolTipsfin_debitos_resumo["Portuguese(Brazil)"]))
		$tdatafin_debitos_resumo[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_debitos_resumo[""] = array();
	$fieldToolTipsfin_debitos_resumo[""] = array();
	$pageTitlesfin_debitos_resumo[""] = array();
	if (count($fieldToolTipsfin_debitos_resumo[""]))
		$tdatafin_debitos_resumo[".isUseToolTips"] = true;
}


	$tdatafin_debitos_resumo[".NCSearch"] = true;



$tdatafin_debitos_resumo[".shortTableName"] = "fin_debitos_resumo";
$tdatafin_debitos_resumo[".nSecOptions"] = 0;
$tdatafin_debitos_resumo[".recsPerRowList"] = 1;
$tdatafin_debitos_resumo[".recsPerRowPrint"] = 1;
$tdatafin_debitos_resumo[".mainTableOwnerID"] = "";
$tdatafin_debitos_resumo[".moveNext"] = 1;
$tdatafin_debitos_resumo[".entityType"] = 0;

$tdatafin_debitos_resumo[".strOriginalTableName"] = "fin_debitos_resumo";





$tdatafin_debitos_resumo[".showAddInPopup"] = false;

$tdatafin_debitos_resumo[".showEditInPopup"] = false;

$tdatafin_debitos_resumo[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_debitos_resumo[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_debitos_resumo[".fieldsForRegister"] = array();

$tdatafin_debitos_resumo[".listAjax"] = false;

	$tdatafin_debitos_resumo[".audit"] = true;

	$tdatafin_debitos_resumo[".locking"] = true;



$tdatafin_debitos_resumo[".list"] = true;



$tdatafin_debitos_resumo[".exportTo"] = true;

$tdatafin_debitos_resumo[".printFriendly"] = true;


$tdatafin_debitos_resumo[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_debitos_resumo[".searchSaving"] = false;
//

$tdatafin_debitos_resumo[".showSearchPanel"] = true;
		$tdatafin_debitos_resumo[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_debitos_resumo[".isUseAjaxSuggest"] = false;
else
	$tdatafin_debitos_resumo[".isUseAjaxSuggest"] = true;

$tdatafin_debitos_resumo[".rowHighlite"] = true;



$tdatafin_debitos_resumo[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_debitos_resumo[".isUseTimeForSearch"] = false;





$tdatafin_debitos_resumo[".allSearchFields"] = array();
$tdatafin_debitos_resumo[".filterFields"] = array();
$tdatafin_debitos_resumo[".requiredSearchFields"] = array();

$tdatafin_debitos_resumo[".allSearchFields"][] = "link_ger_unidade";
	$tdatafin_debitos_resumo[".allSearchFields"][] = "contador2";
	$tdatafin_debitos_resumo[".allSearchFields"][] = "debitooriginal";
	$tdatafin_debitos_resumo[".allSearchFields"][] = "multatotal";
	$tdatafin_debitos_resumo[".allSearchFields"][] = "jurosttl";
	$tdatafin_debitos_resumo[".allSearchFields"][] = "corrmon";
	$tdatafin_debitos_resumo[".allSearchFields"][] = "debitototal";
	$tdatafin_debitos_resumo[".allSearchFields"][] = "processado";
	

$tdatafin_debitos_resumo[".googleLikeFields"] = array();
$tdatafin_debitos_resumo[".googleLikeFields"][] = "link_ger_unidade";
$tdatafin_debitos_resumo[".googleLikeFields"][] = "contador2";
$tdatafin_debitos_resumo[".googleLikeFields"][] = "debitooriginal";
$tdatafin_debitos_resumo[".googleLikeFields"][] = "multatotal";
$tdatafin_debitos_resumo[".googleLikeFields"][] = "jurosttl";
$tdatafin_debitos_resumo[".googleLikeFields"][] = "corrmon";
$tdatafin_debitos_resumo[".googleLikeFields"][] = "debitototal";
$tdatafin_debitos_resumo[".googleLikeFields"][] = "processado";


$tdatafin_debitos_resumo[".advSearchFields"] = array();
$tdatafin_debitos_resumo[".advSearchFields"][] = "link_ger_unidade";
$tdatafin_debitos_resumo[".advSearchFields"][] = "contador2";
$tdatafin_debitos_resumo[".advSearchFields"][] = "debitooriginal";
$tdatafin_debitos_resumo[".advSearchFields"][] = "multatotal";
$tdatafin_debitos_resumo[".advSearchFields"][] = "jurosttl";
$tdatafin_debitos_resumo[".advSearchFields"][] = "corrmon";
$tdatafin_debitos_resumo[".advSearchFields"][] = "debitototal";
$tdatafin_debitos_resumo[".advSearchFields"][] = "processado";

$tdatafin_debitos_resumo[".tableType"] = "list";

$tdatafin_debitos_resumo[".printerPageOrientation"] = 0;
$tdatafin_debitos_resumo[".nPrinterPageScale"] = 100;

$tdatafin_debitos_resumo[".nPrinterSplitRecords"] = 40;

$tdatafin_debitos_resumo[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_debitos_resumo[".geocodingEnabled"] = false;





$tdatafin_debitos_resumo[".listGridLayout"] = 3;

$tdatafin_debitos_resumo[".isDisplayLoading"] = true;


$tdatafin_debitos_resumo[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf

$tdatafin_debitos_resumo[".totalsFields"] = array();
$tdatafin_debitos_resumo[".totalsFields"][] = array(
	"fName" => "contador2",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => '');
$tdatafin_debitos_resumo[".totalsFields"][] = array(
	"fName" => "debitooriginal",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdatafin_debitos_resumo[".totalsFields"][] = array(
	"fName" => "multatotal",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdatafin_debitos_resumo[".totalsFields"][] = array(
	"fName" => "jurosttl",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdatafin_debitos_resumo[".totalsFields"][] = array(
	"fName" => "corrmon",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdatafin_debitos_resumo[".totalsFields"][] = array(
	"fName" => "debitototal",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');

$tdatafin_debitos_resumo[".pageSize"] = 20;

$tdatafin_debitos_resumo[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY ger_cadastro.nome";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_debitos_resumo[".strOrderBy"] = $tstrOrderBy;

$tdatafin_debitos_resumo[".orderindexes"] = array();
$tdatafin_debitos_resumo[".orderindexes"][] = array(8, (1 ? "ASC" : "DESC"), "ger_cadastro.nome");

$tdatafin_debitos_resumo[".sqlHead"] = "SELECT fin_debitos_resumo.link_ger_unidade,  fin_debitos_resumo.contador2,  fin_debitos_resumo.debitooriginal,  fin_debitos_resumo.multatotal,  fin_debitos_resumo.jurosttl,  fin_debitos_resumo.corrmon,  fin_debitos_resumo.debitototal,  ger_cadastro.nome,  fin_debitos_resumo.processado,  ger_unidades.idUnidade";
$tdatafin_debitos_resumo[".sqlFrom"] = "FROM fin_debitos_resumo  INNER JOIN ger_unidades ON fin_debitos_resumo.link_ger_unidade = ger_unidades.idUnidade  INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$tdatafin_debitos_resumo[".sqlWhereExpr"] = "";
$tdatafin_debitos_resumo[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_debitos_resumo[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_debitos_resumo[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_debitos_resumo[".highlightSearchResults"] = true;

$tableKeysfin_debitos_resumo = array();
$tdatafin_debitos_resumo[".Keys"] = $tableKeysfin_debitos_resumo;

$tdatafin_debitos_resumo[".listFields"] = array();
$tdatafin_debitos_resumo[".listFields"][] = "nome";
$tdatafin_debitos_resumo[".listFields"][] = "link_ger_unidade";
$tdatafin_debitos_resumo[".listFields"][] = "idUnidade";
$tdatafin_debitos_resumo[".listFields"][] = "contador2";
$tdatafin_debitos_resumo[".listFields"][] = "debitooriginal";
$tdatafin_debitos_resumo[".listFields"][] = "multatotal";
$tdatafin_debitos_resumo[".listFields"][] = "jurosttl";
$tdatafin_debitos_resumo[".listFields"][] = "corrmon";
$tdatafin_debitos_resumo[".listFields"][] = "debitototal";
$tdatafin_debitos_resumo[".listFields"][] = "processado";

$tdatafin_debitos_resumo[".hideMobileList"] = array();


$tdatafin_debitos_resumo[".viewFields"] = array();

$tdatafin_debitos_resumo[".addFields"] = array();

$tdatafin_debitos_resumo[".masterListFields"] = array();

$tdatafin_debitos_resumo[".inlineAddFields"] = array();

$tdatafin_debitos_resumo[".editFields"] = array();

$tdatafin_debitos_resumo[".inlineEditFields"] = array();

$tdatafin_debitos_resumo[".exportFields"] = array();
$tdatafin_debitos_resumo[".exportFields"][] = "idUnidade";
$tdatafin_debitos_resumo[".exportFields"][] = "nome";
$tdatafin_debitos_resumo[".exportFields"][] = "link_ger_unidade";
$tdatafin_debitos_resumo[".exportFields"][] = "contador2";
$tdatafin_debitos_resumo[".exportFields"][] = "debitooriginal";
$tdatafin_debitos_resumo[".exportFields"][] = "multatotal";
$tdatafin_debitos_resumo[".exportFields"][] = "jurosttl";
$tdatafin_debitos_resumo[".exportFields"][] = "corrmon";
$tdatafin_debitos_resumo[".exportFields"][] = "debitototal";
$tdatafin_debitos_resumo[".exportFields"][] = "processado";

$tdatafin_debitos_resumo[".importFields"] = array();
$tdatafin_debitos_resumo[".importFields"][] = "link_ger_unidade";
$tdatafin_debitos_resumo[".importFields"][] = "contador2";
$tdatafin_debitos_resumo[".importFields"][] = "debitooriginal";
$tdatafin_debitos_resumo[".importFields"][] = "multatotal";
$tdatafin_debitos_resumo[".importFields"][] = "jurosttl";
$tdatafin_debitos_resumo[".importFields"][] = "corrmon";
$tdatafin_debitos_resumo[".importFields"][] = "debitototal";
$tdatafin_debitos_resumo[".importFields"][] = "nome";
$tdatafin_debitos_resumo[".importFields"][] = "processado";
$tdatafin_debitos_resumo[".importFields"][] = "idUnidade";

$tdatafin_debitos_resumo[".printFields"] = array();
$tdatafin_debitos_resumo[".printFields"][] = "idUnidade";
$tdatafin_debitos_resumo[".printFields"][] = "nome";
$tdatafin_debitos_resumo[".printFields"][] = "link_ger_unidade";
$tdatafin_debitos_resumo[".printFields"][] = "contador2";
$tdatafin_debitos_resumo[".printFields"][] = "debitooriginal";
$tdatafin_debitos_resumo[".printFields"][] = "multatotal";
$tdatafin_debitos_resumo[".printFields"][] = "jurosttl";
$tdatafin_debitos_resumo[".printFields"][] = "corrmon";
$tdatafin_debitos_resumo[".printFields"][] = "debitototal";
$tdatafin_debitos_resumo[".printFields"][] = "processado";

//	link_ger_unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "link_ger_unidade";
	$fdata["GoodName"] = "link_ger_unidade";
	$fdata["ownerTable"] = "fin_debitos_resumo";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","link_ger_unidade");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_resumo.link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_guni_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idUnidade";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "grupounidade";

	
	$edata["LookupOrderBy"] = "grupounidade";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_debitos_resumo["link_ger_unidade"] = $fdata;
//	contador2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "contador2";
	$fdata["GoodName"] = "contador2";
	$fdata["ownerTable"] = "fin_debitos_resumo";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","contador2");
	$fdata["FieldType"] = 20;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "contador2";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_resumo.contador2";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_resumo["contador2"] = $fdata;
//	debitooriginal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "debitooriginal";
	$fdata["GoodName"] = "debitooriginal";
	$fdata["ownerTable"] = "fin_debitos_resumo";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","debitooriginal");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "debitooriginal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_resumo.debitooriginal";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_resumo["debitooriginal"] = $fdata;
//	multatotal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "multatotal";
	$fdata["GoodName"] = "multatotal";
	$fdata["ownerTable"] = "fin_debitos_resumo";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","multatotal");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "multatotal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_resumo.multatotal";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_resumo["multatotal"] = $fdata;
//	jurosttl
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "jurosttl";
	$fdata["GoodName"] = "jurosttl";
	$fdata["ownerTable"] = "fin_debitos_resumo";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","jurosttl");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "jurosttl";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_resumo.jurosttl";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_resumo["jurosttl"] = $fdata;
//	corrmon
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "corrmon";
	$fdata["GoodName"] = "corrmon";
	$fdata["ownerTable"] = "fin_debitos_resumo";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","corrmon");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "corrmon";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_resumo.corrmon";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_resumo["corrmon"] = $fdata;
//	debitototal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "debitototal";
	$fdata["GoodName"] = "debitototal";
	$fdata["ownerTable"] = "fin_debitos_resumo";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","debitototal");
	$fdata["FieldType"] = 5;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "debitototal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_resumo.debitototal";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_resumo["debitototal"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ger_cadastro.nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "ger_cadastro_list.php?a=advsearch&type=and&asearchfield[]=nome&asearchopt_nome=Contains&value_nome=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="ger_cadastro_list.php?a=advsearch&type=and&asearchfield[]=nome&asearchopt_nome=Contains&value_nome=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 2;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Link";
	$vdata["hlTitleField"] = "nome";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_debitos_resumo["nome"] = $fdata;
//	processado
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "processado";
	$fdata["GoodName"] = "processado";
	$fdata["ownerTable"] = "fin_debitos_resumo";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","processado");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "processado";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fin_debitos_resumo.processado";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_debitos_resumo["processado"] = $fdata;
//	idUnidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "idUnidade";
	$fdata["GoodName"] = "idUnidade";
	$fdata["ownerTable"] = "ger_unidades";
	$fdata["Label"] = GetFieldLabel("fin_debitos_resumo","idUnidade");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
		$fdata["bListPage"] = true;

	
	
	
	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idUnidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ger_unidades.idUnidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "ger_unidades_view.php?editid1=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="ger_unidades_view.php?editid1=";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Unidade";
	$vdata["hlTitleField"] = "Unidade";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_debitos_resumo["idUnidade"] = $fdata;


$tables_data["fin_debitos_resumo"]=&$tdatafin_debitos_resumo;
$field_labels["fin_debitos_resumo"] = &$fieldLabelsfin_debitos_resumo;
$fieldToolTips["fin_debitos_resumo"] = &$fieldToolTipsfin_debitos_resumo;
$page_titles["fin_debitos_resumo"] = &$pageTitlesfin_debitos_resumo;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_debitos_resumo"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_debitos_resumo"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_debitos_resumo()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "fin_debitos_resumo.link_ger_unidade,  fin_debitos_resumo.contador2,  fin_debitos_resumo.debitooriginal,  fin_debitos_resumo.multatotal,  fin_debitos_resumo.jurosttl,  fin_debitos_resumo.corrmon,  fin_debitos_resumo.debitototal,  ger_cadastro.nome,  fin_debitos_resumo.processado,  ger_unidades.idUnidade";
$proto0["m_strFrom"] = "FROM fin_debitos_resumo  INNER JOIN ger_unidades ON fin_debitos_resumo.link_ger_unidade = ger_unidades.idUnidade  INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY ger_cadastro.nome";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto6["m_sql"] = "fin_debitos_resumo.link_ger_unidade";
$proto6["m_srcTableName"] = "fin_debitos_resumo";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "contador2",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto8["m_sql"] = "fin_debitos_resumo.contador2";
$proto8["m_srcTableName"] = "fin_debitos_resumo";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "debitooriginal",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto10["m_sql"] = "fin_debitos_resumo.debitooriginal";
$proto10["m_srcTableName"] = "fin_debitos_resumo";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "multatotal",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto12["m_sql"] = "fin_debitos_resumo.multatotal";
$proto12["m_srcTableName"] = "fin_debitos_resumo";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "jurosttl",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto14["m_sql"] = "fin_debitos_resumo.jurosttl";
$proto14["m_srcTableName"] = "fin_debitos_resumo";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "corrmon",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto16["m_sql"] = "fin_debitos_resumo.corrmon";
$proto16["m_srcTableName"] = "fin_debitos_resumo";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "debitototal",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto18["m_sql"] = "fin_debitos_resumo.debitototal";
$proto18["m_srcTableName"] = "fin_debitos_resumo";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto20["m_sql"] = "ger_cadastro.nome";
$proto20["m_srcTableName"] = "fin_debitos_resumo";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "processado",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto22["m_sql"] = "fin_debitos_resumo.processado";
$proto22["m_srcTableName"] = "fin_debitos_resumo";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "idUnidade",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto24["m_sql"] = "ger_unidades.idUnidade";
$proto24["m_srcTableName"] = "fin_debitos_resumo";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto26=array();
$proto26["m_link"] = "SQLL_MAIN";
			$proto27=array();
$proto27["m_strName"] = "fin_debitos_resumo";
$proto27["m_srcTableName"] = "fin_debitos_resumo";
$proto27["m_columns"] = array();
$proto27["m_columns"][] = "link_ger_unidade";
$proto27["m_columns"][] = "contador2";
$proto27["m_columns"][] = "processado";
$proto27["m_columns"][] = "debitooriginal";
$proto27["m_columns"][] = "multatotal";
$proto27["m_columns"][] = "jurosttl";
$proto27["m_columns"][] = "corrmon";
$proto27["m_columns"][] = "debitototal";
$obj = new SQLTable($proto27);

$proto26["m_table"] = $obj;
$proto26["m_sql"] = "fin_debitos_resumo";
$proto26["m_alias"] = "";
$proto26["m_srcTableName"] = "fin_debitos_resumo";
$proto28=array();
$proto28["m_sql"] = "";
$proto28["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto28["m_column"]=$obj;
$proto28["m_contained"] = array();
$proto28["m_strCase"] = "";
$proto28["m_havingmode"] = false;
$proto28["m_inBrackets"] = false;
$proto28["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto28);

$proto26["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto26);

$proto0["m_fromlist"][]=$obj;
												$proto30=array();
$proto30["m_link"] = "SQLL_INNERJOIN";
			$proto31=array();
$proto31["m_strName"] = "ger_unidades";
$proto31["m_srcTableName"] = "fin_debitos_resumo";
$proto31["m_columns"] = array();
$proto31["m_columns"][] = "idUnidade";
$proto31["m_columns"][] = "grupo";
$proto31["m_columns"][] = "unidade";
$proto31["m_columns"][] = "link_ger_cadastro";
$proto31["m_columns"][] = "dia_pref_venc";
$proto31["m_columns"][] = "ativo";
$proto31["m_columns"][] = "obs";
$proto31["m_columns"][] = "frideal";
$proto31["m_columns"][] = "ultimousuario";
$proto31["m_columns"][] = "ultimaalteracao";
$proto31["m_columns"][] = "auth";
$obj = new SQLTable($proto31);

$proto30["m_table"] = $obj;
$proto30["m_sql"] = "INNER JOIN ger_unidades ON fin_debitos_resumo.link_ger_unidade = ger_unidades.idUnidade";
$proto30["m_alias"] = "";
$proto30["m_srcTableName"] = "fin_debitos_resumo";
$proto32=array();
$proto32["m_sql"] = "fin_debitos_resumo.link_ger_unidade = ger_unidades.idUnidade";
$proto32["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "fin_debitos_resumo",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto32["m_column"]=$obj;
$proto32["m_contained"] = array();
$proto32["m_strCase"] = "= ger_unidades.idUnidade";
$proto32["m_havingmode"] = false;
$proto32["m_inBrackets"] = false;
$proto32["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto32);

$proto30["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto30);

$proto0["m_fromlist"][]=$obj;
												$proto34=array();
$proto34["m_link"] = "SQLL_INNERJOIN";
			$proto35=array();
$proto35["m_strName"] = "ger_cadastro";
$proto35["m_srcTableName"] = "fin_debitos_resumo";
$proto35["m_columns"] = array();
$proto35["m_columns"][] = "idCadastro";
$proto35["m_columns"][] = "nome";
$proto35["m_columns"][] = "rg";
$proto35["m_columns"][] = "cpf";
$proto35["m_columns"][] = "TelPrim";
$proto35["m_columns"][] = "CompPrim";
$proto35["m_columns"][] = "TelSec";
$proto35["m_columns"][] = "CompSec";
$proto35["m_columns"][] = "TelTerc";
$proto35["m_columns"][] = "CompTerc";
$proto35["m_columns"][] = "log";
$proto35["m_columns"][] = "end";
$proto35["m_columns"][] = "num";
$proto35["m_columns"][] = "comp";
$proto35["m_columns"][] = "bairro";
$proto35["m_columns"][] = "cep";
$proto35["m_columns"][] = "cidade";
$proto35["m_columns"][] = "uf";
$proto35["m_columns"][] = "obs";
$proto35["m_columns"][] = "situacao";
$proto35["m_columns"][] = "morador";
$proto35["m_columns"][] = "cepnet";
$proto35["m_columns"][] = "ativo";
$proto35["m_columns"][] = "processado";
$proto35["m_columns"][] = "email";
$proto35["m_columns"][] = "webuser";
$proto35["m_columns"][] = "ultimousuario";
$proto35["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto35);

$proto34["m_table"] = $obj;
$proto34["m_sql"] = "INNER JOIN ger_cadastro ON ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto34["m_alias"] = "";
$proto34["m_srcTableName"] = "fin_debitos_resumo";
$proto36=array();
$proto36["m_sql"] = "ger_unidades.link_ger_cadastro = ger_cadastro.idCadastro";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "link_ger_cadastro",
	"m_strTable" => "ger_unidades",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "= ger_cadastro.idCadastro";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = false;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto34["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto34);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto38=array();
						$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "fin_debitos_resumo"
));

$proto38["m_column"]=$obj;
$proto38["m_bAsc"] = 1;
$proto38["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto38);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="fin_debitos_resumo";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_debitos_resumo = createSqlQuery_fin_debitos_resumo();


	
		;

										

$tdatafin_debitos_resumo[".sqlquery"] = $queryData_fin_debitos_resumo;

include_once(getabspath("include/fin_debitos_resumo_events.php"));
$tableEvents["fin_debitos_resumo"] = new eventclass_fin_debitos_resumo;
$tdatafin_debitos_resumo[".hasEvents"] = true;

?>