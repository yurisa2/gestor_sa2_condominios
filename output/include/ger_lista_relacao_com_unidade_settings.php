<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_lista_relacao_com_unidade = array();
	$tdatager_lista_relacao_com_unidade[".truncateText"] = true;
	$tdatager_lista_relacao_com_unidade[".NumberOfChars"] = 80;
	$tdatager_lista_relacao_com_unidade[".ShortName"] = "ger_lista_relacao_com_unidade";
	$tdatager_lista_relacao_com_unidade[".OwnerID"] = "";
	$tdatager_lista_relacao_com_unidade[".OriginalTable"] = "ger_lista_relacao_com_unidade";

//	field labels
$fieldLabelsger_lista_relacao_com_unidade = array();
$fieldToolTipsger_lista_relacao_com_unidade = array();
$pageTitlesger_lista_relacao_com_unidade = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_lista_relacao_com_unidade["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_lista_relacao_com_unidade["Portuguese(Brazil)"] = array();
	$pageTitlesger_lista_relacao_com_unidade["Portuguese(Brazil)"] = array();
	$fieldLabelsger_lista_relacao_com_unidade["Portuguese(Brazil)"]["idRelacao"] = "Código";
	$fieldToolTipsger_lista_relacao_com_unidade["Portuguese(Brazil)"]["idRelacao"] = "";
	$fieldLabelsger_lista_relacao_com_unidade["Portuguese(Brazil)"]["relacao"] = "Relação com a unidade";
	$fieldToolTipsger_lista_relacao_com_unidade["Portuguese(Brazil)"]["relacao"] = "";
	$fieldLabelsger_lista_relacao_com_unidade["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsger_lista_relacao_com_unidade["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_lista_relacao_com_unidade["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsger_lista_relacao_com_unidade["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_lista_relacao_com_unidade["Portuguese(Brazil)"]))
		$tdatager_lista_relacao_com_unidade[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_lista_relacao_com_unidade[""] = array();
	$fieldToolTipsger_lista_relacao_com_unidade[""] = array();
	$pageTitlesger_lista_relacao_com_unidade[""] = array();
	if (count($fieldToolTipsger_lista_relacao_com_unidade[""]))
		$tdatager_lista_relacao_com_unidade[".isUseToolTips"] = true;
}


	$tdatager_lista_relacao_com_unidade[".NCSearch"] = true;



$tdatager_lista_relacao_com_unidade[".shortTableName"] = "ger_lista_relacao_com_unidade";
$tdatager_lista_relacao_com_unidade[".nSecOptions"] = 0;
$tdatager_lista_relacao_com_unidade[".recsPerRowList"] = 1;
$tdatager_lista_relacao_com_unidade[".recsPerRowPrint"] = 1;
$tdatager_lista_relacao_com_unidade[".mainTableOwnerID"] = "";
$tdatager_lista_relacao_com_unidade[".moveNext"] = 1;
$tdatager_lista_relacao_com_unidade[".entityType"] = 0;

$tdatager_lista_relacao_com_unidade[".strOriginalTableName"] = "ger_lista_relacao_com_unidade";





$tdatager_lista_relacao_com_unidade[".showAddInPopup"] = false;

$tdatager_lista_relacao_com_unidade[".showEditInPopup"] = false;

$tdatager_lista_relacao_com_unidade[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_lista_relacao_com_unidade[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_lista_relacao_com_unidade[".fieldsForRegister"] = array();

$tdatager_lista_relacao_com_unidade[".listAjax"] = false;

	$tdatager_lista_relacao_com_unidade[".audit"] = true;

	$tdatager_lista_relacao_com_unidade[".locking"] = true;

$tdatager_lista_relacao_com_unidade[".edit"] = true;
$tdatager_lista_relacao_com_unidade[".afterEditAction"] = 1;
$tdatager_lista_relacao_com_unidade[".closePopupAfterEdit"] = 1;
$tdatager_lista_relacao_com_unidade[".afterEditActionDetTable"] = "";

$tdatager_lista_relacao_com_unidade[".add"] = true;
$tdatager_lista_relacao_com_unidade[".afterAddAction"] = 1;
$tdatager_lista_relacao_com_unidade[".closePopupAfterAdd"] = 1;
$tdatager_lista_relacao_com_unidade[".afterAddActionDetTable"] = "";

$tdatager_lista_relacao_com_unidade[".list"] = true;



$tdatager_lista_relacao_com_unidade[".exportTo"] = true;

$tdatager_lista_relacao_com_unidade[".printFriendly"] = true;

$tdatager_lista_relacao_com_unidade[".delete"] = true;

$tdatager_lista_relacao_com_unidade[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_lista_relacao_com_unidade[".searchSaving"] = false;
//

$tdatager_lista_relacao_com_unidade[".showSearchPanel"] = true;
		$tdatager_lista_relacao_com_unidade[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_lista_relacao_com_unidade[".isUseAjaxSuggest"] = false;
else
	$tdatager_lista_relacao_com_unidade[".isUseAjaxSuggest"] = true;

$tdatager_lista_relacao_com_unidade[".rowHighlite"] = true;



$tdatager_lista_relacao_com_unidade[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_lista_relacao_com_unidade[".isUseTimeForSearch"] = false;





$tdatager_lista_relacao_com_unidade[".allSearchFields"] = array();
$tdatager_lista_relacao_com_unidade[".filterFields"] = array();
$tdatager_lista_relacao_com_unidade[".requiredSearchFields"] = array();

$tdatager_lista_relacao_com_unidade[".allSearchFields"][] = "ultimaalteracao";
	$tdatager_lista_relacao_com_unidade[".allSearchFields"][] = "ultimousuario";
	$tdatager_lista_relacao_com_unidade[".allSearchFields"][] = "idRelacao";
	$tdatager_lista_relacao_com_unidade[".allSearchFields"][] = "relacao";
	

$tdatager_lista_relacao_com_unidade[".googleLikeFields"] = array();
$tdatager_lista_relacao_com_unidade[".googleLikeFields"][] = "idRelacao";
$tdatager_lista_relacao_com_unidade[".googleLikeFields"][] = "relacao";
$tdatager_lista_relacao_com_unidade[".googleLikeFields"][] = "ultimousuario";
$tdatager_lista_relacao_com_unidade[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_lista_relacao_com_unidade[".advSearchFields"] = array();
$tdatager_lista_relacao_com_unidade[".advSearchFields"][] = "ultimaalteracao";
$tdatager_lista_relacao_com_unidade[".advSearchFields"][] = "ultimousuario";
$tdatager_lista_relacao_com_unidade[".advSearchFields"][] = "idRelacao";
$tdatager_lista_relacao_com_unidade[".advSearchFields"][] = "relacao";

$tdatager_lista_relacao_com_unidade[".tableType"] = "list";

$tdatager_lista_relacao_com_unidade[".printerPageOrientation"] = 0;
$tdatager_lista_relacao_com_unidade[".nPrinterPageScale"] = 100;

$tdatager_lista_relacao_com_unidade[".nPrinterSplitRecords"] = 40;

$tdatager_lista_relacao_com_unidade[".nPrinterPDFSplitRecords"] = 40;



$tdatager_lista_relacao_com_unidade[".geocodingEnabled"] = false;





$tdatager_lista_relacao_com_unidade[".listGridLayout"] = 3;

$tdatager_lista_relacao_com_unidade[".isDisplayLoading"] = true;


$tdatager_lista_relacao_com_unidade[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_lista_relacao_com_unidade[".pageSize"] = 20;

$tdatager_lista_relacao_com_unidade[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_lista_relacao_com_unidade[".strOrderBy"] = $tstrOrderBy;

$tdatager_lista_relacao_com_unidade[".orderindexes"] = array();

$tdatager_lista_relacao_com_unidade[".sqlHead"] = "select idRelacao,  relacao,  ultimousuario,  ultimaalteracao";
$tdatager_lista_relacao_com_unidade[".sqlFrom"] = "FROM ger_lista_relacao_com_unidade";
$tdatager_lista_relacao_com_unidade[".sqlWhereExpr"] = "";
$tdatager_lista_relacao_com_unidade[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_lista_relacao_com_unidade[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_lista_relacao_com_unidade[".arrGroupsPerPage"] = $arrGPP;

$tdatager_lista_relacao_com_unidade[".highlightSearchResults"] = true;

$tableKeysger_lista_relacao_com_unidade = array();
$tableKeysger_lista_relacao_com_unidade[] = "idRelacao";
$tdatager_lista_relacao_com_unidade[".Keys"] = $tableKeysger_lista_relacao_com_unidade;

$tdatager_lista_relacao_com_unidade[".listFields"] = array();
$tdatager_lista_relacao_com_unidade[".listFields"][] = "idRelacao";
$tdatager_lista_relacao_com_unidade[".listFields"][] = "relacao";

$tdatager_lista_relacao_com_unidade[".hideMobileList"] = array();


$tdatager_lista_relacao_com_unidade[".viewFields"] = array();

$tdatager_lista_relacao_com_unidade[".addFields"] = array();
$tdatager_lista_relacao_com_unidade[".addFields"][] = "relacao";

$tdatager_lista_relacao_com_unidade[".masterListFields"] = array();

$tdatager_lista_relacao_com_unidade[".inlineAddFields"] = array();

$tdatager_lista_relacao_com_unidade[".editFields"] = array();
$tdatager_lista_relacao_com_unidade[".editFields"][] = "ultimaalteracao";
$tdatager_lista_relacao_com_unidade[".editFields"][] = "ultimousuario";
$tdatager_lista_relacao_com_unidade[".editFields"][] = "idRelacao";
$tdatager_lista_relacao_com_unidade[".editFields"][] = "relacao";

$tdatager_lista_relacao_com_unidade[".inlineEditFields"] = array();

$tdatager_lista_relacao_com_unidade[".exportFields"] = array();
$tdatager_lista_relacao_com_unidade[".exportFields"][] = "ultimaalteracao";
$tdatager_lista_relacao_com_unidade[".exportFields"][] = "ultimousuario";
$tdatager_lista_relacao_com_unidade[".exportFields"][] = "idRelacao";
$tdatager_lista_relacao_com_unidade[".exportFields"][] = "relacao";

$tdatager_lista_relacao_com_unidade[".importFields"] = array();
$tdatager_lista_relacao_com_unidade[".importFields"][] = "idRelacao";
$tdatager_lista_relacao_com_unidade[".importFields"][] = "relacao";
$tdatager_lista_relacao_com_unidade[".importFields"][] = "ultimousuario";
$tdatager_lista_relacao_com_unidade[".importFields"][] = "ultimaalteracao";

$tdatager_lista_relacao_com_unidade[".printFields"] = array();

//	idRelacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idRelacao";
	$fdata["GoodName"] = "idRelacao";
	$fdata["ownerTable"] = "ger_lista_relacao_com_unidade";
	$fdata["Label"] = GetFieldLabel("ger_lista_relacao_com_unidade","idRelacao");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idRelacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idRelacao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_lista_relacao_com_unidade["idRelacao"] = $fdata;
//	relacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "relacao";
	$fdata["GoodName"] = "relacao";
	$fdata["ownerTable"] = "ger_lista_relacao_com_unidade";
	$fdata["Label"] = GetFieldLabel("ger_lista_relacao_com_unidade","relacao");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "relacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "relacao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_lista_relacao_com_unidade["relacao"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_lista_relacao_com_unidade";
	$fdata["Label"] = GetFieldLabel("ger_lista_relacao_com_unidade","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_lista_relacao_com_unidade["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_lista_relacao_com_unidade";
	$fdata["Label"] = GetFieldLabel("ger_lista_relacao_com_unidade","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_lista_relacao_com_unidade["ultimaalteracao"] = $fdata;


$tables_data["ger_lista_relacao_com_unidade"]=&$tdatager_lista_relacao_com_unidade;
$field_labels["ger_lista_relacao_com_unidade"] = &$fieldLabelsger_lista_relacao_com_unidade;
$fieldToolTips["ger_lista_relacao_com_unidade"] = &$fieldToolTipsger_lista_relacao_com_unidade;
$page_titles["ger_lista_relacao_com_unidade"] = &$pageTitlesger_lista_relacao_com_unidade;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_lista_relacao_com_unidade"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_lista_relacao_com_unidade"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_lista_relacao_com_unidade()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idRelacao,  relacao,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_lista_relacao_com_unidade";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idRelacao",
	"m_strTable" => "ger_lista_relacao_com_unidade",
	"m_srcTableName" => "ger_lista_relacao_com_unidade"
));

$proto6["m_sql"] = "idRelacao";
$proto6["m_srcTableName"] = "ger_lista_relacao_com_unidade";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "relacao",
	"m_strTable" => "ger_lista_relacao_com_unidade",
	"m_srcTableName" => "ger_lista_relacao_com_unidade"
));

$proto8["m_sql"] = "relacao";
$proto8["m_srcTableName"] = "ger_lista_relacao_com_unidade";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_lista_relacao_com_unidade",
	"m_srcTableName" => "ger_lista_relacao_com_unidade"
));

$proto10["m_sql"] = "ultimousuario";
$proto10["m_srcTableName"] = "ger_lista_relacao_com_unidade";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_lista_relacao_com_unidade",
	"m_srcTableName" => "ger_lista_relacao_com_unidade"
));

$proto12["m_sql"] = "ultimaalteracao";
$proto12["m_srcTableName"] = "ger_lista_relacao_com_unidade";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "ger_lista_relacao_com_unidade";
$proto15["m_srcTableName"] = "ger_lista_relacao_com_unidade";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "idRelacao";
$proto15["m_columns"][] = "relacao";
$proto15["m_columns"][] = "ultimousuario";
$proto15["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "ger_lista_relacao_com_unidade";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "ger_lista_relacao_com_unidade";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_lista_relacao_com_unidade";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_lista_relacao_com_unidade = createSqlQuery_ger_lista_relacao_com_unidade();


	
		;

				

$tdatager_lista_relacao_com_unidade[".sqlquery"] = $queryData_ger_lista_relacao_com_unidade;

include_once(getabspath("include/ger_lista_relacao_com_unidade_events.php"));
$tableEvents["ger_lista_relacao_com_unidade"] = new eventclass_ger_lista_relacao_com_unidade;
$tdatager_lista_relacao_com_unidade[".hasEvents"] = true;

?>