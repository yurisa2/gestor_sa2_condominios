<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_arquivos_unidades = array();
	$tdatager_arquivos_unidades[".truncateText"] = true;
	$tdatager_arquivos_unidades[".NumberOfChars"] = 80;
	$tdatager_arquivos_unidades[".ShortName"] = "ger_arquivos_unidades";
	$tdatager_arquivos_unidades[".OwnerID"] = "link_ger_unidade";
	$tdatager_arquivos_unidades[".OriginalTable"] = "ger_arquivos_unidades";

//	field labels
$fieldLabelsger_arquivos_unidades = array();
$fieldToolTipsger_arquivos_unidades = array();
$pageTitlesger_arquivos_unidades = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"] = array();
	$pageTitlesger_arquivos_unidades["Portuguese(Brazil)"] = array();
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["idArquivosUnidade"] = "idArquivos";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["idArquivosUnidade"] = "";
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["nomedoc"] = "Nome do arquivo";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["nomedoc"] = "";
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["obs"] = "Obs/Descrição";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["arquivo"] = "Arquivo";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["arquivo"] = "";
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["link_ger_unidade"] = "Unidade";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["link_ger_unidade"] = "";
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["link"] = "Link";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["link"] = "";
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["Unidade"] = "Unidade";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["Unidade"] = "";
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["ultimousuario"] = "Ultimo usuário";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_arquivos_unidades["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima alteração";
	$fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_arquivos_unidades["Portuguese(Brazil)"]))
		$tdatager_arquivos_unidades[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_arquivos_unidades[""] = array();
	$fieldToolTipsger_arquivos_unidades[""] = array();
	$pageTitlesger_arquivos_unidades[""] = array();
	if (count($fieldToolTipsger_arquivos_unidades[""]))
		$tdatager_arquivos_unidades[".isUseToolTips"] = true;
}


	$tdatager_arquivos_unidades[".NCSearch"] = true;



$tdatager_arquivos_unidades[".shortTableName"] = "ger_arquivos_unidades";
$tdatager_arquivos_unidades[".nSecOptions"] = 1;
$tdatager_arquivos_unidades[".recsPerRowList"] = 1;
$tdatager_arquivos_unidades[".recsPerRowPrint"] = 1;
$tdatager_arquivos_unidades[".mainTableOwnerID"] = "link_ger_unidade";
$tdatager_arquivos_unidades[".moveNext"] = 1;
$tdatager_arquivos_unidades[".entityType"] = 0;

$tdatager_arquivos_unidades[".strOriginalTableName"] = "ger_arquivos_unidades";





$tdatager_arquivos_unidades[".showAddInPopup"] = false;

$tdatager_arquivos_unidades[".showEditInPopup"] = false;

$tdatager_arquivos_unidades[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_arquivos_unidades[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_arquivos_unidades[".fieldsForRegister"] = array();

$tdatager_arquivos_unidades[".listAjax"] = false;

	$tdatager_arquivos_unidades[".audit"] = true;

	$tdatager_arquivos_unidades[".locking"] = true;

$tdatager_arquivos_unidades[".edit"] = true;
$tdatager_arquivos_unidades[".afterEditAction"] = 1;
$tdatager_arquivos_unidades[".closePopupAfterEdit"] = 1;
$tdatager_arquivos_unidades[".afterEditActionDetTable"] = "";

$tdatager_arquivos_unidades[".add"] = true;
$tdatager_arquivos_unidades[".afterAddAction"] = 1;
$tdatager_arquivos_unidades[".closePopupAfterAdd"] = 1;
$tdatager_arquivos_unidades[".afterAddActionDetTable"] = "";

$tdatager_arquivos_unidades[".list"] = true;

$tdatager_arquivos_unidades[".view"] = true;



$tdatager_arquivos_unidades[".printFriendly"] = true;

$tdatager_arquivos_unidades[".delete"] = true;

$tdatager_arquivos_unidades[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_arquivos_unidades[".searchSaving"] = false;
//

$tdatager_arquivos_unidades[".showSearchPanel"] = true;
		$tdatager_arquivos_unidades[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_arquivos_unidades[".isUseAjaxSuggest"] = false;
else
	$tdatager_arquivos_unidades[".isUseAjaxSuggest"] = true;

$tdatager_arquivos_unidades[".rowHighlite"] = true;



$tdatager_arquivos_unidades[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_arquivos_unidades[".isUseTimeForSearch"] = false;



$tdatager_arquivos_unidades[".badgeColor"] = "6493EA";


$tdatager_arquivos_unidades[".allSearchFields"] = array();
$tdatager_arquivos_unidades[".filterFields"] = array();
$tdatager_arquivos_unidades[".requiredSearchFields"] = array();

$tdatager_arquivos_unidades[".allSearchFields"][] = "link_ger_unidade";
	$tdatager_arquivos_unidades[".allSearchFields"][] = "nomedoc";
	$tdatager_arquivos_unidades[".allSearchFields"][] = "obs";
	$tdatager_arquivos_unidades[".allSearchFields"][] = "arquivo";
	$tdatager_arquivos_unidades[".allSearchFields"][] = "ultimousuario";
	$tdatager_arquivos_unidades[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_arquivos_unidades[".googleLikeFields"] = array();
$tdatager_arquivos_unidades[".googleLikeFields"][] = "nomedoc";
$tdatager_arquivos_unidades[".googleLikeFields"][] = "obs";
$tdatager_arquivos_unidades[".googleLikeFields"][] = "arquivo";
$tdatager_arquivos_unidades[".googleLikeFields"][] = "link_ger_unidade";
$tdatager_arquivos_unidades[".googleLikeFields"][] = "ultimousuario";
$tdatager_arquivos_unidades[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_arquivos_unidades[".advSearchFields"] = array();
$tdatager_arquivos_unidades[".advSearchFields"][] = "link_ger_unidade";
$tdatager_arquivos_unidades[".advSearchFields"][] = "nomedoc";
$tdatager_arquivos_unidades[".advSearchFields"][] = "obs";
$tdatager_arquivos_unidades[".advSearchFields"][] = "arquivo";
$tdatager_arquivos_unidades[".advSearchFields"][] = "ultimousuario";
$tdatager_arquivos_unidades[".advSearchFields"][] = "ultimaalteracao";

$tdatager_arquivos_unidades[".tableType"] = "list";

$tdatager_arquivos_unidades[".printerPageOrientation"] = 0;
$tdatager_arquivos_unidades[".nPrinterPageScale"] = 100;

$tdatager_arquivos_unidades[".nPrinterSplitRecords"] = 40;

$tdatager_arquivos_unidades[".nPrinterPDFSplitRecords"] = 40;



$tdatager_arquivos_unidades[".geocodingEnabled"] = false;





$tdatager_arquivos_unidades[".listGridLayout"] = 3;

$tdatager_arquivos_unidades[".isDisplayLoading"] = true;


$tdatager_arquivos_unidades[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_arquivos_unidades[".pageSize"] = 20;

$tdatager_arquivos_unidades[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_arquivos_unidades[".strOrderBy"] = $tstrOrderBy;

$tdatager_arquivos_unidades[".orderindexes"] = array();

$tdatager_arquivos_unidades[".sqlHead"] = "SELECT idArquivosUnidade,  nomedoc,  obs,  arquivo,  link_ger_unidade,  arquivo AS link,  link_ger_unidade AS Unidade,  ultimousuario,  ultimaalteracao";
$tdatager_arquivos_unidades[".sqlFrom"] = "FROM ger_arquivos_unidades";
$tdatager_arquivos_unidades[".sqlWhereExpr"] = "";
$tdatager_arquivos_unidades[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_arquivos_unidades[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_arquivos_unidades[".arrGroupsPerPage"] = $arrGPP;

$tdatager_arquivos_unidades[".highlightSearchResults"] = true;

$tableKeysger_arquivos_unidades = array();
$tableKeysger_arquivos_unidades[] = "idArquivosUnidade";
$tdatager_arquivos_unidades[".Keys"] = $tableKeysger_arquivos_unidades;

$tdatager_arquivos_unidades[".listFields"] = array();
$tdatager_arquivos_unidades[".listFields"][] = "Unidade";
$tdatager_arquivos_unidades[".listFields"][] = "link_ger_unidade";
$tdatager_arquivos_unidades[".listFields"][] = "nomedoc";
$tdatager_arquivos_unidades[".listFields"][] = "obs";
$tdatager_arquivos_unidades[".listFields"][] = "arquivo";

$tdatager_arquivos_unidades[".hideMobileList"] = array();


$tdatager_arquivos_unidades[".viewFields"] = array();
$tdatager_arquivos_unidades[".viewFields"][] = "Unidade";
$tdatager_arquivos_unidades[".viewFields"][] = "link_ger_unidade";
$tdatager_arquivos_unidades[".viewFields"][] = "nomedoc";
$tdatager_arquivos_unidades[".viewFields"][] = "obs";
$tdatager_arquivos_unidades[".viewFields"][] = "arquivo";
$tdatager_arquivos_unidades[".viewFields"][] = "ultimousuario";
$tdatager_arquivos_unidades[".viewFields"][] = "ultimaalteracao";

$tdatager_arquivos_unidades[".addFields"] = array();
$tdatager_arquivos_unidades[".addFields"][] = "link_ger_unidade";
$tdatager_arquivos_unidades[".addFields"][] = "nomedoc";
$tdatager_arquivos_unidades[".addFields"][] = "obs";
$tdatager_arquivos_unidades[".addFields"][] = "arquivo";
$tdatager_arquivos_unidades[".addFields"][] = "ultimousuario";
$tdatager_arquivos_unidades[".addFields"][] = "ultimaalteracao";

$tdatager_arquivos_unidades[".masterListFields"] = array();

$tdatager_arquivos_unidades[".inlineAddFields"] = array();

$tdatager_arquivos_unidades[".editFields"] = array();
$tdatager_arquivos_unidades[".editFields"][] = "link_ger_unidade";
$tdatager_arquivos_unidades[".editFields"][] = "nomedoc";
$tdatager_arquivos_unidades[".editFields"][] = "obs";
$tdatager_arquivos_unidades[".editFields"][] = "arquivo";
$tdatager_arquivos_unidades[".editFields"][] = "ultimousuario";
$tdatager_arquivos_unidades[".editFields"][] = "ultimaalteracao";

$tdatager_arquivos_unidades[".inlineEditFields"] = array();

$tdatager_arquivos_unidades[".exportFields"] = array();

$tdatager_arquivos_unidades[".importFields"] = array();
$tdatager_arquivos_unidades[".importFields"][] = "idArquivosUnidade";
$tdatager_arquivos_unidades[".importFields"][] = "nomedoc";
$tdatager_arquivos_unidades[".importFields"][] = "obs";
$tdatager_arquivos_unidades[".importFields"][] = "arquivo";
$tdatager_arquivos_unidades[".importFields"][] = "link_ger_unidade";
$tdatager_arquivos_unidades[".importFields"][] = "link";
$tdatager_arquivos_unidades[".importFields"][] = "Unidade";
$tdatager_arquivos_unidades[".importFields"][] = "ultimousuario";
$tdatager_arquivos_unidades[".importFields"][] = "ultimaalteracao";

$tdatager_arquivos_unidades[".printFields"] = array();
$tdatager_arquivos_unidades[".printFields"][] = "Unidade";
$tdatager_arquivos_unidades[".printFields"][] = "link_ger_unidade";
$tdatager_arquivos_unidades[".printFields"][] = "nomedoc";
$tdatager_arquivos_unidades[".printFields"][] = "obs";
$tdatager_arquivos_unidades[".printFields"][] = "arquivo";
$tdatager_arquivos_unidades[".printFields"][] = "ultimousuario";
$tdatager_arquivos_unidades[".printFields"][] = "ultimaalteracao";

//	idArquivosUnidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idArquivosUnidade";
	$fdata["GoodName"] = "idArquivosUnidade";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","idArquivosUnidade");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idArquivosUnidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idArquivosUnidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_arquivos_unidades["idArquivosUnidade"] = $fdata;
//	nomedoc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nomedoc";
	$fdata["GoodName"] = "nomedoc";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","nomedoc");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "nomedoc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nomedoc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=45";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_unidades["nomedoc"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_unidades["obs"] = $fdata;
//	arquivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "arquivo";
	$fdata["GoodName"] = "arquivo";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","arquivo");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "arquivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "arquivo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "arquivos/";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
								$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
	



		$edata["IsRequired"] = true;

	
		$edata["UseTimestamp"] = true;

	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_unidades["arquivo"] = $fdata;
//	link_ger_unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "link_ger_unidade";
	$fdata["GoodName"] = "link_ger_unidade";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","link_ger_unidade");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_selecao_guni_ger_unidades";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "grupounidade2";
	$edata["LinkFieldType"] = 13;
	$edata["DisplayField"] = "grupounidade2";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_unidades["link_ger_unidade"] = $fdata;
//	link
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "link";
	$fdata["GoodName"] = "link";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","link");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "arquivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "arquivo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "arquivos/";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="arquivos/";

	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Download";
	$vdata["hlTitleField"] = "Download";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_arquivos_unidades["link"] = $fdata;
//	Unidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Unidade";
	$fdata["GoodName"] = "Unidade";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","Unidade");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "link_ger_unidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_unidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "http://localhost/copromo/ger_unidades_list.php?a=search&value=1&SearchOption=Equals&SearchField=idUnidade&SearchFor=";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
		$vdata["LinkPrefix"] ="http://localhost/copromo/ger_unidades_list.php?a=search&value=1&SearchOption=Equals&SearchField=idUnidade&SearchFor=";

	
	
				$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Unidade";
	$vdata["hlTitleField"] = "Unidade";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_arquivos_unidades["Unidade"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_unidades["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_arquivos_unidades";
	$fdata["Label"] = GetFieldLabel("ger_arquivos_unidades","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_arquivos_unidades["ultimaalteracao"] = $fdata;


$tables_data["ger_arquivos_unidades"]=&$tdatager_arquivos_unidades;
$field_labels["ger_arquivos_unidades"] = &$fieldLabelsger_arquivos_unidades;
$fieldToolTips["ger_arquivos_unidades"] = &$fieldToolTipsger_arquivos_unidades;
$page_titles["ger_arquivos_unidades"] = &$pageTitlesger_arquivos_unidades;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_arquivos_unidades"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_arquivos_unidades"] = array();


	
				$strOriginalDetailsTable="ger_unidades";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="ger_unidades";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "ger_unidades";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["ger_arquivos_unidades"][0] = $masterParams;
				$masterTablesData["ger_arquivos_unidades"][0]["masterKeys"] = array();
	$masterTablesData["ger_arquivos_unidades"][0]["masterKeys"][]="idUnidade";
				$masterTablesData["ger_arquivos_unidades"][0]["detailKeys"] = array();
	$masterTablesData["ger_arquivos_unidades"][0]["detailKeys"][]="link_ger_unidade";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_arquivos_unidades()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idArquivosUnidade,  nomedoc,  obs,  arquivo,  link_ger_unidade,  arquivo AS link,  link_ger_unidade AS Unidade,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_arquivos_unidades";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idArquivosUnidade",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto6["m_sql"] = "idArquivosUnidade";
$proto6["m_srcTableName"] = "ger_arquivos_unidades";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nomedoc",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto8["m_sql"] = "nomedoc";
$proto8["m_srcTableName"] = "ger_arquivos_unidades";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto10["m_sql"] = "obs";
$proto10["m_srcTableName"] = "ger_arquivos_unidades";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "arquivo",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto12["m_sql"] = "arquivo";
$proto12["m_srcTableName"] = "ger_arquivos_unidades";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto14["m_sql"] = "link_ger_unidade";
$proto14["m_srcTableName"] = "ger_arquivos_unidades";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "arquivo",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto16["m_sql"] = "arquivo";
$proto16["m_srcTableName"] = "ger_arquivos_unidades";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "link";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_unidade",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto18["m_sql"] = "link_ger_unidade";
$proto18["m_srcTableName"] = "ger_arquivos_unidades";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "Unidade";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto20["m_sql"] = "ultimousuario";
$proto20["m_srcTableName"] = "ger_arquivos_unidades";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_arquivos_unidades",
	"m_srcTableName" => "ger_arquivos_unidades"
));

$proto22["m_sql"] = "ultimaalteracao";
$proto22["m_srcTableName"] = "ger_arquivos_unidades";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "ger_arquivos_unidades";
$proto25["m_srcTableName"] = "ger_arquivos_unidades";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "idArquivosUnidade";
$proto25["m_columns"][] = "nomedoc";
$proto25["m_columns"][] = "obs";
$proto25["m_columns"][] = "arquivo";
$proto25["m_columns"][] = "link_ger_unidade";
$proto25["m_columns"][] = "ultimousuario";
$proto25["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "ger_arquivos_unidades";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "ger_arquivos_unidades";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_arquivos_unidades";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_arquivos_unidades = createSqlQuery_ger_arquivos_unidades();


	
		;

									

$tdatager_arquivos_unidades[".sqlquery"] = $queryData_ger_arquivos_unidades;

$tableEvents["ger_arquivos_unidades"] = new eventsBase;
$tdatager_arquivos_unidades[".hasEvents"] = false;

?>