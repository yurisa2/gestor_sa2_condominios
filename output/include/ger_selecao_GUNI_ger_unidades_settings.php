<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_selecao_GUNI_ger_unidades = array();
	$tdatager_selecao_GUNI_ger_unidades[".truncateText"] = true;
	$tdatager_selecao_GUNI_ger_unidades[".NumberOfChars"] = 80;
	$tdatager_selecao_GUNI_ger_unidades[".ShortName"] = "ger_selecao_GUNI_ger_unidades";
	$tdatager_selecao_GUNI_ger_unidades[".OwnerID"] = "";
	$tdatager_selecao_GUNI_ger_unidades[".OriginalTable"] = "ger_selecao_GUNI_ger_unidades";

//	field labels
$fieldLabelsger_selecao_GUNI_ger_unidades = array();
$fieldToolTipsger_selecao_GUNI_ger_unidades = array();
$pageTitlesger_selecao_GUNI_ger_unidades = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"] = array();
	$pageTitlesger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"] = array();
	$fieldLabelsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"]["idUnidade"] = "idUnidade";
	$fieldToolTipsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"]["idUnidade"] = "";
	$fieldLabelsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"]["grupounidade"] = "grupounidade";
	$fieldToolTipsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"]["grupounidade"] = "";
	$fieldLabelsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"]["grupounidade2"] = "Grupounidade2";
	$fieldToolTipsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"]["grupounidade2"] = "";
	if (count($fieldToolTipsger_selecao_GUNI_ger_unidades["Portuguese(Brazil)"]))
		$tdatager_selecao_GUNI_ger_unidades[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_selecao_GUNI_ger_unidades[""] = array();
	$fieldToolTipsger_selecao_GUNI_ger_unidades[""] = array();
	$pageTitlesger_selecao_GUNI_ger_unidades[""] = array();
	if (count($fieldToolTipsger_selecao_GUNI_ger_unidades[""]))
		$tdatager_selecao_GUNI_ger_unidades[".isUseToolTips"] = true;
}


	$tdatager_selecao_GUNI_ger_unidades[".NCSearch"] = true;



$tdatager_selecao_GUNI_ger_unidades[".shortTableName"] = "ger_selecao_GUNI_ger_unidades";
$tdatager_selecao_GUNI_ger_unidades[".nSecOptions"] = 0;
$tdatager_selecao_GUNI_ger_unidades[".recsPerRowList"] = 1;
$tdatager_selecao_GUNI_ger_unidades[".recsPerRowPrint"] = 1;
$tdatager_selecao_GUNI_ger_unidades[".mainTableOwnerID"] = "";
$tdatager_selecao_GUNI_ger_unidades[".moveNext"] = 1;
$tdatager_selecao_GUNI_ger_unidades[".entityType"] = 0;

$tdatager_selecao_GUNI_ger_unidades[".strOriginalTableName"] = "ger_selecao_GUNI_ger_unidades";





$tdatager_selecao_GUNI_ger_unidades[".showAddInPopup"] = false;

$tdatager_selecao_GUNI_ger_unidades[".showEditInPopup"] = false;

$tdatager_selecao_GUNI_ger_unidades[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_selecao_GUNI_ger_unidades[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_selecao_GUNI_ger_unidades[".fieldsForRegister"] = array();

$tdatager_selecao_GUNI_ger_unidades[".listAjax"] = false;

	$tdatager_selecao_GUNI_ger_unidades[".audit"] = true;

	$tdatager_selecao_GUNI_ger_unidades[".locking"] = true;









$tdatager_selecao_GUNI_ger_unidades[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_selecao_GUNI_ger_unidades[".searchSaving"] = false;
//

$tdatager_selecao_GUNI_ger_unidades[".showSearchPanel"] = true;
		$tdatager_selecao_GUNI_ger_unidades[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_selecao_GUNI_ger_unidades[".isUseAjaxSuggest"] = false;
else
	$tdatager_selecao_GUNI_ger_unidades[".isUseAjaxSuggest"] = true;

$tdatager_selecao_GUNI_ger_unidades[".rowHighlite"] = true;



$tdatager_selecao_GUNI_ger_unidades[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_selecao_GUNI_ger_unidades[".isUseTimeForSearch"] = false;





$tdatager_selecao_GUNI_ger_unidades[".allSearchFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".filterFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".requiredSearchFields"] = array();



$tdatager_selecao_GUNI_ger_unidades[".googleLikeFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".googleLikeFields"][] = "idUnidade";
$tdatager_selecao_GUNI_ger_unidades[".googleLikeFields"][] = "grupounidade";
$tdatager_selecao_GUNI_ger_unidades[".googleLikeFields"][] = "grupounidade2";


$tdatager_selecao_GUNI_ger_unidades[".advSearchFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".advSearchFields"][] = "idUnidade";
$tdatager_selecao_GUNI_ger_unidades[".advSearchFields"][] = "grupounidade";
$tdatager_selecao_GUNI_ger_unidades[".advSearchFields"][] = "grupounidade2";

$tdatager_selecao_GUNI_ger_unidades[".tableType"] = "list";

$tdatager_selecao_GUNI_ger_unidades[".printerPageOrientation"] = 0;
$tdatager_selecao_GUNI_ger_unidades[".nPrinterPageScale"] = 100;

$tdatager_selecao_GUNI_ger_unidades[".nPrinterSplitRecords"] = 40;

$tdatager_selecao_GUNI_ger_unidades[".nPrinterPDFSplitRecords"] = 40;



$tdatager_selecao_GUNI_ger_unidades[".geocodingEnabled"] = false;





$tdatager_selecao_GUNI_ger_unidades[".listGridLayout"] = 3;

$tdatager_selecao_GUNI_ger_unidades[".isDisplayLoading"] = true;


$tdatager_selecao_GUNI_ger_unidades[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatager_selecao_GUNI_ger_unidades[".pageSize"] = 20;

$tdatager_selecao_GUNI_ger_unidades[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_selecao_GUNI_ger_unidades[".strOrderBy"] = $tstrOrderBy;

$tdatager_selecao_GUNI_ger_unidades[".orderindexes"] = array();

$tdatager_selecao_GUNI_ger_unidades[".sqlHead"] = "SELECT idUnidade,  	grupounidade,  	grupounidade2";
$tdatager_selecao_GUNI_ger_unidades[".sqlFrom"] = "FROM ger_selecao_GUNI_ger_unidades";
$tdatager_selecao_GUNI_ger_unidades[".sqlWhereExpr"] = "";
$tdatager_selecao_GUNI_ger_unidades[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_selecao_GUNI_ger_unidades[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_selecao_GUNI_ger_unidades[".arrGroupsPerPage"] = $arrGPP;

$tdatager_selecao_GUNI_ger_unidades[".highlightSearchResults"] = true;

$tableKeysger_selecao_GUNI_ger_unidades = array();
$tdatager_selecao_GUNI_ger_unidades[".Keys"] = $tableKeysger_selecao_GUNI_ger_unidades;

$tdatager_selecao_GUNI_ger_unidades[".listFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".listFields"][] = "grupounidade2";

$tdatager_selecao_GUNI_ger_unidades[".hideMobileList"] = array();


$tdatager_selecao_GUNI_ger_unidades[".viewFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".viewFields"][] = "grupounidade2";

$tdatager_selecao_GUNI_ger_unidades[".addFields"] = array();

$tdatager_selecao_GUNI_ger_unidades[".masterListFields"] = array();

$tdatager_selecao_GUNI_ger_unidades[".inlineAddFields"] = array();

$tdatager_selecao_GUNI_ger_unidades[".editFields"] = array();

$tdatager_selecao_GUNI_ger_unidades[".inlineEditFields"] = array();

$tdatager_selecao_GUNI_ger_unidades[".exportFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".exportFields"][] = "grupounidade2";

$tdatager_selecao_GUNI_ger_unidades[".importFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".importFields"][] = "idUnidade";
$tdatager_selecao_GUNI_ger_unidades[".importFields"][] = "grupounidade";
$tdatager_selecao_GUNI_ger_unidades[".importFields"][] = "grupounidade2";

$tdatager_selecao_GUNI_ger_unidades[".printFields"] = array();
$tdatager_selecao_GUNI_ger_unidades[".printFields"][] = "grupounidade2";

//	idUnidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idUnidade";
	$fdata["GoodName"] = "idUnidade";
	$fdata["ownerTable"] = "ger_selecao_GUNI_ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_selecao_GUNI_ger_unidades","idUnidade");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idUnidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idUnidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_selecao_GUNI_ger_unidades["idUnidade"] = $fdata;
//	grupounidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "grupounidade";
	$fdata["GoodName"] = "grupounidade";
	$fdata["ownerTable"] = "ger_selecao_GUNI_ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_selecao_GUNI_ger_unidades","grupounidade");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "grupounidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupounidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_selecao_GUNI_ger_unidades["grupounidade"] = $fdata;
//	grupounidade2
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "grupounidade2";
	$fdata["GoodName"] = "grupounidade2";
	$fdata["ownerTable"] = "ger_selecao_GUNI_ger_unidades";
	$fdata["Label"] = GetFieldLabel("ger_selecao_GUNI_ger_unidades","grupounidade2");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "grupounidade2";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupounidade2";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_selecao_GUNI_ger_unidades["grupounidade2"] = $fdata;


$tables_data["ger_selecao_GUNI_ger_unidades"]=&$tdatager_selecao_GUNI_ger_unidades;
$field_labels["ger_selecao_GUNI_ger_unidades"] = &$fieldLabelsger_selecao_GUNI_ger_unidades;
$fieldToolTips["ger_selecao_GUNI_ger_unidades"] = &$fieldToolTipsger_selecao_GUNI_ger_unidades;
$page_titles["ger_selecao_GUNI_ger_unidades"] = &$pageTitlesger_selecao_GUNI_ger_unidades;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_selecao_GUNI_ger_unidades"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_selecao_GUNI_ger_unidades"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_selecao_GUNI_ger_unidades()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idUnidade,  	grupounidade,  	grupounidade2";
$proto0["m_strFrom"] = "FROM ger_selecao_GUNI_ger_unidades";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idUnidade",
	"m_strTable" => "ger_selecao_GUNI_ger_unidades",
	"m_srcTableName" => "ger_selecao_GUNI_ger_unidades"
));

$proto6["m_sql"] = "idUnidade";
$proto6["m_srcTableName"] = "ger_selecao_GUNI_ger_unidades";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "grupounidade",
	"m_strTable" => "ger_selecao_GUNI_ger_unidades",
	"m_srcTableName" => "ger_selecao_GUNI_ger_unidades"
));

$proto8["m_sql"] = "grupounidade";
$proto8["m_srcTableName"] = "ger_selecao_GUNI_ger_unidades";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "grupounidade2",
	"m_strTable" => "ger_selecao_GUNI_ger_unidades",
	"m_srcTableName" => "ger_selecao_GUNI_ger_unidades"
));

$proto10["m_sql"] = "grupounidade2";
$proto10["m_srcTableName"] = "ger_selecao_GUNI_ger_unidades";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "ger_selecao_GUNI_ger_unidades";
$proto13["m_srcTableName"] = "ger_selecao_GUNI_ger_unidades";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "idUnidade";
$proto13["m_columns"][] = "grupounidade";
$proto13["m_columns"][] = "grupounidade2";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "ger_selecao_GUNI_ger_unidades";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "ger_selecao_GUNI_ger_unidades";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_selecao_GUNI_ger_unidades";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_selecao_GUNI_ger_unidades = createSqlQuery_ger_selecao_GUNI_ger_unidades();


	
		;

			

$tdatager_selecao_GUNI_ger_unidades[".sqlquery"] = $queryData_ger_selecao_GUNI_ger_unidades;

$tableEvents["ger_selecao_GUNI_ger_unidades"] = new eventsBase;
$tdatager_selecao_GUNI_ger_unidades[".hasEvents"] = false;

?>