<?php
function DisplayMasterTableInfo_ger_unidades($params)
{
	$keys = $params["keys"];
	$detailtable = $params["detailtable"];
	$data = $params["masterRecordData"];
	
	$xt = new Xtempl();
	$tName = "ger_unidades";
	$xt->eventsObject = getEventObject($tName);
	
	include_once(getabspath('classes/listpage.php'));
	include_once(getabspath('classes/listpage_simple.php'));
	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = LIST_MASTER;
	$mParams["pageType"] = PAGE_LIST;
	$mParams["flyId"] = $params["recId"];
	$masterPage = ListPage::createListPage($tName, $mParams);
	
	$settings = $masterPage->pSet;
	$viewControls = new ViewControlsContainer($settings, PAGE_LIST, $masterPage);
	
	$keysAssoc = array();
	$showKeys = "";	

	if($detailtable == "fin_cobranca")
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if($detailtable == "ger_moradores")
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if($detailtable == "fin_cobranca_atualizacao_aberto")
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if($detailtable == "jur_processos")
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if($detailtable == "ger_arquivos_unidades")
	{
		$keysAssoc["idUnidade"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idUnidade", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_unidades","idUnidade").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if( !$data || !count($data) )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));
	
	$keylink = "";
	$keylink.= "&key1=".runner_htmlspecialchars(rawurlencode(@$data["idUnidade"]));
	
	$xt->assign("grupounidade_mastervalue", $viewControls->showDBValue("grupounidade", $data, $keylink));
	$format = $settings->getViewFormat("grupounidade");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("grupounidade")))
		$class = ' rnr-field-number';
		
	$xt->assign("grupounidade_class", $class); // add class for field header as field value
	$xt->assign("link_ger_cadastro_mastervalue", $viewControls->showDBValue("link_ger_cadastro", $data, $keylink));
	$format = $settings->getViewFormat("link_ger_cadastro");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("link_ger_cadastro")))
		$class = ' rnr-field-number';
		
	$xt->assign("link_ger_cadastro_class", $class); // add class for field header as field value
	$xt->assign("dia_pref_venc_mastervalue", $viewControls->showDBValue("dia_pref_venc", $data, $keylink));
	$format = $settings->getViewFormat("dia_pref_venc");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("dia_pref_venc")))
		$class = ' rnr-field-number';
		
	$xt->assign("dia_pref_venc_class", $class); // add class for field header as field value
	$xt->assign("ativo_mastervalue", $viewControls->showDBValue("ativo", $data, $keylink));
	$format = $settings->getViewFormat("ativo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("ativo")))
		$class = ' rnr-field-number';
		
	$xt->assign("ativo_class", $class); // add class for field header as field value
	$xt->assign("obs_mastervalue", $viewControls->showDBValue("obs", $data, $keylink));
	$format = $settings->getViewFormat("obs");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("obs")))
		$class = ' rnr-field-number';
		
	$xt->assign("obs_class", $class); // add class for field header as field value
	$xt->assign("frideal_mastervalue", $viewControls->showDBValue("frideal", $data, $keylink));
	$format = $settings->getViewFormat("frideal");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("frideal")))
		$class = ' rnr-field-number';
		
	$xt->assign("frideal_class", $class); // add class for field header as field value

	$layout = GetPageLayout("ger_unidades", 'masterlist');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');
	
	$xt->displayPartial(GetTemplateName("ger_unidades", "masterlist"));
}

?>