<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_cadastro = array();
	$tdatager_cadastro[".truncateText"] = true;
	$tdatager_cadastro[".NumberOfChars"] = 80;
	$tdatager_cadastro[".ShortName"] = "ger_cadastro";
	$tdatager_cadastro[".OwnerID"] = "";
	$tdatager_cadastro[".OriginalTable"] = "ger_cadastro";

//	field labels
$fieldLabelsger_cadastro = array();
$fieldToolTipsger_cadastro = array();
$pageTitlesger_cadastro = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_cadastro["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"] = array();
	$pageTitlesger_cadastro["Portuguese(Brazil)"] = array();
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["idCadastro"] = "Código";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["idCadastro"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["rg"] = "RG";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["rg"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["cpf"] = "CPF";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["cpf"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["TelPrim"] = "Tel. 1";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["TelPrim"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["CompPrim"] = "Complemento 1";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["CompPrim"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["TelSec"] = "Tel. 2";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["TelSec"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["CompSec"] = "Complemento 2";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["CompSec"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["TelTerc"] = "Tel. 3";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["TelTerc"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["CompTerc"] = "Complemento 3";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["CompTerc"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["log"] = "log";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["log"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["end"] = "Endereço";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["end"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["num"] = "num";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["num"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["comp"] = "Comp";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["comp"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["bairro"] = "Bairro";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["bairro"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["cep"] = "Cep";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["cep"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["cidade"] = "Cidade";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["cidade"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["uf"] = "UF";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["uf"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["situacao"] = "Situação";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["situacao"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["morador"] = "Morador";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["morador"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["cepnet"] = "cepnet";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["cepnet"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["ativo"] = "Ativo";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["ativo"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["processado"] = "Processado";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["processado"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["email"] = "Email";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["email"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["webuser"] = "Nome de usuário";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["webuser"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsger_cadastro["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsger_cadastro["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsger_cadastro["Portuguese(Brazil)"]))
		$tdatager_cadastro[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_cadastro[""] = array();
	$fieldToolTipsger_cadastro[""] = array();
	$pageTitlesger_cadastro[""] = array();
	if (count($fieldToolTipsger_cadastro[""]))
		$tdatager_cadastro[".isUseToolTips"] = true;
}


	$tdatager_cadastro[".NCSearch"] = true;



$tdatager_cadastro[".shortTableName"] = "ger_cadastro";
$tdatager_cadastro[".nSecOptions"] = 0;
$tdatager_cadastro[".recsPerRowList"] = 1;
$tdatager_cadastro[".recsPerRowPrint"] = 1;
$tdatager_cadastro[".mainTableOwnerID"] = "";
$tdatager_cadastro[".moveNext"] = 1;
$tdatager_cadastro[".entityType"] = 0;

$tdatager_cadastro[".strOriginalTableName"] = "ger_cadastro";





$tdatager_cadastro[".showAddInPopup"] = false;

$tdatager_cadastro[".showEditInPopup"] = false;

$tdatager_cadastro[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_cadastro[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_cadastro[".fieldsForRegister"] = array();

$tdatager_cadastro[".listAjax"] = false;

	$tdatager_cadastro[".audit"] = true;

	$tdatager_cadastro[".locking"] = true;

$tdatager_cadastro[".edit"] = true;
$tdatager_cadastro[".afterEditAction"] = 1;
$tdatager_cadastro[".closePopupAfterEdit"] = 1;
$tdatager_cadastro[".afterEditActionDetTable"] = "";

$tdatager_cadastro[".add"] = true;
$tdatager_cadastro[".afterAddAction"] = 1;
$tdatager_cadastro[".closePopupAfterAdd"] = 1;
$tdatager_cadastro[".afterAddActionDetTable"] = "";

$tdatager_cadastro[".list"] = true;

$tdatager_cadastro[".view"] = true;


$tdatager_cadastro[".exportTo"] = true;

$tdatager_cadastro[".printFriendly"] = true;

$tdatager_cadastro[".delete"] = true;

$tdatager_cadastro[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_cadastro[".searchSaving"] = false;
//

$tdatager_cadastro[".showSearchPanel"] = true;
		$tdatager_cadastro[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_cadastro[".isUseAjaxSuggest"] = false;
else
	$tdatager_cadastro[".isUseAjaxSuggest"] = true;

$tdatager_cadastro[".rowHighlite"] = true;



$tdatager_cadastro[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_cadastro[".isUseTimeForSearch"] = false;




$tdatager_cadastro[".detailsLinksOnList"] = "1";

$tdatager_cadastro[".allSearchFields"] = array();
$tdatager_cadastro[".filterFields"] = array();
$tdatager_cadastro[".requiredSearchFields"] = array();

$tdatager_cadastro[".allSearchFields"][] = "nome";
	$tdatager_cadastro[".allSearchFields"][] = "rg";
	$tdatager_cadastro[".allSearchFields"][] = "cpf";
	$tdatager_cadastro[".allSearchFields"][] = "TelPrim";
	$tdatager_cadastro[".allSearchFields"][] = "CompPrim";
	$tdatager_cadastro[".allSearchFields"][] = "TelSec";
	$tdatager_cadastro[".allSearchFields"][] = "CompSec";
	$tdatager_cadastro[".allSearchFields"][] = "TelTerc";
	$tdatager_cadastro[".allSearchFields"][] = "CompTerc";
	$tdatager_cadastro[".allSearchFields"][] = "log";
	$tdatager_cadastro[".allSearchFields"][] = "end";
	$tdatager_cadastro[".allSearchFields"][] = "num";
	$tdatager_cadastro[".allSearchFields"][] = "comp";
	$tdatager_cadastro[".allSearchFields"][] = "bairro";
	$tdatager_cadastro[".allSearchFields"][] = "cep";
	$tdatager_cadastro[".allSearchFields"][] = "cidade";
	$tdatager_cadastro[".allSearchFields"][] = "uf";
	$tdatager_cadastro[".allSearchFields"][] = "morador";
	$tdatager_cadastro[".allSearchFields"][] = "email";
	$tdatager_cadastro[".allSearchFields"][] = "ativo";
	$tdatager_cadastro[".allSearchFields"][] = "situacao";
	$tdatager_cadastro[".allSearchFields"][] = "obs";
	$tdatager_cadastro[".allSearchFields"][] = "webuser";
	$tdatager_cadastro[".allSearchFields"][] = "ultimousuario";
	$tdatager_cadastro[".allSearchFields"][] = "ultimaalteracao";
	

$tdatager_cadastro[".googleLikeFields"] = array();
$tdatager_cadastro[".googleLikeFields"][] = "nome";
$tdatager_cadastro[".googleLikeFields"][] = "rg";
$tdatager_cadastro[".googleLikeFields"][] = "cpf";
$tdatager_cadastro[".googleLikeFields"][] = "TelPrim";
$tdatager_cadastro[".googleLikeFields"][] = "CompPrim";
$tdatager_cadastro[".googleLikeFields"][] = "TelSec";
$tdatager_cadastro[".googleLikeFields"][] = "CompSec";
$tdatager_cadastro[".googleLikeFields"][] = "TelTerc";
$tdatager_cadastro[".googleLikeFields"][] = "CompTerc";
$tdatager_cadastro[".googleLikeFields"][] = "log";
$tdatager_cadastro[".googleLikeFields"][] = "end";
$tdatager_cadastro[".googleLikeFields"][] = "num";
$tdatager_cadastro[".googleLikeFields"][] = "comp";
$tdatager_cadastro[".googleLikeFields"][] = "bairro";
$tdatager_cadastro[".googleLikeFields"][] = "cep";
$tdatager_cadastro[".googleLikeFields"][] = "cidade";
$tdatager_cadastro[".googleLikeFields"][] = "uf";
$tdatager_cadastro[".googleLikeFields"][] = "obs";
$tdatager_cadastro[".googleLikeFields"][] = "situacao";
$tdatager_cadastro[".googleLikeFields"][] = "morador";
$tdatager_cadastro[".googleLikeFields"][] = "ativo";
$tdatager_cadastro[".googleLikeFields"][] = "email";
$tdatager_cadastro[".googleLikeFields"][] = "webuser";
$tdatager_cadastro[".googleLikeFields"][] = "ultimousuario";
$tdatager_cadastro[".googleLikeFields"][] = "ultimaalteracao";


$tdatager_cadastro[".advSearchFields"] = array();
$tdatager_cadastro[".advSearchFields"][] = "nome";
$tdatager_cadastro[".advSearchFields"][] = "rg";
$tdatager_cadastro[".advSearchFields"][] = "cpf";
$tdatager_cadastro[".advSearchFields"][] = "TelPrim";
$tdatager_cadastro[".advSearchFields"][] = "CompPrim";
$tdatager_cadastro[".advSearchFields"][] = "TelSec";
$tdatager_cadastro[".advSearchFields"][] = "CompSec";
$tdatager_cadastro[".advSearchFields"][] = "TelTerc";
$tdatager_cadastro[".advSearchFields"][] = "CompTerc";
$tdatager_cadastro[".advSearchFields"][] = "log";
$tdatager_cadastro[".advSearchFields"][] = "end";
$tdatager_cadastro[".advSearchFields"][] = "num";
$tdatager_cadastro[".advSearchFields"][] = "comp";
$tdatager_cadastro[".advSearchFields"][] = "bairro";
$tdatager_cadastro[".advSearchFields"][] = "cep";
$tdatager_cadastro[".advSearchFields"][] = "cidade";
$tdatager_cadastro[".advSearchFields"][] = "uf";
$tdatager_cadastro[".advSearchFields"][] = "morador";
$tdatager_cadastro[".advSearchFields"][] = "email";
$tdatager_cadastro[".advSearchFields"][] = "ativo";
$tdatager_cadastro[".advSearchFields"][] = "situacao";
$tdatager_cadastro[".advSearchFields"][] = "obs";
$tdatager_cadastro[".advSearchFields"][] = "webuser";
$tdatager_cadastro[".advSearchFields"][] = "ultimousuario";
$tdatager_cadastro[".advSearchFields"][] = "ultimaalteracao";

$tdatager_cadastro[".tableType"] = "list";

$tdatager_cadastro[".printerPageOrientation"] = 0;
$tdatager_cadastro[".nPrinterPageScale"] = 100;

$tdatager_cadastro[".nPrinterSplitRecords"] = 40;

$tdatager_cadastro[".nPrinterPDFSplitRecords"] = 40;



$tdatager_cadastro[".geocodingEnabled"] = false;





$tdatager_cadastro[".listGridLayout"] = 3;

$tdatager_cadastro[".isDisplayLoading"] = true;


$tdatager_cadastro[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf

$tdatager_cadastro[".totalsFields"] = array();
$tdatager_cadastro[".totalsFields"][] = array(
	"fName" => "nome",
	"numRows" => 0,
	"totalsType" => "COUNT",
	"viewFormat" => '');

$tdatager_cadastro[".pageSize"] = 20;

$tdatager_cadastro[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY nome";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_cadastro[".strOrderBy"] = $tstrOrderBy;

$tdatager_cadastro[".orderindexes"] = array();
$tdatager_cadastro[".orderindexes"][] = array(2, (1 ? "ASC" : "DESC"), "nome");

$tdatager_cadastro[".sqlHead"] = "select idCadastro,  nome,  rg,  cpf,  TelPrim,  CompPrim,  TelSec,  CompSec,  TelTerc,  CompTerc,  log,  `end`,  num,  comp,  bairro,  cep,  cidade,  uf,  obs,  situacao,  morador,  cepnet,  ativo,  processado,  email,  webuser,  ultimousuario,  ultimaalteracao";
$tdatager_cadastro[".sqlFrom"] = "FROM ger_cadastro";
$tdatager_cadastro[".sqlWhereExpr"] = "";
$tdatager_cadastro[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_cadastro[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_cadastro[".arrGroupsPerPage"] = $arrGPP;

$tdatager_cadastro[".highlightSearchResults"] = true;

$tableKeysger_cadastro = array();
$tableKeysger_cadastro[] = "idCadastro";
$tdatager_cadastro[".Keys"] = $tableKeysger_cadastro;

$tdatager_cadastro[".listFields"] = array();
$tdatager_cadastro[".listFields"][] = "nome";
$tdatager_cadastro[".listFields"][] = "TelPrim";
$tdatager_cadastro[".listFields"][] = "TelSec";
$tdatager_cadastro[".listFields"][] = "morador";
$tdatager_cadastro[".listFields"][] = "ativo";
$tdatager_cadastro[".listFields"][] = "situacao";
$tdatager_cadastro[".listFields"][] = "obs";

$tdatager_cadastro[".hideMobileList"] = array();


$tdatager_cadastro[".viewFields"] = array();
$tdatager_cadastro[".viewFields"][] = "nome";
$tdatager_cadastro[".viewFields"][] = "idCadastro";
$tdatager_cadastro[".viewFields"][] = "rg";
$tdatager_cadastro[".viewFields"][] = "cpf";
$tdatager_cadastro[".viewFields"][] = "TelPrim";
$tdatager_cadastro[".viewFields"][] = "CompPrim";
$tdatager_cadastro[".viewFields"][] = "TelSec";
$tdatager_cadastro[".viewFields"][] = "CompSec";
$tdatager_cadastro[".viewFields"][] = "TelTerc";
$tdatager_cadastro[".viewFields"][] = "CompTerc";
$tdatager_cadastro[".viewFields"][] = "log";
$tdatager_cadastro[".viewFields"][] = "end";
$tdatager_cadastro[".viewFields"][] = "num";
$tdatager_cadastro[".viewFields"][] = "comp";
$tdatager_cadastro[".viewFields"][] = "bairro";
$tdatager_cadastro[".viewFields"][] = "cep";
$tdatager_cadastro[".viewFields"][] = "cidade";
$tdatager_cadastro[".viewFields"][] = "uf";
$tdatager_cadastro[".viewFields"][] = "morador";
$tdatager_cadastro[".viewFields"][] = "email";
$tdatager_cadastro[".viewFields"][] = "ativo";
$tdatager_cadastro[".viewFields"][] = "situacao";
$tdatager_cadastro[".viewFields"][] = "obs";
$tdatager_cadastro[".viewFields"][] = "webuser";
$tdatager_cadastro[".viewFields"][] = "ultimousuario";
$tdatager_cadastro[".viewFields"][] = "ultimaalteracao";

$tdatager_cadastro[".addFields"] = array();
$tdatager_cadastro[".addFields"][] = "nome";
$tdatager_cadastro[".addFields"][] = "rg";
$tdatager_cadastro[".addFields"][] = "cpf";
$tdatager_cadastro[".addFields"][] = "TelPrim";
$tdatager_cadastro[".addFields"][] = "CompPrim";
$tdatager_cadastro[".addFields"][] = "TelSec";
$tdatager_cadastro[".addFields"][] = "CompSec";
$tdatager_cadastro[".addFields"][] = "TelTerc";
$tdatager_cadastro[".addFields"][] = "CompTerc";
$tdatager_cadastro[".addFields"][] = "log";
$tdatager_cadastro[".addFields"][] = "end";
$tdatager_cadastro[".addFields"][] = "num";
$tdatager_cadastro[".addFields"][] = "comp";
$tdatager_cadastro[".addFields"][] = "bairro";
$tdatager_cadastro[".addFields"][] = "cep";
$tdatager_cadastro[".addFields"][] = "cidade";
$tdatager_cadastro[".addFields"][] = "uf";
$tdatager_cadastro[".addFields"][] = "morador";
$tdatager_cadastro[".addFields"][] = "email";
$tdatager_cadastro[".addFields"][] = "ativo";
$tdatager_cadastro[".addFields"][] = "situacao";
$tdatager_cadastro[".addFields"][] = "obs";
$tdatager_cadastro[".addFields"][] = "webuser";

$tdatager_cadastro[".masterListFields"] = array();
$tdatager_cadastro[".masterListFields"][] = "nome";
$tdatager_cadastro[".masterListFields"][] = "TelPrim";
$tdatager_cadastro[".masterListFields"][] = "TelSec";
$tdatager_cadastro[".masterListFields"][] = "morador";
$tdatager_cadastro[".masterListFields"][] = "ativo";
$tdatager_cadastro[".masterListFields"][] = "situacao";
$tdatager_cadastro[".masterListFields"][] = "obs";

$tdatager_cadastro[".inlineAddFields"] = array();

$tdatager_cadastro[".editFields"] = array();
$tdatager_cadastro[".editFields"][] = "nome";
$tdatager_cadastro[".editFields"][] = "idCadastro";
$tdatager_cadastro[".editFields"][] = "rg";
$tdatager_cadastro[".editFields"][] = "cpf";
$tdatager_cadastro[".editFields"][] = "TelPrim";
$tdatager_cadastro[".editFields"][] = "CompPrim";
$tdatager_cadastro[".editFields"][] = "TelSec";
$tdatager_cadastro[".editFields"][] = "CompSec";
$tdatager_cadastro[".editFields"][] = "TelTerc";
$tdatager_cadastro[".editFields"][] = "CompTerc";
$tdatager_cadastro[".editFields"][] = "log";
$tdatager_cadastro[".editFields"][] = "end";
$tdatager_cadastro[".editFields"][] = "num";
$tdatager_cadastro[".editFields"][] = "comp";
$tdatager_cadastro[".editFields"][] = "bairro";
$tdatager_cadastro[".editFields"][] = "cep";
$tdatager_cadastro[".editFields"][] = "cidade";
$tdatager_cadastro[".editFields"][] = "uf";
$tdatager_cadastro[".editFields"][] = "morador";
$tdatager_cadastro[".editFields"][] = "email";
$tdatager_cadastro[".editFields"][] = "ativo";
$tdatager_cadastro[".editFields"][] = "situacao";
$tdatager_cadastro[".editFields"][] = "obs";
$tdatager_cadastro[".editFields"][] = "webuser";

$tdatager_cadastro[".inlineEditFields"] = array();

$tdatager_cadastro[".exportFields"] = array();
$tdatager_cadastro[".exportFields"][] = "nome";
$tdatager_cadastro[".exportFields"][] = "rg";
$tdatager_cadastro[".exportFields"][] = "cpf";
$tdatager_cadastro[".exportFields"][] = "TelPrim";
$tdatager_cadastro[".exportFields"][] = "CompPrim";
$tdatager_cadastro[".exportFields"][] = "TelSec";
$tdatager_cadastro[".exportFields"][] = "CompSec";
$tdatager_cadastro[".exportFields"][] = "TelTerc";
$tdatager_cadastro[".exportFields"][] = "CompTerc";
$tdatager_cadastro[".exportFields"][] = "log";
$tdatager_cadastro[".exportFields"][] = "end";
$tdatager_cadastro[".exportFields"][] = "num";
$tdatager_cadastro[".exportFields"][] = "comp";
$tdatager_cadastro[".exportFields"][] = "bairro";
$tdatager_cadastro[".exportFields"][] = "cep";
$tdatager_cadastro[".exportFields"][] = "cidade";
$tdatager_cadastro[".exportFields"][] = "uf";
$tdatager_cadastro[".exportFields"][] = "morador";
$tdatager_cadastro[".exportFields"][] = "email";
$tdatager_cadastro[".exportFields"][] = "ativo";
$tdatager_cadastro[".exportFields"][] = "situacao";
$tdatager_cadastro[".exportFields"][] = "obs";
$tdatager_cadastro[".exportFields"][] = "webuser";
$tdatager_cadastro[".exportFields"][] = "ultimousuario";
$tdatager_cadastro[".exportFields"][] = "ultimaalteracao";

$tdatager_cadastro[".importFields"] = array();

$tdatager_cadastro[".printFields"] = array();
$tdatager_cadastro[".printFields"][] = "nome";
$tdatager_cadastro[".printFields"][] = "rg";
$tdatager_cadastro[".printFields"][] = "cpf";
$tdatager_cadastro[".printFields"][] = "TelPrim";
$tdatager_cadastro[".printFields"][] = "CompPrim";
$tdatager_cadastro[".printFields"][] = "TelSec";
$tdatager_cadastro[".printFields"][] = "CompSec";
$tdatager_cadastro[".printFields"][] = "TelTerc";
$tdatager_cadastro[".printFields"][] = "CompTerc";
$tdatager_cadastro[".printFields"][] = "log";
$tdatager_cadastro[".printFields"][] = "end";
$tdatager_cadastro[".printFields"][] = "num";
$tdatager_cadastro[".printFields"][] = "comp";
$tdatager_cadastro[".printFields"][] = "bairro";
$tdatager_cadastro[".printFields"][] = "cep";
$tdatager_cadastro[".printFields"][] = "cidade";
$tdatager_cadastro[".printFields"][] = "uf";
$tdatager_cadastro[".printFields"][] = "morador";
$tdatager_cadastro[".printFields"][] = "email";
$tdatager_cadastro[".printFields"][] = "ativo";
$tdatager_cadastro[".printFields"][] = "situacao";
$tdatager_cadastro[".printFields"][] = "obs";
$tdatager_cadastro[".printFields"][] = "webuser";

//	idCadastro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idCadastro";
	$fdata["GoodName"] = "idCadastro";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","idCadastro");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idCadastro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idCadastro";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_cadastro["idCadastro"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["nome"] = $fdata;
//	rg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "rg";
	$fdata["GoodName"] = "rg";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","rg");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "rg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "rg";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["rg"] = $fdata;
//	cpf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "cpf";
	$fdata["GoodName"] = "cpf";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","cpf");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cpf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cpf";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["cpf"] = $fdata;
//	TelPrim
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "TelPrim";
	$fdata["GoodName"] = "TelPrim";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","TelPrim");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "TelPrim";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "TelPrim";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["TelPrim"] = $fdata;
//	CompPrim
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "CompPrim";
	$fdata["GoodName"] = "CompPrim";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","CompPrim");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "CompPrim";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CompPrim";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["CompPrim"] = $fdata;
//	TelSec
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "TelSec";
	$fdata["GoodName"] = "TelSec";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","TelSec");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "TelSec";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "TelSec";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["TelSec"] = $fdata;
//	CompSec
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "CompSec";
	$fdata["GoodName"] = "CompSec";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","CompSec");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "CompSec";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CompSec";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["CompSec"] = $fdata;
//	TelTerc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "TelTerc";
	$fdata["GoodName"] = "TelTerc";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","TelTerc");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "TelTerc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "TelTerc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["TelTerc"] = $fdata;
//	CompTerc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "CompTerc";
	$fdata["GoodName"] = "CompTerc";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","CompTerc");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "CompTerc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CompTerc";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["CompTerc"] = $fdata;
//	log
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "log";
	$fdata["GoodName"] = "log";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","log");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "log";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "log";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "exp_ger_lista_logs";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "log";
	$edata["LinkFieldType"] = 129;
	$edata["DisplayField"] = "log";

	
	$edata["LookupOrderBy"] = "log";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["log"] = $fdata;
//	end
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "end";
	$fdata["GoodName"] = "end";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","end");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "end";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`end`";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["end"] = $fdata;
//	num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "num";
	$fdata["GoodName"] = "num";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","num");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "num";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "num";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["num"] = $fdata;
//	comp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "comp";
	$fdata["GoodName"] = "comp";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","comp");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "comp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comp";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["comp"] = $fdata;
//	bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "bairro";
	$fdata["GoodName"] = "bairro";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","bairro");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["bairro"] = $fdata;
//	cep
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "cep";
	$fdata["GoodName"] = "cep";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","cep");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cep";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cep";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["cep"] = $fdata;
//	cidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "cidade";
	$fdata["GoodName"] = "cidade";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","cidade");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cidade";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["cidade"] = $fdata;
//	uf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "uf";
	$fdata["GoodName"] = "uf";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","uf");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "uf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "uf";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_lista_uf";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "uf";
	$edata["LinkFieldType"] = 129;
	$edata["DisplayField"] = "uf";

	
	$edata["LookupOrderBy"] = "uf";

	
	
		$edata["AllowToAdd"] = true;

	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["uf"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","obs");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["obs"] = $fdata;
//	situacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "situacao";
	$fdata["GoodName"] = "situacao";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","situacao");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "situacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "situacao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["situacao"] = $fdata;
//	morador
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "morador";
	$fdata["GoodName"] = "morador";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","morador");
	$fdata["FieldType"] = 16;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "morador";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "morador";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_cadastro["morador"] = $fdata;
//	cepnet
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "cepnet";
	$fdata["GoodName"] = "cepnet";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","cepnet");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "cepnet";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cepnet";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_cadastro["cepnet"] = $fdata;
//	ativo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "ativo";
	$fdata["GoodName"] = "ativo";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","ativo");
	$fdata["FieldType"] = 16;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ativo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ativo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatager_cadastro["ativo"] = $fdata;
//	processado
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "processado";
	$fdata["GoodName"] = "processado";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","processado");
	$fdata["FieldType"] = 16;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "processado";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "processado";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatager_cadastro["processado"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","email");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Email Hyperlink");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["email"] = $fdata;
//	webuser
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "webuser";
	$fdata["GoodName"] = "webuser";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","webuser");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "webuser";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "webuser";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["webuser"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 28;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "ger_cadastro";
	$fdata["Label"] = GetFieldLabel("ger_cadastro","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatager_cadastro["ultimaalteracao"] = $fdata;


$tables_data["ger_cadastro"]=&$tdatager_cadastro;
$field_labels["ger_cadastro"] = &$fieldLabelsger_cadastro;
$fieldToolTips["ger_cadastro"] = &$fieldToolTipsger_cadastro;
$page_titles["ger_cadastro"] = &$pageTitlesger_cadastro;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_cadastro"] = array();
//	ger_unidades
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="ger_unidades";
		$detailsParam["dOriginalTable"] = "ger_unidades";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "ger_unidades";
	$detailsParam["dCaptionTable"] = GetTableCaption("ger_unidades");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_cadastro"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_cadastro"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_cadastro"][$dIndex]["masterKeys"][]="idCadastro";

				$detailsTablesData["ger_cadastro"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_cadastro"][$dIndex]["detailKeys"][]="link_ger_cadastro";
//	ger_ocorrencias
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="ger_ocorrencias";
		$detailsParam["dOriginalTable"] = "ger_ocorrencias";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "ger_ocorrencias";
	$detailsParam["dCaptionTable"] = GetTableCaption("ger_ocorrencias");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = "1";
*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "0";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["ger_cadastro"][$dIndex] = $detailsParam;

	
		$detailsTablesData["ger_cadastro"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["ger_cadastro"][$dIndex]["masterKeys"][]="idCadastro";

				$detailsTablesData["ger_cadastro"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["ger_cadastro"][$dIndex]["detailKeys"][]="link_ger_cadastro";

// tables which are master tables for current table (detail)
$masterTablesData["ger_cadastro"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_cadastro()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idCadastro,  nome,  rg,  cpf,  TelPrim,  CompPrim,  TelSec,  CompSec,  TelTerc,  CompTerc,  log,  `end`,  num,  comp,  bairro,  cep,  cidade,  uf,  obs,  situacao,  morador,  cepnet,  ativo,  processado,  email,  webuser,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM ger_cadastro";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY nome";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idCadastro",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto6["m_sql"] = "idCadastro";
$proto6["m_srcTableName"] = "ger_cadastro";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto8["m_sql"] = "nome";
$proto8["m_srcTableName"] = "ger_cadastro";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "rg",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto10["m_sql"] = "rg";
$proto10["m_srcTableName"] = "ger_cadastro";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "cpf",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto12["m_sql"] = "cpf";
$proto12["m_srcTableName"] = "ger_cadastro";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "TelPrim",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto14["m_sql"] = "TelPrim";
$proto14["m_srcTableName"] = "ger_cadastro";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "CompPrim",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto16["m_sql"] = "CompPrim";
$proto16["m_srcTableName"] = "ger_cadastro";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "TelSec",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto18["m_sql"] = "TelSec";
$proto18["m_srcTableName"] = "ger_cadastro";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "CompSec",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto20["m_sql"] = "CompSec";
$proto20["m_srcTableName"] = "ger_cadastro";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "TelTerc",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto22["m_sql"] = "TelTerc";
$proto22["m_srcTableName"] = "ger_cadastro";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "CompTerc",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto24["m_sql"] = "CompTerc";
$proto24["m_srcTableName"] = "ger_cadastro";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "log",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto26["m_sql"] = "log";
$proto26["m_srcTableName"] = "ger_cadastro";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "end",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto28["m_sql"] = "`end`";
$proto28["m_srcTableName"] = "ger_cadastro";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "num",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto30["m_sql"] = "num";
$proto30["m_srcTableName"] = "ger_cadastro";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "comp",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto32["m_sql"] = "comp";
$proto32["m_srcTableName"] = "ger_cadastro";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto34["m_sql"] = "bairro";
$proto34["m_srcTableName"] = "ger_cadastro";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "cep",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto36["m_sql"] = "cep";
$proto36["m_srcTableName"] = "ger_cadastro";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "cidade",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto38["m_sql"] = "cidade";
$proto38["m_srcTableName"] = "ger_cadastro";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "uf",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto40["m_sql"] = "uf";
$proto40["m_srcTableName"] = "ger_cadastro";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto42["m_sql"] = "obs";
$proto42["m_srcTableName"] = "ger_cadastro";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "situacao",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto44["m_sql"] = "situacao";
$proto44["m_srcTableName"] = "ger_cadastro";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "morador",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto46["m_sql"] = "morador";
$proto46["m_srcTableName"] = "ger_cadastro";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "cepnet",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto48["m_sql"] = "cepnet";
$proto48["m_srcTableName"] = "ger_cadastro";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "ativo",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto50["m_sql"] = "ativo";
$proto50["m_srcTableName"] = "ger_cadastro";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "processado",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto52["m_sql"] = "processado";
$proto52["m_srcTableName"] = "ger_cadastro";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto54["m_sql"] = "email";
$proto54["m_srcTableName"] = "ger_cadastro";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto56=array();
			$obj = new SQLField(array(
	"m_strName" => "webuser",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto56["m_sql"] = "webuser";
$proto56["m_srcTableName"] = "ger_cadastro";
$proto56["m_expr"]=$obj;
$proto56["m_alias"] = "";
$obj = new SQLFieldListItem($proto56);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto58["m_sql"] = "ultimousuario";
$proto58["m_srcTableName"] = "ger_cadastro";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
						$proto60=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto60["m_sql"] = "ultimaalteracao";
$proto60["m_srcTableName"] = "ger_cadastro";
$proto60["m_expr"]=$obj;
$proto60["m_alias"] = "";
$obj = new SQLFieldListItem($proto60);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto62=array();
$proto62["m_link"] = "SQLL_MAIN";
			$proto63=array();
$proto63["m_strName"] = "ger_cadastro";
$proto63["m_srcTableName"] = "ger_cadastro";
$proto63["m_columns"] = array();
$proto63["m_columns"][] = "idCadastro";
$proto63["m_columns"][] = "nome";
$proto63["m_columns"][] = "rg";
$proto63["m_columns"][] = "cpf";
$proto63["m_columns"][] = "TelPrim";
$proto63["m_columns"][] = "CompPrim";
$proto63["m_columns"][] = "TelSec";
$proto63["m_columns"][] = "CompSec";
$proto63["m_columns"][] = "TelTerc";
$proto63["m_columns"][] = "CompTerc";
$proto63["m_columns"][] = "log";
$proto63["m_columns"][] = "end";
$proto63["m_columns"][] = "num";
$proto63["m_columns"][] = "comp";
$proto63["m_columns"][] = "bairro";
$proto63["m_columns"][] = "cep";
$proto63["m_columns"][] = "cidade";
$proto63["m_columns"][] = "uf";
$proto63["m_columns"][] = "obs";
$proto63["m_columns"][] = "situacao";
$proto63["m_columns"][] = "morador";
$proto63["m_columns"][] = "cepnet";
$proto63["m_columns"][] = "ativo";
$proto63["m_columns"][] = "processado";
$proto63["m_columns"][] = "email";
$proto63["m_columns"][] = "webuser";
$proto63["m_columns"][] = "ultimousuario";
$proto63["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto63);

$proto62["m_table"] = $obj;
$proto62["m_sql"] = "ger_cadastro";
$proto62["m_alias"] = "";
$proto62["m_srcTableName"] = "ger_cadastro";
$proto64=array();
$proto64["m_sql"] = "";
$proto64["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto64["m_column"]=$obj;
$proto64["m_contained"] = array();
$proto64["m_strCase"] = "";
$proto64["m_havingmode"] = false;
$proto64["m_inBrackets"] = false;
$proto64["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto64);

$proto62["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto62);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto66=array();
						$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "ger_cadastro",
	"m_srcTableName" => "ger_cadastro"
));

$proto66["m_column"]=$obj;
$proto66["m_bAsc"] = 1;
$proto66["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto66);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="ger_cadastro";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_cadastro = createSqlQuery_ger_cadastro();


	
		;

																												

$tdatager_cadastro[".sqlquery"] = $queryData_ger_cadastro;

include_once(getabspath("include/ger_cadastro_events.php"));
$tableEvents["ger_cadastro"] = new eventclass_ger_cadastro;
$tdatager_cadastro[".hasEvents"] = true;

?>