<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_cheques = array();
	$tdatafin_cheques[".truncateText"] = true;
	$tdatafin_cheques[".NumberOfChars"] = 80;
	$tdatafin_cheques[".ShortName"] = "fin_cheques";
	$tdatafin_cheques[".OwnerID"] = "";
	$tdatafin_cheques[".OriginalTable"] = "fin_cheques";

//	field labels
$fieldLabelsfin_cheques = array();
$fieldToolTipsfin_cheques = array();
$pageTitlesfin_cheques = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_cheques["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"] = array();
	$pageTitlesfin_cheques["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["idCheque"] = "Código";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["idCheque"] = "";
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["numcheque"] = "Número";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["numcheque"] = "";
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["valor"] = "Valor";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["valor"] = "";
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["data"] = "Data";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["data"] = "";
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["link_fin_despesa"] = "Despesa";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["link_fin_despesa"] = "";
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["link_fin_conta"] = "Conta Bancária";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["link_fin_conta"] = "";
	$fieldLabelsfin_cheques["Portuguese(Brazil)"]["link_ger_grupo"] = "Grupo/Bloco";
	$fieldToolTipsfin_cheques["Portuguese(Brazil)"]["link_ger_grupo"] = "";
	if (count($fieldToolTipsfin_cheques["Portuguese(Brazil)"]))
		$tdatafin_cheques[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_cheques[""] = array();
	$fieldToolTipsfin_cheques[""] = array();
	$pageTitlesfin_cheques[""] = array();
	if (count($fieldToolTipsfin_cheques[""]))
		$tdatafin_cheques[".isUseToolTips"] = true;
}


	$tdatafin_cheques[".NCSearch"] = true;



$tdatafin_cheques[".shortTableName"] = "fin_cheques";
$tdatafin_cheques[".nSecOptions"] = 0;
$tdatafin_cheques[".recsPerRowList"] = 1;
$tdatafin_cheques[".recsPerRowPrint"] = 1;
$tdatafin_cheques[".mainTableOwnerID"] = "";
$tdatafin_cheques[".moveNext"] = 1;
$tdatafin_cheques[".entityType"] = 0;

$tdatafin_cheques[".strOriginalTableName"] = "fin_cheques";





$tdatafin_cheques[".showAddInPopup"] = false;

$tdatafin_cheques[".showEditInPopup"] = false;

$tdatafin_cheques[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_cheques[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_cheques[".fieldsForRegister"] = array();

$tdatafin_cheques[".listAjax"] = false;

	$tdatafin_cheques[".audit"] = true;

	$tdatafin_cheques[".locking"] = true;

$tdatafin_cheques[".edit"] = true;
$tdatafin_cheques[".afterEditAction"] = 1;
$tdatafin_cheques[".closePopupAfterEdit"] = 1;
$tdatafin_cheques[".afterEditActionDetTable"] = "";

$tdatafin_cheques[".add"] = true;
$tdatafin_cheques[".afterAddAction"] = 1;
$tdatafin_cheques[".closePopupAfterAdd"] = 1;
$tdatafin_cheques[".afterAddActionDetTable"] = "";

$tdatafin_cheques[".list"] = true;

$tdatafin_cheques[".view"] = true;


$tdatafin_cheques[".exportTo"] = true;

$tdatafin_cheques[".printFriendly"] = true;

$tdatafin_cheques[".delete"] = true;

$tdatafin_cheques[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_cheques[".searchSaving"] = false;
//

$tdatafin_cheques[".showSearchPanel"] = true;
		$tdatafin_cheques[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_cheques[".isUseAjaxSuggest"] = false;
else
	$tdatafin_cheques[".isUseAjaxSuggest"] = true;

$tdatafin_cheques[".rowHighlite"] = true;



$tdatafin_cheques[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_cheques[".isUseTimeForSearch"] = false;



$tdatafin_cheques[".badgeColor"] = "778899";


$tdatafin_cheques[".allSearchFields"] = array();
$tdatafin_cheques[".filterFields"] = array();
$tdatafin_cheques[".requiredSearchFields"] = array();

$tdatafin_cheques[".allSearchFields"][] = "link_fin_conta";
	$tdatafin_cheques[".allSearchFields"][] = "link_ger_grupo";
	$tdatafin_cheques[".allSearchFields"][] = "idCheque";
	$tdatafin_cheques[".allSearchFields"][] = "numcheque";
	$tdatafin_cheques[".allSearchFields"][] = "valor";
	$tdatafin_cheques[".allSearchFields"][] = "data";
	$tdatafin_cheques[".allSearchFields"][] = "link_fin_despesa";
	$tdatafin_cheques[".allSearchFields"][] = "ultimousuario";
	$tdatafin_cheques[".allSearchFields"][] = "ultimaalteracao";
	

$tdatafin_cheques[".googleLikeFields"] = array();
$tdatafin_cheques[".googleLikeFields"][] = "idCheque";
$tdatafin_cheques[".googleLikeFields"][] = "numcheque";
$tdatafin_cheques[".googleLikeFields"][] = "valor";
$tdatafin_cheques[".googleLikeFields"][] = "data";
$tdatafin_cheques[".googleLikeFields"][] = "link_fin_despesa";
$tdatafin_cheques[".googleLikeFields"][] = "ultimousuario";
$tdatafin_cheques[".googleLikeFields"][] = "ultimaalteracao";
$tdatafin_cheques[".googleLikeFields"][] = "link_fin_conta";
$tdatafin_cheques[".googleLikeFields"][] = "link_ger_grupo";


$tdatafin_cheques[".advSearchFields"] = array();
$tdatafin_cheques[".advSearchFields"][] = "link_fin_conta";
$tdatafin_cheques[".advSearchFields"][] = "link_ger_grupo";
$tdatafin_cheques[".advSearchFields"][] = "idCheque";
$tdatafin_cheques[".advSearchFields"][] = "numcheque";
$tdatafin_cheques[".advSearchFields"][] = "valor";
$tdatafin_cheques[".advSearchFields"][] = "data";
$tdatafin_cheques[".advSearchFields"][] = "link_fin_despesa";
$tdatafin_cheques[".advSearchFields"][] = "ultimousuario";
$tdatafin_cheques[".advSearchFields"][] = "ultimaalteracao";

$tdatafin_cheques[".tableType"] = "list";

$tdatafin_cheques[".printerPageOrientation"] = 0;
$tdatafin_cheques[".nPrinterPageScale"] = 100;

$tdatafin_cheques[".nPrinterSplitRecords"] = 40;

$tdatafin_cheques[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_cheques[".geocodingEnabled"] = false;





$tdatafin_cheques[".listGridLayout"] = 3;

$tdatafin_cheques[".isDisplayLoading"] = true;


$tdatafin_cheques[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf

$tdatafin_cheques[".totalsFields"] = array();
$tdatafin_cheques[".totalsFields"][] = array(
	"fName" => "valor",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Currency');

$tdatafin_cheques[".pageSize"] = 20;

$tdatafin_cheques[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_cheques[".strOrderBy"] = $tstrOrderBy;

$tdatafin_cheques[".orderindexes"] = array();

$tdatafin_cheques[".sqlHead"] = "select idCheque,  numcheque,  valor,  `data`,  link_fin_despesa,  ultimousuario,  ultimaalteracao,  link_fin_conta,  link_ger_grupo";
$tdatafin_cheques[".sqlFrom"] = "FROM fin_cheques";
$tdatafin_cheques[".sqlWhereExpr"] = "";
$tdatafin_cheques[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_cheques[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_cheques[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_cheques[".highlightSearchResults"] = true;

$tableKeysfin_cheques = array();
$tableKeysfin_cheques[] = "idCheque";
$tdatafin_cheques[".Keys"] = $tableKeysfin_cheques;

$tdatafin_cheques[".listFields"] = array();
$tdatafin_cheques[".listFields"][] = "link_ger_grupo";
$tdatafin_cheques[".listFields"][] = "link_fin_conta";
$tdatafin_cheques[".listFields"][] = "numcheque";
$tdatafin_cheques[".listFields"][] = "valor";
$tdatafin_cheques[".listFields"][] = "data";
$tdatafin_cheques[".listFields"][] = "link_fin_despesa";

$tdatafin_cheques[".hideMobileList"] = array();


$tdatafin_cheques[".viewFields"] = array();
$tdatafin_cheques[".viewFields"][] = "link_fin_conta";
$tdatafin_cheques[".viewFields"][] = "link_ger_grupo";
$tdatafin_cheques[".viewFields"][] = "idCheque";
$tdatafin_cheques[".viewFields"][] = "numcheque";
$tdatafin_cheques[".viewFields"][] = "valor";
$tdatafin_cheques[".viewFields"][] = "data";
$tdatafin_cheques[".viewFields"][] = "link_fin_despesa";
$tdatafin_cheques[".viewFields"][] = "ultimousuario";
$tdatafin_cheques[".viewFields"][] = "ultimaalteracao";

$tdatafin_cheques[".addFields"] = array();
$tdatafin_cheques[".addFields"][] = "link_ger_grupo";
$tdatafin_cheques[".addFields"][] = "link_fin_conta";
$tdatafin_cheques[".addFields"][] = "numcheque";
$tdatafin_cheques[".addFields"][] = "valor";
$tdatafin_cheques[".addFields"][] = "data";
$tdatafin_cheques[".addFields"][] = "link_fin_despesa";

$tdatafin_cheques[".masterListFields"] = array();

$tdatafin_cheques[".inlineAddFields"] = array();

$tdatafin_cheques[".editFields"] = array();
$tdatafin_cheques[".editFields"][] = "link_fin_conta";
$tdatafin_cheques[".editFields"][] = "link_ger_grupo";
$tdatafin_cheques[".editFields"][] = "idCheque";
$tdatafin_cheques[".editFields"][] = "numcheque";
$tdatafin_cheques[".editFields"][] = "valor";
$tdatafin_cheques[".editFields"][] = "data";
$tdatafin_cheques[".editFields"][] = "link_fin_despesa";

$tdatafin_cheques[".inlineEditFields"] = array();

$tdatafin_cheques[".exportFields"] = array();
$tdatafin_cheques[".exportFields"][] = "link_fin_conta";
$tdatafin_cheques[".exportFields"][] = "link_ger_grupo";
$tdatafin_cheques[".exportFields"][] = "idCheque";
$tdatafin_cheques[".exportFields"][] = "numcheque";
$tdatafin_cheques[".exportFields"][] = "valor";
$tdatafin_cheques[".exportFields"][] = "data";
$tdatafin_cheques[".exportFields"][] = "link_fin_despesa";
$tdatafin_cheques[".exportFields"][] = "ultimousuario";
$tdatafin_cheques[".exportFields"][] = "ultimaalteracao";

$tdatafin_cheques[".importFields"] = array();

$tdatafin_cheques[".printFields"] = array();
$tdatafin_cheques[".printFields"][] = "link_fin_conta";
$tdatafin_cheques[".printFields"][] = "link_ger_grupo";
$tdatafin_cheques[".printFields"][] = "numcheque";
$tdatafin_cheques[".printFields"][] = "valor";
$tdatafin_cheques[".printFields"][] = "data";
$tdatafin_cheques[".printFields"][] = "link_fin_despesa";

//	idCheque
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idCheque";
	$fdata["GoodName"] = "idCheque";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","idCheque");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idCheque";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idCheque";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cheques["idCheque"] = $fdata;
//	numcheque
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "numcheque";
	$fdata["GoodName"] = "numcheque";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","numcheque");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numcheque";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "numcheque";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=15";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cheques["numcheque"] = $fdata;
//	valor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "valor";
	$fdata["GoodName"] = "valor";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","valor");
	$fdata["FieldType"] = 14;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "valor";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "valor";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Currency");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=15";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cheques["valor"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","data");
	$fdata["FieldType"] = 135;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "data";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`data`";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cheques["data"] = $fdata;
//	link_fin_despesa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "link_fin_despesa";
	$fdata["GoodName"] = "link_fin_despesa";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","link_fin_despesa");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_fin_despesa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_fin_despesa";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_despesas";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idDespesa";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "obs";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cheques["link_fin_despesa"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cheques["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_cheques["ultimaalteracao"] = $fdata;
//	link_fin_conta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "link_fin_conta";
	$fdata["GoodName"] = "link_fin_conta";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","link_fin_conta");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_fin_conta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_fin_conta";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "fin_contas";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idConta";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cheques["link_fin_conta"] = $fdata;
//	link_ger_grupo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "link_ger_grupo";
	$fdata["GoodName"] = "link_ger_grupo";
	$fdata["ownerTable"] = "fin_cheques";
	$fdata["Label"] = GetFieldLabel("fin_cheques","link_ger_grupo");
	$fdata["FieldType"] = 3;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "link_ger_grupo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link_ger_grupo";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "ger_grupo";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
	
	
		
	$edata["LinkField"] = "idGrupo";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "nome";

	
	$edata["LookupOrderBy"] = "nome";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;
	
// End Lookup Settings


	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_cheques["link_ger_grupo"] = $fdata;


$tables_data["fin_cheques"]=&$tdatafin_cheques;
$field_labels["fin_cheques"] = &$fieldLabelsfin_cheques;
$fieldToolTips["fin_cheques"] = &$fieldToolTipsfin_cheques;
$page_titles["fin_cheques"] = &$pageTitlesfin_cheques;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_cheques"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_cheques"] = array();


	
				$strOriginalDetailsTable="fin_despesas";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="fin_despesas";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "fin_despesas";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 0;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["fin_cheques"][0] = $masterParams;
				$masterTablesData["fin_cheques"][0]["masterKeys"] = array();
	$masterTablesData["fin_cheques"][0]["masterKeys"][]="idDespesa";
				$masterTablesData["fin_cheques"][0]["detailKeys"] = array();
	$masterTablesData["fin_cheques"][0]["detailKeys"][]="link_fin_despesa";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_cheques()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idCheque,  numcheque,  valor,  `data`,  link_fin_despesa,  ultimousuario,  ultimaalteracao,  link_fin_conta,  link_ger_grupo";
$proto0["m_strFrom"] = "FROM fin_cheques";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idCheque",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto6["m_sql"] = "idCheque";
$proto6["m_srcTableName"] = "fin_cheques";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "numcheque",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto8["m_sql"] = "numcheque";
$proto8["m_srcTableName"] = "fin_cheques";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "valor",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto10["m_sql"] = "valor";
$proto10["m_srcTableName"] = "fin_cheques";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto12["m_sql"] = "`data`";
$proto12["m_srcTableName"] = "fin_cheques";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "link_fin_despesa",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto14["m_sql"] = "link_fin_despesa";
$proto14["m_srcTableName"] = "fin_cheques";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto16["m_sql"] = "ultimousuario";
$proto16["m_srcTableName"] = "fin_cheques";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto18["m_sql"] = "ultimaalteracao";
$proto18["m_srcTableName"] = "fin_cheques";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "link_fin_conta",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto20["m_sql"] = "link_fin_conta";
$proto20["m_srcTableName"] = "fin_cheques";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "link_ger_grupo",
	"m_strTable" => "fin_cheques",
	"m_srcTableName" => "fin_cheques"
));

$proto22["m_sql"] = "link_ger_grupo";
$proto22["m_srcTableName"] = "fin_cheques";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "fin_cheques";
$proto25["m_srcTableName"] = "fin_cheques";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "idCheque";
$proto25["m_columns"][] = "numcheque";
$proto25["m_columns"][] = "valor";
$proto25["m_columns"][] = "data";
$proto25["m_columns"][] = "link_fin_despesa";
$proto25["m_columns"][] = "ultimousuario";
$proto25["m_columns"][] = "ultimaalteracao";
$proto25["m_columns"][] = "link_fin_conta";
$proto25["m_columns"][] = "link_ger_grupo";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "fin_cheques";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "fin_cheques";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="fin_cheques";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_cheques = createSqlQuery_fin_cheques();


	
		;

									

$tdatafin_cheques[".sqlquery"] = $queryData_fin_cheques;

include_once(getabspath("include/fin_cheques_events.php"));
$tableEvents["fin_cheques"] = new eventclass_fin_cheques;
$tdatafin_cheques[".hasEvents"] = true;

?>