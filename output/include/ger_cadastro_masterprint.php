<?php
include_once(getabspath("classes/printpage.php"));

function DisplayMasterTableInfoForPrint_ger_cadastro($params)
{
	global $cman;
	
	$detailtable = $params["detailtable"];
	$keys = $params["keys"];
	
	$xt = new Xtempl();
	
	$tName = "ger_cadastro";
	$xt->eventsObject = getEventObject($tName);

	$pageType = PAGE_PRINT;

	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = PRINT_MASTER;
	$mParams["pageType"] = $pageType;
	$mParams["tName"] = $tName;
	$masterPage = new PrintPage($mParams);
	
	$cipherer = new RunnerCipherer( $tName );
	$settings = new ProjectSettings($tName, $pageType);
	$connection = $cman->byTable( $tName );
	
	$masterQuery = $settings->getSQLQuery();
	$viewControls = new ViewControlsContainer($settings, $pageType, $masterPage);
	
	$where = "";
	$keysAssoc = array();
	$showKeys = "";

	if( $detailtable == "ger_unidades" )
	{
		$keysAssoc["idCadastro"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("idCadastro", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("idCadastro", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("idCadastro", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_cadastro","idCadastro").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}

	if( $detailtable == "ger_ocorrencias" )
	{
		$keysAssoc["idCadastro"] = $keys[1-1];
				$where.= RunnerPage::_getFieldSQLDecrypt("idCadastro", $connection , $settings , $cipherer) . "=" . $cipherer->MakeDBValue("idCadastro", $keys[1-1], "", true);
		
				$keyValue = $viewControls->showDBValue("idCadastro", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_cadastro","idCadastro").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);	
	}
	
	if( !$where )
		return;
	
	$str = SecuritySQL("Export", $tName );
	if( strlen($str) )
		$where.= " and ".$str;
	
	$strWhere = whereAdd( $masterQuery->m_where->toSql($masterQuery), $where );
	if( strlen($strWhere) )
		$strWhere= " where ".$strWhere." ";
		
	$strSQL = $masterQuery->HeadToSql().' '.$masterQuery->FromToSql().$strWhere.$masterQuery->TailToSql();
	LogInfo($strSQL);
	
	$data = $cipherer->DecryptFetchedArray( $connection->query( $strSQL )->fetchAssoc() );
	if( !$data )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));	
	
	$keylink = "";
	$keylink.= "&key1=".runner_htmlspecialchars(rawurlencode(@$data["idCadastro"]));
	
	$xt->assign("nome_mastervalue", $viewControls->showDBValue("nome", $data, $keylink));
	$format = $settings->getViewFormat("nome");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("nome")))
		$class = ' rnr-field-number';
		
	$xt->assign("nome_class", $class); // add class for field header as field value
	$xt->assign("TelPrim_mastervalue", $viewControls->showDBValue("TelPrim", $data, $keylink));
	$format = $settings->getViewFormat("TelPrim");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("TelPrim")))
		$class = ' rnr-field-number';
		
	$xt->assign("TelPrim_class", $class); // add class for field header as field value
	$xt->assign("TelSec_mastervalue", $viewControls->showDBValue("TelSec", $data, $keylink));
	$format = $settings->getViewFormat("TelSec");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("TelSec")))
		$class = ' rnr-field-number';
		
	$xt->assign("TelSec_class", $class); // add class for field header as field value
	$xt->assign("obs_mastervalue", $viewControls->showDBValue("obs", $data, $keylink));
	$format = $settings->getViewFormat("obs");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("obs")))
		$class = ' rnr-field-number';
		
	$xt->assign("obs_class", $class); // add class for field header as field value
	$xt->assign("situacao_mastervalue", $viewControls->showDBValue("situacao", $data, $keylink));
	$format = $settings->getViewFormat("situacao");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("situacao")))
		$class = ' rnr-field-number';
		
	$xt->assign("situacao_class", $class); // add class for field header as field value
	$xt->assign("morador_mastervalue", $viewControls->showDBValue("morador", $data, $keylink));
	$format = $settings->getViewFormat("morador");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("morador")))
		$class = ' rnr-field-number';
		
	$xt->assign("morador_class", $class); // add class for field header as field value
	$xt->assign("ativo_mastervalue", $viewControls->showDBValue("ativo", $data, $keylink));
	$format = $settings->getViewFormat("ativo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("ativo")))
		$class = ' rnr-field-number';
		
	$xt->assign("ativo_class", $class); // add class for field header as field value

	$layout = GetPageLayout("ger_cadastro", 'masterprint');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');

	$xt->displayPartial(GetTemplateName("ger_cadastro", "masterprint"));
}

?>