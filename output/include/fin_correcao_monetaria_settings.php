<?php
require_once(getabspath("classes/cipherer.php"));




$tdatafin_correcao_monetaria = array();
	$tdatafin_correcao_monetaria[".truncateText"] = true;
	$tdatafin_correcao_monetaria[".NumberOfChars"] = 80;
	$tdatafin_correcao_monetaria[".ShortName"] = "fin_correcao_monetaria";
	$tdatafin_correcao_monetaria[".OwnerID"] = "";
	$tdatafin_correcao_monetaria[".OriginalTable"] = "fin_correcao_monetaria";

//	field labels
$fieldLabelsfin_correcao_monetaria = array();
$fieldToolTipsfin_correcao_monetaria = array();
$pageTitlesfin_correcao_monetaria = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsfin_correcao_monetaria["Portuguese(Brazil)"] = array();
	$fieldToolTipsfin_correcao_monetaria["Portuguese(Brazil)"] = array();
	$pageTitlesfin_correcao_monetaria["Portuguese(Brazil)"] = array();
	$fieldLabelsfin_correcao_monetaria["Portuguese(Brazil)"]["idCorrecao"] = "Código";
	$fieldToolTipsfin_correcao_monetaria["Portuguese(Brazil)"]["idCorrecao"] = "";
	$fieldLabelsfin_correcao_monetaria["Portuguese(Brazil)"]["data"] = "data";
	$fieldToolTipsfin_correcao_monetaria["Portuguese(Brazil)"]["data"] = "";
	$fieldLabelsfin_correcao_monetaria["Portuguese(Brazil)"]["indice"] = "indice";
	$fieldToolTipsfin_correcao_monetaria["Portuguese(Brazil)"]["indice"] = "";
	$fieldLabelsfin_correcao_monetaria["Portuguese(Brazil)"]["ultimousuario"] = "Último usuário";
	$fieldToolTipsfin_correcao_monetaria["Portuguese(Brazil)"]["ultimousuario"] = "";
	$fieldLabelsfin_correcao_monetaria["Portuguese(Brazil)"]["ultimaalteracao"] = "Ultima Alteração";
	$fieldToolTipsfin_correcao_monetaria["Portuguese(Brazil)"]["ultimaalteracao"] = "";
	if (count($fieldToolTipsfin_correcao_monetaria["Portuguese(Brazil)"]))
		$tdatafin_correcao_monetaria[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsfin_correcao_monetaria[""] = array();
	$fieldToolTipsfin_correcao_monetaria[""] = array();
	$pageTitlesfin_correcao_monetaria[""] = array();
	if (count($fieldToolTipsfin_correcao_monetaria[""]))
		$tdatafin_correcao_monetaria[".isUseToolTips"] = true;
}


	$tdatafin_correcao_monetaria[".NCSearch"] = true;



$tdatafin_correcao_monetaria[".shortTableName"] = "fin_correcao_monetaria";
$tdatafin_correcao_monetaria[".nSecOptions"] = 0;
$tdatafin_correcao_monetaria[".recsPerRowList"] = 1;
$tdatafin_correcao_monetaria[".recsPerRowPrint"] = 1;
$tdatafin_correcao_monetaria[".mainTableOwnerID"] = "";
$tdatafin_correcao_monetaria[".moveNext"] = 1;
$tdatafin_correcao_monetaria[".entityType"] = 0;

$tdatafin_correcao_monetaria[".strOriginalTableName"] = "fin_correcao_monetaria";





$tdatafin_correcao_monetaria[".showAddInPopup"] = false;

$tdatafin_correcao_monetaria[".showEditInPopup"] = false;

$tdatafin_correcao_monetaria[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatafin_correcao_monetaria[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatafin_correcao_monetaria[".fieldsForRegister"] = array();

$tdatafin_correcao_monetaria[".listAjax"] = false;

	$tdatafin_correcao_monetaria[".audit"] = true;

	$tdatafin_correcao_monetaria[".locking"] = true;


$tdatafin_correcao_monetaria[".add"] = true;
$tdatafin_correcao_monetaria[".afterAddAction"] = 1;
$tdatafin_correcao_monetaria[".closePopupAfterAdd"] = 1;
$tdatafin_correcao_monetaria[".afterAddActionDetTable"] = "";

$tdatafin_correcao_monetaria[".list"] = true;

$tdatafin_correcao_monetaria[".inlineEdit"] = true;
$tdatafin_correcao_monetaria[".view"] = true;

$tdatafin_correcao_monetaria[".import"] = true;

$tdatafin_correcao_monetaria[".exportTo"] = true;


$tdatafin_correcao_monetaria[".delete"] = true;

$tdatafin_correcao_monetaria[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatafin_correcao_monetaria[".searchSaving"] = false;
//

$tdatafin_correcao_monetaria[".showSearchPanel"] = true;
		$tdatafin_correcao_monetaria[".flexibleSearch"] = true;

if (isMobile())
	$tdatafin_correcao_monetaria[".isUseAjaxSuggest"] = false;
else
	$tdatafin_correcao_monetaria[".isUseAjaxSuggest"] = true;

$tdatafin_correcao_monetaria[".rowHighlite"] = true;



$tdatafin_correcao_monetaria[".addPageEvents"] = false;

// use timepicker for search panel
$tdatafin_correcao_monetaria[".isUseTimeForSearch"] = false;



$tdatafin_correcao_monetaria[".badgeColor"] = "9ACD32";


$tdatafin_correcao_monetaria[".allSearchFields"] = array();
$tdatafin_correcao_monetaria[".filterFields"] = array();
$tdatafin_correcao_monetaria[".requiredSearchFields"] = array();

$tdatafin_correcao_monetaria[".allSearchFields"][] = "data";
	$tdatafin_correcao_monetaria[".allSearchFields"][] = "indice";
	$tdatafin_correcao_monetaria[".allSearchFields"][] = "ultimousuario";
	$tdatafin_correcao_monetaria[".allSearchFields"][] = "ultimaalteracao";
	

$tdatafin_correcao_monetaria[".googleLikeFields"] = array();
$tdatafin_correcao_monetaria[".googleLikeFields"][] = "data";
$tdatafin_correcao_monetaria[".googleLikeFields"][] = "indice";
$tdatafin_correcao_monetaria[".googleLikeFields"][] = "ultimousuario";
$tdatafin_correcao_monetaria[".googleLikeFields"][] = "ultimaalteracao";


$tdatafin_correcao_monetaria[".advSearchFields"] = array();
$tdatafin_correcao_monetaria[".advSearchFields"][] = "data";
$tdatafin_correcao_monetaria[".advSearchFields"][] = "indice";
$tdatafin_correcao_monetaria[".advSearchFields"][] = "ultimousuario";
$tdatafin_correcao_monetaria[".advSearchFields"][] = "ultimaalteracao";

$tdatafin_correcao_monetaria[".tableType"] = "list";

$tdatafin_correcao_monetaria[".printerPageOrientation"] = 0;
$tdatafin_correcao_monetaria[".nPrinterPageScale"] = 100;

$tdatafin_correcao_monetaria[".nPrinterSplitRecords"] = 40;

$tdatafin_correcao_monetaria[".nPrinterPDFSplitRecords"] = 40;



$tdatafin_correcao_monetaria[".geocodingEnabled"] = false;





$tdatafin_correcao_monetaria[".listGridLayout"] = 3;

$tdatafin_correcao_monetaria[".isDisplayLoading"] = true;


$tdatafin_correcao_monetaria[".searchIsRequiredForFilters"] = true;


// view page pdf

// print page pdf


$tdatafin_correcao_monetaria[".pageSize"] = 20;

$tdatafin_correcao_monetaria[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY `data` DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatafin_correcao_monetaria[".strOrderBy"] = $tstrOrderBy;

$tdatafin_correcao_monetaria[".orderindexes"] = array();
$tdatafin_correcao_monetaria[".orderindexes"][] = array(2, (0 ? "ASC" : "DESC"), "`data`");

$tdatafin_correcao_monetaria[".sqlHead"] = "select idCorrecao,  `data`,  indice,  ultimousuario,  ultimaalteracao";
$tdatafin_correcao_monetaria[".sqlFrom"] = "FROM fin_correcao_monetaria";
$tdatafin_correcao_monetaria[".sqlWhereExpr"] = "";
$tdatafin_correcao_monetaria[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatafin_correcao_monetaria[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatafin_correcao_monetaria[".arrGroupsPerPage"] = $arrGPP;

$tdatafin_correcao_monetaria[".highlightSearchResults"] = true;

$tableKeysfin_correcao_monetaria = array();
$tableKeysfin_correcao_monetaria[] = "idCorrecao";
$tdatafin_correcao_monetaria[".Keys"] = $tableKeysfin_correcao_monetaria;

$tdatafin_correcao_monetaria[".listFields"] = array();
$tdatafin_correcao_monetaria[".listFields"][] = "data";
$tdatafin_correcao_monetaria[".listFields"][] = "indice";

$tdatafin_correcao_monetaria[".hideMobileList"] = array();


$tdatafin_correcao_monetaria[".viewFields"] = array();
$tdatafin_correcao_monetaria[".viewFields"][] = "data";
$tdatafin_correcao_monetaria[".viewFields"][] = "indice";
$tdatafin_correcao_monetaria[".viewFields"][] = "ultimousuario";
$tdatafin_correcao_monetaria[".viewFields"][] = "ultimaalteracao";

$tdatafin_correcao_monetaria[".addFields"] = array();
$tdatafin_correcao_monetaria[".addFields"][] = "data";
$tdatafin_correcao_monetaria[".addFields"][] = "indice";

$tdatafin_correcao_monetaria[".masterListFields"] = array();

$tdatafin_correcao_monetaria[".inlineAddFields"] = array();

$tdatafin_correcao_monetaria[".editFields"] = array();

$tdatafin_correcao_monetaria[".inlineEditFields"] = array();
$tdatafin_correcao_monetaria[".inlineEditFields"][] = "data";
$tdatafin_correcao_monetaria[".inlineEditFields"][] = "indice";

$tdatafin_correcao_monetaria[".exportFields"] = array();
$tdatafin_correcao_monetaria[".exportFields"][] = "data";
$tdatafin_correcao_monetaria[".exportFields"][] = "indice";
$tdatafin_correcao_monetaria[".exportFields"][] = "ultimousuario";
$tdatafin_correcao_monetaria[".exportFields"][] = "ultimaalteracao";

$tdatafin_correcao_monetaria[".importFields"] = array();
$tdatafin_correcao_monetaria[".importFields"][] = "idCorrecao";
$tdatafin_correcao_monetaria[".importFields"][] = "data";
$tdatafin_correcao_monetaria[".importFields"][] = "indice";
$tdatafin_correcao_monetaria[".importFields"][] = "ultimousuario";
$tdatafin_correcao_monetaria[".importFields"][] = "ultimaalteracao";

$tdatafin_correcao_monetaria[".printFields"] = array();

//	idCorrecao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idCorrecao";
	$fdata["GoodName"] = "idCorrecao";
	$fdata["ownerTable"] = "fin_correcao_monetaria";
	$fdata["Label"] = GetFieldLabel("fin_correcao_monetaria","idCorrecao");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "idCorrecao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idCorrecao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatafin_correcao_monetaria["idCorrecao"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "fin_correcao_monetaria";
	$fdata["Label"] = GetFieldLabel("fin_correcao_monetaria","data");
	$fdata["FieldType"] = 7;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "data";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`data`";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 12;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_correcao_monetaria["data"] = $fdata;
//	indice
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "indice";
	$fdata["GoodName"] = "indice";
	$fdata["ownerTable"] = "fin_correcao_monetaria";
	$fdata["Label"] = GetFieldLabel("fin_correcao_monetaria","indice");
	$fdata["FieldType"] = 14;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "indice";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "indice";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_correcao_monetaria["indice"] = $fdata;
//	ultimousuario
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "ultimousuario";
	$fdata["GoodName"] = "ultimousuario";
	$fdata["ownerTable"] = "fin_correcao_monetaria";
	$fdata["Label"] = GetFieldLabel("fin_correcao_monetaria","ultimousuario");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimousuario";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimousuario";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

	// the end of search options settings




	$tdatafin_correcao_monetaria["ultimousuario"] = $fdata;
//	ultimaalteracao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "ultimaalteracao";
	$fdata["GoodName"] = "ultimaalteracao";
	$fdata["ownerTable"] = "fin_correcao_monetaria";
	$fdata["Label"] = GetFieldLabel("fin_correcao_monetaria","ultimaalteracao");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ultimaalteracao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ultimaalteracao";

		$fdata["DeleteAssociatedFile"] = true;

		$fdata["CompatibilityMode"] = true;

				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 748;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
	// the end of search options settings




	$tdatafin_correcao_monetaria["ultimaalteracao"] = $fdata;


$tables_data["fin_correcao_monetaria"]=&$tdatafin_correcao_monetaria;
$field_labels["fin_correcao_monetaria"] = &$fieldLabelsfin_correcao_monetaria;
$fieldToolTips["fin_correcao_monetaria"] = &$fieldToolTipsfin_correcao_monetaria;
$page_titles["fin_correcao_monetaria"] = &$pageTitlesfin_correcao_monetaria;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["fin_correcao_monetaria"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["fin_correcao_monetaria"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_fin_correcao_monetaria()
{
$proto0=array();
$proto0["m_strHead"] = "select";
$proto0["m_strFieldList"] = "idCorrecao,  `data`,  indice,  ultimousuario,  ultimaalteracao";
$proto0["m_strFrom"] = "FROM fin_correcao_monetaria";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY `data` DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idCorrecao",
	"m_strTable" => "fin_correcao_monetaria",
	"m_srcTableName" => "fin_correcao_monetaria"
));

$proto6["m_sql"] = "idCorrecao";
$proto6["m_srcTableName"] = "fin_correcao_monetaria";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "fin_correcao_monetaria",
	"m_srcTableName" => "fin_correcao_monetaria"
));

$proto8["m_sql"] = "`data`";
$proto8["m_srcTableName"] = "fin_correcao_monetaria";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "indice",
	"m_strTable" => "fin_correcao_monetaria",
	"m_srcTableName" => "fin_correcao_monetaria"
));

$proto10["m_sql"] = "indice";
$proto10["m_srcTableName"] = "fin_correcao_monetaria";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimousuario",
	"m_strTable" => "fin_correcao_monetaria",
	"m_srcTableName" => "fin_correcao_monetaria"
));

$proto12["m_sql"] = "ultimousuario";
$proto12["m_srcTableName"] = "fin_correcao_monetaria";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "ultimaalteracao",
	"m_strTable" => "fin_correcao_monetaria",
	"m_srcTableName" => "fin_correcao_monetaria"
));

$proto14["m_sql"] = "ultimaalteracao";
$proto14["m_srcTableName"] = "fin_correcao_monetaria";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "fin_correcao_monetaria";
$proto17["m_srcTableName"] = "fin_correcao_monetaria";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "idCorrecao";
$proto17["m_columns"][] = "data";
$proto17["m_columns"][] = "indice";
$proto17["m_columns"][] = "ultimousuario";
$proto17["m_columns"][] = "ultimaalteracao";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "fin_correcao_monetaria";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "fin_correcao_monetaria";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto20=array();
						$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "fin_correcao_monetaria",
	"m_srcTableName" => "fin_correcao_monetaria"
));

$proto20["m_column"]=$obj;
$proto20["m_bAsc"] = 0;
$proto20["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto20);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="fin_correcao_monetaria";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_fin_correcao_monetaria = createSqlQuery_fin_correcao_monetaria();


	
		;

					

$tdatafin_correcao_monetaria[".sqlquery"] = $queryData_fin_correcao_monetaria;

include_once(getabspath("include/fin_correcao_monetaria_events.php"));
$tableEvents["fin_correcao_monetaria"] = new eventclass_fin_correcao_monetaria;
$tdatafin_correcao_monetaria[".hasEvents"] = true;

?>