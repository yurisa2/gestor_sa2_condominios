<?php
function DisplayMasterTableInfo_ger_grupo($params)
{
	$keys = $params["keys"];
	$detailtable = $params["detailtable"];
	$data = $params["masterRecordData"];
	
	$xt = new Xtempl();
	$tName = "ger_grupo";
	$xt->eventsObject = getEventObject($tName);
	
	include_once(getabspath('classes/listpage.php'));
	include_once(getabspath('classes/listpage_simple.php'));
	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = LIST_MASTER;
	$mParams["pageType"] = PAGE_LIST;
	$mParams["flyId"] = $params["recId"];
	$masterPage = ListPage::createListPage($tName, $mParams);
	
	$settings = $masterPage->pSet;
	$viewControls = new ViewControlsContainer($settings, PAGE_LIST, $masterPage);
	
	$keysAssoc = array();
	$showKeys = "";	

	if($detailtable == "fin_despesas")
	{
		$keysAssoc["idGrupo"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idGrupo", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_grupo","idGrupo").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if($detailtable == "ger_unidades")
	{
		$keysAssoc["idGrupo"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idGrupo", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_grupo","idGrupo").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if($detailtable == "fin_bco_ext1")
	{
		$keysAssoc["idGrupo"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idGrupo", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_grupo","idGrupo").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if($detailtable == "SaldosBlocos")
	{
		$keysAssoc["idGrupo"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idGrupo", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_grupo","idGrupo").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if($detailtable == "ger_arquivos_grupos")
	{
		$keysAssoc["idGrupo"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("idGrupo", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("ger_grupo","idGrupo").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if( !$data || !count($data) )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));
	
	$keylink = "";
	$keylink.= "&key1=".runner_htmlspecialchars(rawurlencode(@$data["idGrupo"]));
	
	$xt->assign("idGrupo_mastervalue", $viewControls->showDBValue("idGrupo", $data, $keylink));
	$format = $settings->getViewFormat("idGrupo");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("idGrupo")))
		$class = ' rnr-field-number';
		
	$xt->assign("idGrupo_class", $class); // add class for field header as field value
	$xt->assign("nome_mastervalue", $viewControls->showDBValue("nome", $data, $keylink));
	$format = $settings->getViewFormat("nome");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("nome")))
		$class = ' rnr-field-number';
		
	$xt->assign("nome_class", $class); // add class for field header as field value
	$xt->assign("obs_mastervalue", $viewControls->showDBValue("obs", $data, $keylink));
	$format = $settings->getViewFormat("obs");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("obs")))
		$class = ' rnr-field-number';
		
	$xt->assign("obs_class", $class); // add class for field header as field value

	$layout = GetPageLayout("ger_grupo", 'masterlist');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');
	
	$xt->displayPartial(GetTemplateName("ger_grupo", "masterlist"));
}

?>