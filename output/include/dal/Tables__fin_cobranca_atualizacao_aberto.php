<?php
$dalTablefin_cobranca_atualizacao_aberto = array();
$dalTablefin_cobranca_atualizacao_aberto["idCobranca"] = array("type"=>3,"varname"=>"idCobranca");
$dalTablefin_cobranca_atualizacao_aberto["auth"] = array("type"=>200,"varname"=>"auth");
$dalTablefin_cobranca_atualizacao_aberto["vencimento"] = array("type"=>7,"varname"=>"vencimento");
$dalTablefin_cobranca_atualizacao_aberto["numdoc"] = array("type"=>3,"varname"=>"numdoc");
$dalTablefin_cobranca_atualizacao_aberto["datadoc"] = array("type"=>135,"varname"=>"datadoc");
$dalTablefin_cobranca_atualizacao_aberto["vlrcob"] = array("type"=>5,"varname"=>"vlrcob");
$dalTablefin_cobranca_atualizacao_aberto["link_ger_unidade"] = array("type"=>3,"varname"=>"link_ger_unidade");
$dalTablefin_cobranca_atualizacao_aberto["multa"] = array("type"=>5,"varname"=>"multa");
$dalTablefin_cobranca_atualizacao_aberto["jurosam"] = array("type"=>5,"varname"=>"jurosam");
$dalTablefin_cobranca_atualizacao_aberto["obs"] = array("type"=>200,"varname"=>"obs");
$dalTablefin_cobranca_atualizacao_aberto["conta"] = array("type"=>3,"varname"=>"conta");
$dalTablefin_cobranca_atualizacao_aberto["multacalculada"] = array("type"=>5,"varname"=>"multacalculada");
$dalTablefin_cobranca_atualizacao_aberto["mesesemaberto"] = array("type"=>3,"varname"=>"mesesemaberto");
$dalTablefin_cobranca_atualizacao_aberto["juroscalculado"] = array("type"=>5,"varname"=>"juroscalculado");
$dalTablefin_cobranca_atualizacao_aberto["correcao"] = array("type"=>5,"varname"=>"correcao");
$dalTablefin_cobranca_atualizacao_aberto["vlrtotal"] = array("type"=>5,"varname"=>"vlrtotal");

$dal_info["Tables__fin_cobranca_atualizacao_aberto"] = &$dalTablefin_cobranca_atualizacao_aberto;
?>