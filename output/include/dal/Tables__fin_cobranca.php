<?php
$dalTablefin_cobranca = array();
$dalTablefin_cobranca["idCobranca"] = array("type"=>3,"varname"=>"idCobranca");
$dalTablefin_cobranca["vencimento"] = array("type"=>7,"varname"=>"vencimento");
$dalTablefin_cobranca["tipo"] = array("type"=>200,"varname"=>"tipo");
$dalTablefin_cobranca["numdoc"] = array("type"=>3,"varname"=>"numdoc");
$dalTablefin_cobranca["datadoc"] = array("type"=>135,"varname"=>"datadoc");
$dalTablefin_cobranca["vlrdoc"] = array("type"=>5,"varname"=>"vlrdoc");
$dalTablefin_cobranca["desc"] = array("type"=>5,"varname"=>"desc");
$dalTablefin_cobranca["outded"] = array("type"=>5,"varname"=>"outded");
$dalTablefin_cobranca["moramulta"] = array("type"=>5,"varname"=>"moramulta");
$dalTablefin_cobranca["outacr"] = array("type"=>5,"varname"=>"outacr");
$dalTablefin_cobranca["vlrcob"] = array("type"=>5,"varname"=>"vlrcob");
$dalTablefin_cobranca["link_ger_unidade"] = array("type"=>3,"varname"=>"link_ger_unidade");
$dalTablefin_cobranca["liq"] = array("type"=>16,"varname"=>"liq");
$dalTablefin_cobranca["env"] = array("type"=>16,"varname"=>"env");
$dalTablefin_cobranca["imp"] = array("type"=>16,"varname"=>"imp");
$dalTablefin_cobranca["obs"] = array("type"=>200,"varname"=>"obs");
$dalTablefin_cobranca["conta"] = array("type"=>3,"varname"=>"conta");
$dalTablefin_cobranca["link_ger_origem"] = array("type"=>200,"varname"=>"link_ger_origem");
$dalTablefin_cobranca["tipodoc"] = array("type"=>200,"varname"=>"tipodoc");
$dalTablefin_cobranca["dtpagamento"] = array("type"=>7,"varname"=>"dtpagamento");
$dalTablefin_cobranca["baixado"] = array("type"=>16,"varname"=>"baixado");
$dalTablefin_cobranca["motivobaixa"] = array("type"=>200,"varname"=>"motivobaixa");
$dalTablefin_cobranca["acordo"] = array("type"=>16,"varname"=>"acordo");
$dalTablefin_cobranca["dtacordo"] = array("type"=>7,"varname"=>"dtacordo");
$dalTablefin_cobranca["detaacordo"] = array("type"=>200,"varname"=>"detaacordo");
$dalTablefin_cobranca["ultimousuario"] = array("type"=>200,"varname"=>"ultimousuario");
$dalTablefin_cobranca["ultimaalteracao"] = array("type"=>135,"varname"=>"ultimaalteracao");
$dalTablefin_cobranca["acordoativo"] = array("type"=>16,"varname"=>"acordoativo");
$dalTablefin_cobranca["numidacordo"] = array("type"=>200,"varname"=>"numidacordo");
$dalTablefin_cobranca["cartas"] = array("type"=>3,"varname"=>"cartas");
$dalTablefin_cobranca["auth"] = array("type"=>200,"varname"=>"auth");
$dalTablefin_cobranca["multa"] = array("type"=>5,"varname"=>"multa");
$dalTablefin_cobranca["jurosam"] = array("type"=>5,"varname"=>"jurosam");
	$dalTablefin_cobranca["idCobranca"]["key"]=true;

$dal_info["Tables__fin_cobranca"] = &$dalTablefin_cobranca;
?>