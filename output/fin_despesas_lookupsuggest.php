<?php
ini_set("display_errors","1");
ini_set("display_startup_errors","1");

include("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

include("include/fin_despesas_variables.php");



$field = postvalue('searchField');
$value = postvalue('searchFor');
$lookupValue = postvalue('lookupValue');
$LookupSQL = "";
$response = array();
$output = "";

	if(!@$_SESSION["UserID"]) { return;	}
	if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Edit") && !CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Add") && !CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search")) { return;	}

	if($field=="link_fin_meiospgto") 
	{
	
		$LookupSQL = "SELECT ";
		$LookupSQL .= "`idMeiospgto`";
		$LookupSQL .= ",`meiopgto`";
		$LookupSQL .= " FROM `fin_meiospgto` ";
		$LookupSQL .= " WHERE ";
		$LookupSQL .= "`meiopgto` LIKE '".db_addslashes($value)."%'";
		$LookupSQL.= " ORDER BY `meiopgto`";
		}
	if($field=="link_fin_tipogasto") 
	{
	
		$LookupSQL = "SELECT ";
		$LookupSQL .= "`tipodegasto`";
		$LookupSQL .= ",`tipodegasto`";
		$LookupSQL .= " FROM `fin_tipogastos` ";
		$LookupSQL .= " WHERE ";
		$LookupSQL .= "`tipodegasto` LIKE '".db_addslashes($value)."%'";
		$LookupSQL.= " ORDER BY `tipodegasto`";
		}
	if($field=="link_fin_tipodoc") 
	{
	
		$LookupSQL = "SELECT ";
		$LookupSQL .= "`idTipodocs`";
		$LookupSQL .= ",`tipodoc`";
		$LookupSQL .= " FROM `fin_tipodocs` ";
		$LookupSQL .= " WHERE ";
		$LookupSQL .= "`tipodoc` LIKE '".db_addslashes($value)."%'";
	}
	if($field=="link_fin_fornecedor") 
	{
	
		$LookupSQL = "SELECT ";
		$LookupSQL .= "`idForn`";
		$LookupSQL .= ",`nomefantasia`";
		$LookupSQL .= " FROM `fin_fornecedores` ";
		$LookupSQL .= " WHERE ";
		$LookupSQL .= "`nomefantasia` LIKE '".db_addslashes($value)."%'";
	}
	if($field=="link_fin_conta") 
	{
	
		$LookupSQL = "SELECT ";
		$LookupSQL .= "`idConta`";
		$LookupSQL .= ",`nome`";
		$LookupSQL .= " FROM `fin_contas` ";
		$LookupSQL .= " WHERE ";
		$LookupSQL .= "`nome` LIKE '".db_addslashes($value)."%'";
		$LookupSQL.= " ORDER BY `nome`";
		}
	if($field=="link_ger_grupo") 
	{
	
		$LookupSQL = "SELECT ";
		$LookupSQL .= "`idGrupo`";
		$LookupSQL .= ",`nome`";
		$LookupSQL .= " FROM `ger_grupo` ";
		$LookupSQL .= " WHERE ";
		$LookupSQL .= "`nome` LIKE '".db_addslashes($value)."%'";
		$LookupSQL.= " ORDER BY `nome`";
		}

$rs=db_query($LookupSQL,$conn);

$found=false;
while ($data = db_fetch_numarray($rs)) 
{
	if(!$found && $data[0]==$lookupValue)
		$found=true;
	$response[] = $data[0];
	$response[] = $data[1];
}


for($i=0;$i<count($response) && $i<40;$i++)
	echo $response[$i]."\n";
/*
if ($output = array_chunk($response,40)) {
	foreach( $output[0] as $value ) {
		echo $value."\n";
		//echo str_replace("\n","\\n",$value)."\n";
	}
}
*/
?>