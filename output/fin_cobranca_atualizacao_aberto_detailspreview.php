<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

require_once("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

require_once("include/fin_cobranca_atualizacao_aberto_variables.php");

$mode = postvalue("mode");

if(!isLogged())
{ 
	return;
}
if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search"))
{
	return;
}

require_once("classes/searchclause.php");

$cipherer = new RunnerCipherer($strTableName);

require_once('include/xtempl.php');
$xt = new Xtempl();





$layout = new TLayout("detailspreview_bootstrap", "AvenueWhite_label", "MobileWhite_label");
$layout->version = 3;
	$layout->bootstrapTheme = "yeti";
$layout->blocks["bare"] = array();
$layout->containers["dcount"] = array();
$layout->container_properties["dcount"] = array(  );
$layout->containers["dcount"][] = array("name"=>"bsdetailspreviewcount",
	"block"=>"", "substyle"=>1  );

$layout->skins["dcount"] = "";

$layout->blocks["bare"][] = "dcount";
$layout->containers["detailspreviewgrid"] = array();
$layout->container_properties["detailspreviewgrid"] = array(  );
$layout->containers["detailspreviewgrid"][] = array("name"=>"detailspreviewfields",
	"block"=>"details_data", "substyle"=>1  );

$layout->skins["detailspreviewgrid"] = "";

$layout->blocks["bare"][] = "detailspreviewgrid";
$page_layouts["fin_cobranca_atualizacao_aberto_detailspreview"] = $layout;




$recordsCounter = 0;

//	process masterkey value
$mastertable = postvalue("mastertable");
$masterKeys = my_json_decode(postvalue("masterKeys"));
$sessionPrefix = "_detailsPreview";
if($mastertable != "")
{
	$_SESSION[$sessionPrefix."_mastertable"]=$mastertable;
//	copy keys to session
	$i = 1;
	if(is_array($masterKeys) && count($masterKeys) > 0)
	{
		while(array_key_exists ("masterkey".$i, $masterKeys))
		{
			$_SESSION[$sessionPrefix."_masterkey".$i] = $masterKeys["masterkey".$i];
			$i++;
		}
	}
	if(isset($_SESSION[$sessionPrefix."_masterkey".$i]))
		unset($_SESSION[$sessionPrefix."_masterkey".$i]);
}
else
	$mastertable = $_SESSION[$sessionPrefix."_mastertable"];

$params = array();
$params['id'] = 1;
$params['xt'] = &$xt;
$params['tName'] = $strTableName;
$params['pageType'] = "detailspreview";
$pageObject = new DetailsPreview($params);

if($mastertable == "ger_unidades")
{
	$where = "";
		$formattedValue = make_db_value("link_ger_unidade",$_SESSION[$sessionPrefix."_masterkey1"]);
	if( $formattedValue == "null" )
		$where .= $pageObject->getFieldSQLDecrypt("link_ger_unidade") . " is null";
	else
		$where .= $pageObject->getFieldSQLDecrypt("link_ger_unidade") . "=" . $formattedValue;
}

$str = SecuritySQL("Search", $strTableName);
if(strlen($str))
	$where.=" and ".$str;
$strSQL = $gQuery->gSQLWhere($where);

$strSQL.=" ".$gstrOrderBy;

$rowcount = $gQuery->gSQLRowCount($where, $pageObject->connection);
$xt->assign("row_count",$rowcount);
if($rowcount) 
{
	$xt->assign("details_data",true);

	$display_count = 10;
	if($mode == "inline")
		$display_count*=2;
		
	if($rowcount>$display_count+2)
	{
		$xt->assign("display_first",true);
		$xt->assign("display_count",$display_count);
	}
	else
		$display_count = $rowcount;

	$rowinfo = array();
	
	require_once getabspath('classes/controls/ViewControlsContainer.php');
	$pSet = new ProjectSettings($strTableName, PAGE_LIST);
	$viewContainer = new ViewControlsContainer($pSet, PAGE_LIST);
	$viewContainer->isDetailsPreview = true;

	$b = true;
	$qResult = $pageObject->connection->query( $strSQL );
	$data = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	while($data && $recordsCounter<$display_count) {
		$recordsCounter++;
		$row = array();
		$keylink = "";
		$keylink.="&key1=".runner_htmlspecialchars(rawurlencode(@$data["idCobranca"]));
	
	
	//	vencimento - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("vencimento", $data, $keylink);
			$row["vencimento_value"] = $value;
			$format = $pSet->getViewFormat("vencimento");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("vencimento")))
				$class = ' rnr-field-number';
			$row["vencimento_class"] = $class;
	//	numdoc - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("numdoc", $data, $keylink);
			$row["numdoc_value"] = $value;
			$format = $pSet->getViewFormat("numdoc");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("numdoc")))
				$class = ' rnr-field-number';
			$row["numdoc_class"] = $class;
	//	vlrcob - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("vlrcob", $data, $keylink);
			$row["vlrcob_value"] = $value;
			$format = $pSet->getViewFormat("vlrcob");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("vlrcob")))
				$class = ' rnr-field-number';
			$row["vlrcob_class"] = $class;
	//	mesesemaberto - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("mesesemaberto", $data, $keylink);
			$row["mesesemaberto_value"] = $value;
			$format = $pSet->getViewFormat("mesesemaberto");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("mesesemaberto")))
				$class = ' rnr-field-number';
			$row["mesesemaberto_class"] = $class;
	//	juroscalculado - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("juroscalculado", $data, $keylink);
			$row["juroscalculado_value"] = $value;
			$format = $pSet->getViewFormat("juroscalculado");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("juroscalculado")))
				$class = ' rnr-field-number';
			$row["juroscalculado_class"] = $class;
	//	correcao - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("correcao", $data, $keylink);
			$row["correcao_value"] = $value;
			$format = $pSet->getViewFormat("correcao");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("correcao")))
				$class = ' rnr-field-number';
			$row["correcao_class"] = $class;
	//	multacalculada - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("multacalculada", $data, $keylink);
			$row["multacalculada_value"] = $value;
			$format = $pSet->getViewFormat("multacalculada");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("multacalculada")))
				$class = ' rnr-field-number';
			$row["multacalculada_class"] = $class;
	//	vlrtotal - Number
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("vlrtotal", $data, $keylink);
			$row["vlrtotal_value"] = $value;
			$format = $pSet->getViewFormat("vlrtotal");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("vlrtotal")))
				$class = ' rnr-field-number';
			$row["vlrtotal_class"] = $class;
	//	unidade - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("unidade", $data, $keylink);
			$row["unidade_value"] = $value;
			$format = $pSet->getViewFormat("unidade");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("unidade")))
				$class = ' rnr-field-number';
			$row["unidade_class"] = $class;
	//	obs - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("obs", $data, $keylink);
			$row["obs_value"] = $value;
			$format = $pSet->getViewFormat("obs");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("obs")))
				$class = ' rnr-field-number';
			$row["obs_class"] = $class;
	//	unidadeir - Custom
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("unidadeir", $data, $keylink);
			$row["unidadeir_value"] = $value;
			$format = $pSet->getViewFormat("unidadeir");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("unidadeir")))
				$class = ' rnr-field-number';
			$row["unidadeir_class"] = $class;
	//	nome - Hyperlink
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("nome", $data, $keylink);
			$row["nome_value"] = $value;
			$format = $pSet->getViewFormat("nome");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("nome")))
				$class = ' rnr-field-number';
			$row["nome_class"] = $class;
	//	bolSantander - Hyperlink
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("bolSantander", $data, $keylink);
			$row["bolSantander_value"] = $value;
			$format = $pSet->getViewFormat("bolSantander");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("bolSantander")))
				$class = ' rnr-field-number';
			$row["bolSantander_class"] = $class;
		$rowinfo[] = $row;
		if ($b) {
			$rowinfo2[] = $row;
			$b = false;
		}
		$data = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	}
	$xt->assign_loopsection("details_row",$rowinfo);
	$xt->assign_loopsection("details_row_header",$rowinfo2); // assign class for header
}
$returnJSON = array("success" => true);
$xt->load_template(GetTemplateName("fin_cobranca_atualizacao_aberto", "detailspreview"));
$returnJSON["body"] = $xt->fetch_loaded();

if($mode!="inline")
{
	$returnJSON["counter"] = postvalue("counter");
	$layout = GetPageLayout(GoodFieldName($strTableName), 'detailspreview');
	if($layout)
	{
		foreach($layout->getCSSFiles(isRTL(), isMobile()) as $css)
		{
			$returnJSON['CSSFiles'][] = $css;
		}
	}	
}	

echo printJSON($returnJSON);
exit();
?>