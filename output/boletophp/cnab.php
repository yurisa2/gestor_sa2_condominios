<?php
date_default_timezone_set('America/Sao_Paulo');
include_once 'include/config_boleto.php';
include '../CnabPHP/vendor/autoload.php';
include_once '../include/dbcommon.php';

$contador001 = "1";

$codigo_banco = Cnab\Banco::ITAU;
$arquivo = new Cnab\Remessa\Cnab400\Arquivo($codigo_banco);
$arquivo->configure(array(
    'data_geracao'  => new DateTime(),
    'data_gravacao' => new DateTime(),
    'nome_fantasia' => $cedente, // seu nome de empresa
    'razao_social'  => $cedente,  // sua razão social
    'cnpj'          => $cnpj, // seu cnpj completo
    'banco'         => $codigo_banco, //código do banco
    'logradouro'    => $logradouro,
    'numero'        => $log_num,
    'bairro'        => $bairro,
    'cidade'        => $cidade,
    'uf'            => $uf,
    'cep'           => $cep_emitente,
    'agencia'       => $agencia,
    'conta'         => $conta, // número da conta
    'conta_dac'     => $conta_dv, // digito da conta
));

$nome_arquivo = 'remessas/'.time().'.rem';

while ($contador001 <= (count($_REQUEST)-1))
{
$numdoco = ($_REQUEST['nnum'.$contador001]);

  if(isset($numdoco))
  {

    $sql = "SELECT
    idCobranca,
    datadoc,
    vencimento,
    ger_grupo.nome as nomegrupo,unidade,
    ger_cadastro.nome,
    vlrcob,
    fin_cobranca.obs as obes,
    right(numdoc,7) as numdoc,
    log,
    end,
    num,
    comp,
    bairro,
    cep,
    cidade,
    uf,
    multa,
    jurosam,
    cpf

    FROM `fin_cobranca`
    inner join ger_unidades on link_ger_unidade=idUnidade
    inner join ger_cadastro on link_ger_cadastro=idCadastro
    inner join ger_grupo on grupo = idGrupo
    where idCobranca=$numdoco
    " ;

    $rs = CustomQuery($sql);
    $row = db_fetch_array($rs);

    $cep_sacado = trim(str_replace('-', '', $row["cep"]));
    if(strlen($cep_sacado)<>8) $cep_sacado = $cep_emitente;

    // você pode adicionar vários boletos em uma remessa


    $detalhe_array = array(
        'codigo_ocorrencia' => 1, // 1 = Entrada de título, futuramente poderemos ter uma constante
        'nosso_numero'      => $numdoco,
        'numero_documento'  => $numdoco,
        'carteira'          => $carteira,
        'especie'           => Cnab\Especie::ITAU_DUPLICATA_DE_SERVICO, // Você pode consultar as especies Cnab\Especie
        'valor'             => $row["vlrcob"], // Valor do boleto
        'instrucao1'        => 1, // 1 = Protestar com (Prazo) dias, 2 = Devolver após (Prazo) dias, futuramente poderemos ter uma constante
        'instrucao2'        => 0, // preenchido com zeros
        'sacado_nome'       => $row["nome"], // O Sacado é o cliente, preste atenção nos campos abaixo
        'sacado_tipo'       => 'cpf', //campo fixo, escreva 'cpf' (sim as letras cpf) se for pessoa fisica, cnpj se for pessoa juridica
        'sacado_cpf'        => $row["cpf"],
        'sacado_logradouro' => $row["log"].' '.$row["end"].' '.$row["num"],
        'sacado_bairro'     => $row["bairro"],
        'sacado_cep'        => $cep_sacado, // sem hífem
        'sacado_cidade'     => $row["cidade"],
        'sacado_uf'         => $row["uf"],
        'data_vencimento'   => new DateTime($row["vencimento"]),
        'data_cadastro'     => new DateTime(date("Y-m-d")),
        'juros_de_um_dia'     => number_format(($row["vlrcob"]*$row["jurosam"]/30/100),2), // Valor do juros de 1 dia'
        'data_desconto'       => new DateTime(date("Y-m-d")),
        'valor_desconto'      => 0.0, // Valor do desconto
        'prazo'               => 5, // prazo de dias para o cliente pagar após o vencimento
        'taxa_de_permanencia' => '0', //00 = Acata Comissão por Dia (recomendável), 51 Acata Condições de Cadastramento na CAIXA
        'mensagem'            => $row['obes'],
        'data_multa'          => new DateTime($row["vencimento"]), // data da multa
        'valor_multa'         => number_format($row["vlrcob"]*$row["multa"]/100,2), // valor da multa
    );


    $arquivo->insertDetalhe($detalhe_array);
  } // fim do if isset

$contador001++;
}

// para salvar
echo '<pre>';
// var_dump($detalhe_array);
$arquivo->save($nome_arquivo);

echo "<a href=$nome_arquivo>BAIXAR ARQUIVO REMESSA (Botão da Direita e Salvar Como)</a>";
?>
