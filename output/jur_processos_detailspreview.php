<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

require_once("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

require_once("include/jur_processos_variables.php");

$mode = postvalue("mode");

if(!isLogged())
{ 
	return;
}
if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search"))
{
	return;
}

require_once("classes/searchclause.php");

$cipherer = new RunnerCipherer($strTableName);

require_once('include/xtempl.php');
$xt = new Xtempl();





$layout = new TLayout("detailspreview_bootstrap", "AvenueWhite_label", "MobileWhite_label");
$layout->version = 3;
	$layout->bootstrapTheme = "yeti";
$layout->blocks["bare"] = array();
$layout->containers["dcount"] = array();
$layout->container_properties["dcount"] = array(  );
$layout->containers["dcount"][] = array("name"=>"bsdetailspreviewcount",
	"block"=>"", "substyle"=>1  );

$layout->skins["dcount"] = "";

$layout->blocks["bare"][] = "dcount";
$layout->containers["detailspreviewgrid"] = array();
$layout->container_properties["detailspreviewgrid"] = array(  );
$layout->containers["detailspreviewgrid"][] = array("name"=>"detailspreviewfields",
	"block"=>"details_data", "substyle"=>1  );

$layout->skins["detailspreviewgrid"] = "";

$layout->blocks["bare"][] = "detailspreviewgrid";
$page_layouts["jur_processos_detailspreview"] = $layout;




$recordsCounter = 0;

//	process masterkey value
$mastertable = postvalue("mastertable");
$masterKeys = my_json_decode(postvalue("masterKeys"));
$sessionPrefix = "_detailsPreview";
if($mastertable != "")
{
	$_SESSION[$sessionPrefix."_mastertable"]=$mastertable;
//	copy keys to session
	$i = 1;
	if(is_array($masterKeys) && count($masterKeys) > 0)
	{
		while(array_key_exists ("masterkey".$i, $masterKeys))
		{
			$_SESSION[$sessionPrefix."_masterkey".$i] = $masterKeys["masterkey".$i];
			$i++;
		}
	}
	if(isset($_SESSION[$sessionPrefix."_masterkey".$i]))
		unset($_SESSION[$sessionPrefix."_masterkey".$i]);
}
else
	$mastertable = $_SESSION[$sessionPrefix."_mastertable"];

$params = array();
$params['id'] = 1;
$params['xt'] = &$xt;
$params['tName'] = $strTableName;
$params['pageType'] = "detailspreview";
$pageObject = new DetailsPreview($params);

if($mastertable == "ger_unidades")
{
	$where = "";
		$formattedValue = make_db_value("link_ger_unidade",$_SESSION[$sessionPrefix."_masterkey1"]);
	if( $formattedValue == "null" )
		$where .= $pageObject->getFieldSQLDecrypt("link_ger_unidade") . " is null";
	else
		$where .= $pageObject->getFieldSQLDecrypt("link_ger_unidade") . "=" . $formattedValue;
}

$str = SecuritySQL("Search", $strTableName);
if(strlen($str))
	$where.=" and ".$str;
$strSQL = $gQuery->gSQLWhere($where);

$strSQL.=" ".$gstrOrderBy;

$rowcount = $gQuery->gSQLRowCount($where, $pageObject->connection);
$xt->assign("row_count",$rowcount);
if($rowcount) 
{
	$xt->assign("details_data",true);

	$display_count = 10;
	if($mode == "inline")
		$display_count*=2;
		
	if($rowcount>$display_count+2)
	{
		$xt->assign("display_first",true);
		$xt->assign("display_count",$display_count);
	}
	else
		$display_count = $rowcount;

	$rowinfo = array();
	
	require_once getabspath('classes/controls/ViewControlsContainer.php');
	$pSet = new ProjectSettings($strTableName, PAGE_LIST);
	$viewContainer = new ViewControlsContainer($pSet, PAGE_LIST);
	$viewContainer->isDetailsPreview = true;

	$b = true;
	$qResult = $pageObject->connection->query( $strSQL );
	$data = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	while($data && $recordsCounter<$display_count) {
		$recordsCounter++;
		$row = array();
		$keylink = "";
		$keylink.="&key1=".runner_htmlspecialchars(rawurlencode(@$data["idProc"]));
	
	
	//	idProc - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("idProc", $data, $keylink);
			$row["idProc_value"] = $value;
			$format = $pSet->getViewFormat("idProc");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("idProc")))
				$class = ' rnr-field-number';
			$row["idProc_class"] = $class;
	//	Forum - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("Forum", $data, $keylink);
			$row["Forum_value"] = $value;
			$format = $pSet->getViewFormat("Forum");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("Forum")))
				$class = ' rnr-field-number';
			$row["Forum_class"] = $class;
	//	partes - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("partes", $data, $keylink);
			$row["partes_value"] = $value;
			$format = $pSet->getViewFormat("partes");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("partes")))
				$class = ' rnr-field-number';
			$row["partes_class"] = $class;
	//	vara - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("vara", $data, $keylink);
			$row["vara_value"] = $value;
			$format = $pSet->getViewFormat("vara");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("vara")))
				$class = ' rnr-field-number';
			$row["vara_class"] = $class;
	//	numgde - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("numgde", $data, $keylink);
			$row["numgde_value"] = $value;
			$format = $pSet->getViewFormat("numgde");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("numgde")))
				$class = ' rnr-field-number';
			$row["numgde_class"] = $class;
	//	controle - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("controle", $data, $keylink);
			$row["controle_value"] = $value;
			$format = $pSet->getViewFormat("controle");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("controle")))
				$class = ' rnr-field-number';
			$row["controle_class"] = $class;
	//	tipo - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("tipo", $data, $keylink);
			$row["tipo_value"] = $value;
			$format = $pSet->getViewFormat("tipo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("tipo")))
				$class = ' rnr-field-number';
			$row["tipo_class"] = $class;
	//	obs - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("obs", $data, $keylink);
			$row["obs_value"] = $value;
			$format = $pSet->getViewFormat("obs");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("obs")))
				$class = ' rnr-field-number';
			$row["obs_class"] = $class;
	//	ano - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ano", $data, $keylink);
			$row["ano_value"] = $value;
			$format = $pSet->getViewFormat("ano");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ano")))
				$class = ' rnr-field-number';
			$row["ano_class"] = $class;
	//	ReuAutor - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ReuAutor", $data, $keylink);
			$row["ReuAutor_value"] = $value;
			$format = $pSet->getViewFormat("ReuAutor");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ReuAutor")))
				$class = ' rnr-field-number';
			$row["ReuAutor_class"] = $class;
	//	Titulo - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("Titulo", $data, $keylink);
			$row["Titulo_value"] = $value;
			$format = $pSet->getViewFormat("Titulo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("Titulo")))
				$class = ' rnr-field-number';
			$row["Titulo_class"] = $class;
	//	recurso - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("recurso", $data, $keylink);
			$row["recurso_value"] = $value;
			$format = $pSet->getViewFormat("recurso");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("recurso")))
				$class = ' rnr-field-number';
			$row["recurso_class"] = $class;
	//	distrib - Short Date
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("distrib", $data, $keylink);
			$row["distrib_value"] = $value;
			$format = $pSet->getViewFormat("distrib");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("distrib")))
				$class = ' rnr-field-number';
			$row["distrib_class"] = $class;
	//	ultimousuario - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ultimousuario", $data, $keylink);
			$row["ultimousuario_value"] = $value;
			$format = $pSet->getViewFormat("ultimousuario");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ultimousuario")))
				$class = ' rnr-field-number';
			$row["ultimousuario_class"] = $class;
	//	ultimaalteracao - Datetime
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ultimaalteracao", $data, $keylink);
			$row["ultimaalteracao_value"] = $value;
			$format = $pSet->getViewFormat("ultimaalteracao");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ultimaalteracao")))
				$class = ' rnr-field-number';
			$row["ultimaalteracao_class"] = $class;
	//	link_ger_unidade - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("link_ger_unidade", $data, $keylink);
			$row["link_ger_unidade_value"] = $value;
			$format = $pSet->getViewFormat("link_ger_unidade");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("link_ger_unidade")))
				$class = ' rnr-field-number';
			$row["link_ger_unidade_class"] = $class;
	//	situacao - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("situacao", $data, $keylink);
			$row["situacao_value"] = $value;
			$format = $pSet->getViewFormat("situacao");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("situacao")))
				$class = ' rnr-field-number';
			$row["situacao_class"] = $class;
	//	Unidade - 
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("Unidade", $data, $keylink);
			$row["Unidade_value"] = $value;
			$format = $pSet->getViewFormat("Unidade");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("Unidade")))
				$class = ' rnr-field-number';
			$row["Unidade_class"] = $class;
	//	ativo - Checkbox
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("ativo", $data, $keylink);
			$row["ativo_value"] = $value;
			$format = $pSet->getViewFormat("ativo");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("ativo")))
				$class = ' rnr-field-number';
			$row["ativo_class"] = $class;
	//	linkdebitos - Custom
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("linkdebitos", $data, $keylink);
			$row["linkdebitos_value"] = $value;
			$format = $pSet->getViewFormat("linkdebitos");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("linkdebitos")))
				$class = ' rnr-field-number';
			$row["linkdebitos_class"] = $class;
	//	unidadeir - Custom
			$viewContainer->recId = $recordsCounter;
		    $value = $viewContainer->showDBValue("unidadeir", $data, $keylink);
			$row["unidadeir_value"] = $value;
			$format = $pSet->getViewFormat("unidadeir");
			$class = "rnr-field-text";
			if($format==FORMAT_FILE) 
				$class = ' rnr-field-file'; 
			if($format==FORMAT_AUDIO)
				$class = ' rnr-field-audio';
			if($format==FORMAT_CHECKBOX)
				$class = ' rnr-field-checkbox';
			if($format==FORMAT_NUMBER || IsNumberType($pSet->getFieldType("unidadeir")))
				$class = ' rnr-field-number';
			$row["unidadeir_class"] = $class;
		$rowinfo[] = $row;
		if ($b) {
			$rowinfo2[] = $row;
			$b = false;
		}
		$data = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	}
	$xt->assign_loopsection("details_row",$rowinfo);
	$xt->assign_loopsection("details_row_header",$rowinfo2); // assign class for header
}
$returnJSON = array("success" => true);
$xt->load_template(GetTemplateName("jur_processos", "detailspreview"));
$returnJSON["body"] = $xt->fetch_loaded();

if($mode!="inline")
{
	$returnJSON["counter"] = postvalue("counter");
	$layout = GetPageLayout(GoodFieldName($strTableName), 'detailspreview');
	if($layout)
	{
		foreach($layout->getCSSFiles(isRTL(), isMobile()) as $css)
		{
			$returnJSON['CSSFiles'][] = $css;
		}
	}	
}	

echo printJSON($returnJSON);
exit();
?>