<?php
class ConnectionManager
{
	/**
	 * Cached Connection objects
	 * @type Array
	 */
	protected $cache = array();

	/**
	 * Project connections data
	 * @type Array
	 */
	protected $_connectionsData;

	/**
	 * Project connections data
	 * @type Array
	 */
	protected $_connectionsIdByName = array();

	
	/**
	 * An array storing the correspondence between project
	 * datasource tables names and connections ids
	 * @type Array
	 */	
	protected $_tablesConnectionIds;
	
	
	/**
	 * @constructor
	 */
	function __construct()
	{
		$this->_setConnectionsData();
		$this->_setTablesConnectionIds();
	}
	
	/**
	 * Get connection object by the table name
	 * @param String tName
	 * @return Connection
	 */
	public function byTable( $tName )
	{
		$connId = $this->_tablesConnectionIds[ $tName ];
		if( !$connId )
			return $this->getDefault();
		return $this->byId( $connId );
	}

	/**
	 * Get connection object by the connection name
	 * @param String connName
	 * @return Connection
	 */	
	public function byName( $connName )
	{
		$connId = $this->getIdByName( $connName );
		if( !$connId )
			return $this->getDefault();
		return $this->byId( $connId );
	}
	
	/**
	 * Get connection id by the connection name
	 * @param String connName
	 * @return String
	 */	
	protected function getIdByName( $connName )
	{
		return $this->_connectionsIdByName[ $connName ];
	}
	
	/**
	 * Get connection object by the connection id 
	 * @param String connId
	 * @return Connection
	 */	
	public function byId( $connId )
	{
		if( !isset( $this->cache[ $connId ] ) )
			$this->cache[ $connId ] = $this->getConnection( $connId );

		return $this->cache[ $connId ];
	}
	
	/**
	 * Get the default db connection class
	 * @return Connection
	 */
	public function getDefault()
	{
		return $this->byId( "Tables" );
	}

	/**
	 * Get the users table db connection 
	 * @return Connection
	 */	
	public function getForLogin()
	{
		return $this->byId( "Tables" );
	}
	
	/**
	 * Get the log table db connection 
	 * @return Connection
	 */	
	public function getForAudit()
	{
		return $this->byId( "Tables" );
	}
	
	/**
	 * Get the locking table db connection 
	 * @return Connection
	 */		
	public function getForLocking()
	{
		return $this->byId( "Tables" );
	}	
	
	/**
	 * Get the 'ug_groups' table db connection 
	 * @return Connection
	 */	
	public function getForUserGroups()
	{
		return $this->byId( "Tables" );
	}		

	/**
	 * Get the saved searches table db connection 
	 * @return Connection
	 */	
	public function getForSavedSearches()
	{
		return $this->getDefault();
	}

	/**
	 * Get the webreports tables db connection 
	 * @return Connection
	 */		
	public function getForWebReports()
	{
		return $this->getDefault();
	}
	
	/**
	 * @param String connId
	 * @return Connection
	 */
	protected function getConnection( $connId )
	{
		include_once getabspath("connections/Connection.php");
		
		$data = $this->_connectionsData[ $connId ];	
		switch( $data["connStringType"] )
		{
			case "mysql":
				if( useMySQLiLib() )
				{
					include_once getabspath("connections/MySQLiConnection.php");
					return new MySQLiConnection( $data );
				}
				
				include_once getabspath("connections/MySQLConnection.php");	
				return new MySQLConnection( $data );	

			case "mssql":
			case "compact":
				if( useMSSQLWinConnect() )
				{
					include_once getabspath("connections/MSSQLWinConnection.php");
					return new MSSQLWinConnection( $data );
				}
				if( isSqlsrvExtLoaded() )
				{
					include_once getabspath("connections/MSSQLSrvConnection.php");	
					return new MSSQLSrvConnection( $data );
				}
				
				include_once getabspath("connections/MSSQLUnixConnection.php");
				return new MSSQLUnixConnection( $data );			

			case "msaccess":
			case "odbc":
			case "odbcdsn":
			case "custom":
			case "file":
				if( stripos($data["ODBCString"], 'Provider=') !== false )
				{
					include_once getabspath("connections/ADOConnection.php");
					return new ADOConnection( $data );
				}
				
				include_once getabspath("connections/ODBCConnection.php");
				return new ODBCConnection( $data );
			
			case "oracle":
				include_once getabspath("connections/OracleConnection.php");
				return new OracleConnection( $data );

			case "postgre":
				include_once getabspath("connections/PostgreConnection.php");
				return new PostgreConnection( $data );

			case "db2":
				include_once getabspath("connections/DB2Connection.php");
				return new DB2Connection( $data );

			case "informix":
				include_once getabspath("connections/InformixConnection.php");
				return new InformixConnection( $data );

			case "sqlite":
				include_once getabspath("connections/SQLite3Connection.php");
				return new SQLite3Connection( $data );
		}
	}
	
	/**
	 * Set the data representing the project's 
	 * db connection properties
	 */	 
	protected function _setConnectionsData()
	{
		// content of this function can be modified on demo account
		// variable names $data and $connectionsData are important

		$connectionsData = array();
		
		$data = array();
		$data["dbType"] = 0;
		$data["connId"] = "Tables";
		$data["connName"] = "MySQL";
		$data["connStringType"] = "mysql";
		$data["connectionString"] = "mysql;10.10.10.176;sa2;sa2;;gestor_sa2;http://localhost/sa2_copromo/phprunner.php;1"; //currently unused
		
		$this->_connectionsIdByName["MySQL"] = "Tables";
		
		$data["connInfo"] = array();
		$data["ODBCUID"] = "sa2";
		$data["ODBCPWD"] = "sa2";
		$data["leftWrap"] = "`";
		$data["rightWrap"] = "`";
		
		$data["DBPath"] = "db"; //currently unused	
		$data["useServerMapPath"] = 1; //currently unused
		
		$data["connInfo"][0] = "10.10.10.176";
		$data["connInfo"][1] = "sa2";
		$data["connInfo"][2] = "sa2";
		$data["connInfo"][3] = "";
		$data["connInfo"][4] = "gestor_sa2";
		$data["connInfo"][5] = "http://localhost/sa2_copromo/phprunner.php"; //currently unused
		$data["connInfo"][6] = "1"; //currently unused
		$data["ODBCString"] = "DRIVER={MySQL ODBC 3.51 Driver};Server=10.10.10.176;Uid=sa2;Pwd=sa2;Database=gestor_sa2;OPTION=3";
		// encription set
		$data["EncryptInfo"] = array();
		$data["EncryptInfo"]["mode"] = 0;
		$data["EncryptInfo"]["alg"]  = 128;
		$data["EncryptInfo"]["key"]  = "";

		$connectionsData["Tables"] = $data;
		$this->_connectionsData = $connectionsData;
	}
	
	/**
	 * Set the data representing the correspondence between 
	 * the project's table names and db connections
	 */	 
	protected function _setTablesConnectionIds()
	{
		$connectionsIds = array();
		$connectionsIds["fin_cheques"] = "Tables";
		$connectionsIds["fin_cobranca"] = "Tables";
		$connectionsIds["fin_correcao_monetaria"] = "Tables";
		$connectionsIds["fin_despesas"] = "Tables";
		$connectionsIds["fin_fornecedores"] = "Tables";
		$connectionsIds["fin_meiospgto"] = "Tables";
		$connectionsIds["fin_tipodocs"] = "Tables";
		$connectionsIds["fin_tipogastos"] = "Tables";
		$connectionsIds["ger_cadastro"] = "Tables";
		$connectionsIds["ger_lista_uf"] = "Tables";
		$connectionsIds["ger_moradores"] = "Tables";
		$connectionsIds["ger_unidades"] = "Tables";
		$connectionsIds["ger_lista_relacao_com_unidade"] = "Tables";
		$connectionsIds["fin_cobranca_atualizacao_aberto"] = "Tables";
		$connectionsIds["ger_usuarios"] = "Tables";
		$connectionsIds["admin_rights"] = "Tables";
		$connectionsIds["admin_members"] = "Tables";
		$connectionsIds["admin_users"] = "Tables";
		$connectionsIds["jur_processos"] = "Tables";
		$connectionsIds["exp_lista_foruns"] = "Tables";
		$connectionsIds["fin_contas"] = "Tables";
		$connectionsIds["ger_selecao_ger_unidades"] = "Tables";
		$connectionsIds["ger_selecao_guni_ger_unidades"] = "Tables";
		$connectionsIds["ger_ocorrencias"] = "Tables";
		$connectionsIds["exp_ger_lista_logs"] = "Tables";
		$connectionsIds["ger_op_inserirtdboleto"] = "Tables";
		$connectionsIds["fin_debitos_contador"] = "Tables";
		$connectionsIds["fin_debitos_resumo"] = "Tables";
		$connectionsIds["ger_cadastro_moradores_plista"] = "Tables";
		$connectionsIds["ger_grupo"] = "Tables";
		$connectionsIds["loginattempts"] = "Tables";
		$connectionsIds["ger_arquivos_unidades"] = "Tables";
		$connectionsIds["ger_controle_de_acessos"] = "Tables";
		$connectionsIds["fin_bco_ext1"] = "Tables";
		$connectionsIds["SaldosBlocos"] = "Tables";
		$connectionsIds["ger_arquivos_geral"] = "Tables";
		$connectionsIds["ger_arquivos_grupos"] = "Tables";
		$connectionsIds["ger_arquivos_adm"] = "Tables";
		$this->_tablesConnectionIds = $connectionsIds;
	}
	
	/**
	 * Check if It's possible to add to one table's sql query 
	 * an sql subquery to another table.
	 * Access doesn't support subqueries from the same table as main.
	 * @param String dataSourceTName1
	 * @param String dataSourceTName2
	 * @return Boolean
	 */
	public function checkTablesSubqueriesSupport( $dataSourceTName1, $dataSourceTName2 )
	{
		$connId1 = $this->_tablesConnectionIds[ $dataSourceTName1 ];
		$connId2 = $this->_tablesConnectionIds[ $dataSourceTName2 ];
		
		if( $connId1 != $connId2 )
			return false;

		if( $this->_connectionsData[ $connId1 ]["dbType"] == nDATABASE_Access && $dataSourceTName1 == $dataSourceTName2 )
			return false;
			
		return true;	
	}
	
	/**
	 * Close db connections
	 * @destructor
	 */
	function __desctruct() 
	{
		foreach( $this->cache as $connection )
		{
			$connection->close();
		}
	}
}
?>
