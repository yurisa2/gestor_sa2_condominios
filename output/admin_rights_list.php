<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

require_once("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

require_once("include/admin_rights_variables.php");

if( !Security::processAdminPageSecurity( false ) )
	return;


$tables = array();
$pageMask = array();
$table = "fin_cheques";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_cheques", " " . "Cheques");
$table = "fin_cobranca";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "M";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_cobranca", " " . "Cobrança");
$table = "fin_correcao_monetaria";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "I";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_correcao_monetaria", " " . "Correção Monetária (c)");
$table = "fin_despesas";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_despesas", " " . "Despesas");
$table = "fin_fornecedores";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_fornecedores", " " . "Fornecedores");
$table = "fin_meiospgto";
$mask="";
	$mask .= "A";
	$mask .= "E";
		$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_meiospgto", " " . "Meios de pagamento (c)");
$table = "fin_tipodocs";
$mask="";
	$mask .= "A";
	$mask .= "E";
		$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_tipodocs", " " . "Tipos de Documentos (c)");
$table = "fin_tipogastos";
$mask="";
	$mask .= "A";
	$mask .= "E";
		$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_tipogastos", " " . "Tipos de gastos (c)");
$table = "ger_cadastro";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_cadastro", " " . "Cadastro geral");
$table = "ger_lista_uf";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_lista_uf", " " . "Lista de UF (c)");
$table = "ger_moradores";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_moradores", " " . "Moradores");
$table = "ger_unidades";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_unidades", " " . "Unidades");
$table = "ger_lista_relacao_com_unidade";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_lista_relacao_com_unidade", " " . "Lista de Rel. com a Unidade (c)");
$table = "fin_cobranca_atualizacao_aberto";
$mask="";
				$mask .= "S";
$mask .= "P";
$mask .= "I";
$mask .= "M";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_cobranca_atualizacao_aberto", " " . "Débitos");
$table = "ger_usuarios";
$mask="";
			$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_usuarios", " " . "Usuários");
$table = "jur_processos";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "I";

$pageMask[$table] = $mask;
$tables[$table] = array("jur_processos", " " . "Processos");
$table = "exp_lista_foruns";
$mask="";
				$mask .= "S";
$mask .= "P";
$mask .= "I";

$pageMask[$table] = $mask;
$tables[$table] = array("exp_lista_foruns", " " . "Lista de fórums (c)");
$table = "fin_contas";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "I";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_contas", " " . "Contas controladas (c)");
$table = "ger_selecao_ger_unidades";
$mask="";
			
$pageMask[$table] = $mask;
$tables[$table] = array("ger_selecao_ger_unidades", " " . "ger_selecao_ger_unidades");
$table = "ger_selecao_guni_ger_unidades";
$mask="";
			
$pageMask[$table] = $mask;
$tables[$table] = array("ger_selecao_guni_ger_unidades1", " " . "ger_selecao_GUNI_ger_unidades");
$table = "ger_ocorrencias";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "I";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_ocorrencias", " " . "Ocorrências");
$table = "exp_ger_lista_logs";
$mask="";
	$mask .= "A";
			$mask .= "S";
$mask .= "P";
$mask .= "I";

$pageMask[$table] = $mask;
$tables[$table] = array("exp_ger_lista_logs", " " . "Logradouros (c)");
$table = "ger_op_inserirtdboleto";
$mask="";
	$mask .= "A";
		
$pageMask[$table] = $mask;
$tables[$table] = array("ger_op_inserirtdboleto", " " . "Adicionar Boletos em massa");
$table = "fin_debitos_contador";
$mask="";
				$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_debitos_contador", " " . "Lista de presenca");
$table = "fin_debitos_resumo";
$mask="";
				$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_debitos_resumo", " " . "Débitos - Resumo");
$table = "ger_cadastro_moradores_plista";
$mask="";
				$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_cadastro_moradores_plista", " " . "Lista de Recebimento");
$table = "ger_grupo";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "I";
$mask .= "M";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_grupo", " " . "Grupos/Blocos");
$table = "loginattempts";
$mask="";
			$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("loginattempts", " " . "Tentativas de conexão (c)(v)");
$table = "ger_arquivos_unidades";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "M";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_arquivos_unidades", " " . "Arquivos Unidades");
$table = "ger_controle_de_acessos";
$mask="";
				$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_controle_de_acessos", " " . "Acessos (c)(v)");
$table = "fin_bco_ext1";
$mask="";
				$mask .= "S";
$mask .= "P";
$mask .= "I";
$mask .= "M";

$pageMask[$table] = $mask;
$tables[$table] = array("fin_bco_ext1", " " . "Extrato");
$table = "SaldosBlocos";
$mask="";
				$mask .= "S";
$mask .= "P";
$mask .= "I";
$mask .= "M";

$pageMask[$table] = $mask;
$tables[$table] = array("SaldosBlocos", " " . "Saldo dos blocos");
$table = "ger_arquivos_geral";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "I";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_arquivos_geral", " " . "Arquivos Gerais");
$table = "ger_arquivos_grupos";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";
$mask .= "I";
$mask .= "M";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_arquivos_grupos", " " . "Arquivos do Grupo");
$table = "ger_arquivos_adm";
$mask="";
	$mask .= "A";
	$mask .= "E";
	$mask .= "D";
	$mask .= "S";
$mask .= "P";

$pageMask[$table] = $mask;
$tables[$table] = array("ger_arquivos_adm", " " . "Arquivos Administrativos");




$layout = new TLayout("admin_rights_list_bootstrap", "AvenueWhite_label", "MobileWhite_label");
$layout->version = 3;
	$layout->bootstrapTheme = "yeti";
$layout->blocks["bottom"] = array();
$layout->containers["pagination"] = array();
$layout->container_properties["pagination"] = array(  );
$layout->containers["pagination"][] = array("name"=>"pagination",
	"block"=>"pagination_block", "substyle"=>1  );

$layout->skins["pagination"] = "";

$layout->blocks["bottom"][] = "pagination";
$layout->blocks["center"] = array();
$layout->containers["ugcontrols"] = array();
$layout->container_properties["ugcontrols"] = array(  );
$layout->containers["ugcontrols"][] = array("name"=>"ugrightsgroup",
	"block"=>"", "substyle"=>1  );

$layout->skins["ugcontrols"] = "";

$layout->blocks["center"][] = "ugcontrols";
$layout->containers["ugcontrols_1"] = array();
$layout->container_properties["ugcontrols_1"] = array(  );
$layout->containers["ugcontrols_1"][] = array("name"=>"ugbuttons",
	"block"=>"", "substyle"=>1  );

$layout->containers["ugcontrols_1"][] = array("name"=>"ugrightbuttons",
	"block"=>"savebuttons_block", "substyle"=>1  );

$layout->skins["ugcontrols_1"] = "";

$layout->blocks["center"][] = "ugcontrols_1";
$layout->containers["grid"] = array();
$layout->container_properties["grid"] = array(  );
$layout->containers["grid"][] = array("name"=>"ugrightsblock",
	"block"=>"", "substyle"=>1  );

$layout->skins["grid"] = "";

$layout->blocks["center"][] = "grid";
$layout->blocks["top"] = array();
$layout->containers["menu"] = array();
$layout->container_properties["menu"] = array(  );
$layout->containers["menu"][] = array("name"=>"title",
	"block"=>"", "substyle"=>1  );

$layout->containers["menu"][] = array("name"=>"hmenu",
	"block"=>"menu_block", "substyle"=>1  );

$layout->skins["menu"] = "";

$layout->blocks["top"][] = "menu";
$layout->blocks["topmiddle"] = array();
$layout->containers["loggedcontrol"] = array();
$layout->container_properties["loggedcontrol"] = array(  );
$layout->containers["loggedcontrol"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"logged" );
$layout->containers["logged"] = array();
$layout->container_properties["logged"] = array(  );
$layout->containers["logged"][] = array("name"=>"loggedas",
	"block"=>"security_block", "substyle"=>1  );

$layout->skins["logged"] = "";


$layout->skins["loggedcontrol"] = "";

$layout->blocks["topmiddle"][] = "loggedcontrol";
$layout->containers["messagerow"] = array();
$layout->container_properties["messagerow"] = array(  );
$layout->containers["messagerow"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"message" );
$layout->containers["message"] = array();
$layout->container_properties["message"] = array(  );
$layout->containers["message"][] = array("name"=>"message",
	"block"=>"message_block", "substyle"=>1  );

$layout->skins["message"] = "";


$layout->skins["messagerow"] = "";

$layout->blocks["topmiddle"][] = "messagerow";
$page_layouts["admin_rights_list"] = $layout;




require_once('include/xtempl.php');
require_once('classes/listpage.php');
require_once('classes/rightspage.php');

$xt = new Xtempl();

$options = array();
$options["pageType"] = PAGE_LIST;
$options["id"] = postvalue("id") ? postvalue("id") : 1;
$options["mode"] = RIGHTS_PAGE;
$options['xt'] = &$xt;


$options["tables"] = $tables;
$options["pageMasks"] = $pageMask;

$pageObject = ListPage::createListPage($strTableName, $options);

if( postvalue("a") == "saveRights" )
{
	$modifiedRights = my_json_decode(postvalue('values'));
	$pageObject->saveRights($modifiedRights);
	return;
}

 
// add buttons if exist

// prepare code for build page
$pageObject->prepareForBuildPage();

// show page depends of mode
$pageObject->showPage();
	


?>
